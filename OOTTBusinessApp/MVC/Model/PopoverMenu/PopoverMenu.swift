//
//  PopoverMenu.swift
//  SampleScreens
//
//  Created by Bharat Kumar Pathak on 04/07/17.
//  Copyright © 2017 Konstant. All rights reserved.
//

import UIKit

protocol PopoverMenuDelegate: class {
    func didSelectedItemAt(index: Int)
    
}

class PopoverMenu: UIView, UITableViewDelegate, UITableViewDataSource {
    
    var menuItems = [String]()
    weak var menuDelegate: PopoverMenuDelegate?
    @IBOutlet weak var menuTableView: UITableView!
    
    func instanceFromNib() -> UIView {
        return UINib(nibName: "PopoverMenu", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
    
    override func layoutSubviews() {
        
        self.layer.cornerRadius = CGFloat(5)
        self.clipsToBounds = true
        self.menuTableView.isScrollEnabled = false
        
        var viewFrame: CGRect = self.frame
        viewFrame.size.height = self.menuTableView.contentSize.height
        self.frame = viewFrame
    }
    
    func updateView()
    {
        self.menuTableView.reloadData()
    }
    
    //Mark:- UITableView Delegate and Datasource Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.menuItems.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 30.5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: PopoverMenuCell = UINib(nibName: "PopoverMenu", bundle: nil).instantiate(withOwner: nil, options: nil)[1] as! PopoverMenuCell
        cell.menuItemLabel.text = self.menuItems[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        menuDelegate?.didSelectedItemAt(index: indexPath.row)
    }

}
