//
//  ModelAddChatGroup.swift
//  OOTTBusinessApp
//
//  Created by Santosh on 10/10/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import Foundation
import UIKit

struct GroupMember {
    var userID = ""
    var username = ""
    var picture = ""
    
    init()
    {
        
    }
    
    mutating func updateModel(usingDictionary dictionary : [String:Any])
    {
        if let _id = dictionary["_id"] as? String
        {
            self.userID = _id
        }
        
        if let name = dictionary["name"] as? String
        {
            self.username = name
        }
        
        if let picture = dictionary["picture"] as? String
        {
            self.picture = picture
        }
    }
}

class ModelAddChatGroup
{
    var groupName : String?
    
    var groupMemberCanInvite = false
    
    var groupMemberCanMakeMoves = false
    
    var groupPicture : UIImage?
    
    var groupPictureURL : String?
    
    var groupInviteMembers : [GroupMember]?
    
    var groupMemberLeader : GroupMember?
    
    var groupMemberCoLeader : GroupMember?
    
    init() {
        
    }
    
    func getDictionary() -> [String:Any]
    {
        let params = [
            "name" : groupName!,
            "leader" : groupMemberLeader!.userID,
//            "coleader" : groupMemberCoLeader!.userID,
//            "canInvite" : groupMemberCanInvite ? "1" : "0",
//            "canMove" : groupMemberCanMakeMoves ? "1" : "0",
            "invitees" : String (groupInviteMembers!.map{$0.userID}.joined(separator: ",")) ?? ""
        ] as [String : Any]
        return params
    }
    
    func updateModel(usingDictionary dictionary : [String:Any])
    {
        if let name = dictionary["name"] as? String
        {
            self.groupName = name
        }
        
        if let canInvite = dictionary["canInvite"] as? Bool
        {
            self.groupMemberCanInvite = canInvite
        }
        
        if let canMove = dictionary["canMove"] as? Bool
        {
            self.groupMemberCanMakeMoves = canMove
        }
        
        if let picture = dictionary["picture"] as? String
        {
            self.groupPictureURL = picture
        }
        
        if let coleaders = dictionary["coleader"] as? [[String:Any]]
        {
            if let firstColeader = coleaders.first
            {
                var coleaderInfo = GroupMember ()
                coleaderInfo.userID = firstColeader["user_id"] as! String
                coleaderInfo.picture = firstColeader["picture"] as! String
                coleaderInfo.username = firstColeader["name"] as! String
                
                self.groupMemberCoLeader = coleaderInfo
            }
        }
        
        if let leader = dictionary["leader"] as? [String:Any]
        {
            var leaderInfo = GroupMember ()
            leaderInfo.userID = leader["user_id"] as! String
            leaderInfo.picture = leader["picture"] as! String
            leaderInfo.username = leader["name"] as! String
            
            self.groupMemberLeader = leaderInfo
        }
        
        if let invitee = dictionary["invitee"] as? [[String:Any]]
        {
            let inviteeTemp = invitee.filter({ (inviteeInfo) -> Bool in
               
                if inviteeInfo["user_id"] as? String == groupMemberLeader?.userID
                {
                    return false
                }
                
                return true
            })
            
            if groupInviteMembers == nil
            {
                groupInviteMembers = [GroupMember]()
            }
            
            for inviteeInfo in inviteeTemp
            {
                var inviteeMember = GroupMember ()
                inviteeMember.username = inviteeInfo["name"] as! String
                inviteeMember.userID = inviteeInfo["user_id"] as! String
                inviteeMember.picture = inviteeInfo["picture"] as! String
                
                groupInviteMembers?.append(inviteeMember)
            }
        }
    }
}
