//
//  ModelUser.swift
//  OOTTBusinessApp
//
//  Created by Bharat Kumar Pathak on 12/07/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import Foundation

//MARK:-
class ModelUser
{
    
//    {
//    "type": true,
//    "data": {
//    "_id": "595245915d0a6f36140efe39",
//    "updated": "2017-06-27T11:46:25.644Z",
//    "created": "2017-06-27T11:46:25.644Z",
//    "name": "flower",
//    "email": "flower@yahoo.in",
//    "password": "$2a$10$XjggREKTxvEZeB0YXz3XCOtjNwjivq721zoJQuEsu0XMJLFLnd7m.",
//    "osType": "android",
//    "owner": "5952332a1305ad355c23d2af",
//    "mobileNo": "123456",
//    "moves": [],
//    "employeeRole": "Manager",
//    "open": "",
//    "location": "",
//    "about": "",
//    "waitingTime": 10,
//    "attended": 0,
//    "attendee": [],
//    "coverPicture": "default.png",
//    "contact": "",
//    "businessImages": [],
//    "menuImages": [],
//    "occupancy": 0,
//    "dressCode": "",
//    "charges": "",
//    "info": "",
//    "scene": [],
//    "__v": 0,
//    "ratingCount": 0,
//    "totalRating": 0,
//    "followings": [
//    "595245915d0a6f36140efe39"
//    ],
//    "likes": [],
//    "followers": [
//    "595245915d0a6f36140efe39"
//    ],
//    "accounts": [],
//    "badge": 0,
//    "lastNotification": "2017-06-27T11:46:25.620Z",
//    "isNotify": 1,
//    "language": "en",
//    "picture": "http://192.168.0.131/oott/dev/public/uploads/users/Ao9vEhMRjwT7WkHvPlOyzTjJCzF54lCm.jpeg",
//    "isArchive": false,
//    "active": true,
//    "deviceToken": "",
//    "isVerified": true,
//    "facebookId": "",
//    "sourceType": "email",
//    "userType": "employee",
//    "isProfileDone": false,
//    "followingsCount": 1,
//    "followersCount": 1,
//    "movesCount": 0,
//    "likesCount": 0,
//    "isFollowed": 1
//    },
//    "message": "User found"
//    }
    
    var userID : String?
    var userPicture: String?
    var userName : String?
    var userFullName : String?
    var userStatus : String?
    var userStatusID : String?
    var userStatusMakeMove : String?
    var commentsCount : String?
    var likesCount : String?
    var isInvited : Bool?
    var isGhostMode : Bool?
    var isFollowed : Bool?
    var isBlocked = false
    var isAccountDeleted = false
    var isPrivateUser = false
    var isRequestedToFollow = false
//  ------------------------------------
    var followingCount : String?
    var followersCount : String?
    var vipCount : String?
//  ------------------------------------
    var userDescription : String?
//  ------------------------------------
    var frequentVisits: [ModelFrequentVisits]?
    
    init() {
        userID = ""
        userPicture = ""
        userName = ""
        userFullName = ""
//        userStatus = ""
        commentsCount = "0"
        likesCount = "0"
        isInvited = false
        isFollowed = false
        isBlocked = false
        followingCount = "0"
        followersCount = "0"
        isGhostMode = false
        vipCount = "0"
        userDescription = ""
        frequentVisits = [ModelFrequentVisits]()
    }
    
    deinit {
        print("ModelFriendsListing deinit")
    }
    
    func updateModel(usingDictionary dictionary:[String:Any]) -> Void
    {
        if let id = dictionary["_id"] as? String
        {
            userID = id
        }
        
        if let isGhost = dictionary["isGhostAccount"] as? Bool
        {
            isGhostMode = isGhost
        }
        
        if let isBlock = dictionary["isBlocked"] as? Bool
        {
            isBlocked = isBlock
        }
        
        if let isArchive = dictionary["isArchive"] as? Bool
        {
            isAccountDeleted = isArchive
        }
        
        if let isPrivateAccount = dictionary["isPrivateAccount"] as? Bool
        {
            isPrivateUser = isPrivateAccount
        }
        
        if let isRequested = dictionary["isRequested"] as? Bool
        {
            isRequestedToFollow = isRequested
        }
        
        if let picture = dictionary["picture"] as? String
        {
            userPicture = picture
        }
        if let desc = dictionary["description"] as? String
        {
            userDescription = desc
        }
        if let name = dictionary["name"] as? String
        {
            userFullName = name
        }
        if let uName = dictionary["username"] as? String
        {
            userName = uName
        }
        if let lastCheckIn = dictionary["lastCheckin"] as? [String:Any]
        {
            if let status = lastCheckIn["status"] as? String
            {
                userStatus = status
            }
            if let comments = lastCheckIn["checkinCommentCount"] as? Int
            {
                commentsCount = String(comments)
            }
            if let likes = lastCheckIn["checkinLikeCount"] as? Int
            {
                likesCount = String(likes)
            }
            if let _id = lastCheckIn["_id"] as? String
            {
                userStatusID = _id
            }
        }
        if let currentMove = dictionary["currentMove"] as? [String:Any]
        {
            if let status = currentMove["status"] as? String
            {
                userStatusMakeMove = status
            }
            if let _id = currentMove["_id"] as? String
            {
                userStatusID = _id
            }
        }
        if let isinvited = dictionary["isInvited"] as? Bool
        {
            isInvited = isinvited
        }
        if let isfollowed = dictionary["isFollowed"] as? Bool
        {
            isFollowed = isfollowed
        }
        if let followings = dictionary["followingsCount"] as? Int
        {
            followingCount = String(followings)
        }
        if let followers = dictionary["followersCount"] as? Int
        {
            followersCount = String(followers)
        }
        if let vips = dictionary["favouriteCount"] as? Int
        {
            vipCount = String(vips)
        }
        if let frequentVisitedBusinesses = dictionary["checkins"] as? [[String:Any]]
        {
            frequentVisits?.removeAll()
            for frequentVisitedBusiness in frequentVisitedBusinesses {
                let frequentVisitsModel = ModelFrequentVisits()
                frequentVisitsModel.updateModel(usingDictionary: frequentVisitedBusiness)
                frequentVisits?.append(frequentVisitsModel)
            }
        }
    }
}
