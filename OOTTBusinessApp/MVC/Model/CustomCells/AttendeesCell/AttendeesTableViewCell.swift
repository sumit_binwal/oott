//
//  AttendeesTableViewCell.swift
//  SampleScreens
//
//  Created by Bharat Kumar Pathak on 04/07/17.
//  Copyright © 2017 Konstant. All rights reserved.
//

import UIKit

class AttendeesTableViewCell: UITableViewCell {

    @IBOutlet weak var attendyImageView: UIImageView!
    @IBOutlet weak var attendyNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.attendyImageView.makeRound()
        self.attendyImageView.createBorder(withColor: color_cellImageBorder, andBorderWidth: 0.5)
        
        self.backgroundColor=UIColor.clear

        self.disbaleSelection()
    }
    
    override func layoutSubviews() {
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func updateCell(usingModel model : ModelAttendees)
    {
        self.attendyImageView.setIndicatorStyle(.white)
        self.attendyImageView.setShowActivityIndicator(true)
        self.attendyImageView.sd_setImage(with: URL (string: model.userPicture), completed: nil)
        
        self.attendyNameLabel.text = model.userFullName
    }
    
}
