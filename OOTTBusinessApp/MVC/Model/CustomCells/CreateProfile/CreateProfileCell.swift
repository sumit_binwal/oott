//
//  CreateProfileCell.swift
//  OOTTBusinessApp
//
//  Created by Sumit Sharma on 14/06/2017.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit
import GooglePlacePicker

let kCellIdentifier_create_profile = "createProfileCell"
let kCellIdentifier_upload_image = "uploadImageCell"
let kCellIdentifier_create_profile_textView = "createProfileTextVwCell"


//MARK:- LoginCellDelegate Method Declaration
protocol CreateProfileCellDelegate : class
{
    func createProfileCell(cell:CreateProfileCell, updatedInputfieldText:Any) -> Void
}

//MARK:- CellType Enum Type Declaration
enum ProfileCellType : Int {
    
    case businessName
    case scenes
    case bio
    case contact
    case location
    case genralInfo
    case website
    case coverCharge
    case dressCode
    case totalOccupancy
    case openTill
    case menuListing
    case uploadImage
    case radius
    case none
}


class CreateProfileCell: UITableViewCell, UITextFieldDelegate, UITextViewDelegate, HoursViewDelegate, CoverChargeDelegate {
    
    //MARK:- Image Cell Title Label
    @IBOutlet var labelCellTitle: UILabel!
    
    //MARK:- Collection View
    @IBOutlet var collectionViewImages: UICollectionView!
    
    //MARK:- Divider line
    @IBOutlet weak var bottomDividerLine: UIView!
    
    //MARK:- Input Textfield
    @IBOutlet weak var inputTextField: UITextField!
    
    @IBOutlet var inputTextViewField: KMPlaceholderTextView!
    
    //MARK:- CellType Enum Instance
    var cellType = ProfileCellType.none
    
    //MARK:- Default font size
    let fontSize : CGFloat = 17 * scaleFactorX
    
    //MARK:- LoginCellDelegate Instance
    weak var profileDelegate : CreateProfileCellDelegate?
    
    var model:ModelCreateProfile = ModelCreateProfile(withKey: CreateProfileEnum.none.rawValue, value: "")
    
    //MARK:- awakeFromNib
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.disbaleSelection()
        
        self.cellType = .none
        initialSetupOfCell()
    }
    
    //MARK:- deinit
    deinit {
        print("CreateProfileCell deinit")
    }
    
    //MARK:- Initial Cell Setup
    func initialSetupOfCell() -> Void
    {
        if self.inputTextField != nil
        {
            self.inputTextField.font = UIFont(name: FONT_PROXIMA_LIGHT, size: fontSize)
            self.inputTextField.setPlaceholderColor(color_placeholderColor)
            self.inputTextField.text=""
            
            self.inputTextField.delegate = self
            
            self.bottomDividerLine.backgroundColor = color_dividerLine
        }
        if self.inputTextViewField != nil
        {
            self.inputTextViewField.placeholderColor = color_placeholderColor
            self.inputTextViewField.text=""
            
            self.inputTextViewField.delegate = self
            
            self.bottomDividerLine.backgroundColor = color_dividerLine
        }
    }
    
    //MARK:- layoutSubviews
    override func layoutSubviews()
    {
        switch self.cellType
        {
            
        case .businessName:
            self.inputTextField.placeholder = "Business Name"
            self.inputTextField.keyboardType = .default
            self.inputTextField.isUserInteractionEnabled = true
            self.inputTextField.tag = ProfileCellType.businessName.rawValue
            
            model.keyValue = self.inputTextField.text!
            model.keyName = CreateProfileEnum.businessName.rawValue
            
        case .radius:
            
            //self.inputTextField.keyboardType = .default
            self.inputTextField.isUserInteractionEnabled = true
            self.inputTextField.tag = ProfileCellType.radius.rawValue
//            self.inputTextField.inputView = UtilityClass.getNormalPickerView()
            self.inputTextField.placeholder = "Location Size"
            
            model.keyValue = self.inputTextField.text!
            model.keyName = CreateProfileEnum.radius.rawValue
            
//            if cellType ==  ProfileCellType.radius
//            {
//                addTapGesture()
//            }
            
        case .scenes:
            self.inputTextField.placeholder = "Scenes"
            self.inputTextField.keyboardType = .default
            self.inputTextField.isUserInteractionEnabled = true
            self.inputTextField.tag = ProfileCellType.scenes.rawValue
            
            model.keyValue = self.inputTextField.text!
            model.keyName = CreateProfileEnum.scene.rawValue
            
        case .bio:
            self.inputTextViewField.placeholder = "Bio"
            self.inputTextViewField.keyboardType = .default
            self.inputTextViewField.isUserInteractionEnabled = true
            self.inputTextViewField.tag = ProfileCellType.bio.rawValue
            
            self.isAccessibilityElement = true
            self.accessibilityLabel = "Bio"
            
            model.keyValue = self.inputTextViewField.text!
            model.keyName = CreateProfileEnum.about.rawValue
            
        case .contact:
            self.inputTextField.placeholder = "Contact"
            self.inputTextField.keyboardType = .asciiCapableNumberPad
            self.inputTextField.isUserInteractionEnabled = true
            self.inputTextField.tag = ProfileCellType.contact.rawValue
            
            model.keyValue = self.inputTextField.text!
            model.keyName = CreateProfileEnum.contact.rawValue
            
        case .location:
            self.inputTextField.placeholder = "Location"
            self.inputTextField.keyboardType = .default
            self.inputTextField.isUserInteractionEnabled = false
            self.inputTextField.tag = ProfileCellType.location.rawValue
            
            model.keyValue = self.inputTextField.text!
            model.keyName = CreateProfileEnum.location.rawValue
            
            if cellType ==  ProfileCellType.location
            {
                addTapGesture()
            }
            
        case .genralInfo:
            self.inputTextViewField.placeholder = "General Information"
            self.inputTextViewField.keyboardType = .default
            self.inputTextViewField.isUserInteractionEnabled = true
            self.inputTextViewField.tag = ProfileCellType.genralInfo.rawValue
            
            model.keyValue = self.inputTextViewField.text!
            model.keyName = CreateProfileEnum.info.rawValue
            self.isAccessibilityElement = true
            self.accessibilityLabel = "General Information"
            
        case .website:
            self.inputTextField.placeholder = "Website"
            self.inputTextField.keyboardType = .URL
            self.inputTextField.isUserInteractionEnabled = true
            self.inputTextField.tag = ProfileCellType.website.rawValue
            
            model.keyValue = self.inputTextField.text!
            model.keyName = CreateProfileEnum.website.rawValue
            
        case .coverCharge:
            self.inputTextField.placeholder = "Cover Charges"
            self.inputTextField.keyboardType = .numberPad
            self.inputTextField.isUserInteractionEnabled = false
            self.inputTextField.tag = ProfileCellType.coverCharge.rawValue
            
            if cellType ==  ProfileCellType.coverCharge
            {
                addTapGesture()
            }
//            model.keyValue = self.inputTextField.text!
            model.keyName = CreateProfileEnum.charges.rawValue
            
        case .dressCode:
            self.inputTextField.placeholder = "Dress Code"
            self.inputTextField.keyboardType = .default
            self.inputTextField.isUserInteractionEnabled = true
            self.inputTextField.tag = ProfileCellType.dressCode.rawValue
            
            model.keyValue = self.inputTextField.text!
            model.keyName = CreateProfileEnum.dressCode.rawValue
            
        case .totalOccupancy:
            self.inputTextField.placeholder = "Total occupancy"
            self.inputTextField.keyboardType = .numberPad
            self.inputTextField.isUserInteractionEnabled = true
            self.inputTextField.tag = ProfileCellType.totalOccupancy.rawValue
            
            model.keyValue = self.inputTextField.text!
            model.keyName = CreateProfileEnum.totalOccupancy.rawValue
            
        case .openTill:
            self.inputTextField.placeholder = "Hours"
            self.inputTextField.text = ""
            self.inputTextField.keyboardType = .default
            self.inputTextField.isUserInteractionEnabled = false
            self.inputTextField.tag = ProfileCellType.openTill.rawValue
            
            if cellType ==  ProfileCellType.openTill
            {
                addTapGesture()
            }
            
//            model.keyValue = self.inputTextField.text!
            model.keyName = CreateProfileEnum.openTill.rawValue
            
        default: break
            
        }
    }
    
    
    
    
    //MARK:- UITextField Delegate
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
//        if textField.isAskingCanBecomeFirstResponder
//        {
//            return true
//        }
        if inputTextField.tag == ProfileCellType.radius.rawValue {
            guard let delegate = profileDelegate else {
                return false
            }
            delegate.createProfileCell(cell: self, updatedInputfieldText: model.keyValue)
            return true
        }
        if textField.tag == ProfileCellType.openTill.rawValue || textField.tag == ProfileCellType.coverCharge.rawValue
        {
            
            return false
//            textField.inputView = UtilityClass.getTimePicker24Format()
//            let timePicker = textField.inputView! as! UIDatePicker
//            
//            if (textField.text?.isEmpty)!
//            {
//                textField.text = timePicker.date.get24HourFormat()
//                model.keyValue = self.inputTextField.text!
//                
//                guard let delegate = profileDelegate else {
//                    return true
//                }
//                delegate.createProfileCell(cell: self, updatedInputfieldText: model.keyValue)
//            }
//            else
//            {
//                timePicker.date = Date.getTime(fromString: model.keyValue)
//            }
//            
//            timePicker.addTarget(self, action: #selector(onTimePickerValueChanged(sender:)), for: .valueChanged)
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var newString : String?
        var finalCount = 0
        
        if string.count == 0
        {
            if textField.text?.count==0
            {
                finalCount = 0
                newString = textField.text
            }
            else
            {
                finalCount = textField.text!.count - 1
                var newStr = textField.text! as NSString
                newStr = newStr.replacingCharacters(in: range, with: string) as NSString
                newString = newStr as String
            }
        }
        else
        {
            finalCount = textField.text!.count + 1
            var newStr = textField.text! as NSString
            
            newStr = newStr.replacingCharacters(in: range, with: string) as NSString
            
            newString = newStr as String
        }
        
        if textField.tag == ProfileCellType.contact.rawValue
        {
            if finalCount > 10
            {
                return false
            }
        }
        
//        if textField.tag == ProfileCellType.coverCharge.rawValue
//        {
//            newString = newString?.replacingOccurrences(of: "$", with: "")
//            if newString?.count != 0
//            {
//                newString = "$".appending(newString!)
//            }
//            textField.text = newString!
//            model.keyValue = newString!
//
//            guard let delegate = profileDelegate else {
//                return true
//            }
//            delegate.createProfileCell(cell: self, updatedInputfieldText: model.keyValue)
//            return false
//        }
        
        // Scenes tag get functionality
//        if textField.tag == ProfileCellType.scenes.rawValue
//        {
//            let fieldText = newString! as NSString
//            var firstPart = "", secondPart = ""
//            
//            if (string == "")
//            {
//                
//                firstPart = fieldText.substring(to: range.location)
//                secondPart = fieldText.substring(from: range.location)
//                
//            }
//            else {
//                
//                firstPart = fieldText.substring(to: range.location+1)
//                secondPart = fieldText.substring(from: range.location+1)
//            }
//        }
//        else
//        {
            model.keyValue = newString!
        
            guard let delegate = profileDelegate else {
                return true
            }
            delegate.createProfileCell(cell: self, updatedInputfieldText: model.keyValue)
//        }
        
        return true
    }
    
    //MARK:- DOB Value Update
    func onTimePickerValueChanged(sender : UIDatePicker) -> Void {
        self.inputTextField.text = sender.date.get24HourFormat()
        model.keyValue = self.inputTextField.text!
        
        guard let delegate = profileDelegate else {
            return
        }
        delegate.createProfileCell(cell: self, updatedInputfieldText: model.keyValue)
    }
    
    //MARK:- UITextView Delegate
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if text == "\n" {
            return false
        }
        
        guard let tableViewCreateProfile : UITableView = self.superviewOfClassType(UITableView.self) as? UITableView else
        {
            return false
        }
        
        let currentOffset = tableViewCreateProfile.contentOffset
        UIView.setAnimationsEnabled(false)
        tableViewCreateProfile.beginUpdates()
        tableViewCreateProfile.endUpdates()
        UIView.setAnimationsEnabled(true)
        tableViewCreateProfile.setContentOffset(currentOffset, animated: false)
        
        var finalCount = 0
        var newString : String?
        
        if text.isEmpty
        {
            if (textView.text?.isEmpty)!
            {
                finalCount = 0
                newString = textView.text
            }
            else
            {
                finalCount = textView.text!.count - 1
                var newStr = textView.text! as NSString
                newStr = newStr.replacingCharacters(in: range, with: text) as NSString
                newString = newStr as String
            }
        }
        else
        {
            finalCount = textView.text!.count + 1
            var newStr = textView.text! as NSString
            
            newStr = newStr.replacingCharacters(in: range, with: text) as NSString
            
            newString = newStr as String
        }
        
        if (finalCount>240)
        {
            return false;
        }
        
        model.keyValue = newString!
        
        guard let delegate = profileDelegate else {
            return true
        }
        delegate.createProfileCell(cell: self, updatedInputfieldText: model.keyValue)
        
        return true
    }
    
    
    //MARK:- Update value from Model
    func updateValue(usingModel: ModelCreateProfile) -> Void
    {
        if cellType == ProfileCellType.openTill
        {
            model.keyValue = usingModel.keyValue
            return
        }
        if cellType == ProfileCellType.coverCharge
        {
            model.keyValue = usingModel.keyValue
            return
        }
        if inputTextField != nil
        {
            inputTextField.text = usingModel.keyValue as? String
            return
        }
        if inputTextViewField != nil
        {
            inputTextViewField.text = usingModel.keyValue as? String
            return
        }
    }
    
    func addTapGesture() -> Void
    {
        self.contentView.gestureRecognizers?.removeAll()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(onTapGesture(tapGesture:)))
        tapGesture.numberOfTapsRequired = 1
        self.contentView.addGestureRecognizer(tapGesture)
    }
    
    //MARK:- Tap Gesture Action
    func onTapGesture(tapGesture : UITapGestureRecognizer) -> Void
    {
        switch tapGesture.state {
        case .ended:
            
            if cellType == .radius {
                
                guard let delegate = profileDelegate else {
                    return;
                }
                delegate.createProfileCell(cell: self, updatedInputfieldText: model.keyValue)
                
                return;
            }
            
            if cellType == ProfileCellType.location
            {
                let center = CLLocationCoordinate2DMake(LocationManager.sharedInstance().newLatitude, LocationManager.sharedInstance().newLongitude)
                
                let northEast = CLLocationCoordinate2DMake(center.latitude, center.longitude)
                
                let southWest = CLLocationCoordinate2DMake(center.latitude, center.longitude)
                
                let viewPort = GMSCoordinateBounds (coordinate: northEast, coordinate: southWest)
                
                let pickerConfig = GMSPlacePickerConfig (viewport: viewPort)
                
                let placePicker = GMSPlacePicker (config: pickerConfig)
                
                placePicker.pickPlace(callback: {[weak self] (result, error) in
                   
                    guard let `self` = self else {return}
                    
                    if result != nil
                    {
                        let finalResult = result!
                        var formattedAddress = finalResult.name
                        if finalResult.formattedAddress != nil
                        {
                            formattedAddress = finalResult.formattedAddress!.isEmpty ? finalResult.name : String(finalResult.formattedAddress!)
                        }
                        
                        self.model.keyValue = formattedAddress
                        self.model.latitude = String(finalResult.coordinate.latitude)
                        self.model.longitude = String(finalResult.coordinate.longitude)
                        
                        self.inputTextField.text = self.model.keyValue as? String
                        
                        guard let delegate = self.profileDelegate else {
                            return
                        }
                        delegate.createProfileCell(cell: self, updatedInputfieldText: self.model.keyValue)
                    }
                })
                
                return;
            }
            
            if cellType == ProfileCellType.openTill
            {
                let hoursView : HoursViewController = UIStoryboard.getHourSelectionViewStoryboard().instantiateInitialViewController() as! HoursViewController
                
                hoursView.hoursDelegate = self
                
                if let newValue = model.keyValue as? [String:String] {
                    if !newValue.isEmpty
                    {
                        hoursView.dictHours = newValue
                    }
                }
                
                if let vc = appDelegate?.window??.visibleViewController()
                {
                    vc.navigationController?.pushViewController(hoursView, animated: true)
                }
                return;
            }
            
            if cellType == ProfileCellType.coverCharge
            {
                let coverChargeView : CoverChargeViewController = UIStoryboard.getHourSelectionViewStoryboard().instantiateViewController(withIdentifier: "coverChargeVC") as! CoverChargeViewController
                
                coverChargeView.delegate = self
                
                if let newValue = model.keyValue as? [String:String] {
                    if !newValue.isEmpty
                    {
                        coverChargeView.dictCoverCharge = newValue
                    }
                }
                
                if let vc = appDelegate?.window??.visibleViewController()
                {
                    vc.navigationController?.pushViewController(coverChargeView, animated: true)
                }
                return;
            }
            
        default:
            break
        }
    }
    
    //MARK:- HoursViewDelegate -> Dictionary object
    func hoursView(didTapOnDoneWithFinalObject finalObject: [String : String]) {
        let params = finalObject
        model.keyValue = params
        
        guard let delegate = profileDelegate else {
            return
        }
        delegate.createProfileCell(cell: self, updatedInputfieldText: model.keyValue)
    }
    
    func coverChargeView(didTapOnDoneWithFinalObject finalObject: [String : String]) {
        let params = finalObject
        model.keyValue = params
        
        guard let delegate = profileDelegate else {
            return
        }
        delegate.createProfileCell(cell: self, updatedInputfieldText: model.keyValue)
    }
}

