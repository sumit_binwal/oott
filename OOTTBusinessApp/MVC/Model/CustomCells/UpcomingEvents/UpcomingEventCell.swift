//
//  UpcomingEventCell.swift
//  OOTTBusinessApp
//
//  Created by Santosh on 15/06/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit

let kCellIdentifier_upcomingEvent_sectionHeader = "sectionHeaderCell"

let kCellIdentifier_upcomingEvent_eventCell = "upcomingEventsCell"

class UpcomingEventCell: UITableViewCell {

    
    @IBOutlet weak var labelEventName: UILabel!
    
    @IBOutlet weak var labelEventDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.disbaleSelection()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
