//
//  ChatViewCollectionLikeCell.swift
//  OOTTBusinessApp
//
//  Created by Santosh on 28/06/18.
//  Copyright © 2018 KonstantInfoSolutions. All rights reserved.
//

import UIKit

class ChatViewCollectionLikeCell: UICollectionViewCell {

    @IBOutlet weak var imageViewButton: UIButton!
    
    @IBOutlet weak var username: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
