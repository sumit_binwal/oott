//
//  ChangeStatusCell.swift
//  OOTTBusinessApp
//
//  Created by Santosh on 10/01/18.
//  Copyright © 2018 KonstantInfoSolutions. All rights reserved.
//

import UIKit

let kCellIdentifier_businessGroup_HeaderCell = "businessGroupHeaderCell"

class ChangeStatusCell: UITableViewCell {

    
    @IBOutlet weak var groupStatus: UILabel!
    @IBOutlet weak var buttonChangeStatus: UIButton!
    
    weak var delegate: ChangeStatusCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        let yourAttributes : [String : Any] = [
            NSForegroundColorAttributeName : color_pink,
            NSFontAttributeName : UIFont(name: FONT_PROXIMA_SEMIBOLD, size: 15)!,
            NSUnderlineStyleAttributeName : NSUnderlineStyle.styleSingle.rawValue
        ]
        
        buttonChangeStatus.setAttributedTitle(NSAttributedString(string: "UPDATE STATUS", attributes: yourAttributes), for: .normal)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func onButtonChangeStatusAction(_ sender: UIButton) {
        guard let tempDelegate = delegate else {
            return
        }
        tempDelegate.changeStatusCell(self, didTapOnChangeStatusButton: sender)
    }
    
    func updateCell(usingModel model: GroupDetailModel) {
        groupStatus.text = model.groupStatus
    }
}

protocol ChangeStatusCellDelegate: class {
    func changeStatusCell(_ cell: ChangeStatusCell, didTapOnChangeStatusButton changeStatusButton: UIButton)
}
