//
//  CreateEventCell.swift
//  OOTTBusinessApp
//
//  Created by Sumit Sharma on 29/06/2017.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

let kCellIdentifier_inputevent_startend_cell = "loginStartDateCell"
let kCellIdentifier_inputevent_title_cell = "eventTitleCell"
let kCellIdentifier_inputevent_detail_cell = "eventTextViewDetailCell"




//MARK:- CellType Enum Type Declaration
enum CreateEventCellType : Int {
    
    case eventTitle
    case startDateTime
    case endDateTime
    case description
    case none
}

//MARK:- EventCellDelegate Method Declaration
protocol CreateEventCellDelegate : class
{
    func createEventCell(cell:CreateEventCell, updatedInputfieldText:String) -> Void
}

class CreateEventCell: UITableViewCell, UITextFieldDelegate, UITextViewDelegate, NSLayoutManagerDelegate
{
    @IBOutlet var textViewDiscription: KMPlaceholderTextView!
    @IBOutlet var textFieldTitle: UITextField!
    @IBOutlet var bottomDeviderLine: UIView!
    @IBOutlet var bottomDeviderLine1: UIView!
    @IBOutlet var textFieldDate: UITextField!
    @IBOutlet var textFieldTime: UITextField!
    
    var model: ModelCreateEvent = ModelCreateEvent(withKey: EventEnum.none.rawValue, value: "")
    
    //MARK:- CellType Enum Instance
    var cellType = CreateEventCellType.none
    
    //MARK:- CreateEventDelegate Instance
    weak var eventDelegate : CreateEventCellDelegate?
    
    
    //MARK:- Default font size
    let fontSize : CGFloat = 17 * scaleFactorX
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.disbaleSelection()
        self.selectionStyle = UITableViewCellSelectionStyle.none
        self.cellType = .none
        self.initialSetupOfCell()
        
    }
    
    //MARK:- Initial Cell Setup
    func initialSetupOfCell() -> Void
    {
        if self.textFieldTitle != nil
        {
            self.textFieldTitle.font = UIFont(name: FONT_PROXIMA_LIGHT, size: fontSize)
            self.textFieldTitle.setPlaceholderColor(color_placeholderColor)
            self.textFieldTitle.text=""
            
            self.textFieldTitle.delegate = self
            self.bottomDeviderLine.backgroundColor = color_dividerLine
            
        }
        else if self.textViewDiscription != nil
        {
            self.textViewDiscription.placeholderColor = color_placeholderColor
            self.textViewDiscription.text=""
            self.textViewDiscription.layoutManager.delegate=self
            self.textViewDiscription.delegate = self
            self.bottomDeviderLine.backgroundColor = color_dividerLine
            
        }
        else if self.textFieldDate != nil
        {
            
            self.textFieldDate.font = UIFont(name: FONT_PROXIMA_LIGHT, size: fontSize)
            self.textFieldDate.setPlaceholderColor(color_placeholderColor)
            self.textFieldDate.text=""
            self.textFieldDate.delegate = self
            
            self.textFieldTime.font = UIFont(name: FONT_PROXIMA_LIGHT, size: fontSize)
            self.textFieldTime.setPlaceholderColor(color_placeholderColor)
            self.textFieldTime.text=""
            self.textFieldTime.delegate = self
            
            self.bottomDeviderLine1.backgroundColor = color_dividerLine
            self.bottomDeviderLine.backgroundColor = color_dividerLine
        }
    }
    
    override func layoutSubviews() {
        switch self.cellType {
        case .eventTitle:
            self.textFieldTitle.placeholder = "Event Title"
            self.textFieldTitle.keyboardType = .default
            self.textFieldTitle.isUserInteractionEnabled = true
            self.textFieldTitle.tag = CreateEventCellType.eventTitle.rawValue
            
            model.keyValue = self.textFieldTitle.text!
            model.keyName = EventEnum.title.rawValue
            
        case .startDateTime:
            self.textFieldDate.placeholder = "Start Date"
            self.textFieldDate.keyboardType = .default
            self.textFieldDate.isUserInteractionEnabled = true
            self.textFieldDate.tag = CreateEventCellType.startDateTime.rawValue
            
            self.textFieldTime.placeholder = "Start Time"
            self.textFieldTime.keyboardType = .default
            self.textFieldTime.isUserInteractionEnabled = true
            self.textFieldTime.tag = CreateEventCellType.startDateTime.rawValue
            model.keyValue = String(self.textFieldDate.text! + "divider" + self.textFieldTime.text! )
            model.keyName = EventEnum.startTime.rawValue
            
        case .endDateTime:
            self.textFieldDate.placeholder = "End Date"
            self.textFieldDate.keyboardType = .default
            self.textFieldDate.isUserInteractionEnabled = true
            self.textFieldDate.tag = CreateEventCellType.startDateTime.rawValue
            
            self.textFieldTime.placeholder = "End Time"
            self.textFieldTime.keyboardType = .default
            self.textFieldTime.isUserInteractionEnabled = true
            self.textFieldTime.tag = CreateEventCellType.startDateTime.rawValue
            
            model.keyValue = String(self.textFieldDate.text! + "divider" + self.textFieldTime.text! )
            model.keyName = EventEnum.endTime.rawValue
            
        case .description:
            self.textViewDiscription.placeholder = "Description"
            self.textViewDiscription.keyboardType = .default
            self.textViewDiscription.isUserInteractionEnabled = true
            self.textViewDiscription.tag = CreateEventCellType.description.rawValue
            
            model.keyValue = textViewDiscription.text!
            model.keyName = EventEnum.discription.rawValue
            
        default:
            break
        }
    }
    
    
    //MARK:- UITextView Delegate
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        guard let tableViewCreateProfile : UITableView = self.superviewOfClassType(UITableView.self) as? UITableView else
        {
            return false
        }
        
        if text == "\n" {
            return false
        }
        
        let currentOffset = tableViewCreateProfile.contentOffset
        UIView.setAnimationsEnabled(false)
        tableViewCreateProfile.beginUpdates()
        tableViewCreateProfile.endUpdates()
        UIView.setAnimationsEnabled(true)
        tableViewCreateProfile.setContentOffset(currentOffset, animated: false)
        
        var finalCount = 0
        var newString : String?
        
        if text.isEmpty
        {
            if (textView.text?.isEmpty)!
            {
                finalCount = 0
                newString = textView.text
            }
            else
            {
                finalCount = textView.text!.characters.count - 1
                
                newString = textView.text?.substring(to: (textView.text?.index((textView.text?.startIndex)!, offsetBy: (textView.text?.characters.count)!-1))!)
            }
        }
        else
        {
            finalCount = textView.text!.characters.count + 1
            var newStr = textView.text! as NSString
            
            newStr = newStr.replacingCharacters(in: range, with: text) as NSString
            
            newString = newStr as String
        }
        
        //        if cellType != ProfileCellType.scenes
        //        {
        if (finalCount>140)
        {
            let index = newString!.index(newString!.startIndex, offsetBy: 140)
            //str.substring(to: index)  // Hello
            newString = newString!.substring(to: index)
            textView.text=newString!
            
            
            model.keyValue = newString!
            print("*****************" + model.keyValue)
            //
            guard let delegate = eventDelegate else {
                return true
            }
            delegate.createEventCell(cell: self, updatedInputfieldText: model.keyValue)
            
            IQKeyboardManager.sharedManager().reloadLayoutIfNeeded()
            
            //let index = str.index(str.startIndex, offsetBy: 5)
            let currentOffset = tableViewCreateProfile.contentOffset
            UIView.setAnimationsEnabled(false)
            tableViewCreateProfile.beginUpdates()
            tableViewCreateProfile.endUpdates()
            UIView.setAnimationsEnabled(true)
            tableViewCreateProfile.setContentOffset(currentOffset, animated: false)
            return false;
        }
        
        model.keyValue = newString!
        print("*****************" + model.keyValue)
        //
        guard let delegate = eventDelegate else {
            return true
        }
        delegate.createEventCell(cell: self, updatedInputfieldText: model.keyValue)
        
        IQKeyboardManager.sharedManager().reloadLayoutIfNeeded()
        
        return true
    }
    
    //MARK:- NSLayoutManager Delegate
    func layoutManager(_ layoutManager: NSLayoutManager, lineSpacingAfterGlyphAt glyphIndex: Int, withProposedLineFragmentRect rect: CGRect) -> CGFloat {
        return 5
    }
    
    
    //MARK:- UITextField Delegate
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
//        if textField.isAskingCanBecomeFirstResponder
//        {
//            return true
//        }
        var startDate:String
        if model.keyName == EventEnum.startTime.rawValue {
            startDate=model.keyValue
        }
        
        
        
        
        if textField.tag == CreateEventCellType.startDateTime.rawValue
        {
            if textField==textFieldDate
            {
                
                
                textField.inputView = UtilityClass.getDatePickerView()
                let timePicker = textField.inputView! as! UIDatePicker
                if (textField.text?.isEmpty)!
                {
                    textField.text = timePicker.date.getStringForFormat(format: "d MMM yyyy")
                    model.keyValue = String(self.textFieldDate.text! + "divider" + self.textFieldTime.text!)
                    
                    guard let delegate = eventDelegate else {
                        return true
                    }
                    delegate.createEventCell(cell: self, updatedInputfieldText: model.keyValue)
                }
                else
                {
                    if let dateStr = model.keyValue.components(separatedBy: "divider").first {
                        timePicker.date = Date.getDate(fromString: dateStr)
                    }
                }
                timePicker.addTarget(self, action: #selector(onDatePickerValueChanged(sender:)), for: .valueChanged)
                
                
            }
            else if textField==textFieldTime
                
            {
                
                
                
                
                textField.inputView = UtilityClass.getTimePicker12Format()
                let timePicker = textField.inputView! as! UIDatePicker
                if (textField.text?.isEmpty)!
                {
                    textField.text = timePicker.date.get24HourFormat(format: "h:mm a")
                    
                    model.keyValue = String(self.textFieldDate.text! + "divider" + self.textFieldTime.text!)
                    
                    guard let delegate = eventDelegate else {
                        return true
                    }
                    delegate.createEventCell(cell: self, updatedInputfieldText: model.keyValue)
                }
                else
                {
                    if let dateStr = model.keyValue.components(separatedBy: "divider").last {
                        timePicker.date = Date.getTime(fromString: dateStr)
                    }
                }
                timePicker.addTarget(self, action: #selector(onTimePickerValueChanged(sender:)), for: .valueChanged)
                
            }
            
            
        }
        return true
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var newString : String?
        
        if string.characters.count == 0
        {
            if textField.text?.characters.count==0
            {
                newString = textField.text
            }
            else
            {
                newString = textField.text?.substring(to: (textField.text?.index((textField.text?.startIndex)!, offsetBy: (textField.text?.characters.count)!-1))!)
            }
        }
        else
        {
            var newStr = textField.text! as NSString
            
            newStr = newStr.replacingCharacters(in: range, with: string) as NSString
            
            newString = newStr as String
        }
        
        model.keyValue = newString!
        print("*****************" + model.keyValue)
        
        guard let delegate = eventDelegate else {
            return true
        }
        delegate.createEventCell(cell: self, updatedInputfieldText: model.keyValue)
        
        return true
    }
    
    //MARK:- DOB Value Update
    func onDatePickerValueChanged(sender : UIDatePicker) -> Void {
        self.textFieldDate.text = sender.date.getStringForFormat(format: "d MMM yyyy")
        model.keyValue = String(self.textFieldDate.text! + "divider" + self.textFieldTime.text!)
        
        guard let delegate = eventDelegate else {
            return
        }
        delegate.createEventCell(cell: self, updatedInputfieldText: model.keyValue)
    }
    
    //MARK:- DOB Value Update
    func onTimePickerValueChanged(sender : UIDatePicker) -> Void {
        self.textFieldTime.text = sender.date.get24HourFormat(format: "h:mm a")
        model.keyValue = String(self.textFieldDate.text! + "divider" + self.textFieldTime.text!)
        
        
        guard let delegate = eventDelegate else {
            return
        }
        delegate.createEventCell(cell: self, updatedInputfieldText: model.keyValue)
        
    }
    
    
    //MARK:- Update value from Model
    func updateValue(usingModel: ModelCreateEvent) -> Void
    {
        if usingModel.keyName == EventEnum.title.rawValue
        {
            textFieldTitle.text = usingModel.keyValue
            return
        }
        
        if usingModel.keyName == EventEnum.discription.rawValue
        {
            textViewDiscription.text = usingModel.keyValue
            return
        }
        textFieldDate.text = usingModel.keyValue.components(separatedBy: "divider").first!
        textFieldTime.text = usingModel.keyValue.components(separatedBy: "divider").last!
    }
}
extension UITextField {
    open override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        return false
    }
}
