//
//  InsightsHeaderCell.swift
//  OOTTBusinessApp
//
//  Created by Santosh on 15/01/18.
//  Copyright © 2018 KonstantInfoSolutions. All rights reserved.
//

import UIKit

let kCellIdentifier_InsightsHeaderCell = "insightsHeaderCell"

class InsightsHeaderCell: UITableViewCell {

    @IBOutlet weak var imageHeaderIcon: UIImageView!
    
    @IBOutlet weak var labelHeaderTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        backgroundColor = .clear
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func updateCellContents(forRow row: Int) {
        if row == 0 {
            imageHeaderIcon.image = #imageLiteral(resourceName: "insightGenderIcon")
            labelHeaderTitle.text = "Gender"
        }
        else
        {
            imageHeaderIcon.image = #imageLiteral(resourceName: "insightAgeRangeIcon")
            labelHeaderTitle.text = "Age Range"
        }
    }
    
}
