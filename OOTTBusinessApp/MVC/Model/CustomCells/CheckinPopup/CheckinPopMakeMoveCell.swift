//
//  CheckinPopMakeMoveCell.swift
//  OOTTBusinessApp
//
//  Created by Santosh on 29/06/18.
//  Copyright © 2018 KonstantInfoSolutions. All rights reserved.
//

import UIKit

let kCellIdentifier_CheckinPopupMakeMoveCell = "checkinPopupMakeMoveCell"

protocol CheckinPopMakeMoveCellDelegate: class {
    func checkinMakeMoveCell(_ cell: CheckinPopMakeMoveCell, didTapOnSoloButton soloButton: UIButton)
    func checkinMakeMoveCell(_ cell: CheckinPopMakeMoveCell, didTapOnGroupButton groupButton: UIButton)
    func checkinMakeMoveCell(_ cell: CheckinPopMakeMoveCell, didTapOnCancelButton cancelButton: UIButton)
}

class CheckinPopMakeMoveCell: UITableViewCell {

    weak var delegate: CheckinPopMakeMoveCellDelegate?
    @IBOutlet weak var buttonSolo: UIButton!
    @IBOutlet weak var buttonChooseGroup: UIButton!
    @IBOutlet weak var buttonCancel: UIButton!
    @IBAction func onButtonSoloAction(_ sender: UIButton) {
        delegate?.checkinMakeMoveCell(self, didTapOnSoloButton: sender)
    }
    @IBAction func onButtonChooseGroupAction(_ sender: UIButton) {
        delegate?.checkinMakeMoveCell(self, didTapOnGroupButton: sender)
    }
    @IBAction func onButtonCancelAction(_ sender: UIButton) {
        delegate?.checkinMakeMoveCell(self, didTapOnCancelButton: sender)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        buttonSolo.layer.cornerRadius = 4 * scaleFactorX
        buttonChooseGroup.layer.cornerRadius = 4 * scaleFactorX
        buttonCancel.layer.cornerRadius = 4 * scaleFactorX
        
        buttonCancel.layer.borderWidth = 1
        buttonCancel.layer.borderColor = UIColor (RED: 273, GREEN: 43, BLUE: 93, ALPHA: 1).cgColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
