//
//  CheckinPopupCell.swift
//  OOTTBusinessApp
//
//  Created by Santosh on 16/01/18.
//  Copyright © 2018 KonstantInfoSolutions. All rights reserved.
//

import UIKit

let kCellIdentifier_CheckinPopupCell = "checkinPopupCell"

enum CheckinPopupCellEnum {
    case lineWait
    case crowded
}

class CheckinPopupCell: UITableViewCell {
    
    var selectedOption: Int = 0
    
    var cellType = CheckinPopupCellEnum.lineWait
    
    
    weak var delegate: CheckinPopupCellDelegate?

    @IBOutlet weak var labelQuestion: UILabel!
    @IBOutlet weak var buttonOption1: UIButton!
    @IBOutlet weak var buttonOption2: UIButton!
    @IBOutlet weak var buttonOption3: UIButton!
    @IBOutlet weak var buttonOption4: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        labelQuestion.makeAdaptiveFont()
        buttonOption1.titleLabel?.makeAdaptiveFont()
        buttonOption2.titleLabel?.makeAdaptiveFont()
        buttonOption3.titleLabel?.makeAdaptiveFont()
        buttonOption4.titleLabel?.makeAdaptiveFont()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func onButtonOption1Action(_ sender: UIButton) {
        selectedOption = 1
        updateView()
    }
    
    @IBAction func onButtonOption2Action(_ sender: UIButton) {
        selectedOption = 2
        updateView()
    }
    
    @IBAction func onButtonOption3Action(_ sender: UIButton) {
        selectedOption = 3
        updateView()
    }
    
    @IBAction func onButtonOption4Action(_ sender: UIButton) {
        selectedOption = 4
        updateView()
    }
    
    func updateView() {
        var optionValue = ""
        switch selectedOption {
        case 1:
            buttonOption1.setImage(#imageLiteral(resourceName: "radioOnIcon"), for: .normal)
            buttonOption2.setImage(#imageLiteral(resourceName: "radioOffIcon"), for: .normal)
            buttonOption3.setImage(#imageLiteral(resourceName: "radioOffIcon"), for: .normal)
            buttonOption4.setImage(#imageLiteral(resourceName: "radioOffIcon"), for: .normal)
            optionValue = cellType == .lineWait ? "1" : "1"
        case 2:
            buttonOption2.setImage(#imageLiteral(resourceName: "radioOnIcon"), for: .normal)
            buttonOption1.setImage(#imageLiteral(resourceName: "radioOffIcon"), for: .normal)
            buttonOption3.setImage(#imageLiteral(resourceName: "radioOffIcon"), for: .normal)
            buttonOption4.setImage(#imageLiteral(resourceName: "radioOffIcon"), for: .normal)
            optionValue = cellType == .lineWait ? "2" : "2"
        case 3:
            buttonOption3.setImage(#imageLiteral(resourceName: "radioOnIcon"), for: .normal)
            buttonOption2.setImage(#imageLiteral(resourceName: "radioOffIcon"), for: .normal)
            buttonOption1.setImage(#imageLiteral(resourceName: "radioOffIcon"), for: .normal)
            buttonOption4.setImage(#imageLiteral(resourceName: "radioOffIcon"), for: .normal)
            optionValue = cellType == .lineWait ? "3" : "3"
        case 4:
            buttonOption4.setImage(#imageLiteral(resourceName: "radioOnIcon"), for: .normal)
            buttonOption2.setImage(#imageLiteral(resourceName: "radioOffIcon"), for: .normal)
            buttonOption3.setImage(#imageLiteral(resourceName: "radioOffIcon"), for: .normal)
            buttonOption1.setImage(#imageLiteral(resourceName: "radioOffIcon"), for: .normal)
            optionValue = cellType == .lineWait ? "4" : "4"
        default:
            break
        }
        buttonOption1.setNeedsDisplay()
        buttonOption2.setNeedsDisplay()
        buttonOption3.setNeedsDisplay()
        buttonOption4.setNeedsDisplay()
        guard let tempDelegate = delegate else {
            return
        }
        tempDelegate.checkinPopupCell(self, didUpdateWithOption: optionValue.trimmingCharacters(in: .whitespaces))
    }
    
    func updateCell(usingModel model: CheckinQA) {
        labelQuestion.text = model.question
        buttonOption1.setTitle("   " + model.option1, for: .normal)
        buttonOption2.setTitle("   " + model.option2, for: .normal)
        buttonOption3.setTitle("   " + model.option3, for: .normal)
        buttonOption4.setTitle("   " + model.option4, for: .normal)
        
        buttonOption1.setNeedsDisplay()
        buttonOption2.setNeedsDisplay()
        buttonOption3.setNeedsDisplay()
        buttonOption4.setNeedsDisplay()
    }
}


protocol CheckinPopupCellDelegate: class {
    func checkinPopupCell(_ cell: CheckinPopupCell, didUpdateWithOption option: String)
}









