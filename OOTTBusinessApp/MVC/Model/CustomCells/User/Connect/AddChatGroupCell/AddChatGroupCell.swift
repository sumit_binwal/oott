//
//  AddChatGroupCell.swift
//  OOTTBusinessApp
//
//  Created by Santosh on 09/10/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit

let kCellIdentifier_AddGroup_GroupName = "cellGroupName"
let kCellIdentifier_AddGroup_GroupSettingTitle = "cellSettingsTitle"
let kCellIdentifier_AddGroup_GroupMemberSettings = "cellMemberSettings"
let kCellIdentifier_AddGroup_GroupInviteMembers = "cellInviteMembers"
let kCellIdentifier_AddGroup_GroupLeaderMember = "cellLeaderMember"
let kCellIdentifier_AddGroup_GroupMember = "cellMembers"

enum AddGroupCellType : Int {
    case groupName
    case groupSettingTitle
    case groupSettingMembers
    case groupInviteMembers
    case groupMembers
    case groupLeaderMember
    case groupCoLeaderMember
    case none
}

protocol AddChatGroupCellDelegate : class {
    func addGroupCell(_ cell : AddChatGroupCell, didTapOnCanInviteButton inviteButton : UIButton)
    func addGroupCell(_ cell : AddChatGroupCell, didTapOnMakeMovesButton makeMovesButton : UIButton)
    func addGroupCell(_ cell : AddChatGroupCell, didUpdateGroupName groupName : String)
}

class AddChatGroupCell: UITableViewCell {

    
    
    @IBOutlet weak var bottomDividerLine: UIView!
    @IBOutlet weak var inputTextField: UITextField!
    
    @IBOutlet weak var buttonCanInvite: UIButton!
    @IBOutlet weak var buttonCanMakeMoves: UIButton!
  
    
    @IBOutlet weak var inviteMemberNameLabel: UILabel!
    
    @IBOutlet weak var inviteMemberArrowImage: UIImageView!
    @IBOutlet weak var inviteMemberImageView: UIImageView!
    
    
    @IBOutlet weak var groupLeaderLabel: UILabel!
    @IBOutlet weak var groupLeaderNameLabel: UILabel!
    
    weak var cellDelegate : AddChatGroupCellDelegate?
    
    //MARK:- CellType Enum Instance
    var cellType = AddGroupCellType.none
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        if inputTextField != nil
        {
            inputTextField.setPlaceholderColor(UIColor.white)
            self.inputTextField.placeholder = "Group Name"
            bottomDividerLine.backgroundColor = color_dividerLine
        }
        
        if inviteMemberImageView != nil
        {
            inviteMemberImageView.makeRound()
        }
        
        self.disbaleSelection()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        switch cellType {
        case .groupName:
            self.inputTextField.placeholder = "Group Name"
            self.inputTextField.keyboardType = .asciiCapable
            self.inputTextField.delegate = self
            self.inputTextField.tag = AddGroupCellType.groupName.rawValue
            
        case .groupLeaderMember:
            self.groupLeaderLabel.text = "Leader"
            
        case .groupCoLeaderMember:
            self.groupLeaderLabel.text = "Co-Leader"
        default:
            break
        }
    }

    
    @IBAction func onButtonCanInviteAction(_ sender: UIButton) {
        guard let delegate = cellDelegate else {
            return
        }
        
        delegate.addGroupCell(self, didTapOnCanInviteButton: sender)
    }
    
    @IBAction func onButtonCanMakeMovesAction(_ sender: UIButton) {
        guard let delegate = cellDelegate else {
            return
        }
        
        delegate.addGroupCell(self, didTapOnMakeMovesButton: sender)
    }
    
    func updateCell(usingModel model : ModelAddChatGroup, forIndex index : Int)
    {
        if inputTextField != nil
        {
            inputTextField.text = model.groupName
        }
        
        if cellType == .groupSettingMembers {
            
            do
            {
                var image = #imageLiteral(resourceName: "toggle_off")
                if model.groupMemberCanMakeMoves
                {
                    image = #imageLiteral(resourceName: "toggle_on")
                }
                
                self.buttonCanMakeMoves.setImage(image, for: .normal)
            }
            
            do
            {
                var image = #imageLiteral(resourceName: "toggle_off")
                if model.groupMemberCanInvite
                {
                    image = #imageLiteral(resourceName: "toggle_on")
                }
                
                self.buttonCanInvite.setImage(image, for: .normal)
            }
        }
        
        // Members
        if cellType == .groupInviteMembers
        {
            self.inviteMemberNameLabel.text = "Invite Members"
            self.inviteMemberArrowImage.isHidden = false
        }
        
        if cellType == .groupMembers
        {
            if let members = model.groupInviteMembers
            {
                self.inviteMemberNameLabel.text = members[index-2].username
                self.inviteMemberArrowImage.isHidden = true
                
                self.inviteMemberImageView.setIndicatorStyle(.white)
                self.inviteMemberImageView.setShowActivityIndicator(true)
                
                self.inviteMemberImageView.sd_setImage(with: URL (string: members[index-2].picture), completed: nil)
            }
        }
        
        if cellType == .groupLeaderMember
        {
            self.groupLeaderNameLabel.text = model.groupMemberLeader?.username ?? "Select Leader"
            self.contentView.viewWithTag(101)?.isHidden = true
        }
        
        if cellType == .groupCoLeaderMember
        {
            self.groupLeaderNameLabel.text = model.groupMemberCoLeader?.username ?? "Select Co-Leader"
            self.contentView.viewWithTag(101)?.isHidden = true
        }
    }
}

extension AddChatGroupCell : UITextFieldDelegate
{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var newString : String?
        var finalCount = 0
        
        if string.characters.count == 0
        {
            if textField.text?.characters.count==0
            {
                finalCount = 0
                newString = textField.text
            }
            else
            {
                finalCount = textField.text!.characters.count - 1
                var newStr = textField.text! as NSString
                newStr = newStr.replacingCharacters(in: range, with: string) as NSString
                newString = newStr as String
            }
        }
        else
        {
            finalCount = textField.text!.characters.count + 1
            var newStr = textField.text! as NSString
            
            newStr = newStr.replacingCharacters(in: range, with: string) as NSString
            
            newString = newStr as String
        }
        
        guard let delegate = cellDelegate else {
            return true
        }
        
        delegate.addGroupCell(self, didUpdateGroupName: newString!)
        
        return true
    }
}
