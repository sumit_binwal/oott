//
//  ConnectGroupCell.swift
//  OOTTBusinessApp
//
//  Created by Bharat Kumar Pathak on 10/07/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit

let kCellIdentifier_connect_group_cell = "connectGroupCell"

class ConnectGroupCell: UITableViewCell {
    
    @IBOutlet weak var gradientSelectionImageView: UIImageView!
    @IBOutlet weak var groupImageView: UIImageView!
    @IBOutlet weak var groupNameLabel: UILabel!
    @IBOutlet weak var groupTimeLabel: UILabel!
    @IBOutlet weak var groupLocationLabel: UILabel!
    @IBOutlet weak var friendsCountLabel: UILabel!
    @IBOutlet weak var peopleCountLabel: UILabel!
    @IBOutlet weak var unseenBackgroundView: UIView!
    @IBOutlet weak var unseenCountLabel: UILabel!
    
    
    
    // MARK:-
    override func awakeFromNib() {
        super.awakeFromNib()
        self.groupImageView.makeRound()
        self.groupImageView.createBorder(withColor: color_cellImageBorder, andBorderWidth: 0.3)
        self.disbaleSelection()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // MARK:-
    override func layoutSubviews() {
        
        self.unseenBackgroundView.layer.cornerRadius = self.unseenBackgroundView.frame.size.height/2
    }
    
    func updateCell(usingModel model : ModelGroupChatList)
    {
        groupNameLabel.text = model.groupName
        
        groupImageView.setIndicatorStyle(.white)
        groupImageView.setShowActivityIndicator(true)
        
        groupImageView.sd_setImage(with: URL (string: model.groupPicture), completed: nil)
        
        friendsCountLabel.text = String(model.friendsCount)
        peopleCountLabel.text = String(model.peopleCount)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ" //Your date format\
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
        dateFormatter.timeZone = NSTimeZone(name: "UTC")! as TimeZone
        let date : NSDate = dateFormatter.date(from: model.groupCreateTime)! as NSDate //according to date format your date string
        
        self.groupTimeLabel.text = UtilityClass.timeAgoSinceDate(date as Date, currentDate:NSDate() as Date , numericDates: false)
        
        if model.unreadCount == 0
        {
            unseenBackgroundView.isHidden = true
        }
        else
        {
            unseenBackgroundView.isHidden = false
            unseenCountLabel.text = UtilityClass.getFormattedNumberString(usingString: String (model.unreadCount))
        }
        
        if model.isBusinessChat
        {
            self.contentView.viewWithTag(101)?.isHidden = true
            self.groupLocationLabel.isHidden = true
            self.gradientSelectionImageView.isHidden = false
        }
        else
        {
            self.gradientSelectionImageView.isHidden = true
            if model.lastCheckIn == nil || model.lastCheckIn?.checkinStatus == nil
            {
                self.contentView.viewWithTag(101)?.isHidden = true
                self.groupLocationLabel.isHidden = true
            }
            else
            {
                self.contentView.viewWithTag(101)?.isHidden = false
                self.groupLocationLabel.isHidden = false
                self.groupLocationLabel.text = model.lastCheckIn?.checkinStatus
            }
        }
        if groupLocationLabel != nil {
            self.groupLocationLabel.textColor = color_pink
        }
        if let imageView = self.contentView.viewWithTag(101) as? UIImageView {
            imageView.image = model.isMakeMoveStatus ? #imageLiteral(resourceName: "runningIcon") : #imageLiteral(resourceName: "location")
        }
    }
    
}
