//
//  CommentCell.swift
//  OOTTBusinessApp
//
//  Created by Bharat Kumar Pathak on 13/07/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit

let kCellIdentifier_comment_cell = "commentCell"

class CommentCell: UITableViewCell {

    @IBOutlet weak var commenterImageView: UIImageView!
    @IBOutlet weak var commenterNameLabel: UILabel!
    @IBOutlet weak var commentTimeLabel: UILabel!
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var buttonLikeUnLike: UIButton!
    @IBOutlet var buttonLikeCount: UIButton!
    
    weak var delegate: CommentCellDelegate?
    
    // MARK:-
    override func awakeFromNib() {
        super.awakeFromNib()
        self.commenterImageView.makeRound()
        self.commenterImageView.createBorder(withColor: color_cellImageBorder, andBorderWidth: 0.5)
        
        self.disbaleSelection()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    //MARK:- Update cell contents
    func updateCell(usingModel model:ModelCheckInComments)
    {
        self.commenterNameLabel.text = model.username
        self.commentLabel.attributedText = model.commentString.getAttributedString(10, font: self.commentLabel.font)
        if let pictureURL = URL (string: model.userPicture)
        {
            self.commenterImageView.setIndicatorStyle(.gray)
            self.commenterImageView.setShowActivityIndicator(true)
            self.commenterImageView.sd_setImage(with: pictureURL, completed: nil)
        }
        if model.timeString != nil
        {
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ" //Your date format\
            dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
            dateFormatter.timeZone = NSTimeZone(name: "UTC")! as TimeZone
            let date : NSDate = dateFormatter.date(from: model.timeString )! as NSDate //according to date format your date string
        
            self.commentTimeLabel.text = UtilityClass.timeAgoSinceDate(date as Date, currentDate:NSDate() as Date , numericDates: false)
        }
        
        if model.isLiked
        {
            self.buttonLikeUnLike.setImage(#imageLiteral(resourceName: "liked"), for: .normal)
        }
        else
        {
            self.buttonLikeUnLike.setImage(#imageLiteral(resourceName: "like"), for: .normal)
        }
        
        if model.likesCount == "0"
        {
            self.buttonLikeCount.isHidden = true
        }
        else
        {
            self.buttonLikeCount.isHidden = false

            if model.likesCount == "1"
            {
                self.buttonLikeCount.setTitle("\(model.likesCount) like", for: .normal)
            }
            else
            {
                self.buttonLikeCount.setTitle("\(model.likesCount) likes", for: .normal)
            }
            
        }
    }
    
    @IBAction func onButtonLikeCountAction(_ sender: UIButton) {
        guard let tempDelegate = delegate else {
            return
        }
        tempDelegate.commentCell(self, didTapOnLikeUnLikeButton: sender)
    }
    //MARK:- Like Unlike Action
    @IBAction func onButtonLikeUnlikeAction(_ sender: UIButton) {
        guard let tempDelegate = delegate else {
            return
        }
        tempDelegate.commentCell(self, didTapOnLikeUnLikeButton: sender)
    }
}

protocol CommentCellDelegate: class
{
    func commentCell(_ cell: CommentCell, didTapOnLikeUnLikeButton likeUnlikeButton: UIButton)
    
}
