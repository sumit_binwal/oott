//
//  FriendsCell.swift
//  OOTTBusinessApp
//
//  Created by Bharat Kumar Pathak on 10/07/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit

let kCellIdentifier_friends_cell = "friendsCell"

class FriendsCell: UITableViewCell {

    @IBOutlet weak var friendImageView: UIImageView!
    @IBOutlet weak var friendNameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var friendLocationLabel: UILabel!
    @IBOutlet weak var commentCountLabel: UILabel!
    @IBOutlet weak var likeCountLabel: UILabel!
    @IBOutlet var updatedTimeLabel: UILabel!
    @IBOutlet weak var inviteToGroupButton: UIButton!
    @IBOutlet weak var commentButton: UIButton!
    
    
    @IBOutlet weak var likeUnLikeImageview: UIImageView!
    @IBOutlet weak var likeUnlikeButton: UIButton!
    
    weak var cellDelegate: FriendsCellDelegate?
    
    // MARK:-
    override func awakeFromNib() {
        super.awakeFromNib()
        self.disbaleSelection()
        friendNameLabel.textColor = .white
        descriptionLabel.text = ""
        self.friendImageView.makeRound()
        self.friendImageView.createBorder(withColor: color_cellImageBorder, andBorderWidth: 0.5)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // MARK:-
    override func layoutSubviews() {
        
//        self.friendImageView.makeRound()
//        self.friendImageView.createBorder(withColor: color_cellImageBorder, andBorderWidth: 0.5)
//        self.inviteToGroupButton.layer.cornerRadius = CGFloat(2.5)
    }
    
    // MARK:-
    @IBAction func onInviteButtonAction(_ sender: UIButton) {
        
        guard let tempDelegate = self.cellDelegate else {
            return
        }
        tempDelegate.friendCell(cell: self, didTapOnInviteButton: sender)
    }
    
    @IBAction func onCommentButtonAction(_ sender: UIButton) {
        
        guard let tempDelegate = self.cellDelegate else {
            return
        }
        tempDelegate.friendCell(cell: self, didTapOnCommentButton: sender)
    }
    
    @IBAction func onLikeUnLikeButtonAction(_ sender: UIButton) {
        guard let tempDelegate = self.cellDelegate else {
            return
        }
        tempDelegate.friendCell(cell: self, didTapOnLikeUnLikeButton: sender)
    }
    
    
    //MARK:- Update Cell Using Model
    func updateData(usingModel model:ModelFriendsListing) -> Void
    {
        // Friend name
        self.friendNameLabel.text = model.friendName
        
        // Profile picture
        if model.friendPicture != nil
        {
            self.friendImageView.setIndicatorStyle(.white)
            self.friendImageView.setShowActivityIndicator(true)
            self.friendImageView.sd_setImage(with: URL (string: model.friendPicture!), completed: nil)
        }
        
        if (model.updateStatusTime?.count)! > 0
        {
            let dateFormatter = DateFormatter()
            //2018-06-25T06:23:08.278Z
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ" //Your date format\
            dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
            dateFormatter.timeZone = NSTimeZone(name: "UTC")! as TimeZone
            let date : NSDate = dateFormatter.date(from: model.updateStatusTime!) as! NSDate //according to date format your date string
            self.updatedTimeLabel.text = UtilityClass.timeAgoSinceDate(date as Date, currentDate:NSDate() as Date , numericDates: false)

        }
        else
        {
            self.updatedTimeLabel.text = ""
        }
        
        

        
        self.friendLocationLabel.text = ""
        self.friendLocationLabel.textColor = color_pink
        
        if let txt = model.friendStatus
        {
            if txt.isEmptyString() || model.isAccountDeleted || model.isGhostMode! {
                self.contentView.viewWithTag(250)?.isHidden = true
                self.contentView.viewWithTag(251)?.isHidden = true
                self.contentView.viewWithTag(252)?.isHidden = true
                self.contentView.viewWithTag(253)?.isHidden = true
                self.contentView.viewWithTag(254)?.isHidden = true
                self.contentView.viewWithTag(255)?.isHidden = true
                self.contentView.viewWithTag(256)?.isHidden = true
            }
            else
            {
                self.contentView.viewWithTag(250)?.isHidden = false
                self.contentView.viewWithTag(251)?.isHidden = false
                self.contentView.viewWithTag(252)?.isHidden = false
                self.contentView.viewWithTag(253)?.isHidden = false
                self.contentView.viewWithTag(254)?.isHidden = false
                self.contentView.viewWithTag(255)?.isHidden = false
                self.contentView.viewWithTag(256)?.isHidden = false
                
                if let imgView = self.contentView.viewWithTag(250) as? UIImageView
                {
                    imgView.image = #imageLiteral(resourceName: "location")
                }
                self.friendLocationLabel.text = model.friendStatus
            }
        }
        else
        {
            if let txt = model.friendStatusMakeMove
            {
                if txt.isEmptyString() || model.isAccountDeleted || model.isGhostMode! {
                    self.contentView.viewWithTag(250)?.isHidden = true
                    self.contentView.viewWithTag(251)?.isHidden = true
                    self.contentView.viewWithTag(252)?.isHidden = true
                    self.contentView.viewWithTag(253)?.isHidden = true
                    self.contentView.viewWithTag(254)?.isHidden = true
                    self.contentView.viewWithTag(255)?.isHidden = true
                    self.contentView.viewWithTag(256)?.isHidden = true
                }
                else
                {
                    self.contentView.viewWithTag(250)?.isHidden = false
                    self.contentView.viewWithTag(251)?.isHidden = false
                    self.contentView.viewWithTag(252)?.isHidden = false
                    self.contentView.viewWithTag(253)?.isHidden = false
                    self.contentView.viewWithTag(254)?.isHidden = false
                    self.contentView.viewWithTag(255)?.isHidden = false
                    self.contentView.viewWithTag(256)?.isHidden = false
                    
                    if let imgView = self.contentView.viewWithTag(250) as? UIImageView
                    {
                        imgView.image = #imageLiteral(resourceName: "runningIcon")
                    }
                    self.friendLocationLabel.text = model.friendStatusMakeMove
                }
            }
            else
            {
                self.contentView.viewWithTag(250)?.isHidden = true
                self.contentView.viewWithTag(251)?.isHidden = true
                self.contentView.viewWithTag(252)?.isHidden = true
                self.contentView.viewWithTag(253)?.isHidden = true
                self.contentView.viewWithTag(254)?.isHidden = true
                self.contentView.viewWithTag(255)?.isHidden = true
                self.contentView.viewWithTag(256)?.isHidden = true
            }
        }
        
        self.commentCountLabel.text = UtilityClass.getFormattedNumberString(usingString: model.commentsCount!)
        self.likeCountLabel.text = UtilityClass.getFormattedNumberString(usingString: model.likesCount!)
//        self.inviteToGroupButton.alpha = model.isInvited! ? 0.5 : 1
        
        self.descriptionLabel.text = model.description
        
        if model.isLiked
        {
            self.likeUnLikeImageview.image = #imageLiteral(resourceName: "liked")
        }
        else
        {
            self.likeUnLikeImageview.image = #imageLiteral(resourceName: "like")
        }
        
//        inviteToGroupButton.isHidden = model.isAccountDeleted
    }
}

protocol FriendsCellDelegate: class {
    
    func friendCell(cell: FriendsCell, didTapOnInviteButton inviteButton: UIButton)
    func friendCell(cell: FriendsCell, didTapOnCommentButton commentButton: UIButton)
    func friendCell(cell: FriendsCell, didTapOnLikeUnLikeButton likeUnLikeButton: UIButton)
}
