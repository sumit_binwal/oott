//
//  SuggestedFriendsCell.swift
//  OOTTBusinessApp
//
//  Created by Bharat Kumar Pathak on 10/07/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit

let kCellIdentifier_suggested_friends_cell = "suggestedFriendsCell"

class SuggestedFriendsCell: UITableViewCell {

    @IBOutlet weak var suggestedFriendImageView: UIImageView!
    @IBOutlet weak var suggestedFriendNameLabel: UILabel!
    @IBOutlet weak var mutualFriendsCountLabel: UILabel!
    @IBOutlet weak var followUnfollowButton: UIButton!
    
    weak var cellDelegate: SuggestedFriendsCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.disbaleSelection()
        self.suggestedFriendImageView.makeRound()
        self.suggestedFriendImageView.createBorder(withColor: color_cellImageBorder, andBorderWidth: 0.3)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func layoutSubviews() {
        
//        self.suggestedFriendImageView.makeRound()
//        self.suggestedFriendImageView.createBorder(withColor: color_cellImageBorder, andBorderWidth: 0.3)
        self.followUnfollowButton.layer.cornerRadius = CGFloat(2.5)
    }

    @IBAction func onFollowUnfollowAction(_ sender: UIButton) {
        
        guard let tempDelegate = self.cellDelegate else {
            return
        }
        tempDelegate.suggestedFriendsCell(cell: self, didTapOnFollowButton: sender)
    }
    
    func updateCell(usingModel model : ModelFriendsListing)
    {
        self.suggestedFriendNameLabel.text = model.friendName
        
        // Profile picture
        if model.friendPicture != nil
        {
            self.suggestedFriendImageView.setIndicatorStyle(.white)
            self.suggestedFriendImageView.setShowActivityIndicator(true)
            self.suggestedFriendImageView.sd_setImage(with: URL (string: model.friendPicture!), completed: nil)
        }
        self.mutualFriendsCountLabel.text = model.getMutualFriendsString()
        mutualFriendsCountLabel.isHidden = model.isAccountDeleted
        followUnfollowButton.isHidden = model.isAccountDeleted
        
        if !model.isAccountDeleted {
            if model.isPrivateUser, model.isRequestedForFollow {
                self.followUnfollowButton.backgroundColor = .clear
                self.followUnfollowButton.setTitle("Requested".uppercased(), for: .normal)
                self.followUnfollowButton.setTitleColor(color_cellImageBorder, for: .normal)
                self.followUnfollowButton.isUserInteractionEnabled = false
            }
            else
            {
                self.followUnfollowButton.backgroundColor = color_cellImageBorder
                self.followUnfollowButton.setTitle("Follow".uppercased(), for: .normal)
                self.followUnfollowButton.setTitleColor(.white, for: .normal)
                self.followUnfollowButton.isUserInteractionEnabled = true
            }
        }
    }
    
}

protocol SuggestedFriendsCellDelegate: class {
    
    func suggestedFriendsCell(cell: SuggestedFriendsCell, didTapOnFollowButton followButton: UIButton)
}
