//
//  BlockedUserCell.swift
//  OOTTBusinessApp
//
//  Created by Santosh on 10/11/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit

let kCellIdentifier_BlockedUsersCell = "blockUserCell"


protocol BlockedUserCellDelegate : class {
    func blockedCell(_ cell : BlockedUserCell, didTapOnUnblockUser unblockUserButton : UIButton)
}

class BlockedUserCell: UITableViewCell {

    
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var buttonUnblock: UIButton!
    
    weak var delegate : BlockedUserCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.viewWithTag(200)?.backgroundColor = color_pink
        self.buttonUnblock.setTitle("unblock".uppercased(), for: .normal)
        self.buttonUnblock.setTitleColor(.white, for: .normal)
        
        self.userImageView.makeRound()
        self.userImageView.createBorder(withColor: color_cellImageBorder, andBorderWidth: 0.5)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func onButtonUnblockAction(_ sender: UIButton) {
        
        guard let delegate = delegate else {return}
        
        delegate.blockedCell(self, didTapOnUnblockUser: sender)
    }
    
    func updateCellContents(usingModel model : GroupMember)
    {
        self.userName.text = model.username
        
        self.userImageView.setIndicatorStyle(.white)
        self.userImageView.setShowActivityIndicator(true)
        
        self.userImageView.sd_setImage(with: URL (string: model.picture), completed: nil)
    }
}
