//
//  FollowingCell.swift
//  OOTTBusinessApp
//
//  Created by Bharat Kumar Pathak on 10/07/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit

let kCellIdentifier_following_cell = "followingCell"

class FollowingCell: UITableViewCell {

    @IBOutlet weak var followingImageView: UIImageView!
    @IBOutlet weak var followingNameLabel: UILabel!
    @IBOutlet weak var followingLocationLabel: UILabel!
    @IBOutlet weak var followUnfollowButton: UIButton!
    
    @IBOutlet weak var labelActivityTime: UILabel!
    
    
    var isFromActivity =  false
    
    
    weak var cellDelegate: FollowingCellDelegate?
    weak var activityDelegate: FollowingActivityCellDelegate?
    
    // MARK:-
    override func awakeFromNib() {
        super.awakeFromNib()
        self.disbaleSelection()
        self.followingImageView.makeRound()
        followingImageView.isUserInteractionEnabled = true
        self.followingImageView.createBorder(withColor: color_cellImageBorder, andBorderWidth: 0.3)
        addTapGesture(onView: followingImageView)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    // MARK:-
    override func layoutSubviews() {
        
//        self.followingImageView.makeRound()
//        self.followingImageView.createBorder(withColor: color_cellImageBorder, andBorderWidth: 0.3)
        
        if let tempButton = self.followUnfollowButton {
            tempButton.layer.cornerRadius = CGFloat(2.5)
            tempButton.layer.borderColor = color_cellImageBorder.cgColor
            tempButton.layer.borderWidth = CGFloat(0.5)
        }
    }
    
    //MARK:- Tap gesture On ImageView
    func addTapGesture(onView view : UIView)
    {
        let tapGesture = UITapGestureRecognizer (target: self, action: #selector(onTapGesture(gesture:)));
        tapGesture.numberOfTapsRequired = 1;
        view.addGestureRecognizer(tapGesture)
    }
    
    func onTapGesture(gesture: UITapGestureRecognizer)
    {
        if gesture.state == .ended
        {
            guard let tempDelegate = self.activityDelegate else {
                return
            }
            tempDelegate.followingCellDidTapOnImageView(self)
        }
    }
    
    // MARK:-
    @IBAction func onFollowUnfollowButtonAction(_ sender: UIButton) {
        
        guard let tempDelegate = self.cellDelegate else {
            return
        }
        tempDelegate.followingCell(cell: self, didTapOnFollowUnfollowButton: sender)
    }
    
    //MARK:- Update Cell Using Model
    func updateData(usingModel model:ModelFriendsListing) -> Void
    {
        // Friend name
        self.followingNameLabel.text = model.friendName
        
        // Profile picture
        if model.friendPicture != nil
        {
            self.followingImageView.setIndicatorStyle(.white)
            self.followingImageView.setShowActivityIndicator(true)
            self.followingImageView.sd_setImage(with: URL (string: model.friendPicture!), completed: nil)
        }
        if let txt = model.friendStatus {
            
            if model.isGhostMode!
            {
                self.followingLocationLabel.text = ""

            }
            else
            {
                self.followingLocationLabel.text = txt

            }
            if let imageView = self.contentView.viewWithTag(250) as? UIImageView {
                imageView.image = #imageLiteral(resourceName: "address")
            }
        }
        else {
            if model.isGhostMode!
            {
                self.followingLocationLabel.text = ""
                
            }
            else
            {
                self.followingLocationLabel.text = model.friendStatusMakeMove

            }
            if let imageView = self.contentView.viewWithTag(250) as? UIImageView {
                imageView.image = #imageLiteral(resourceName: "runningIcon")
            }
        }
//        if let txt = model.friendStatusMakeMove {
//            self.followingLocationLabel.text = txt
//            if let imageView = self.contentView.viewWithTag(250) as? UIImageView {
//                imageView.image = #imageLiteral(resourceName: "runningIcon")
//            }
//        }
//        else {
//            self.followingLocationLabel.text = model.friendStatus
//            if let imageView = self.contentView.viewWithTag(250) as? UIImageView {
//                imageView.image = #imageLiteral(resourceName: "address")
//            }
//        }
        self.followingLocationLabel.textColor = color_pink
        
        if let txt = self.followingLocationLabel.text
        {
            if txt.isEmptyString() {
                self.contentView.viewWithTag(250)?.isHidden = true
            }
            else
            {
                self.contentView.viewWithTag(250)?.isHidden = false
            }
        }
        else
        {
            self.contentView.viewWithTag(250)?.isHidden = true
        }
        
        if self.followUnfollowButton != nil {
            if model.isPrivateUser, model.isRequestedForFollow {
                self.followUnfollowButton.backgroundColor = .clear
                self.followUnfollowButton.setTitle("Requested".uppercased(), for: .normal)
                self.followUnfollowButton.setTitleColor(color_cellImageBorder, for: .normal)
                
                self.followUnfollowButton.isEnabled = false
                
                return
            }
            
            self.followUnfollowButton.isEnabled = true
            if model.isFollowed! {
                self.followUnfollowButton.backgroundColor = .clear
                self.followUnfollowButton.setTitle("Following".uppercased(), for: .normal)
                self.followUnfollowButton.setTitleColor(color_cellImageBorder, for: .normal)
            } else {
                if model.isRequestedForFollow {
                    self.followUnfollowButton.backgroundColor = .clear
                    self.followUnfollowButton.setTitle("Requested".uppercased(), for: .normal)
                    self.followUnfollowButton.setTitleColor(color_cellImageBorder, for: .normal)
                    
                    self.followUnfollowButton.isEnabled = false
                    return
                }
                self.followUnfollowButton.backgroundColor = color_cellImageBorder
                self.followUnfollowButton.setTitle("Follow".uppercased(), for: .normal)
                self.followUnfollowButton.setTitleColor(.white, for: .normal)
            }
            
            followUnfollowButton.isHidden = model.isAccountDeleted
            if model.isSameUser {
                followUnfollowButton.isHidden = true
            }
            
            if let userType = model.userType {
                if userType != AppUserTypeEnum.user.rawValue {
                    followUnfollowButton.isHidden = true
                }
            }
        }
        
        if model.isAccountDeleted {
            followingLocationLabel.isHidden = true
            self.contentView.viewWithTag(250)?.isHidden = true
        }
    }
    
    //MARK:- Update Cell Using Model (Activity Listing)
    func updateCell(usingModel model:ModelFollowingActivity) -> Void
    {
        // Friend name
        self.followingNameLabel.text = model.activityMessage!
        
        // Profile picture
        if model.activityBy?.pictureString != nil
        {
            self.followingImageView.setIndicatorStyle(.white)
            self.followingImageView.setShowActivityIndicator(true)
            self.followingImageView.sd_setImage(with: URL (string: (model.activityBy?.pictureString)!), completed: nil)
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ" //Your date format\
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
        dateFormatter.timeZone = NSTimeZone(name: "UTC")! as TimeZone
        let date : NSDate = dateFormatter.date(from: model.activityTime! )! as NSDate //according to date format your date string
        
        self.labelActivityTime.text = UtilityClass.timeAgoSinceDate(date as Date, currentDate:NSDate() as Date , numericDates: false)
        
        self.followingLocationLabel.text = model.activityBy?.lastCheckIn?.checkinStatus!
        
        if let txt = self.followingLocationLabel.text
        {
            if txt.isEmptyString() {
                self.contentView.viewWithTag(250)?.isHidden = true
            }
            else
            {
                self.contentView.viewWithTag(250)?.isHidden = false
            }
        }
        else
        {
            self.contentView.viewWithTag(250)?.isHidden = true
        }
    }
}

protocol FollowingCellDelegate: class {
    func followingCell(cell: FollowingCell, didTapOnFollowUnfollowButton followUnfollowButton: UIButton)
}

protocol FollowingActivityCellDelegate: class {
    
    func followingCellDidTapOnImageView(_ cell: FollowingCell)
}

