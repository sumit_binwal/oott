//
//  FollowersCell.swift
//  OOTTBusinessApp
//
//  Created by Bharat Kumar Pathak on 10/07/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit

let kCellIdentifier_followers_cell = "followersCell"
let kCellIdentifier_followers_cell_MakeMove = "makeMoveInviteCell"


class FollowersCell: UITableViewCell {

    @IBOutlet weak var followerImageView: UIImageView!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var followUnfollowButton: UIButton!
    
    @IBOutlet weak var labelActivityTime: UILabel!
    @IBOutlet weak var buttonAcceptInvitation: UIButton!
    @IBOutlet weak var buttonDeclineInvitation: UIButton!
    
    weak var cellDelegate: FollowersCellDelegate?
    weak var activityDelegate: FollowersActivityCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.disbaleSelection()
        followerImageView.isUserInteractionEnabled = true
        self.followerImageView.makeRound()
        self.followerImageView.createBorder(withColor: color_cellImageBorder, andBorderWidth: 0.3)
        if followUnfollowButton != nil
        {
            self.followUnfollowButton.layer.cornerRadius = CGFloat(2.5)
        }
        
        addTapGesture(onView: followerImageView)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func layoutSubviews() {
        
//        self.followerImageView.makeRound()
//        self.followerImageView.createBorder(withColor: color_cellImageBorder, andBorderWidth: 0.3)
    }
    
    //MARK:- Tap gesture On ImageView
    func addTapGesture(onView view : UIView)
    {
        let tapGesture = UITapGestureRecognizer (target: self, action: #selector(onTapGesture(gesture:)));
        tapGesture.numberOfTapsRequired = 1;
        view.addGestureRecognizer(tapGesture)
    }
    
    func onTapGesture(gesture: UITapGestureRecognizer)
    {
        if gesture.state == .ended
        {
            guard let tempDelegate = self.activityDelegate else {
                return
            }
            tempDelegate.followersCellDidTapOnImageView(self)
        }
    }

    @IBAction func onFollowUnfollowAction(_ sender: UIButton) {
        
        guard let tempDelegate = self.cellDelegate else {
            return
        }
        tempDelegate.followersCell(cell: self, didTapOnFollowUnfollowButton: sender)
    }
    
    func updateCell(usingModel model : ModelMyActivity)
    {
        if model.activityMessage != nil
        {
            self.messageLabel.text = model.activityMessage!
        }
                
        // Profile picture
        if model.otherUser?.pictureString != nil
        {
            self.followerImageView.setIndicatorStyle(.white)
            self.followerImageView.setShowActivityIndicator(true)
            self.followerImageView.sd_setImage(with: URL (string: (model.otherUser?.pictureString!)!), completed: nil)
        }
        
        
        if self.followUnfollowButton != nil {
            if (model.otherUser?.isPrivateUser)!, model.isRequestedToFollow {
                self.followUnfollowButton.backgroundColor = .clear
                self.followUnfollowButton.setTitle("Requested".uppercased(), for: .normal)
                self.followUnfollowButton.setTitleColor(color_cellImageBorder, for: .normal)
                
                self.followUnfollowButton.isEnabled = false
                
                return
            }
            
            self.followUnfollowButton.isEnabled = true
            if model.isFollowed {
                self.followUnfollowButton.backgroundColor = .clear
                self.followUnfollowButton.setTitle("Following".uppercased(), for: .normal)
                self.followUnfollowButton.setTitleColor(color_cellImageBorder, for: .normal)
            } else {
                if model.isRequestedToFollow {
                    self.followUnfollowButton.backgroundColor = .clear
                    self.followUnfollowButton.setTitle("Requested".uppercased(), for: .normal)
                    self.followUnfollowButton.setTitleColor(color_cellImageBorder, for: .normal)
                    
                    self.followUnfollowButton.isEnabled = false
                    return
                }
                self.followUnfollowButton.backgroundColor = color_cellImageBorder
                self.followUnfollowButton.setTitle("Follow".uppercased(), for: .normal)
                self.followUnfollowButton.setTitleColor(.white, for: .normal)
            }
            
            self.followUnfollowButton.isHidden = !model.shouldDisplayFollowUnfollowButton
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ" //Your date format\
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
        dateFormatter.timeZone = NSTimeZone(name: "UTC")! as TimeZone
        let date : NSDate = dateFormatter.date(from: model.activityTime! )! as NSDate //according to date format your date string
        
        self.labelActivityTime.text = UtilityClass.timeAgoSinceDate(date as Date, currentDate:NSDate() as Date , numericDates: false)
    }
    
    @IBAction func onButtonAcceptInvitation(_ sender: UIButton) {
        guard let tempDelegate = self.cellDelegate else {
            return
        }
        tempDelegate.followersCell(cell: self, didTapOnAcceptIniviteButton: sender)
    }
    
    @IBAction func onButtonDeclineInvitation(_ sender: UIButton) {
        guard let tempDelegate = self.cellDelegate else {
            return
        }
        tempDelegate.followersCell(cell: self, didTapOnDeclineIniviteButton: sender)
    }
}

//MARK:-
//MARK:- FollowersCellDelegate
protocol FollowersCellDelegate: class {
    
    func followersCell(cell: FollowersCell, didTapOnFollowUnfollowButton followUnfollowButton : UIButton)
    func followersCell(cell: FollowersCell, didTapOnAcceptIniviteButton inivteButton: UIButton)
    func followersCell(cell: FollowersCell, didTapOnDeclineIniviteButton inivteButton: UIButton)
}

//MARK:-
//MARK:- ActivityCellDelegate
protocol FollowersActivityCellDelegate: class {
    func followersCellDidTapOnImageView(_ cell: FollowersCell)
}

