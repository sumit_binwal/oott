//
//  BusinessGroupCell.swift
//  OOTTBusinessApp
//
//  Created by Bharat Kumar Pathak on 06/07/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit
import SDWebImage

let kCellIdentifier_business_group_cell = "businessGroupCell"

// MARK:-
// MARK:- BusinessGroupCell
class BusinessGroupCell: UITableViewCell {

    @IBOutlet var verifiedIcon: UIImageView!
    @IBOutlet weak var groupImageView: UIImageView!
    @IBOutlet weak var groupNameLabel: UILabel!
    @IBOutlet weak var groupDistanceLabel: UILabel!
    @IBOutlet weak var groupLineWaitLabel: UILabel!
    @IBOutlet weak var groupCrowdedLabel: UILabel!
    
    @IBOutlet weak var friendImageViewBottom: UIImageView!
    @IBOutlet weak var friendImageViewMiddle: UIImageView!
    @IBOutlet weak var friendImageViewTop: UIImageView!
    @IBOutlet weak var groupFriendsCount: UILabel!
    @IBOutlet weak var groupPeopleLikeCount: UILabel!
    
    @IBOutlet weak var buttonMakeMove: UIButton!
    @IBOutlet weak var buttonLikeUnlike: UIButton!
    
    @IBOutlet weak var labelOpenTill: UILabel!
    
    weak var cellDelegate: BusinessGroupCellDelegate?
    
    // MARK:- awakeFromNib
    override func awakeFromNib() {
        super.awakeFromNib()
        groupNameLabel.makeAdaptiveFont()
        groupDistanceLabel.makeAdaptiveFont()
        groupLineWaitLabel.makeAdaptiveFont()
        groupCrowdedLabel.makeAdaptiveFont()
        groupFriendsCount.makeAdaptiveFont()
        groupPeopleLikeCount.makeAdaptiveFont()
        self.groupImageView.makeRound()
        
        self.disbaleSelection()
    }

    // MARK:- setSelected
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // MARK:- layoutSubviews
    override func layoutSubviews() {
        
//        self.groupImageView.makeRound()
//        self.friendImageViewBottom.makeRound()
//        self.friendImageViewMiddle.makeRound()
//        self.friendImageViewTop.makeRound()
        
        let borderColor: UIColor = UIColor.init(RED: 11.0, GREEN: 32.0, BLUE: 61.0, ALPHA: 1.0)
        let borderWidth: CGFloat = 1.5
        
//        self.friendImageViewBottom.createBorder(withColor: borderColor, andBorderWidth: borderWidth)
//        self.friendImageViewMiddle.createBorder(withColor: borderColor, andBorderWidth: borderWidth)
//        self.friendImageViewTop.createBorder(withColor: borderColor, andBorderWidth: borderWidth)
    }

    // MARK:- Like Unlike Action
    @IBAction func onLikeAction(_ sender: UIButton) {
        
        guard let delegate = self.cellDelegate else {
            return
        }
        delegate.groupCell(cell: self, didTapOnLikeButton: sender)
    }
    
    // MARK:- Friends Count Action
    @IBAction func onFriendsAction(_ sender: UIButton) {
        
        guard let delegate = self.cellDelegate else {
            return
        }
        delegate.groupCell(cell: self, didTapOnFriendsButton: sender)
    }
    
    // MARK:- People Liked Action
    @IBAction func onPeopleLikedAction(_ sender: UIButton) {
        
        guard let delegate = self.cellDelegate else {
            return
        }
        delegate.groupCell(cell: self, didTapOnHeartButton: sender)
    }
    
    // MARK:- Make Move Action
    @IBAction func onMakeMovesAction(_ sender: UIButton) {
        
        guard let delegate = self.cellDelegate else {
            return
        }
        delegate.groupCell(cell: self, didTapOnMakeMovesButton: sender)
    }
    
    // MARK:-
    func updateFriendsImages(usingModel model : ModelGroupListing) {
        
        if model.friendsOnBusiness != nil
        {
            let totalIndex = model.friendsOnBusiness!.count > 3 ? 3 : model.friendsOnBusiness!.count
            
            if totalIndex == 0
            {
//                self.friendImageViewBottom.isHidden = true
//                self.friendImageViewTop.isHidden = true
//                self.friendImageViewMiddle.isHidden = true
                return
            }
            
            for index in 0...totalIndex-1
            {
                let dict = model.friendsOnBusiness![index] as! [String:String]
                let imageString = dict["picture"]
                
                switch index {
                case 0:
                    self.friendImageViewTop.sd_setImage(with: URL (string: imageString!), completed:
                        {[weak self] (image, error, cache, url) in
                            guard let `self` = self else {return}
                            if error != nil
                            {
                                self.friendImageViewTop.isHidden = true
                            }
                            else
                            {
                                self.friendImageViewTop.isHidden = false
                            }
                    })
                case 1:
                    self.friendImageViewMiddle.sd_setImage(with: URL (string: imageString!), completed:
                        {[weak self] (image, error, cache, url) in
                            guard let `self` = self else {return}
                            if error != nil
                            {
                                self.friendImageViewMiddle.isHidden = true
                            }
                            else
                            {
                                self.friendImageViewMiddle.isHidden = false
                            }
                    })
                case 2:
                    self.friendImageViewBottom.sd_setImage(with: URL (string: imageString!), completed:
                        {[weak self] (image, error, cache, url) in
                            guard let `self` = self else {return}
                            if error != nil
                            {
                                self.friendImageViewBottom.isHidden = true
                            }
                            else
                            {
                                self.friendImageViewBottom.isHidden = false
                            }
                    })
                default:
                    break
                }
                
            }
        }
        else
        {
            self.friendImageViewBottom.isHidden = true
            self.friendImageViewTop.isHidden = true
            self.friendImageViewMiddle.isHidden = true
        }
    }
    
    //MARK:- Update Cell Using Model
    func updateData(usingModel model:ModelGroupListing) -> Void
    {
        // Group name
        self.groupNameLabel.text = model.groupName
       
        // Profile picture
        if model.pictureString != nil
        {
            self.groupImageView.setIndicatorStyle(.white)
            self.groupImageView.setShowActivityIndicator(true)
            self.groupImageView.sd_setImage(with: URL (string: model.pictureString!), completed: nil)
        }
        
        self.groupCrowdedLabel.text = model.groupCrowd// model.getCrowdedString()
        self.groupLineWaitLabel.text = model.lineWaitTime// model.getLineWaitString()
        self.groupFriendsCount.text = UtilityClass.getFormattedNumberString(usingString: model.friendsCount!)
        self.groupDistanceLabel.text = model.distance?.appending("mi")
        self.groupPeopleLikeCount.text = UtilityClass.getFormattedNumberString(usingString: model.peopleLiked!)

        if model.isMakeMove! {
            self.buttonMakeMove.setTitle("CANCEL", for: .normal)
        }
        else
            
        {
         self.buttonMakeMove.setTitle("MAKE MOVES", for: .normal)
        }
        
        self.buttonMakeMove.alpha = model.isMakeMove! ? 0.7 : 1

        if model.isBusinessVerified!
    {
        self.verifiedIcon.isHidden = false
        }
        else
    {
        self.verifiedIcon.isHidden = true
        }
        
//        self.buttonMakeMove.isEnabled = !model.isMakeMove!
        
        //self.buttonMakeMove.titleLabel?.text = "Cancel"
        
//        UtilityClass.isBusinessExistInFavouriteArray(id: model.groupID!)
//        {[unowned self] (isExist, index) in
//            if isExist
//            {
//                self.buttonLikeUnlike.setImage(#imageLiteral(resourceName: "liked"), for: .normal)
//            }
//            else
//            {
//                self.buttonLikeUnlike.setImage(#imageLiteral(resourceName: "like"), for: .normal)
//            }
//        }
        
        if model.isFavourite!
        {
            self.buttonLikeUnlike.setImage(#imageLiteral(resourceName: "liked"), for: .normal)
        }
        else
        {
            self.buttonLikeUnlike.setImage(#imageLiteral(resourceName: "like"), for: .normal)
        }
        
        self.labelOpenTill.text = model.openTillToday
        
//        updateFriendsImages(usingModel: model)
    }
}

//MARK:-
//MARK:- Business Group Cell Protocol
protocol BusinessGroupCellDelegate: class {
    
    func groupCell(cell : BusinessGroupCell, didTapOnLikeButton likeButton: UIButton)
    func groupCell(cell : BusinessGroupCell, didTapOnFriendsButton friendsButton: UIButton)
    func groupCell(cell : BusinessGroupCell, didTapOnHeartButton heartButton: UIButton)
    func groupCell(cell : BusinessGroupCell, didTapOnMakeMovesButton makeMoveButton: UIButton)
}
