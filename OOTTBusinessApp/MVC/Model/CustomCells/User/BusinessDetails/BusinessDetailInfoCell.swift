//
//  BusinessDetailInfoCell.swift
//  OOTTBusinessApp
//
//  Created by Santosh on 19/02/18.
//  Copyright © 2018 KonstantInfoSolutions. All rights reserved.
//

import UIKit

let kCellIdentifier_BusinessDetail_InfoCell = "businessDetailInfoCell"
class BusinessDetailInfoCell: UITableViewCell {

    @IBOutlet weak var businessGeneralInfo: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateCell(usingModel model: ModelGroupDetail) {
        businessGeneralInfo.text = model.groupGeneralInfo
    }
}
