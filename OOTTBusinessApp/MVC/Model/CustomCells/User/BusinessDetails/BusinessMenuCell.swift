//
//  BusinessMenuCell.swift
//  OOTTBusinessApp
//
//  Created by Bharat Kumar Pathak on 14/07/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit

let kCellIdentifier_business_menu_cell = "businessMenuCell"

class BusinessMenuCell: UITableViewCell {

    @IBOutlet weak var menuBorderView: UIView!
    @IBOutlet weak var menuNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.disbaleSelection()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func layoutSubviews() {
        
        self.menuBorderView.layer.cornerRadius = CGFloat(2.5)
        self.menuBorderView.layer.borderColor = UIColor.white.cgColor
        self.menuBorderView.layer.borderWidth = CGFloat(0.5)
    }
    
    func updateCell(usingModel model : GroupImages)
    {
        if model.imageCaption != nil
        {
            self.menuNameLabel.text = model.imageCaption
        }
    }
    
}
