//
//  BusinessFriendsLikesCell.swift
//  OOTTBusinessApp
//
//  Created by Bharat Kumar Pathak on 14/07/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit

let kCellIdentifier_business_friend_likes_cell = "businessFriendsLikesCell"
let kCellIdentifier_business_likes_cell = "businessLikesCell"

protocol BusinessFriendsLikesCellDelegate :  class {
    func businessLikeCell(_ cell:BusinessFriendsLikesCell, didTapOnFriendsHereButton friendsHereButton : UIButton)
    
    func businessLikeCell(_ cell:BusinessFriendsLikesCell, didTapOnLikesButton likesButton : UIButton)
    
    func businessLikeCell(_ cell:BusinessFriendsLikesCell, didTapOnVIPButton vipButton : UIButton)
    
    func businessLikeCell(_ cell:BusinessFriendsLikesCell, didTapOnLikeUnlikeButton likeUnlikeButton : UIButton)
}

class BusinessFriendsLikesCell: UITableViewCell {

    
    weak var delegate : BusinessFriendsLikesCellDelegate?
    
    @IBOutlet weak var friendImageViewBottom: UIImageView!
    @IBOutlet weak var friendImageViewMiddle: UIImageView!
    @IBOutlet weak var friendImageViewTop: UIImageView!
    
    
    @IBOutlet weak var labelFriendsHereTotal: UILabel!
    @IBOutlet weak var buttonLikeUnlike: UIButton!
    @IBAction func onButtonLikeUnlikeAction(_ sender: Any) {
        guard let newDelegate = delegate else {
            return
        }
        newDelegate.businessLikeCell(self, didTapOnLikeUnlikeButton: sender as! UIButton)
    }
    
    @IBAction func onFriendsHereAction(_ sender: UIButton) {
        guard let newDelegate = delegate else {
            return
        }
        newDelegate.businessLikeCell(self, didTapOnFriendsHereButton: sender)
    }
    
    
    @IBOutlet weak var labelPeopleLikedCount: UILabel!
    
    @IBAction func onPeopleLikedAction(_ sender: UIButton) {
        guard let newDelegate = delegate else {
            return
        }
        newDelegate.businessLikeCell(self, didTapOnLikesButton: sender)
    }
    
    
    @IBOutlet weak var labelVIPCount: UILabel!
    
    @IBAction func onVIPAction(_ sender: UIButton) {
        guard let newDelegate = delegate else {
            return
        }
        newDelegate.businessLikeCell(self, didTapOnVIPButton: sender)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.disbaleSelection()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // MARK:- layoutSubviews
    override func layoutSubviews() {
        
//        self.friendImageViewBottom.makeRound()
//        self.friendImageViewMiddle.makeRound()
//        self.friendImageViewTop.makeRound()
//
//        let borderColor: UIColor = UIColor.init(RED: 11.0, GREEN: 32.0, BLUE: 61.0, ALPHA: 1.0)
//        let borderWidth: CGFloat = 1.5
//
//        self.friendImageViewBottom.createBorder(withColor: borderColor, andBorderWidth: borderWidth)
//        self.friendImageViewMiddle.createBorder(withColor: borderColor, andBorderWidth: borderWidth)
//        self.friendImageViewTop.createBorder(withColor: borderColor, andBorderWidth: borderWidth)
    }

    // MARK:-
    func updateCell(usingModel model : ModelGroupDetail)
    {
        if labelFriendsHereTotal != nil {
            labelFriendsHereTotal.text = UtilityClass.getFormattedNumberString(usingString: model.friendsCount)
            labelFriendsHereTotal.textColor = color_pink
        }

        labelPeopleLikedCount.text = UtilityClass.getFormattedNumberString(usingString: model.peopleLiked)
        labelVIPCount.text = UtilityClass.getFormattedNumberString(usingString: model.upcomingEventCount)
        
        self.buttonLikeUnlike.isUserInteractionEnabled = true
        if model.isFavourite
        {
            self.buttonLikeUnlike.setImage(#imageLiteral(resourceName: "liked"), for: .normal)
        }
        else
        {
            self.buttonLikeUnlike.setImage(#imageLiteral(resourceName: "like"), for: .normal)
        }
        if UtilityClass.getUserTypeData() == AppUserTypeEnum.provider.rawValue || UtilityClass.getUserTypeData() == AppUserTypeEnum.employee.rawValue{
            self.buttonLikeUnlike.setImage(#imageLiteral(resourceName: "liked"), for: .normal)
            self.buttonLikeUnlike.isUserInteractionEnabled = false
        }
    }
}
