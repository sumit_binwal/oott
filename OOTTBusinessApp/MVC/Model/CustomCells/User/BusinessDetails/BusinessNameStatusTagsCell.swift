//
//  BusinessNameStatusTagsCell.swift
//  OOTTBusinessApp
//
//  Created by Bharat Kumar Pathak on 14/07/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit

let kCellIdentifier_business_name_status_tags_cell = "businessNameStatusTagsCell"


protocol BusinessNameStatusTagsCellDelegate : class {
    func businessStatusTagCell(_ cell:BusinessNameStatusTagsCell, didTapOnOpenTimingButton openTimeButton:UIButton)
    
    func businessStatusTagCell(_ cell:BusinessNameStatusTagsCell, didTapOnFavoriteButton favoriteButton:UIButton)
}

class BusinessNameStatusTagsCell: UITableViewCell {

    @IBOutlet weak var businessNameLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var lineWaitLabel: UILabel!
    @IBOutlet var verifiedBusinessIcon: UIImageView!
    @IBOutlet weak var crowdedLabel: UILabel!
    @IBOutlet weak var openTillLabel: UILabel!
    @IBOutlet weak var likeUnlikeButton: UIButton!
    @IBOutlet weak var openTillButton: UIButton!
    
    @IBOutlet weak var businessStatusLabel: UILabel!
    @IBOutlet weak var businessTagsLabel: UILabel!
    
    weak var delegate : BusinessNameStatusTagsCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.disbaleSelection()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
        
    //MARK:- Update Cell Using Model
    func updateData(usingModel model:ModelGroupDetail) -> Void
    {
        self.businessNameLabel.text = model.groupName
        self.distanceLabel.text = model.distance.appending("mi")
        self.lineWaitLabel.text = model.lineWaitTime// model.getLineWaitString()
        self.crowdedLabel.text = model.groupCrowd// model.getCrowdedString()
        self.openTillLabel.text = model.openTillToday
        
        if !(model.groupStatus.isEmptyString()) {
            self.businessStatusLabel.text = model.groupStatus
            self.businessStatusLabel.isHidden = false
        } else {
            self.businessStatusLabel.isHidden = true
        }
        
        if !(model.groupTags.isEmptyString()) {
            self.businessTagsLabel.text = model.groupTags
            self.businessTagsLabel.isHidden = false
        } else {
            self.businessTagsLabel.isHidden = true
        }
        
        if model.isBusinessVerified
        {
            self.verifiedBusinessIcon.isHidden = false
        }
        else
        
        {
            self.verifiedBusinessIcon.isHidden = true
        }
        
//        if model.isFavourite
//        {
//            self.likeUnlikeButton.setImage(#imageLiteral(resourceName: "liked"), for: .normal)
//        }
//        else
//        {
//            self.likeUnlikeButton.setImage(#imageLiteral(resourceName: "like"), for: .normal)
//        }
//        if UtilityClass.getUserTypeData() == AppUserTypeEnum.provider.rawValue {
//            likeUnlikeButton.isHidden = true
//        }
    }

    @IBAction func onLikeUnlikeButtonAction(_ sender: UIButton) {
        guard let newDelegate = delegate else {
            return
        }
        newDelegate.businessStatusTagCell(self, didTapOnFavoriteButton: sender)
    }
    
    @IBAction func onOpenTillButtonAction(_ sender: UIButton) {
        guard let newDelegate = delegate else {
            return
        }
        newDelegate.businessStatusTagCell(self, didTapOnOpenTimingButton: sender)
    }
}


