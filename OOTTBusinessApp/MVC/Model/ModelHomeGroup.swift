//
//  ModelHomeGroup.swift
//  OOTTBusinessApp
//
//  Created by Santosh on 10/07/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import Foundation

//MARK:-
enum GroupFilterName : String {
    case distance = "dist"
    case lineWaitTime = "waitingTime"
    case numberOfFriends = "friendsOnBusinessCount"
    case numberOfPeople = "attendeeCount"
    case isClosedBar = "isclosed"
}

//MARK:-
enum PriorityType : String {
    case lowToHigh = "1"
    case highToLow = "-1"
    case random = "0"
}

//MARK:-
class ModelGroupFilter
{
    var longitude : String?
    var latitude : String?
    var locationName : String?
    var scene : String?
    var closedBar : Bool
    var sortType : GroupFilterName
    var priorityTypeValue : String?
    var sort : PriorityType
    var sliderValue : String?
    
    init() {
        self.sort = .lowToHigh
        self.sortType = .distance
        longitude = String (LocationManager.sharedInstance().newLongitude)
        latitude = String (LocationManager.sharedInstance().newLatitude)
        locationName = ""
        scene = ""
        closedBar = false
        priorityTypeValue = ""//"Low to High"
        sliderValue = "25"
    }
    
    deinit {
        print("ModelGroupFilter Deinit")
    }
    
    func getDictionary() -> [String:Any]
    {
        var params : [String:Any] = [String:Any]()
        
        if let finalLongitude = longitude
        {
            params["longitude"] = finalLongitude
        }
        if let finalLatitude = latitude
        {
            params["latitude"] = finalLatitude
        }
        if let finalscene = scene
        {
            params["scene"] = finalscene
        }
        params["sortType"] = sortType.rawValue
        
        switch sortType {
        case .distance:
            params["sort"] = sliderValue!
//        case .lineWaitTime:
//            params["sort"] = sliderValue!
            
        
        default:
            params["sort"] = sort.rawValue
        }
        
        params["isclosed"] = closedBar
        return params
    }
}

//MARK:-
class ModelGroupListing
{
    var groupID : String?
    var groupName : String?
    var lineWaitTime : String?
    var groupCrowd : String?
    var friendsCount : String?
    var peopleLiked : String?
    var distance : String?
    var pictureString : String?
    var coverPictureString : String?
    var isFavourite : Bool?
    var isMakeMove : Bool?
    var groupStatus : String?
    var totalOccupancy : String?
    var totalAttendee : String?
    var latitude : String?
    var longitude : String?
    var isBusinessVerified : Bool?
    
//    var groupOpenTill : String?
    var groupOpenTill : [String:String]?
    var openTillToday = "NA"
    var groupTags : [String]?
    
    var friendsOnBusiness : [[String:String]]?
    
    var isBusinessClosed = false
    
    init() {
        groupID = ""
        groupName = ""
        lineWaitTime = "0"
        groupCrowd = "0"
        friendsCount = "0"
        peopleLiked = "0"
        distance = "0"
        isFavourite = false
        isMakeMove = false
        groupStatus = ""
        totalOccupancy = "0"
        totalAttendee = "0"
        latitude = "0"
        longitude = "0"
        isBusinessVerified = false
//        groupOpenTill = ""
        groupTags = [String]()
    }
    
    deinit {
        print("ModelGroupListing deinit")
    }
    
    func getCrowdedString() -> String
    {
//        var attendeeCount = Float (totalAttendee!)!
//        var occupancyCount = Float (totalOccupancy!)!
//        if occupancyCount == 0
//        {
//            occupancyCount = 1
//            attendeeCount = 0
//        }
        let finalString = groupCrowd?.appending("% Crowded")
        return finalString!
    }
    
    func getLineWaitString() -> String
    {
        var finalString = UtilityClass.checkAndConvertMinutesToHour(usingString: lineWaitTime!).appending(" Line")
        finalString = finalString.localizedCapitalized
        return finalString
    }
    
    func updateModel(usingDictionary dictionary:[String:Any]) -> Void
    {
        if let id = dictionary["_id"] as? String
        {
            groupID = id
        }
        if let isVerified = dictionary["verified"] as? Bool
        {
            isBusinessVerified = isVerified
        }
        
        if let name = dictionary["name"] as? String
        {
            groupName = name
        }
        if let picture = dictionary["picture"] as? String
        {
            pictureString = picture
        }
        if let coverPicture = dictionary["coverPicture"] as? String
        {
            coverPictureString = coverPicture
        }
        if let waitingTime = dictionary["waitingTime"] as? Int
        {
            switch waitingTime {
            case 1:
                lineWaitTime = "No line"
                break
            case 2:
                lineWaitTime = "Less than 10 min."
                break
            case 3:
                lineWaitTime = "20 min."
                break
            case 4:
                lineWaitTime = "30 min. or more"
                break
            default:
                lineWaitTime = ""
                break
            }
//            lineWaitTime = String(waitingTime)
        }
        
        if let crowded = dictionary["crowded"] as? Int
        {
            switch crowded {
            case 1:
                groupCrowd = "Starting up"
                break
            case 2:
                groupCrowd = "Lively"
                break
            case 3:
                groupCrowd = "Crowded"
                break
            case 4:
                groupCrowd = "Packed"
                break
            default:
                groupCrowd = ""
                break
            }
//            groupCrowd = String(crowded)
        }
        if let friendsOnBusinessCount = dictionary["friendsOnBusinessCount"] as? Int
        {
            friendsCount = String(friendsOnBusinessCount)
        }
        if let likesCount = dictionary["likesCount"] as? Int
        {
            peopleLiked = String(likesCount)
        }
        if let occupancy = dictionary["occupancy"] as? Int
        {
            totalOccupancy = String(occupancy)
        }
        if let attendeeCount = dictionary["attended"] as? Int
        {
            totalAttendee = String(attendeeCount)
        }
        if let dist = dictionary["dist"] as? Double
        {
            distance = UtilityClass.getFormattedFloatString(dist, uptoDecimalLimit: 2) // Miles
        }
        if let longitde = dictionary["longitude"] as? Double
        {
            longitude = String(longitde)
        }
        if let latitde = dictionary["latitude"] as? Double
        {
            latitude = String(latitde)
        }
        if let isMoved = dictionary["isMoved"] as? Bool
        {
            isMakeMove = isMoved
        }
        if let liked = dictionary["isFavourite"] as? Bool
        {
            isFavourite = liked
        }
        if let open = dictionary["open"] as? [String:String]
        {
            groupOpenTill = open
        }
        
        if groupOpenTill != nil {
            let timeForToday = groupOpenTill![Date ().getStringForFormat(format: "EEEE")]!
            openTillToday = timeForToday.components(separatedBy: "-").last!
        }
        
        if let closedNow = dictionary["closedNow"] as? Bool
        {
            isBusinessClosed = closedNow
        }
        
        if isBusinessClosed == true {
            openTillToday = "Closed"
        }
//        if let openTill = dictionary["open"] as? String
//        {
//            groupOpenTill = openTill
//        }
        if let tags = dictionary["scene"] as? [String]
        {
            groupTags = tags
        }
        if let friendsBusiness = dictionary["friendsOnBusiness"] as? [[String:String]]
        {
            friendsOnBusiness = friendsBusiness
        }
        
//        if isFavourite!
//        {
//            UtilityClass.addToFavouriteArray(theID: groupID!)
//        }
    }
}


class GroupImages
{
    var imageID : String?
    var imageCaption : String?
    var imageURLString : String?
    
    func updateModel(usingDictionary dictionary : [String:String])
    {
        if let id = dictionary["_id"]
        {
            imageID = id
        }
        
        if let caption = dictionary["caption"]
        {
            imageCaption = caption
        }
        
        if let file_url = dictionary["file_url"]
        {
            imageURLString = file_url
        }
    }
}

class ModelGroupDetail
{
    var groupID : String = ""
    var groupName : String = ""
    var lineWaitTime : String = "0"
    var groupCrowd : String = "0"
    var friendsCount : String = "0"
    var peopleLiked : String = "0"
    var vipCount : String = "0"
    var distance : String = "0"
    var pictureString : String = ""
    var coverPictureString : String = ""
    var isFavourite : Bool = false
    var isMakeMove : Bool = false
    var groupStatus : String = ""
    var totalOccupancy : String = "0"
    var totalAttendee : String = "0"
    var isBusinessVerified : Bool = false

    var groupLocation : String = ""
    var groupLatitude : String = "0.0"
    var groupLongitude : String = "0.0"
    var groupWebsite: String = ""
    
    var groupGeneralInfo : String = ""
    var groupAbout : String = ""
    
    var groupDressCode : String = ""
    var groupCharges : [String:String]?
    
    var groupContact : String = ""
    var groupCountryCode : String = ""
    var groupCallingNumber : String = ""
    var groupEmail : String = ""
    
    var groupOpenTill : [String:String]?
    var groupTags : String = ""
    
    var groupMenuImages = [GroupImages]()
    var groupBusinessImages = [GroupImages]()
    
    var openTillToday = "Closed"
    var timeForToday = "Closed"
    
    var coverChargeForToday = "Closed"
    
    var upcomingEventCount = "0"
    
    var radius = "Medium"
    
    var isBusinessClosed = false
    
    func updateModel(usingDictionary dictionary : [String:Any])
    {
        if let id = dictionary["busines_id"] as? String
        {
            groupID = id
        }
        
        if let isVerified = dictionary["verified"] as? Bool
        {
            isBusinessVerified = isVerified
        }
        
        if let name = dictionary["name"] as? String
        {
            groupName = name
        }
        
        if let website = dictionary["website"] as? String
        {
            groupWebsite = website
        }
        
        if let rad = dictionary["radius"] as? String
        {
            radius = rad
        }
        
        if let isMoved = dictionary["isMoved"] as? Bool
        {
            isMakeMove = isMoved
        }
        
        if let friendsOnBusinessCount = dictionary["friendsOnBusinessCount"] as? Int
        {
            friendsCount = String(friendsOnBusinessCount)
        }
        if let likesCount = dictionary["likesCount"] as? Int
        {
            peopleLiked = String(likesCount)
        }
        
        if let vipCo = dictionary["vipCount"] as? Int
        {
            vipCount = String(vipCo)
        }
        
        if let coverPicture = dictionary["coverPicture"] as? String
        {
            coverPictureString = coverPicture
        }
        
        if let picture = dictionary["picture"] as? String
        {
            pictureString = picture
        }
        
        if let status = dictionary["status"] as? String
        {
            groupStatus = status
        }
        
        if let dist = dictionary["dist"] as? Double
        {
            distance = UtilityClass.getFormattedFloatString(dist, uptoDecimalLimit: 2)
        }
        
        if let waitingTime = dictionary["waitingTime"] as? Int
        {
            switch waitingTime {
            case 1:
                lineWaitTime = "No line"
                break
            case 2:
                lineWaitTime = "Less than 10 min."
                break
            case 3:
                lineWaitTime = "20 min."
                break
            case 4:
                lineWaitTime = "30 min. or more"
                break
            default:
                lineWaitTime = ""
                break
            }
//            lineWaitTime = String(waitingTime)
        }
        
        if let crowded = dictionary["crowded"] as? Int
        {
            switch crowded {
            case 1:
                groupCrowd = "Starting up"
                break
            case 2:
                groupCrowd = "Lively"
                break
            case 3:
                groupCrowd = "Crowded"
                break
            case 4:
                groupCrowd = "Packed"
                break
            default:
                groupCrowd = ""
                break
            }
//            groupCrowd = String(crowded)
        }
        
        if let attended = dictionary["attended"] as? Int
        {
            totalAttendee = String(attended)
        }
        
        if let location = dictionary["location"] as? String
        {
            groupLocation = location
        }
        
        if let latitude = dictionary["latitude"] as? Double
        {
            groupLatitude = String (latitude)
        }
        
        if let longitude = dictionary["longitude"] as? Double
        {
            groupLongitude = String (longitude)
        }
        
        if let info = dictionary["info"] as? String
        {
            groupGeneralInfo = info
        }
        
        if let about = dictionary["about"] as? String
        {
            groupAbout = about
        }
        
        if let dressCode = dictionary["dressCode"] as? String
        {
            groupDressCode = dressCode
        }
        
        if let charges = dictionary["charges"] as? [String:String]
        {
            groupCharges = charges
        }
        
        if let occupancy = dictionary["occupancy"] as? Int
        {
            totalOccupancy = String(occupancy)
        }
        
        if let contact = dictionary["contact"] as? String
        {
            groupContact = contact
        }
        
        if let countryCode = dictionary["countryCode"] as? String
        {
            groupCountryCode = countryCode
        }
        
        if let isLiked = dictionary["isLiked"] as? Bool
        {
            isFavourite = isLiked
        }
        
        if let email = dictionary["email"] as? String
        {
            groupEmail = email
        }
        
        if let menuImages = dictionary["menuImages"] as? [[String:String]]
        {
            if !menuImages.isEmpty
            {
                for dictionary in menuImages
                {
                    let imageData = GroupImages ()
                    imageData.updateModel(usingDictionary: dictionary)
                    groupMenuImages.append(imageData)
                }
            }
        }
        
        
        if let businessImages = dictionary["businessImages"] as? [[String:String]]
        {
            if !businessImages.isEmpty
            {
                for dictionary in businessImages
                {
                    let imageData = GroupImages ()
                    imageData.updateModel(usingDictionary: dictionary)
                    groupBusinessImages.append(imageData)
                }
            }
        }
        
        if let upcommingCount = dictionary["upcommingCount"] as? Int
        {
            upcomingEventCount = String(upcommingCount)
        }
        
        if let scene = dictionary["scene"] as? [String]
        {
            groupTags = getScenesTagString(fromArray: scene)
        }
        
        if let open = dictionary["open"] as? [String:String]
        {
            groupOpenTill = open
        }
        
        if groupOpenTill != nil {
            timeForToday = groupOpenTill![Date ().getStringForFormat(format: "EEEE")]!
            openTillToday = timeForToday.components(separatedBy: "-").last!
            
//            if timeForToday.isEmpty {
//                timeForToday = "Closed"
//                openTillToday = "Closed"
//            }
        }
        
        if let closedNow = dictionary["closedNow"] as? Bool
        {
            isBusinessClosed = closedNow
        }
        
        if isBusinessClosed == true {
            timeForToday = "Closed"
            openTillToday = "Closed"
        }
        
        if groupCharges != nil {
            coverChargeForToday = groupCharges![Date ().getStringForFormat(format: "EEEE")] ?? "Closed"
            if coverChargeForToday.isEmptyString() {
                coverChargeForToday = "Closed"
            }
        }
    }
    
    deinit {
        print("ModelGroupListing deinit")
    }
    
    func getCrowdedString() -> String
    {
//        var attendeeCount = Float (totalAttendee)!
//        var occupancyCount = Float (totalOccupancy)!
//        if occupancyCount == 0
//        {
//            occupancyCount = 1
//            attendeeCount = 0
//        }
        let finalString = groupCrowd.appending("% Crowded")
        return finalString
    }
    
    func getLineWaitString() -> String
    {
        var finalString = UtilityClass.checkAndConvertMinutesToHour(usingString: lineWaitTime).appending(" Line")
        finalString = finalString.localizedCapitalized
        return finalString
    }
    
    func getScenesTagString(fromArray:[String]) -> String
    {
        var finalString = ""
        
        var newArray = [String]()
        
        if !fromArray.isEmpty
        {
            for tag in fromArray
            {
                newArray.append("#"+tag.getTrimmedText())
            }
            
            finalString = newArray.joined(separator: " ")
        }
        
        return finalString
    }
}
