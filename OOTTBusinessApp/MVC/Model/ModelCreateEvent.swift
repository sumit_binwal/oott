//
//  ModelCreateEvent.swift
//  OOTTBusinessApp
//
//  Created by Sumit Sharma on 29/06/2017.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit

enum EventEnum : String
{
    case title = "title"
    case startTime = "start"
    case endTime = "end"
    case discription = "description"
    case none = "none"
}
class ModelCreateEvent
{
    var keyValue : String
    var keyName : String
        
    
    init(withKey : EventEnum.RawValue, value : Any)
    {
        self.keyName = withKey
        self.keyValue = value as! String
    }
    
    deinit {
        print("ModelLogin deinit")
    }
}
