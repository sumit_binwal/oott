//
//  ModelLogin.swift
//  OOTTBusinessApp
//
//  Created by Santosh on 19/06/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit

enum LoginEnum : String
{
    case email = "email"
    case password = "password"
    case fullname = "name"
    case userType = "userType"
    case mobileNo = "mobileNo"
    case countryCode = "countryCode"
    case otp = "otp"
    case username = "username"
    case dob = "birthdate"
    case socialId = "socialId"
    case sourceType = "sourceType"
    case confirmPassword = "confirmPassword"
    case employeeRole = "employeeRole"
    
    case none = "none"
}

class ModelLogin
{
    var keyValue : String
    var keyName : String
    
    
    
    enum ValidEnum {
        case valid
        case inValid
        case empty
    }
    var validUsername = ValidEnum.empty
    
    init(withKey : LoginEnum.RawValue, value : String)
    {
        self.keyName = withKey
        self.keyValue = value
    }
    
    deinit {
        print("ModelLogin deinit")
    }
}
