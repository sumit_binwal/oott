//
//  ModelFriends.swift
//  OOTTBusinessApp
//
//  Created by Bharat Kumar Pathak on 12/07/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import Foundation

//MARK:-
class ModelFriendsListing
{
    var friendID : String?
    var friendPicture: String?
    var friendName : String?
    var friendStatus : String?
    var friendStatusMakeMove : String?
    var friendStatusMakeMoveID : String?
    var commentsCount : String?
    var description : String?
    var updateStatusTime : String?
    var likesCount : String?
    var isInvited : Bool?
    var isFollowed : Bool?
    var isGhostMode : Bool?
    
    var mutualFriends : String?
    
    var isSameUser = false
    
    var checkInID : String?
    
    var isLiked = false
    
    var isAccountDeleted = false
    
    var isPrivateUser = false
    
    var isRequestedForFollow = false
    
    var userType: String?
    
    
    
    
    init() {
        friendID = ""
        friendPicture = ""
        friendName = ""
        description = ""
//        friendStatus = ""
        commentsCount = "0"
        likesCount = "0"
        isGhostMode = false
        isInvited = false
        isFollowed = false
        mutualFriends = "0"
        updateStatusTime = ""
//        checkInID = ""
    }
    
    deinit {
        print("ModelFriendsListing deinit")
    }
    
    func updateModel(usingDictionary dictionary:[String:Any]) -> Void
    {
        if let id = dictionary["_id"] as? String
        {
            friendID = id
        }
        if let ghostMode = dictionary["isGhostAccount"] as? Bool
        {
            isGhostMode = ghostMode
        }
        if let statusTime = dictionary["lastStatusUpdated"] as? String
        {
            updateStatusTime = statusTime
        }
        if let userTyp = dictionary["userType"] as? String
        {
            userType = userTyp
        }
        
        if let desc = dictionary["description"] as? String
        {
            description = desc
        }
        if let isArchive = dictionary["isArchive"] as? Bool
        {
            isAccountDeleted = isArchive
        }
        if let isPrivate = dictionary["isPrivateAccount"] as? Bool
        {
            isPrivateUser = isPrivate
        }
        if let isRequested = dictionary["isRequested"] as? Bool
        {
            isRequestedForFollow = isRequested
        }
        if let picture = dictionary["picture"] as? String
        {
            friendPicture = picture
        }
        if let name = dictionary["name"] as? String
        {
            friendName = name
        }
        if let isliked = dictionary["isLiked"] as? Bool
        {
            isLiked = isliked
        }
        if let friends = dictionary["mutualFriend"] as? Int
        {
            mutualFriends = String(friends)
        }
        if let currentMove = dictionary["currentMove"] as? [String:Any]
        {
            if let status = currentMove["status"] as? String
            {
                friendStatusMakeMove = status
            }
            
            if let _id = currentMove["_id"] as? String
            {
                checkInID = _id
            }
            
            if let comments = currentMove["checkinCommentCount"] as? Int
            {
                commentsCount = String(comments)
            }
            
            if let likes = currentMove["checkinLikeCount"] as? Int
            {
                likesCount = String(likes)
            }
        }
        if let lastCheckIn = dictionary["lastCheckin"] as? [String:Any]
        {
            if let id = lastCheckIn["_id"] as? String
            {
                checkInID = id
            }
            if let status = lastCheckIn["status"] as? String
            {
                friendStatus = status
            }
            if let comments = lastCheckIn["checkinCommentCount"] as? Int
            {
                commentsCount = String(comments)
            }
            if let likes = lastCheckIn["checkinLikeCount"] as? Int
            {
                likesCount = String(likes)
            }
        }
        if let isinvited = dictionary["isInvited"] as? Bool
        {
            isInvited = isinvited
        }
        if let isfollowed = dictionary["isFollowed"] as? Bool
        {
            isFollowed = isfollowed
        }
        
        if friendID == UtilityClass.getUserIDData()
        {
            isSameUser = true
        }
    }
    
    func getMutualFriendsString() -> String
    {
        if mutualFriends != nil
        {
            return mutualFriends!.appending(" Mutual Friends").uppercased()
        }
        return ""
    }
}

//MARK:-
class ModelCheckInComments
{
    var commentString = ""
    var commentID = ""
    var username = ""
    var userPicture = ""
    var timeString = ""
    var userID = ""
    var isLiked = false
    var likesCount = "0"
    
    
    
//    "commentBy": {
//    "_id": "598c1040f8332c2a47a3e21d",
//    "name": "mahip",
//    "picture": "http://192.168.0.131/oott/dev/public/uploads/users/RwRNT6kTmxyKhgL0DKR34XHl4kYcP8bF.jpeg",
//    "userType": "Customer",
//    "likesCount": 0,
//    "movesCount": 0,
//    "followersCount": 0,
//    "followingsCount": 0
//    },
//    "_id": "59a8fee45f16cc1d02f41662",
//    "updated": "2017-09-01T06:32:04.648Z",
//    "created": "01 Sep  2017, 12:02 PM",
//    "comment": "nice place grt drink"
    
    func updateModel(usingDictionary dictionary:[String:Any])
    {
        if let id = dictionary["_id"] as? String
        {
            self.commentID = id
        }
        if let comment = dictionary["comment"] as? String
        {
            self.commentString = comment
        }
        if let commentBy = dictionary["commentBy"] as? [String:Any]
        {
            if let id = commentBy["_id"] as? String
            {
                self.userID = id
            }
            if let name = commentBy["name"] as? String
            {
                self.username = name
            }
            if let picture = commentBy["picture"] as? String
            {
                self.userPicture = picture
            }
        }
        if let time = dictionary["created"] as? String
        {
            self.timeString = time
        }
        
        if let isLiked = dictionary["isLiked"] as? Bool
        {
            self.isLiked = isLiked
        }
        
        if let likesCount = dictionary["likesCount"] as? Int
        {
            self.likesCount = String(likesCount)
        }
    }
}
