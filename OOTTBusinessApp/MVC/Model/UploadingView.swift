//
//  UploadingView.swift
//  OOTTBusinessApp
//
//  Created by Santosh on 01/02/18.
//  Copyright © 2018 KonstantInfoSolutions. All rights reserved.
//

import UIKit

class UploadingView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    var loaderView: BRCircularProgressView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.isUserInteractionEnabled = true
        self.backgroundColor = UIColor (RED: 0, GREEN: 0, BLUE: 0, ALPHA: 0.5)
        
        addLoader()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.isUserInteractionEnabled = true
        self.backgroundColor = UIColor (RED: 0, GREEN: 0, BLUE: 0, ALPHA: 0.3)
        addLoader()
    }
    
    func addLoader() {
        loaderView = BRCircularProgressView (frame: CGRect (x: 0, y: 0, width: 80 * scaleFactorX, height: 80 * scaleFactorX))
        
        loaderView.progress = 0
        
        loaderView.center = self.center
        
        loaderView.setCircleStrokeWidth(4 * scaleFactorX)
        loaderView.setCircleStrokeColor(.black, circleFillColor: .clear, progressCircleStrokeColor: color_pink, progressCircleFillColor: .clear)
        
        addSubview(loaderView)
    }
    
    func updateLoader(withValue value: CGFloat) {
        loaderView.progress = value
    }
}
