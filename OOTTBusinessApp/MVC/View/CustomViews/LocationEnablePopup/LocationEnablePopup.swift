//
//  LocationEnablePopup.swift
//  OOTTBusinessApp
//
//  Created by Santosh on 11/12/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit

class LocationEnablePopup: UIView {

    
    @IBOutlet weak var labelLocationPopupDescription: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let xibView = Bundle.main.loadNibNamed("LocationEnablePopup", owner: self, options: nil)?[0] as! UIView
        xibView.tag = 5001
        xibView.frame = CGRect(x: 0, y: 0, width: frame.size.width, height: frame.size.height)
        self.addSubview(xibView)
        self.tag = 5001
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        let xibView = Bundle.main.loadNibNamed("LocationEnablePopup", owner: self, options: nil)?[0] as! UIView
        xibView.tag = 5001;
        self.addSubview(xibView)
        self.tag = 5001;
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.viewWithTag(5001)?.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height)
    }
    
    @IBAction func onGoToSettingsAction(_ sender: UIButton) {
        UIApplication.shared.open(URL (string: UIApplicationOpenSettingsURLString)!, options: [:], completionHandler: nil)
    }
}
