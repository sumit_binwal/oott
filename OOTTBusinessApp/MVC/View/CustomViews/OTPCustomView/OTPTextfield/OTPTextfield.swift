//
//  OTPTextfield.swift
//  OOTTUserApp
//
//  Created by Santosh on 07/06/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit

@objc protocol OTPTextFieldDelegate
{
    @objc optional func OTPTextFieldDidPressBackspace(textField : OTPTextfield) -> Void
}

class OTPTextfield: UITextField {
    
    weak var otpDelegate : OTPTextFieldDelegate?
    

    override func deleteBackward() {
        guard let delegate = otpDelegate else
        {
            return
        }
        delegate.OTPTextFieldDidPressBackspace!(textField: self)
    }

}
