//
//  OTPView.swift
//  OOTTUserApp
//
//  Created by Santosh on 07/06/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit

class OTPView: UIView, OTPTextFieldDelegate, UITextFieldDelegate {
    
    @IBOutlet var otpFields: [OTPTextfield]!
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        let xibView = Bundle.main.loadNibNamed("OTPView", owner: self, options: nil)?[0] as! UIView
        xibView.tag = 1001;
        self.addSubview(xibView)
        
        for inputField in otpFields {
            inputField.tag = otpFields.index(of: inputField)!
            inputField.otpDelegate = self
            inputField.delegate = self
            inputField.textColor = UIColor.white
            inputField.text = ""
            inputField.keyboardType = .numberPad
            inputField.superview?.isAccessibilityElement = true
            inputField.superview?.accessibilityLabel = String(inputField.tag+1)
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.viewWithTag(1001)?.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height)
    }
    
    func OTPTextFieldDidPressBackspace(textField: OTPTextfield)
    {
        var prevTextField : OTPTextfield?
        
        if textField.tag == 0
        {
            textField.text = ""
            textField.resignFirstResponder()
        }
        else
        {
            textField.text = ""
            prevTextField = otpFields[textField.tag - 1]
            prevTextField!.becomeFirstResponder()
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        let newString = string.trimmingCharacters(in: .whitespaces)
        
        if newString.isEmpty
        {
            return true
        }
        
        var nextTextField : OTPTextfield?
        
        if textField.tag == otpFields.count-1
        {
            textField.text = newString
            textField.resignFirstResponder()
        }
        else
        {
            textField.text = newString
            nextTextField = otpFields[textField.tag + 1]
            nextTextField!.becomeFirstResponder()
        }
        
        return false
    }
    
    func isOTPFieldsValid() -> Bool
    {
        for inputField in otpFields {
            if (inputField.text?.isEmpty)!
            {
                inputField.becomeFirstResponder()
                return false
            }
        }
        return true
    }
    
    func getOTPValue() -> String
    {
        var otpValue : String = ""
        
        for inputField in otpFields {
            otpValue = otpValue.appending(inputField.text!)
        }
        return otpValue
    }
}
