//
//  ChatViewController.swift
//  SampleScreens
//
//  Created by Bharat Kumar Pathak on 04/07/17.
//  Copyright © 2017 Konstant. All rights reserved.
//

import UIKit

class ChatViewController: UIViewController, PopoverMenuDelegate {

    var isPopoverMenuVisible: Bool = false
    var popoverMenu: PopoverMenu!
    
    var stringGroupStatus = "No Status"
    
    
    @IBOutlet var titleView: UIView!
    @IBOutlet weak var groupImageButton: UIBarButtonItem!
    @IBOutlet weak var backButton: UIBarButtonItem!
    @IBOutlet weak var moreButton: UIBarButtonItem!
    
    @IBOutlet weak var groupName: UILabel!
    @IBOutlet weak var groupStatus: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializePopoverMenu()
        updateViewLayouts()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- Action Events
    
    @IBAction func onBackAction(_ sender: UIBarButtonItem) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onGroupImageAction(_ sender: UIBarButtonItem) {
    }
    
    @IBAction func onPopoverMenuAction(_ sender: UIBarButtonItem) {
        
        updatePopoverMenuVisibility()
    }
    
    //MARK:- Custom Methods
    
    func initializePopoverMenu(){
        
        popoverMenu = PopoverMenu().instanceFromNib() as! PopoverMenu
        
        let scaleFactorX: CGFloat = (self.view.frame.size.width/375.0)
        let menuWidth = 112.0*scaleFactorX
        let menuXOrigin = self.view.frame.size.width-menuWidth-(13.5*scaleFactorX)
        popoverMenu.frame = CGRect(x: menuXOrigin, y: UIDevice.isIphoneX ? 70*scaleFactorY : 64, width: menuWidth, height: 0)

        popoverMenu.menuItems = ["Members", "Events", "Settings"]
        popoverMenu.menuDelegate = self
    }
    
    func updateViewLayouts(){
        
        self.navigationItem.titleView = self.titleView
        
        
        let leftBarButton = self.setLeftBarButtonItem(withImage: #imageLiteral(resourceName: "backButton"))
        leftBarButton.action = #selector(onBackButtonAction)
        leftBarButton.target = self
        
        
        let rightBarButton = self.setCreateImageViewBarButtonItem()
        let imageviewCustom = rightBarButton.customView as! UIImageView
        imageviewCustom.sd_setImage(with: URL (string: UtilityClass.getUserImageString()!), completed: nil)
        
        let rightBarButtonMore = self.setMoreMenuBarButtonItem(withImage: #imageLiteral(resourceName: "moreMenu"))
        rightBarButtonMore.action = #selector(onPopoverMenuAction)
        rightBarButtonMore.target = self

        self.navigationItem.rightBarButtonItems=[rightBarButtonMore,rightBarButton]
     
        groupName.text = UtilityClass.getUserNameData()
        groupStatus.text = stringGroupStatus
    }
    
    
    func onBackButtonAction() -> Void
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    func onMoreButtonAction() -> Void
    {
    }
    
    func onProfileImageButtonAction() -> Void
    {
    }
    
    func updatePopoverMenuVisibility(){
        
        if(isPopoverMenuVisible){
            popoverMenu.removeFromSuperview()
        } else {
            self.view.addSubview(popoverMenu)
        }
        isPopoverMenuVisible = !isPopoverMenuVisible
    }
    
    //MARK: - PopoverMenu Delegate Methods
    
    func didSelectedItemAt(index: Int) {
        
        updatePopoverMenuVisibility();
        print("selected \(index)")
        
        switch index {
        case 0:
            //        Attendees
            let attendee = self.storyboard?.instantiateViewController(withIdentifier: "attendeesVC") as! AttendeesViewController
            self.navigationController?.pushViewController(attendee, animated: true)
            break
        case 1:
            
            //Event Listing
            let eventListing = UIStoryboard.getEventListStoryboard().instantiateViewController(withIdentifier: "eventListVC") as! EventListingViewController

            self.navigationController?.pushViewController(eventListing, animated: true)

            break
        case 2 :
            //        businessSettingsVC
            let settingsVC = self.storyboard?.instantiateViewController(withIdentifier: "businessSettingsVC") as! BusinessSettingViewController
            self.navigationController?.pushViewController(settingsVC, animated: true)

            
            break
        default:
            break
        }
    }

}
