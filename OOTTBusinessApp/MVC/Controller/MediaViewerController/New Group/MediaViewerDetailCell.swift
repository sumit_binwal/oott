//
//  MediaViewerDetailCell.swift
//  OOTTBusinessApp
//
//  Created by Santosh on 01/02/18.
//  Copyright © 2018 KonstantInfoSolutions. All rights reserved.
//

import UIKit

let kCellIdentifier_MediaViewerDetailCellImage = "mediaViewerDetailCellImage"
let kCellIdentifier_MediaViewerDetailCellVideo = "mediaViewerDetailCellVideo"
class MediaViewerDetailCell: UICollectionViewCell {
    
    @IBOutlet weak var mediaImageZoomnigView: ZoomImageView!
    
    @IBOutlet weak var mediaVideoThumbnail: UIImageView!
    
    override func awakeFromNib() {
        
    }
    
    func updateCellContents(usingModel model: ModelMediaViewer) {
        if model.isImageMedia {
            DispatchQueue.global(qos: .background).async { [weak self] in
                guard let `self` = self else {return}
                let image = UIImage (data: try! Data (contentsOf: model.mediaLocalURL))
                
                DispatchQueue.main.async(execute: { [weak self] in
                     guard let `self` = self else {return}
                    self.mediaImageZoomnigView.image = image
                    self.mediaImageZoomnigView.zoomMode = .fit
                })
            }
        }
        else {
            DispatchQueue.global(qos: .background).async { [weak self] in
                guard let `self` = self else {return}
                let image = UtilityClass.getThumbnailFromVideo(model.mediaLocalURL)
                
                DispatchQueue.main.async(execute: { [weak self] in
                    guard let `self` = self else {return}
                    self.mediaVideoThumbnail.image = image
                    self.mediaVideoThumbnail.contentMode = .scaleAspectFit
                })
            }
        }
    }
}
