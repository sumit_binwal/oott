//
//  UserProfileViewController.swift
//  OOTTBusinessApp
//
//  Created by Bharat Kumar Pathak on 07/07/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit
import PKHUD

class UserProfileViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, ProfileNameImageCellDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, ProfileActionsCellDelegate, ProfileFollowersCellDelegate {

    @IBOutlet weak var tableUserProfile: UITableView!
    
    var dictionaryUserProfile = ModelUser()
    var targetUserModel: ModelFriendsListing? // model passed from other view controller
    var targetUserID: String?
    
    let actionsCellIndex = 1
    let descriptionCellIndex = 3
    let frequentVisitsCellIndex = 4
    
    var isLoggedInUser = true
    
    var refreshControl: UIRefreshControl!
    
    var frequentVisitIncrement = 1;
    @IBOutlet weak var contraintTableBottom: NSLayoutConstraint!
    
    // MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        
        applyRefreshControl()
        
//        if !isLoggedInUser, let userID = targetUserModel?.friendID {
//            
//            // other user
//            getUserProfileApi(forUserId: userID)
//            
//        } else if isLoggedInUser {
//            
//            // logged-in user
//            getUserProfileApi(forUserId: UtilityClass.getUserIDData()!)
//        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        showNavigationBar()
        
        if !isLoggedInUser, let userID = targetUserID {
            
//            // other user
//            SocketManager.sharedInstance().sendMessage(SocketModelSendMessage (userID, message: "how are you?"), messageHandler: { (messageDict, statusCode) in
//                
//            })
            getUserProfileApi(forUserId: userID)
            
        } else if isLoggedInUser {
            
            // logged-in user
            getUserProfileApi(forUserId: UtilityClass.getUserIDData()!)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    deinit {
        print("UserProfileViewController deinit")
    }
    
    //MARK:- Initial Setup View
    
    func setupView() -> Void
    {
        self.navigationItem.title = "Profile".uppercased()
        
        
        if isLoggedInUser {
            
            let settingsButton = self.setRightBarButtonItem(withImage: #imageLiteral(resourceName: "settingIcon"))
            settingsButton.action = #selector(onSettingsButtonAction)
            settingsButton.target = self
            
        } else {
            contraintTableBottom.constant = 0
            let backBarButton = self.setLeftBarButtonItem(withImage: #imageLiteral(resourceName: "backButton"))
            backBarButton.action = #selector(onBackButtonAction)
            backBarButton.target = self
            
            if UtilityClass.getUserTypeData() == AppUserTypeEnum.user.rawValue {
                let chatButton = self.setRightBarButtonItem(withImage: #imageLiteral(resourceName: "groupChatBlue"))
                chatButton.action = #selector(onChatButtonAction)
                chatButton.target = self
            }
        }
        
        tableUserProfile.isHidden = true
    }
    
    func setupTableView()  {
        tableUserProfile.delegate = nil
        tableUserProfile.dataSource = nil
        tableUserProfile.delegate = self
        tableUserProfile.dataSource = self
    }
    
    // MARK:- Refresh Control
    func applyRefreshControl() {
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.refreshPage), for: UIControlEvents.valueChanged)
        self.tableUserProfile.addSubview(refreshControl)
    }
    
    func refreshPage(sender:AnyObject) {
        if !isLoggedInUser, let userID = targetUserID {
            
            // other user
            getUserProfileApi(forUserId: userID)
            
        } else if isLoggedInUser {
            
            // logged-in user
            getUserProfileApi(forUserId: UtilityClass.getUserIDData()!)
        }
    }
    
    //MARK:- Settings button action
    
    func onSettingsButtonAction() -> Void {
        let settingsViewController = UIStoryboard.getUserSettingsStoryboard().instantiateInitialViewController() as! UserSettingsViewController
        settingsViewController.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(settingsViewController, animated: true)
    }
    
    func onBackButtonAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Chat Button Action
    func onChatButtonAction() {
        print("chat button tapped")
        
        let chatViewController = UIStoryboard.getChatViewStoryboard().instantiateInitialViewController() as! GroupChatViewController
        
        chatViewController.hidesBottomBarWhenPushed = true
        
        chatViewController.chatUsername.text = dictionaryUserProfile.userName!
        
        chatViewController.chatUserID = dictionaryUserProfile.userID!
        
        chatViewController.isUserBlocked = dictionaryUserProfile.isBlocked
        
        self.navigationController?.pushViewController(chatViewController, animated: true)
    }

    // MARK:- UITableView Delegate and Datasource Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isLoggedInUser {
            return 5
        }
        if dictionaryUserProfile.isPrivateUser, !dictionaryUserProfile.isFollowed! {
            return 3
        }
        return 5
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == frequentVisitsCellIndex {
        
            if (dictionaryUserProfile.frequentVisits?.isEmpty)! {
                return 0
            }
        }
        
        if isLoggedInUser, indexPath.row == actionsCellIndex
        {
            return 0
        }
        if UtilityClass.getUserTypeData() != AppUserTypeEnum.user.rawValue, indexPath.row == actionsCellIndex {
            return 0
        }
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 72 * scaleFactorX
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            
            let nameImageCell: ProfileNameImageCell = tableView.dequeueReusableCell(withIdentifier: kCellIdentifier_profile_image_cell) as! ProfileNameImageCell
            nameImageCell.tag = indexPath.row
            nameImageCell.cellDelegate = self
            nameImageCell.updateData(usingModel: dictionaryUserProfile)
            return nameImageCell
        
        }  else if indexPath.row == actionsCellIndex {
            
            let actionsCell: ProfileActionsCell = tableView.dequeueReusableCell(withIdentifier: kCellIdentifier_profile_actions_cell) as! ProfileActionsCell
            actionsCell.tag = indexPath.row
            actionsCell.cellDelegate = self
            actionsCell.updateData(usingModel: dictionaryUserProfile)
            return actionsCell
            
        } else if indexPath.row == 2 {
            
            let followersCell: ProfileFollowersCell = tableView.dequeueReusableCell(withIdentifier: kCellIdentifier_profile_followers_cell) as! ProfileFollowersCell
            followersCell.tag = indexPath.row
            followersCell.cellDelegate = self
            followersCell.updateData(usingModel: dictionaryUserProfile)
            return followersCell
            
        }  else if indexPath.row == descriptionCellIndex {
            
            let descriptionCell: ProfileDescriptionCell = tableView.dequeueReusableCell(withIdentifier: kCellIdentifier_profile_description_cell) as! ProfileDescriptionCell
            descriptionCell.tag = indexPath.row
            descriptionCell.updateCell(withModel: dictionaryUserProfile)
            return descriptionCell
            
        } else {
            
            let frequentVisitsCell: FrequentVisitedCell = tableView.dequeueReusableCell(withIdentifier: kCellIdentifier_frequent_visited_cell) as! FrequentVisitedCell
            frequentVisitsCell.tag = indexPath.row
            frequentVisitsCell.frequentVisitsCollectionView.delegate = self
            frequentVisitsCell.frequentVisitsCollectionView.dataSource = self
            frequentVisitsCell.frequentVisitsCollectionView.reloadData()
            return frequentVisitsCell
        }
    }
    
    // MARK:- ProfileNameImageCell Delegate Methods
    
    func profileNameImageCell(cell: ProfileNameImageCell, didTapOnUserImageButton userImageButton: UIButton) {
        
    }
    
    func profileNameImageCellDidTapOnLocationText(cell: ProfileNameImageCell) {
        print("location tap")
        
        let commentListViewController = UIStoryboard.getUserFriendsStoryboard().instantiateViewController(withIdentifier: "commentsListingViewController") as! CommentsListingViewController
        commentListViewController.checkInID = dictionaryUserProfile.userStatusID
        commentListViewController.hidesBottomBarWhenPushed = true
        commentListViewController.isCheckingOwnProfile = true
        commentListViewController.likeCount = Int(dictionaryUserProfile.likesCount!)!
        self.navigationController?.pushViewController(commentListViewController, animated: true)
    }
    
    // MARK:- ProfileActionCell Delegate Methods
    
    func profileActionCell(cell: ProfileActionsCell, didTapOnInviteToGroupButton inviteButton: UIButton) {
        let myGroupListNav : UINavigationController = UIStoryboard.getUserHomeStoryboard().instantiateViewController(withIdentifier: "myGroupListNav") as! UINavigationController
        
        let mygroupListVC = myGroupListNav.viewControllers.first as! MyGroupListViewController
        
        mygroupListVC.isMakeMoveInvitation = false
        
        mygroupListVC.userID = dictionaryUserProfile.userID!
        mygroupListVC.businessName = dictionaryUserProfile.userName
        
        self.navigationController?.present(myGroupListNav, animated: true, completion: nil)
    }
    
    func profileActionCell(cell: ProfileActionsCell, didTapOnFollowUnfollowButton followUnfollowButton: UIButton) {
        
        if dictionaryUserProfile.isFollowed! {
        
            performUnfollowUserApi(forUserID: dictionaryUserProfile.userID!, actionsCell: cell)
        } else {
        
            performFollowUserApi(forUserID: dictionaryUserProfile.userID!, actionsCell: cell)
        }
    }
    
    // MARK:- ProfileFollowersCellDelegate -> Fav Button
    func profileFollowerCell(_ cell: ProfileFollowersCell, didTapOnVIPButton vipButton: UIButton) {
        let myGroupListVC : MyGroupListViewController = UIStoryboard.getUserHomeStoryboard().instantiateViewController(withIdentifier: "myGroupListVc") as! MyGroupListViewController
        myGroupListVC.hidesBottomBarWhenPushed = true
        myGroupListVC.isViewFavGroupMode = true
        myGroupListVC.userID = dictionaryUserProfile.userID!
        
        self.navigationController?.pushViewController(myGroupListVC, animated: true)
    }
    
    // MARK:- ProfileFollowersCellDelegate -> Follower Button
    func profileFollowerCell(_ cell: ProfileFollowersCell, didTapOnFollowerButton followerButton: UIButton) {
        
        let followFollowingVC = UIStoryboard.getUserHomeStoryboard().instantiateViewController(withIdentifier: "groupLikesViewController") as! GroupLikesViewController
        followFollowingVC.viewType = .followersList
        
        followFollowingVC.hidesBottomBarWhenPushed = true
        
        followFollowingVC.userID = dictionaryUserProfile.userID!
        
        self.navigationController?.pushViewController(followFollowingVC, animated: true)
    }
    
    // MARK:- ProfileFollowersCellDelegate -> Following Button
    func profileFollowerCell(_ cell: ProfileFollowersCell, didTapOnFollowingButton followingButton: UIButton) {
        
        let followFollowingVC = UIStoryboard.getUserHomeStoryboard().instantiateViewController(withIdentifier: "groupLikesViewController") as! GroupLikesViewController
        followFollowingVC.viewType = .followingList
        
        followFollowingVC.hidesBottomBarWhenPushed = true
        
        followFollowingVC.userID = dictionaryUserProfile.userID!
        
        self.navigationController?.pushViewController(followFollowingVC, animated: true)
    }
    
    // MARK:- UICollectionView Delegate and Datasource Methods
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if let frequentVisitsCount = dictionaryUserProfile.frequentVisits?.count
        {
            return frequentVisitsCount > 3*frequentVisitIncrement ? 3*frequentVisitIncrement+1 : frequentVisitsCount
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10 * scaleFactorX
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 90*scaleFactorX, height: 145*scaleFactorX)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView .dequeueReusableCell(withReuseIdentifier: kCellIdentifier_frequent_collection_cell, for: indexPath) as! FrequentCollectionCell
        
        cell.contentView.frame = cell.bounds
        cell.contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        cell.tag = indexPath.item
        cell.updateLayouts(withIncrementCount: frequentVisitIncrement)
        
//        cell.updateCellData(usingModel: dictionaryUserProfile.frequentVisits![indexPath.row])
        cell.updateData(usingArray: dictionaryUserProfile.frequentVisits!)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        
        guard let cell = collectionView.cellForItem(at: indexPath) as? FrequentCollectionCell else {
            return
        }

        if cell.isShowMoreCell {
//            cell.incrementCount += 1
            frequentVisitIncrement += 1
            collectionView.reloadData()
            return
        }
        
        let arrayBusiness = dictionaryUserProfile.frequentVisits![indexPath.row]
        let businessID = arrayBusiness.businessID
        
        let detailsViewController = UIStoryboard.getBusinessDetailsStoryboard().instantiateViewController(withIdentifier: "businessDetailsViewController") as! BusinessDetailsViewController
        detailsViewController.businessId = businessID!
        detailsViewController.isFromDetail = true
        detailsViewController.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(detailsViewController, animated: true)
    }
    
    
    //MARK:- Get User Profile Api
    
    func getUserProfileApi(forUserId userID:String) -> Void
    {
        HUD.show(.systemActivity, onView: self.view.window)
        
        let urlToHit = EndPoints.getUserProfileApi(UtilityClass.getUserSidData()!, userID).path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .get, parameters: nil)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else { return }
            
            self.refreshControl.endRefreshing()
            
            if error != nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: error?.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            
            let responseDictionary = dictionary!
            let message = responseDictionary["message"] as! String
            let type = responseDictionary["type"] as! Bool
            
            if statusCode == 203
            {
                // show alert
                UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            if statusCode == 200
            {
                if type
                {
                    // success
                    let dataDictionary = responseDictionary["data"] as! [String:Any] // As dictionary
                    //                    if onRefresh {
                    //                        self.arrayListing.removeAll()
                    //                    }
                    self.updateModelDictionary(usingDictionary: dataDictionary)
                    return
                }
                else
                {
                    // alert
                    UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
            }
        }
    }
    
    //MARK:- Updating Model Dictionary
    func updateModelDictionary(usingDictionary dictionary : [String:Any]) -> Void
    {
        frequentVisitIncrement = 1
        let dataDictionary = dictionary
        let model = ModelUser() // Model creation
        model.updateModel(usingDictionary: dataDictionary) // Updating model
        dictionaryUserProfile = model // Adding model to dictionary
        
        // updating title
        self.navigationItem.title = dictionaryUserProfile.userName
        
        if dictionaryUserProfile.isAccountDeleted {
                UtilityClass.showAlertWithTitle(title: "Profile Deactivated", message: "User has deactivated the profile.", onViewController: self, withButtonArray: nil, dismissHandler: {[weak self] (buttonIndex) in
                    guard let `self` = self else {return}
                    self.onBackButtonAction()
                })
            return
        }
        
        UtilityClass.updateFullName(dictionaryUserProfile.userFullName!)
        
//        setupTableView()
        tableUserProfile.reloadData()
        tableUserProfile.isHidden = false
        
        if dictionaryUserProfile.isPrivateUser, !dictionaryUserProfile.isFollowed!, !isLoggedInUser {
            self.navigationItem.rightBarButtonItem?.isEnabled = false
        }
        else
        {
            self.navigationItem.rightBarButtonItem?.isEnabled = true
        }
    }
    
    //MARK:-
    func performFollowUserApi(forUserID userID:String, actionsCell:ProfileActionsCell) -> Void
    {
        UtilityClass.showNetworkActivityLoader(show: true)
        
        let urlToHit = EndPoints.followUser(UtilityClass.getUserSidData()!, userID).path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .get, parameters: nil)
        { [weak self] (data, dictionary, statusCode, error) in
            
            UtilityClass.showNetworkActivityLoader(show: false)
            
            guard let `self` = self else { return }
            
            actionsCell.followUnfollowButton.isUserInteractionEnabled = false
            
            if error != nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: error?.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            
            let responseDictionary = dictionary!
            let message = responseDictionary["message"] as! String
            let type = responseDictionary["type"] as! Bool
            
            if statusCode == 203
            {
                // show alert
                UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            if statusCode == 200
            {
                if type
                {
                    // success
                    actionsCell.followUnfollowButton.isUserInteractionEnabled = true
                    
                    self.refreshPage(sender: UIButton ())
                    
//                    let index = actionsCell.tag
//                    self.dictionaryUserProfile.isFollowed = true
//                    self.tableUserProfile.reloadData()
//
//                    if let objectIndex = arraySuggestedListing.index(where: { (model) -> Bool in
//                        return model.friendID == self.dictionaryUserProfile.userID
//                    })
//                    {
//                        arraySuggestedListing.remove(at: objectIndex)
//                    }
                    return
                }
                else
                {
                    // alert
                    UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
            }
        }
    }
    
    //MARK:-
    func performUnfollowUserApi(forUserID userID:String, actionsCell:ProfileActionsCell) -> Void
    {
        UtilityClass.showNetworkActivityLoader(show: true)
        
        let urlToHit = EndPoints.unfollowUser(UtilityClass.getUserSidData()!, userID).path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .get, parameters: nil)
        { [weak self] (data, dictionary, statusCode, error) in
            
            UtilityClass.showNetworkActivityLoader(show: false)
            
            guard let `self` = self else { return }
            
            actionsCell.followUnfollowButton.isUserInteractionEnabled = false
            
            if error != nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: error?.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            
            let responseDictionary = dictionary!
            let message = responseDictionary["message"] as! String
            let type = responseDictionary["type"] as! Bool
            
            if statusCode == 203
            {
                // show alert
                UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            if statusCode == 200
            {
                if type
                {
                    // success
                    actionsCell.followUnfollowButton.isUserInteractionEnabled = true
//                    let index = actionsCell.tag
                    self.dictionaryUserProfile.isFollowed = false
                    self.tableUserProfile.reloadData()
//                    self.tableUserProfile.reloadRows(at: [IndexPath (row: index, section: 0)], with: .none)
                    
                    if let objectIndex = arrayFollowingListing.index(where: { (model) -> Bool in
                        return model.friendID == self.dictionaryUserProfile.userID
                    })
                    {
                        arrayFollowingListing.remove(at: objectIndex)
                    }
                    if self.dictionaryUserProfile.isPrivateUser, !self.dictionaryUserProfile.isFollowed!, !self.isLoggedInUser {
                        self.navigationItem.rightBarButtonItem?.isEnabled = false
                    }
                    else
                    {
                        self.navigationItem.rightBarButtonItem?.isEnabled = true
                    }
                    return
                }
                else
                {
                    // alert
                    UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
            }
            
        }
    }
    
}
