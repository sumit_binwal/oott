//
//  UserEditProfile.swift
//  OOTTBusinessApp
//
//  Created by Sumit Sharma on 06/09/2017.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit
import PKHUD

private enum GenderTypeEnum : Int {
    case NotSpecified
    case Male
    case Female
    
    var title : String
    {
        switch self {
        case .Female:
            return "Female"
        case .Male:
            return "Male"
        default:
            return "Not Specified"
        }
    }
    
}

class EditProfileModel
{
    var userID = ""
    var username = ""
    var fullname = ""
    var picture = ""
    var email = ""
    var dob = ""
    var gender = ""
    var description = ""
    
    func updateModel(usingDictionary dictionary : [String:Any])
    {
        if let id = dictionary["_id"] as? String
        {
            self.userID = id
        }
        
        if let userN = dictionary["username"] as? String
        {
            self.username = userN
        }
        
        if let fullN = dictionary["name"] as? String
        {
            self.fullname = fullN
        }
        
        if let pictureString = dictionary["picture"] as? String
        {
            self.picture = pictureString
        }
        
        if let emailString = dictionary["email"] as? String
        {
            self.email = emailString
        }
        
        if let dobString = dictionary["birthdate"] as? String
        {
            self.dob = dobString
        }
        
        if let descString = dictionary["description"] as? String
        {
            self.description = descString
        }
        
        if let gender = dictionary["gender"] as? String
        {
            self.gender = gender
        }
    }
    
    func getDictionary() -> [String:Any]
    {
        return [
            "name" : self.fullname,
            "birthdate" : self.dob,
            "gender" : self.gender,
            "description" : self.description
        ]
    }
    
    deinit {
        print("EditProfileModel Deinit")
    }
}

class UserEditProfile: UIViewController,UITableViewDelegate,UITableViewDataSource, EditProfileCellDelegate
{
    
    @IBOutlet weak var tableEditProfile: UITableView!
    
    @IBOutlet var tableFooterView: UIView!
    @IBOutlet var buttonUpdateProfile: UIButton!
    
    var modelEditProfile = EditProfileModel ()
    
    var selectedGenderIndex = 0
    var arrayGenderList = ["Not Specified", "Female", "Male"]
    
    
   //MARK:- View Controller Lyf Cycle Method
    override func viewDidLoad()
    {
        super.viewDidLoad()
        setupView()
        fetchUserDetails()
        setupTableview()
    }
    
    deinit {
        print("UserEditProfile Deinit")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    func setupView()
    {
        self.title = "edit profile".uppercased()
        
        let backBarButton = self.setLeftBarButtonItem(withImage: #imageLiteral(resourceName: "backButton"))
        backBarButton.action = #selector(onBackButtonAction)
        backBarButton.target = self
    }
    
    func onBackButtonAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setupTableview()
    {
        tableEditProfile.delegate = self
        tableEditProfile.dataSource = self
        tableFooterView.frame = CGRect (x: 0, y: 0, width: tableEditProfile.frame.size.width * scaleFactorX, height: tableFooterView.frame.size.height * scaleFactorX)
//        buttonUpdateProfile.center = CGPoint (x: tableEditProfile.frame.size.width/2, y: buttonUpdateProfile.frame.size.height/2)
        tableEditProfile.tableFooterView = tableFooterView
    }
    
    func fetchUserDetails()
    {
        modelEditProfile.updateModel(usingDictionary: UtilityClass.getUserInfoData())
        selectedGenderIndex = arrayGenderList.index(of: modelEditProfile.gender)!
    }
    
    //MARK:- Table View Delegate Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 5;
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0
        {
            return 185 * scaleFactorX
        }
        if indexPath.row == 4
        {
            return 134 * scaleFactorX
        }
        return 64 * scaleFactorX
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        var cellIdentifier = kCellIdentifier_edit_profile_userDetail
        
        var cellType = EditProfileCellType.userDetail
        
        
        switch indexPath.row
        {
        case 0:
            cellIdentifier = kCellIdentifier_edit_profile_userDetail
            cellType = EditProfileCellType.userDetail
            break
        case 1:
            cellIdentifier = kCellIdentifier_edit_profile_Input
            cellType = EditProfileCellType.name
            break
        case 2:
            cellIdentifier = kCellIdentifier_edit_profile_Input
            cellType = EditProfileCellType.dob
            break
        case 3:
            cellIdentifier = kCellIdentifier_edit_profile_Input
            cellType = EditProfileCellType.gender
            break
        case 4:
            cellIdentifier = kCellIdentifier_edit_profile_Description
            cellType = EditProfileCellType.description
            break
        default:
            break
        }
        let cell : EditProfileCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! EditProfileCell
        cell.cellType = cellType
        cell.editProfileDelegate = self
        cell.updateCell(usingModel: modelEditProfile, forIndex: indexPath.row)
        return cell
    }
    
    @IBAction func onUpdateProfileAction(_ sender: UIButton) {
        
        if validateFields()
        {
            performEditProfileApi()
        }
    }
    
    //MARK:- EditProfileCellDelegate -> Inputfield Updated
    func editProfileCell(_ cell: EditProfileCell, didUpdatedWithValue updatedValue: String) {
        print("*****",updatedValue)
        switch cell.cellType {
        case .description:
            modelEditProfile.description = updatedValue
        case .name:
            modelEditProfile.fullname = updatedValue
        case .dob:
            
            let datePicker = cell.inputField.inputView! as! UIDatePicker
            
            if (cell.inputField.text?.isEmpty)!
            {
                cell.inputField.text = datePicker.date.getStringForFormat(format: "MM-dd-yyyy")
                modelEditProfile.dob = cell.inputField.text!
            }
            else
            {
                datePicker.date = Date.getDate(fromString: modelEditProfile.dob)
            }
            
            datePicker.addTarget(self, action: #selector(onDatePickerValueChanged(sender:)), for: .valueChanged)
            
            break
        case .gender:
            
            let pickerview = cell.inputField.inputView! as! UIPickerView
            pickerview.delegate = nil
            pickerview.delegate = self;
            if (cell.inputField.text?.isEmpty)!
            {
                cell.inputField.text = arrayGenderList[0]
                modelEditProfile.dob = cell.inputField.text!
                selectedGenderIndex = 0;
            }
            else
            {
                pickerview.selectRow(selectedGenderIndex, inComponent: 0, animated: true)
            }
            
            
            break
        default:
            break
        }
    }
    
    //MARK:- EditProfileCellDelegate -> Profile Image Tapped
    func editProfileCell(_ cell: EditProfileCell, didTapOnProfileImageButton profileImageButton: UIButton)
    {
        UtilityClass.showActionSheetWithTitle(title: "Add Profile Picture", message: "Select from options", onViewController: self, withButtonArray: ["Camera", "Photos"]) { (buttonIndex : Int) in
            if buttonIndex == 0
            {
                self.openPhotoPickerForType(imageSelectType: .camera)
            }
            else
            {
                self.openPhotoPickerForType(imageSelectType: .photos)
            }
        }
    }
    
    //MARK:- Show ImagePicker
    func openPhotoPickerForType(imageSelectType : ImageSelectType) -> Void
    {
        var isCamera = false
        
        if imageSelectType == ImageSelectType.camera
        {
            if !UIImagePickerController.isSourceTypeAvailable(.camera
                ) {
                UtilityClass.showAlertWithTitle(title: "This device does not have Camera. Try Photos!", message: nil, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            else
            {
                isCamera = true
            }
        }
        
        let imagePicker = UIImagePickerController.init()
        imagePicker.sourceType = isCamera ? .camera : .photoLibrary
        imagePicker.allowsEditing = true
        imagePicker.delegate = self
        
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func performEditProfileApi()
    {
        HUD.show(.systemActivity, onView: self.view.window)
        
        let params = modelEditProfile.getDictionary()
        
        let urlToHit = EndPoints.editProfile(UtilityClass.getUserSidData()!).path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .post, parameters: params)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else { return }
            
            if error != nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: error?.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            
            let responseDictionary = dictionary!
            let message = responseDictionary["message"] as! String
            let type = responseDictionary["type"] as! Bool
            
            if statusCode == 203
            {
                // show alert
                UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            if statusCode == 200
            {
                if type
                {
                    // success
                    var userDict = UtilityClass.getUserInfoData()
                    userDict["name"] = self.modelEditProfile.fullname
                    userDict["description"] = self.modelEditProfile.description
                    userDict["birthdate"] = self.modelEditProfile.dob
                    userDict["gender"] = self.modelEditProfile.gender
                    UtilityClass.saveUserInfoData(userDict: userDict)
                    
                    UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: { (buttonIndex) in
                        self.navigationController?.popViewController(animated: true)
                    })
                    
                    return
                }
                else
                {
                    // alert
                    UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
            }
        }
    }
    
    func uploadProfileImage(_ profileImage : UIImage)
    {
        HUD.show(.systemActivity, onView: self.view.window)
        
        let urlToHit = EndPoints.editProfile(UtilityClass.getUserSidData()!).path
        
        let imageData = UIImageJPEGRepresentation(profileImage, 0.7)
        
        
        AppWebHandler.sharedInstance().uploadImages(fromURL: urlToHit, imagesArray: [ImageDataDict (data: imageData!, name: "picture")], otherParameters: nil)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else { return }
            
            if error != nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: error?.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            
            let responseDictionary = dictionary!
            let message = responseDictionary["message"] as! String
            let type = responseDictionary["type"] as! Bool
            
            if statusCode == 203
            {
                // show alert
                UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            if statusCode == 200
            {
                if type
                {
                    // success
                    let dataDictionary = responseDictionary["data"] as! [String:Any]
                    
                    var userDict = UtilityClass.getUserInfoData()
                    userDict["picture"] = dataDictionary["picture"]
                    UtilityClass.saveUserInfoData(userDict: userDict)
                    
                    return
                }
                else
                {
                    // alert
                    UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
            }
            
        }
    }
}

//MARK:-
//MARK:- Extension
extension UserEditProfile : UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIPickerViewDelegate,UIPickerViewDataSource
{

    func onDatePickerValueChanged(sender : UIDatePicker) -> Void {
        modelEditProfile.dob = sender.date.getStringForFormat(format: "MM-dd-yyyy")
        let editCell = tableEditProfile.cellForRow(at: IndexPath (row: 2, section: 0)) as! EditProfileCell
        editCell.inputField.text = modelEditProfile.dob
    }
    
    func validateFields()->Bool
    {
        if modelEditProfile.fullname.isEmptyString()
        {
            UtilityClass.showAlertWithTitle(title: App_Name, message: "Please provide full name", onViewController: self, withButtonArray: nil, dismissHandler: nil)
            return false
        }
        return true
    }
    
    //MARK: UIImagePickerControllerDelegate
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let image = info[UIImagePickerControllerEditedImage] as? UIImage
        {
            let editCell = tableEditProfile.cellForRow(at: IndexPath (row: 0, section: 0)) as! EditProfileCell
            
            editCell.buttonProfileImage.setImage(image, for: .normal)
            
            uploadProfileImage(image)
        }
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    //MARK:
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrayGenderList.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrayGenderList[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        modelEditProfile.gender = arrayGenderList[row]
        let editCell = tableEditProfile.cellForRow(at: IndexPath (row: 3, section: 0)) as! EditProfileCell
        editCell.inputField.text = modelEditProfile.gender
        selectedGenderIndex = row
    }
}
