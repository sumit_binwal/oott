//
//  BlockedUsersListViewController.swift
//  OOTTBusinessApp
//
//  Created by Santosh on 10/11/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit
import PKHUD
import DZNEmptyDataSet

class BlockedUsersListViewController: UIViewController {

    
    @IBOutlet weak var tableBlockedUsers: UITableView!
    
    var arrayBlockedUser = [GroupMember]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        setupNavigationView()
        setupTableview()
        getBlockedUserListApi()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //MARK:-
    func setupNavigationView() {
        
        self.title = "Blocked Users".uppercased()
        
        let leftBarButton = self.setLeftBarButtonItem(withImage: #imageLiteral(resourceName: "backButton"))
        leftBarButton.action = #selector(onBackButtonAction)
        leftBarButton.target = self
    }
    
    //MARK:-
    func setupTableview()
    {
        tableBlockedUsers.delegate = self
        tableBlockedUsers.dataSource = self
        
        tableBlockedUsers.emptyDataSetDelegate = self
        tableBlockedUsers.emptyDataSetSource = self
    }
    
    //MARK:-
    func onBackButtonAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- UITableView Delegate and DataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayBlockedUser.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 77 * scaleFactorX
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : BlockedUserCell = tableView.dequeueReusableCell(withIdentifier: kCellIdentifier_BlockedUsersCell) as! BlockedUserCell
        
        cell.delegate = self
        
        cell.tag = indexPath.row
        
        cell.updateCellContents(usingModel: arrayBlockedUser[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    //MARK:- DZNEmptyDataSetSource -> Title
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        
        let text = "No Blocked Users found".capitalized
        
        let attributes = [NSFontAttributeName:UIFont .boldSystemFont(ofSize: 16*scaleFactorX), NSForegroundColorAttributeName:UIColor.white]
        
        return NSAttributedString (string: text, attributes: attributes)
    }
    
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
    
    //MARK:- Get Blocked User List Api
    func getBlockedUserListApi()
    {
        HUD.show(.systemActivity, onView: self.view.window)
        
        let urlToHit = EndPoints.getBlockedUserList(UtilityClass.getUserSidData()!).path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .get, parameters: nil)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else { return }
            
            if error != nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: error?.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            
            let responseDictionary = dictionary!
            let message = responseDictionary["message"] as! String
            let type = responseDictionary["type"] as! Bool
            
            if statusCode == 203
            {
                return
            }
            
            if statusCode == 200
            {
                if type
                {
                    // success
                    let dataArray = responseDictionary["data"] as! [[String:Any]] // As array
                    print(dataArray)
                    self.updateModelArray(dataArray)
                    return
                }
                else
                {
                    // alert
                    UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
            }
        }
    }
    
    //MARK:-
    func updateModelArray(_ usingArray : [[String:Any]])
    {
        arrayBlockedUser.removeAll()
        let dataArray = usingArray
        
        for dict in dataArray // Iterating dictionaries
        {
            var model =  GroupMember() // Model creation
            model.updateModel(usingDictionary: dict) // Updating model
            arrayBlockedUser.append(model) // Adding model to array
        }
        tableBlockedUsers.reloadData()
    }
    
    func unblockUser(_ atIndex : Int)
    {
        guard let appDeleg = appDelegate as? AppDelegate else {
            return
        }
        
        if !appDeleg.isInternetAvailable()
        {
            return
        }
        
        HUD.show(.systemActivity, onView: self.view.window)
        
        let userID = arrayBlockedUser[atIndex].userID
        
        SocketManager.sharedInstance().unBlock(userID)
        {[weak self] (dictionary, statusCode) in
            
            HUD.hide()
            
            guard let `self` = self else {
                return
            }
            
            if statusCode == 200
            {
                self.getBlockedUserListApi()
            }
        }
        
    }
}

//MARK:-
//MARK:- extension
extension BlockedUsersListViewController : UITableViewDelegate, UITableViewDataSource, DZNEmptyDataSetDelegate, DZNEmptyDataSetSource, BlockedUserCellDelegate
{
    //MARK:- BlockedUserCellDelegate -> Unblock button
    func blockedCell(_ cell: BlockedUserCell, didTapOnUnblockUser unblockUserButton: UIButton) {
        unblockUser(cell.tag)
    }
}
