//
//  UserTabsController.swift
//  OOTTBusinessApp
//
//  Created by Bharat Kumar Pathak on 07/07/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit
import InAppNotify
import CoreLocation

var currentUserID : String?
var checkInTimer = Timer()

class UserTabsController: UITabBarController {

    // MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupLayouts()
        SocketManager.sharedInstance().userSID = UtilityClass.getUserSidData()!
        SocketManager.sharedInstance().connectSocket
            {[weak self] (data, ack) in
                
                guard let `self` = self else { return }
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: NOTIFICATION_SOCKET_CONNECTED), object: nil)
                self.applyReceiveMessageListener()
                self.groupDeletedListener()
                
                weak var weakSelf = self
                
                
                checkInTimer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(weakSelf?.updateUserLocation), userInfo: nil, repeats: true)
//                DispatchQueue.main.asyncAfter(deadline: DispatchTime .now() + .seconds(3))
//                {
//                    weakSelf?.updateUserLocation()
//                }
        }
    }
    
    // MARK:- didReceiveMemoryWarning
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK:- deinit
    deinit {
//        currentUserID = nil
//        SocketManager.sharedInstance().removeIncomingMessagesListener()
//        SocketManager.sharedInstance().disconnectSocket()
        print("UserTabsController deinit")
    }
    
    // MARK:- Initial layout setup
    func setupLayouts() {
        
        isCheckinPopupVisible = false
        
        self.tabBar.backgroundImage = #imageLiteral(resourceName: "tabsBackground").resizableImage(withCapInsets: UIEdgeInsetsMake(0, 0, 0, 0), resizingMode: .stretch)
        
        let numberOfItems = CGFloat((self.tabBar.items!.count))
        
        let tabBarItemSize = CGSize(width: (self.tabBar.frame.width) / numberOfItems,
                                    height: (self.tabBar.frame.height+50))
        self.tabBar.selectionIndicatorImage = UIImage.imageWithColor(color: UIColor.init(white: 0, alpha: 0.3), size: tabBarItemSize).resizableImage(withCapInsets: UIEdgeInsets.init(top: 0, left: 0, bottom: 1, right: 0))
    }
    
    // MARK:- Checkin -> User location Emit
    func updateUserLocation()
    {
        print("update location fired")

        print(Date.init())
        if SocketManager.sharedInstance().isSocketConnected(), LocationManager.sharedInstance().isLocationAccessAllowed()
        {
            let location1 = CLLocation (latitude: LocationManager.sharedInstance().newLatitude, longitude: LocationManager.sharedInstance().newLongitude)
            
            let location2 = CLLocation (latitude: userDefault.double(forKey: UserDefaults.MyApp.checkinLatitude), longitude: userDefault.double(forKey: UserDefaults.MyApp.checkinLongitude))
            
            if UtilityClass.getCoordinatDistance(location1: location1, location2: location2) >= 50 {
                SocketManager.sharedInstance().updateUserLocation(withLatitude: LocationManager.sharedInstance().newLatitude, longitude: LocationManager.sharedInstance().newLongitude, loginToken: UtilityClass.getUserSidData()!, messageHandler: { (checkinData, statusCode) in
                    if statusCode == 200 { // Show popup
                        if isCheckinPopupVisible
                        {
                            return
                        }
                        isCheckinPopupVisible = true
                        if let tempDelegate = appDelegate as? AppDelegate
                        {
                            tempDelegate.showCheckinPopup(withDetails: checkinData, type: .checkin)
                        }
                    }
                })
            }
        }
       // weak var weakSelf = self
        


      
        
//        DispatchQueue.main.asyncAfter(deadline: DispatchTime .now() + .seconds(10))
//        {
//
//            weakSelf?.updateUserLocation()
//        }
    }
    
    // MARK:- Tab bar select index
    func updateTabBar(withIndex selectedIndex: Int)
    {
        self.selectedIndex = selectedIndex
    }
    
    // MARK:- Message Listener (New Message)
    func applyReceiveMessageListener()
    {
        SocketManager.sharedInstance().receiveIncomingMessages
            {[weak self] (messageDict, statusCode) in
                if statusCode == 200
                {
                    guard let `self` = self else {return}
                    self.manageTheIncomingMessage(usingDictionary: messageDict)
                }
        }
        
        
        
        // Group message listner
        SocketManager.sharedInstance().receiveIncomingGroupMessages
            {[weak self] (messageDict, statusCode) in
                if statusCode == 200
                {
                    guard let `self` = self else {return}
                    self.manageTheIncomingMessage(usingDictionary: messageDict)
                }
        }
        
    }
    
    // MARK:- Group Deleted Listener
    func groupDeletedListener()
    {
        SocketManager.sharedInstance().groupDeletedListener
            {[weak self] (messageDict, statusCode) in
                if statusCode == 200
                {
                    guard let `self` = self else {return}
                    self.manageGroupDeleteCase(messageDict["group_id"] as! String)
                }
        }
    }
    
    func manageTheIncomingMessage(usingDictionary dictionary:[String:Any])
    {
        let model = self.getNewModel(usingDictionary: dictionary)
        
        // If chat view is open...
        if let groupChatVC = appDelegate?.window??.visibleViewController() as? GroupChatViewController
        {
            // If same group chat is open...
            if model.roomID == currentUserID
            {
                // Simply add new message model...
                groupChatVC.addNewModel(usingDictionary: dictionary)
            }
            else if model.senderID == currentUserID, model.roomID.isEmpty // Single chat is open
            {
                // Simply add new message model...
                groupChatVC.addNewModel(usingDictionary: dictionary)
            }
            else
            {
                // Show popup notification to user
//                if let chatListVC = appDelegate?.window??.visibleViewController() as? UserConnectViewController
//                {
//                    if chatListVC.segmentedPager.pager.indexForSelectedPage == 1
//                    {
//                        if let connctVC = chatListVC.segmentedViewControllers[1] as? ConnectDirectViewController
//                        {
//                            connctVC.viewAboutToBeSelected()
//                        }
//                    }
//                }
                self.createInAppNotification(usingModel: model)
            }
        }
        else
        {
            // Show popup notification to user
            if let chatListVC = appDelegate?.window??.visibleViewController() as? UserConnectViewController
            {
                if chatListVC.segmentedPager.pager.indexForSelectedPage == 1
                {
                    if let connctVC = chatListVC.segmentedViewControllers[1] as? ConnectDirectViewController
                    {
                        connctVC.viewAboutToBeSelected()
                    }
                }
                else
                {
                    if let connctVC = chatListVC.segmentedViewControllers[0] as? ConnectGroupsViewController
                    {
                        connctVC.viewAboutToBeSelected()
                    }
                }
            }
            self.createInAppNotification(usingModel: model)
        }
    }
    
    // MARK:- Message Model Creation
    func getNewModel(usingDictionary dictionary : [String:Any]) -> ModelChatMessage
    {
        let model = ModelChatMessage () // Model creation
        model.updateModel(usingDictionary: dictionary) // Updating model
        return model
    }
    
    // MARK:- InApp Notification Fire
    func createInAppNotification(usingModel model:ModelChatMessage)
    {
        let title = model.roomID.isEmpty ? model.senderName : String (model.senderName+" @ "+model.groupName)
        
        let picture = model.roomID.isEmpty ? model.senderPicture : model.groupPicture
        
        var message = model.message
        
        if model.isImageMedia {
            message = "Sent a Photo"
        }
        if model.isVideoMedia {
            message = "Sent a Video"
        }
        
        let announce = Announcement (title: title, subtitle: message, image: nil, urlImage: picture, duration: 5, interactionType: .none, userInfo: model)
        {[weak self] (callBackType, str, announce) in
            
            guard let `self` = self else {return}
            
            if callBackType == CallbackType.tap
            {
                if let chatInfo = announce.userInfo as? ModelChatMessage
                {
                    let isGroup = !chatInfo.roomID.isEmpty
                    
                    // open screen
                    let chatViewController = UIStoryboard.getChatViewStoryboard().instantiateInitialViewController() as! GroupChatViewController
                    
                    chatViewController.hidesBottomBarWhenPushed = true
                    
                    chatViewController.isGroupChat = isGroup
                    
                    chatViewController.isBusinessChat = chatInfo.isBusinessGroup
                    
                    chatViewController.chatUsername.text = isGroup ? chatInfo.groupName : chatInfo.senderName
                    
                    if isGroup
                    {
                        chatViewController.chatPicture = chatInfo.groupPicture

                        chatViewController.chatRoomID = chatInfo.roomID
                    }
                    else
                    {
                        chatViewController.chatUserID = chatInfo.senderID
                    }
                    
                    if let navVC = self.selectedViewController as? UINavigationController
                    {
                        navVC.pushViewController(chatViewController, animated: true)
                    }
                }
            }
        }
        
        InAppNotify .Show(announce, to: self.selectedViewController!)
    }
    
    func manageGroupDeleteCase(_ groupID : String)
    {
        // If chat view is open...
        if let groupChatVC = appDelegate?.window??.visibleViewController() as? GroupChatViewController
        {
            // If same group chat is open...
            if groupID == currentUserID
            {
                // Simply add new message model...
                groupChatVC.groupDeleted()
            }
        }
        else
        {
            if let chatListVC = appDelegate?.window??.visibleViewController() as? UserConnectViewController
            {
                if chatListVC.segmentedPager.pager.indexForSelectedPage == 0
                {
                    if let connctVC = chatListVC.segmentedViewControllers[0] as? ConnectGroupsViewController
                    {
                        connctVC.viewAboutToBeSelected()
                    }
                }
            }
        }
    }
}
