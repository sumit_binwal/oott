//
//  FoodMenuViewController.swift
//  OOTTBusinessApp
//
//  Created by Santosh on 27/07/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit

class FoodMenuViewController: UIViewController {

    
    var foodMenuUrlString : String!
    var titleName : String!
    

    @IBOutlet weak var imageViewFoodMenu: ZoomImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        displayMenuImage()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        print("FoodMenuViewController Deinit")
    }

    func setupView() {
        
        self.title = titleName.uppercased()
        
        let backBarButton = self.setLeftBarButtonItem(withImage: #imageLiteral(resourceName: "backButtonWhite"))
        backBarButton.action = #selector(onBackButtonAction)
        backBarButton.target = self
    }
    
    func onBackButtonAction() {
        self.navigationController?.popViewController(animated: true)
    }

    func displayMenuImage()
    {
        imageViewFoodMenu.imageView.setIndicatorStyle(.whiteLarge)
        imageViewFoodMenu.imageView.setShowActivityIndicator(true)
        imageViewFoodMenu.imageView.sd_setImage(with: URL (string: foodMenuUrlString))
        {[weak self] (image, error, cacheType, url) in
            guard let `self` = self else {return}
            if error != nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: "Image failed to download", onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            self.imageViewFoodMenu.imageView.setShowActivityIndicator(false)
            self.imageViewFoodMenu.zoomMode = .fit
//            self.imageViewFoodMenu.image = image

        }
    }
    
    func updateImageScroller(withImage : UIImage?)
    {
        imageViewFoodMenu.image = withImage
    }
}
