//  CommentsListingViewController.swift
//  OOTTBusinessApp
//
//  Created by Bharat Kumar Pathak on 13/07/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit
import DZNEmptyDataSet
import PKHUD

private let MaxChatWindowHeight = 160 * scaleFactorX

class CommentsListingViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    
    @IBOutlet weak var containerTextBox: UIView!
    
    @IBOutlet weak var inputTextView: KMPlaceholderTextView!
    
    @IBOutlet weak var buttonSend: UIButton!
    
    @IBOutlet weak var tableCommentListing: UITableView!
    
    @IBOutlet weak var constraintContainerTextBoxHeight: NSLayoutConstraint!
    
    @IBOutlet weak var constraintSendButtonHeight: NSLayoutConstraint!
    
    @IBOutlet weak var constraintSendButtonWidth: NSLayoutConstraint!
    
    var originalContainerTextBoxHeight : CGFloat = 0
    var originalInputTextViewHeight : CGFloat = 0
    
    
    var arrayCommentList = [ModelCheckInComments]()
    
    var refreshControl: UIRefreshControl!
    
    var checkInID : String?
    
    var isCheckingOwnProfile = false
    
    var likeCount = 0
    
    
    
    // MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupTableview()
        applyRefreshControl()
        
        
        
        if checkInID != nil
        {
            getUserCommentsApi()
        }
    }
    
    // MARK:- didReceiveMemoryWarning
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK:- Initial setup view
    func setupView() {
        
        self.navigationItem.title = "Comments".uppercased()
        
        let leftBarButton = self.setLeftBarButtonItem(withImage: #imageLiteral(resourceName: "backButton"))
        leftBarButton.action = #selector(onBackButtonAction)
        leftBarButton.target = self
        
        let rightBarButton = self.setRightBarButtonItem(withImage: #imageLiteral(resourceName: "liked"))
        rightBarButton.action = #selector(onLikeButtonAction)
        rightBarButton.target = self
        
        constraintContainerTextBoxHeight.constant = constraintContainerTextBoxHeight.constant * scaleFactorX
        originalContainerTextBoxHeight = constraintContainerTextBoxHeight.constant
        
        print(originalContainerTextBoxHeight - self.inputTextView.frame.size.height)
        
        containerTextBox.viewWithTag(10)?.layer.cornerRadius = 5 * scaleFactorX
        inputTextView.delegate = self
        inputTextView.placeholder = "Enter comment"
    }
    
    // MARK:- Setup Tableview
    func setupTableview()
    {
        tableCommentListing.delegate = self
        tableCommentListing.dataSource = self
        tableCommentListing.emptyDataSetSource = self
        tableCommentListing.emptyDataSetDelegate = self
    }
    
    // MARK:- Refresh Control
    func applyRefreshControl() {
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.refreshPage), for: UIControlEvents.valueChanged)
        self.tableCommentListing.addSubview(refreshControl)
    }
    
    func refreshPage(sender:AnyObject)
    {
        if checkInID != nil
        {
            getUserCommentsApi()
        }
        else
        {
            refreshControl.endRefreshing()
        }
    }
    
    // MARK:- On Back Action
    func onBackButtonAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK:- Like button Action
    func onLikeButtonAction() {
        let likeVC = UIStoryboard.getUserHomeStoryboard().instantiateViewController(withIdentifier: "groupLikesViewController") as! GroupLikesViewController
        likeVC.viewType = .statusLikes
        
        likeVC.hidesBottomBarWhenPushed = true
        
        likeVC.userID = checkInID!
        
        self.navigationController?.pushViewController(likeVC, animated: true)
    }
    
    // MARK:- UITableView Delegate and Datasource Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayCommentList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80 * scaleFactorY
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: CommentCell = tableView.dequeueReusableCell(withIdentifier: kCellIdentifier_comment_cell) as! CommentCell
        
        cell.tag = indexPath.row
        
        cell.delegate = self
        
        cell.updateCell(usingModel: arrayCommentList[indexPath.row])
        
        return cell
    }
    
    //MARK:- DZNEmptyDataSetSource -> Title
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        
        let text = "No Comments"
        
        let attributes = [NSFontAttributeName:UIFont .boldSystemFont(ofSize: 16*scaleFactorX), NSForegroundColorAttributeName:UIColor.white]
        
        return NSAttributedString (string: text, attributes: attributes)
    }
    
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
    
    
    //MARK:- Get User Checkin Comments Api
    func getUserCommentsApi() -> Void
    {
        HUD.show(.systemActivity, onView: self.view.window)
        
        let urlToHit = EndPoints.getUserCheckinComments(UtilityClass.getUserSidData()!, checkInID!).path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .get, parameters: nil)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else { return }
            
            self.refreshControl.endRefreshing()
            
            if error != nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: error?.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            
            let responseDictionary = dictionary!
            let message = responseDictionary["message"] as! String
            let type = responseDictionary["type"] as! Bool
            
            if statusCode == 203
            {
                self.containerTextBox.isHidden = true
                // show alert
                UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            if statusCode == 200
            {
                if type
                {
                    self.containerTextBox.isHidden = false
                    // success
                    self.arrayCommentList.removeAll()
                    
                    let dataArray = responseDictionary["data"] as! [[String:Any]] // As array
                    
                    self.updateModelArray(usingArray: dataArray)
                    return
                }
                else
                {
                    // alert
                    UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
            }
        }
    }
    
    //MARK:- Updating Model Array
    func updateModelArray(usingArray array : [[String:Any]]) -> Void
    {
        let dataArray = array
        
        for dict in dataArray // Iterating dictionaries
        {
            let model = ModelCheckInComments () // Model creation
            model.updateModel(usingDictionary: dict) // Updating model
            arrayCommentList.append(model) // Adding model to array
        }
        tableCommentListing.reloadData()
        
//        if isCheckingOwnProfile {
//            self.navigationItem.title = "\(arrayCommentList.count) comments-\(likeCount) likes".uppercased()
//        }
        //        tableCommentListing.scrollToRow(at: IndexPath (row: self.arrayCommentList.count-1, section: 0), at: .top, animated: true)
    }
    
    
    @IBAction func onSendButtonAction(_ sender: UIButton)
    {
        if !inputTextView.text.isEmptyString()
        {
            sender.isUserInteractionEnabled = false
            addCommentApi()
        }
    }
    
    //MARK:- Add Comment Api
    func addCommentApi() -> Void
    {
        self.view.endEditing(true)
        
        HUD.show(.systemActivity, onView: self.view.window)
        
        let params : [String:String] = ["comment" : inputTextView.text]
        
        
        let urlToHit = EndPoints.addCommentOnCheckIn(UtilityClass.getUserSidData()!, checkInID!).path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .post, parameters: params)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else { return }
            
            self.buttonSend.isUserInteractionEnabled = true
            
            if error != nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: error?.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            
            let responseDictionary = dictionary!
            let message = responseDictionary["message"] as! String
            let type = responseDictionary["type"] as! Bool
            
            if statusCode == 203
            {
                // show alert
                UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            if statusCode == 200
            {
                if type
                {
                    // success
                    self.inputTextView.text = ""
                    self.textViewDidChange(self.inputTextView)
                    
                    let dataDict = responseDictionary["data"] as! [String:Any]
                    
                    let newModel = ModelCheckInComments()
                    newModel.updateModel(usingDictionary: dataDict)
                    
                    if self.arrayCommentList.count == 0
                    {
                        self.arrayCommentList.append(newModel)
                    }
                    else
                    {
                        self.arrayCommentList.insert(newModel, at: 0)
                    }
                    
                    
                    self.tableCommentListing.reloadData()
                    self.tableCommentListing.scrollToRow(at: IndexPath (row: 0, section: 0), at: .top, animated: true)
                    
//                    if self.isCheckingOwnProfile {
//                        self.navigationItem.title = "\(self.arrayCommentList.count) comments-\(self.likeCount) likes".uppercased()
//                    }
                    
                    return
                }
                else
                {
                    // alert
                    UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
            }
        }
    }
    
}



//MARK:-
//MARK:- Extension
extension CommentsListingViewController : UITextViewDelegate
{
    //MARK:- UITextViewDelegate
    func textViewDidChange(_ textView: UITextView) {
        
        self.inputTextView.isScrollEnabled = false
        
        let newHeight = textView.intrinsicContentSize.height + (constraintContainerTextBoxHeight.constant - textView.frame.size.height)
        
        constraintContainerTextBoxHeight.constant = max(originalContainerTextBoxHeight, min(newHeight, MaxChatWindowHeight))
        
        if constraintContainerTextBoxHeight.constant >= MaxChatWindowHeight
        {
            self.inputTextView.isScrollEnabled = true
            return
        }
        
        UIView.animate(withDuration: 0.25, delay: 0.0, usingSpringWithDamping: 1.0, initialSpringVelocity: 1.0, options: .curveLinear, animations:
            {
                self.view.layoutIfNeeded()
        }, completion: nil)
    }
}

extension CommentsListingViewController: CommentCellDelegate
{
    func commentCell(_ cell: CommentCell, didTapOnLikeUnLikeButton likeUnlikeButton: UIButton) {
        likeUnlikeButton.isUserInteractionEnabled = false
        if likeUnlikeButton.tag == 999
        {
            let likeVC = UIStoryboard.getUserHomeStoryboard().instantiateViewController(withIdentifier: "groupLikesViewController") as! GroupLikesViewController
            likeVC.viewType = .statusCommentLike
            likeVC.hidesBottomBarWhenPushed = true
            likeVC.userID = checkInID!
            likeVC.commentID = arrayCommentList[cell.tag].commentID
            self.navigationController?.pushViewController(likeVC, animated: true)
        }
        else
        {
            if arrayCommentList[cell.tag].isLiked
            {
                performUnLike(onCell: cell)
            }
            else
            {
                performLike(onCell: cell)
            }
        }
    }
    
    func performLike(onCell cell:CommentCell)
    {
        let editingCell = cell
        
        let commentID = arrayCommentList[cell.tag].commentID
        
        UtilityClass.showNetworkActivityLoader(show: true)
        
        let urlToHit = EndPoints.likeCheckinComment(UtilityClass.getUserSidData()!, checkInID!, commentID).path
        
        print(urlToHit)
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .get, parameters: nil, completionHandler:
            { [weak self] (data, dictionary, statusCode, error) in
                
                UtilityClass.showNetworkActivityLoader(show: false)
                
                guard let `self` = self else { return }
                
                editingCell.buttonLikeUnLike.isUserInteractionEnabled = true
                
                if error != nil
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: error?.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
                
                
                let responseDictionary = dictionary!
                let message = responseDictionary["message"] as! String
                let type = responseDictionary["type"] as! Bool
                
                if statusCode == 203
                {
                    // show alert
                    UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
                
                if statusCode == 200
                {
                    if type
                    {
                        // success
                        let model = self.arrayCommentList[editingCell.tag]
                        
                        model.isLiked = true
                        
                        model.likesCount = String (Int (model.likesCount)! + 1)
                        
                        self.arrayCommentList[editingCell.tag] = model
                        
                        self.tableCommentListing.reloadData()
                        
                        return
                    }
                    else
                    {
                        // alert
                        UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                        return
                    }
                }
        })
    }
    
    func performUnLike(onCell cell:CommentCell)
    {
        let editingCell = cell
        
        let commentID = arrayCommentList[cell.tag].commentID
        
        UtilityClass.showNetworkActivityLoader(show: true)
        
        UtilityClass.showNetworkActivityLoader(show: true)
        
        let urlToHit = EndPoints.UnlikeCheckinComment(UtilityClass.getUserSidData()!, checkInID!, commentID).path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .get, parameters: nil, completionHandler:
            { [weak self] (data, dictionary, statusCode, error) in
                
                UtilityClass.showNetworkActivityLoader(show: false)
                
                guard let `self` = self else { return }
                
                editingCell.buttonLikeUnLike.isUserInteractionEnabled = true
                
                if error != nil
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: error?.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
                
                
                let responseDictionary = dictionary!
                let message = responseDictionary["message"] as! String
                let type = responseDictionary["type"] as! Bool
                
                if statusCode == 203
                {
                    // show alert
                    UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
                
                if statusCode == 200
                {
                    if type
                    {
                        // success
                        let model = self.arrayCommentList[editingCell.tag]
                        
                        model.isLiked = false
                        
                        model.likesCount = String (Int (model.likesCount)! - 1)
                        
                        self.arrayCommentList[editingCell.tag] = model
                        
                        self.tableCommentListing.reloadData()
                        
                        return
                    }
                    else
                    {
                        // alert
                        UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                        return
                    }
                }
        })
    }
}
