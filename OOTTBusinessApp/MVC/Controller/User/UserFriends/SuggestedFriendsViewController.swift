//
//  SuggestedFriendsViewController.swift
//  OOTTBusinessApp
//
//  Created by Bharat Kumar Pathak on 10/07/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit
import PKHUD
import DZNEmptyDataSet

var arraySuggestedListing = [ModelFriendsListing]()

class SuggestedFriendsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, SuggestedFriendsCellDelegate, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    
    
    @IBOutlet weak var tableSuggestedFriends: UITableView!
    
    
    
    var refreshControl: UIRefreshControl!
    
    var currentPage = 1
    var limit = 0
    
    
    var isPageRefreshing = true
    var isEndOfResult = false
    
    var isViewLoadedd: Bool = false
    
    var didDisappear = false
    
    
    // MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTableview()
        
//        getSuggestedFriendsApi(onRefresh: false, shouldShowHUD: true)
        applyRefreshControl()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if !isViewLoadedd {
            getSuggestedFriendsApi(onRefresh: true,shouldShowHUD: true)
            isViewLoadedd = true
        }
        else {
            if didDisappear {
                didDisappear = false
                getSuggestedFriendsApi(onRefresh: true,shouldShowHUD: false)
            }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector (getSuggestedFriendsSearchApi), name: NSNotification.Name(rawValue: "applySearchForSuggestedFriends"), object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    deinit {
        print("SuggestedFriendsViewController deinit")
        arraySuggestedListing.removeAll()
    }
    
    func viewAboutToBeSelected()
    {
        getSuggestedFriendsApi(onRefresh: true,shouldShowHUD: false)
    }
    
    func setupTableview()
    {
        tableSuggestedFriends.delegate = self
        tableSuggestedFriends.dataSource = self
        
        tableSuggestedFriends.emptyDataSetSource = self
        tableSuggestedFriends.emptyDataSetDelegate = self
    }
    
    // MARK:- Refresh Control
    func applyRefreshControl() {
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.refreshPage), for: UIControlEvents.valueChanged)
        self.tableSuggestedFriends.addSubview(refreshControl)
    }
    
    func refreshPage(sender:AnyObject) {
        currentPage = 0
        isPageRefreshing = true
        getSuggestedFriendsApi(onRefresh: true,shouldShowHUD: true)
    }
    
    // MARK:- UITableView Delegate and Datasource Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arraySuggestedListing.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 78 * scaleFactorX
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: SuggestedFriendsCell = tableView.dequeueReusableCell(withIdentifier: kCellIdentifier_suggested_friends_cell) as! SuggestedFriendsCell
        cell.cellDelegate = self
        cell.tag = indexPath.row
        cell.updateCell(usingModel: arraySuggestedListing[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if arraySuggestedListing[indexPath.row] != nil {
            navigateToProfile(forUser: arraySuggestedListing[indexPath.row])
        }
    }
    
    //MARK:- DZNEmptyDataSetSource -> Title
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        
        let text = "No suggested users"
        
        let attributes = [NSFontAttributeName:UIFont .boldSystemFont(ofSize: 16*scaleFactorX), NSForegroundColorAttributeName:UIColor.white]
        
        return NSAttributedString (string: text, attributes: attributes)
    }
    
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
    
    // MARK:- Navigation Methods
    
    func navigateToProfile(forUser userModel:ModelFriendsListing) {
        
        didDisappear = true
        
        let profileViewController = UIStoryboard.getUserProfileStoryboard().instantiateViewController(withIdentifier: "userProfileViewController") as! UserProfileViewController
        profileViewController.targetUserID = userModel.friendID
        profileViewController.isLoggedInUser = false
        profileViewController.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(profileViewController, animated: true)
    }

    // MARK:- SuggestedFriendsCellDelegate -> Follow Button
    
    func suggestedFriendsCell(cell: SuggestedFriendsCell, didTapOnFollowButton followButton: UIButton) {
        
        followButton.isUserInteractionEnabled = false
        
        let index = cell.tag
        
        let userID = arraySuggestedListing[index].friendID!
        
        performFollowUserApi(forUserID: userID, actionsCell: cell)
    }
    
    
    //MARK:- Get suggested(friends) Listing Api
    func getSuggestedFriendsApi(onRefresh: Bool, shouldShowHUD showHUD:Bool) -> Void
    {
        if showHUD
        {
            HUD.show(.systemActivity, onView: self.view.window)
        }
        
        let urlToHit = EndPoints.getSuggestedFriendsList(UtilityClass.getUserSidData()!,String(1)).path
        print("urlToHit = ", urlToHit)
        
        let params = [
            "keyword" : searchFriendsModel.searchString
        ]
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .post, parameters: params)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else { return }
            
            self.isPageRefreshing = false
            self.isEndOfResult = true
            
            self.refreshControl.endRefreshing()
            
            if error != nil
            {
                self.currentPage = self.currentPage > 0 ? self.currentPage-1 : 0
                UtilityClass.showAlertWithTitle(title: App_Name, message: error?.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            let responseDictionary = dictionary!
            print("responseDictionary = ", responseDictionary)
            
            let message = responseDictionary["message"] as! String
            let type = responseDictionary["type"] as! Bool
            
            print(responseDictionary)
            if statusCode == 203
            {
                // show alert
//                UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                self.currentPage = self.currentPage > 0 ? self.currentPage-1 : 0
                
                return
            }
            
            if statusCode == 200
            {
                if type // Friends found
                {
                    // success
                    let dataArray = responseDictionary["data"] as! [[String:Any]] // As array
                    let dataLimit = responseDictionary["limit"] as! Int
                    if onRefresh {
                        self.currentPage = 0
                        arraySuggestedListing.removeAll()
                    }
                    if dataArray.count == dataLimit
                    {
                        self.isEndOfResult = false
                    }
                    self.updateModelArray(usingArray: dataArray)
                    return
                }
                else // No more friends in paging...end of paging
                {
                    // alert
                    self.currentPage = self.currentPage > 0 ? self.currentPage-1 : 0
                    UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
            }
        }
    }
    
    //MARK:- Updating Model Array
    func updateModelArray(usingArray array : [[String:Any]]) -> Void
    {
        arraySuggestedListing.removeAll()
        let dataArray = array
        
        for dict in dataArray // Iterating dictionaries
        {
            let model = ModelFriendsListing () // Model creation
            model.updateModel(usingDictionary: dict) // Updating model
            arraySuggestedListing.append(model) // Adding model to array
        }
        tableSuggestedFriends.reloadData()
    }
    
    //MARK:- Get suggested(friends) Listing Api
    func getSuggestedFriendsSearchApi() -> Void
    {
        getSuggestedFriendsApi(onRefresh: true, shouldShowHUD: true)
        return
        
        let onRefresh = true
        
        let urlToHit = EndPoints.getSuggestedFriendsSearchList(UtilityClass.getUserSidData()!).path
        
        print("url = ", urlToHit)
        
        let params = [
            "keyword" : searchFriendsModel.searchString
        ]
        
        print("params = ", params)
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .post, parameters: params)
        {[weak self] (data, dictionary, statusCode, error) in
            
            guard let `self` = self else { return }
            
            self.isPageRefreshing = false
            self.isEndOfResult = true
            
            self.refreshControl.endRefreshing()
            
            if error != nil
            {
                self.currentPage = self.currentPage > 0 ? self.currentPage-1 : 0
                UtilityClass.showAlertWithTitle(title: App_Name, message: error?.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            let responseDictionary = dictionary!
            print("responseDictionary = ", responseDictionary)
            
            let message = responseDictionary["message"] as! String
            let type = responseDictionary["type"] as! Bool
            
            if statusCode == 203
            {
                // show alert
                //                UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                self.currentPage = self.currentPage > 0 ? self.currentPage-1 : 0
                
                return
            }
            
            if statusCode == 200
            {
                if type // Friends found
                {
                    // success
                    let dataArray = responseDictionary["data"] as! [[String:Any]] // As array
                    let dataLimit = responseDictionary["limit"] as! Int
                    if onRefresh {
                        self.currentPage = 0
                        arraySuggestedListing.removeAll()
                    }
                    if dataArray.count == dataLimit
                    {
                        self.isEndOfResult = false
                    }
                    self.updateModelArray(usingArray: dataArray)
                    return
                }
                else // No more friends in paging...end of paging
                {
                    // alert
                    self.currentPage = self.currentPage > 0 ? self.currentPage-1 : 0
                    UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
            }
        }
    }
    
    //MARK:-
    func performFollowUserApi(forUserID userID:String, actionsCell:SuggestedFriendsCell) -> Void
    {
        UtilityClass.showNetworkActivityLoader(show: true)
        
        let urlToHit = EndPoints.followUser(UtilityClass.getUserSidData()!, userID).path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .get, parameters: nil)
        { [weak self] (data, dictionary, statusCode, error) in
            
            UtilityClass.showNetworkActivityLoader(show: false)
            
            guard let `self` = self else { return }
            
            actionsCell.followUnfollowButton.isUserInteractionEnabled = true
            
            if error != nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: error?.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            
            let responseDictionary = dictionary!
            let message = responseDictionary["message"] as! String
            let type = responseDictionary["type"] as! Bool
            
            if statusCode == 203
            {
                // show alert
                UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            if statusCode == 200
            {
                if type
                {
                    // success
                    let index = actionsCell.tag
                    if arraySuggestedListing[index].isPrivateUser {
                        arraySuggestedListing[index].isRequestedForFollow = true
                    }
                    else {
                        arraySuggestedListing.remove(at: index)
                    }
                    self.tableSuggestedFriends.reloadData()
                    return
                }
                else
                {
                    // alert
                    UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
            }
            
        }
    }
}

extension SuggestedFriendsViewController
{
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        
//        if isEndOfResult
//        {
//            return
//        }
//        if tableSuggestedFriends.contentOffset.y + tableSuggestedFriends.frame.size.height >= tableSuggestedFriends.contentSize.height
//        {
//            if !isPageRefreshing
//            {
//                isPageRefreshing = true
//                
//                currentPage+=1
//                
//                getSuggestedFriendsApi(onRefresh: false)
//            }
//        }
//        
//    }
}
