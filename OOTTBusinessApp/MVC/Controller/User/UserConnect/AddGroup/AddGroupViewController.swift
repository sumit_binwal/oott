//
//  AddGroupViewController.swift
//  OOTTBusinessApp
//
//  Created by Santosh on 04/10/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit
import PKHUD

//MARK:-
//MARK:- AddGroupViewController
class AddGroupViewController: UIViewController {
    
    @IBOutlet weak var tableAddGroup: UITableView!
    
    @IBOutlet var headerImageSelect: UIView!
    
    
    @IBOutlet var footerDoneButton: UIView!
    @IBOutlet weak var buttonDone: UIButton!
    
    @IBOutlet weak var buttonImageGroup: UIButton!
    
    var groupModel = ModelAddChatGroup ()
    
    var isEditGroup = false
    var groupID = ""
    
    var isNameChanged = false
    var isPhotoChanged = false
    var previousName: String?
    
    //MARK:-
    deinit {
        print("AddGroupViewController Deinit")
    }
    
    //MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        tableAddGroup.isHidden = isEditGroup
        
        if !isEditGroup
        {
            var leaderMember = GroupMember ()
            leaderMember.picture = UtilityClass.getUserPictureData()!
            leaderMember.userID = UtilityClass.getUserIDData()!
            leaderMember.username = UtilityClass.getUserNameData()!
            
            groupModel.groupMemberLeader = leaderMember
        }
        else
        {
            userGroupDetails()
        }
        setupView()
        setupTableview()
    }

    //MARK:-
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:-
    func setupView()
    {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        
        self.title = isEditGroup ? "Edit Group".uppercased() : "Create Group".uppercased()
        
        let leftBarButton = self.setLeftBarButtonItem(withImage: #imageLiteral(resourceName: "backButton"))
        leftBarButton.action = #selector(onBackButtonAction)
        leftBarButton.target = self
    }
    
    //MARK:-
    func setupTableview()
    {
        headerImageSelect.frame = CGRect (x: 0, y: 0, width: tableAddGroup.frame.size.width, height: headerImageSelect.frame.size.height * scaleFactorX)
        
        tableAddGroup.tableHeaderView = headerImageSelect
        
        footerDoneButton.frame = CGRect (x: 0, y: 0, width: tableAddGroup.frame.size.width, height: footerDoneButton.frame.size.height * scaleFactorX)
        
        tableAddGroup.tableFooterView = footerDoneButton
        
        buttonImageGroup.layer.cornerRadius=buttonImageGroup.frame.size.width/2*scaleFactorX
        buttonImageGroup.clipsToBounds=true;
        buttonImageGroup.layer.borderColor = color_cellImageBorder.cgColor
        buttonImageGroup.layer.borderWidth=0.5
        buttonImageGroup.imageView?.contentMode = .scaleAspectFill
        
        tableAddGroup.dataSource = self
        tableAddGroup.delegate = self
    }
    
    // MARK:- On Back Action
    func onBackButtonAction() {
        self.view.endEditing(true)
        if isEditGroup {
            self.navigationController?.popViewController(animated: true)
        }
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    func goToRootViewController()
    {
        if SocketManager.sharedInstance().isSocketConnected(), isNameChanged
        {
            SocketManager.sharedInstance().sendGroupMessage(SocketModelSendGroupMessage (), messageDict: ["isGroupName":true,"room_id": groupID, "name": groupModel.groupName ?? ""]) { result, statusCode in
                
            }
        }
        if SocketManager.sharedInstance().isSocketConnected(), isPhotoChanged
        {
            SocketManager.sharedInstance().sendGroupMessage(SocketModelSendGroupMessage (), messageDict: ["isGroupPhoto":true,"room_id": groupID]) { result, statusCode in
                
            }
        }
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    // MARK:-
    @IBAction func onButtonImageGroupAction(_ sender: UIButton) {
        UtilityClass.showActionSheetWithTitle(title: "Add Profile Image", message: "Select from options", onViewController: self, withButtonArray: ["Camera", "Photos"])
        {[weak self] (buttonIndex : Int) in
            guard let `self` = self else {return}
            if buttonIndex == 0
            {
                self.openPhotoPickerForType(imageSelectType: .camera, imageTypeIndex: sender.tag)
            }
            else
            {
                self.openPhotoPickerForType(imageSelectType: .photos, imageTypeIndex: sender.tag)
                
            }
        }
    }
    
    //MARK:- Show ImagePicker
    func openPhotoPickerForType(imageSelectType : ImageSelectType, imageTypeIndex:NSInteger) -> Void
    {
        var isCamera = false
        
        if imageSelectType == ImageSelectType.camera
        {
            if !UIImagePickerController.isSourceTypeAvailable(.camera
                ) {
                UtilityClass.showAlertWithTitle(title: "This device does not have Camera. Try Photos!", message: nil, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            else
            {
                isCamera = true
            }
        }
        
        let imagePicker = UIImagePickerController.init()
        imagePicker.allowsEditing = true
        imagePicker.sourceType = isCamera ? .camera : .photoLibrary
        imagePicker.delegate = self
        imagePicker.view.tag=imageTypeIndex
        
        imagePicker.navigationBar.isTranslucent = false
        imagePicker.navigationBar.barTintColor = UIColor.init(RED: 10, GREEN: 23, BLUE: 42, ALPHA: 0.6)
        imagePicker.navigationBar.tintColor = UIColor.white
        imagePicker.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
        
        
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    //MARK:- UIImagePickerControllerDelegate
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        picker.dismiss(animated: true, completion: nil)
        
        if let image = info[UIImagePickerControllerEditedImage] as? UIImage
        {
            isPhotoChanged = true
            buttonImageGroup.setImage(image, for: .normal)
        }
    }
    
    //MARK:- UITableView Delegate and Datasource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isEditGroup {
            return 1
        }
        if groupModel.groupInviteMembers != nil
        {
            return 2+(groupModel.groupInviteMembers?.count)!+1
        }
        return 2 //Earlier 4
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0
        {
            return 44 * scaleFactorX
        }
//        else if indexPath.row == 1
//        {
//            return 63 * scaleFactorX
//        }
//        else if indexPath.row == 2
//        {
//            return 106 * scaleFactorX
//        }
        else if indexPath.row == 1 // Earlier 3
        {
            return 64 * scaleFactorX
        }
        return 75 * scaleFactorX
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell : AddChatGroupCell!
        
        var cellIdentifier = ""
        
        var newCellType = AddGroupCellType.none
        
        
        switch indexPath.row {
        case 0:
            cellIdentifier = kCellIdentifier_AddGroup_GroupName
            newCellType = .groupName
//        case 1:
//            cellIdentifier = kCellIdentifier_AddGroup_GroupSettingTitle
//            newCellType = .groupSettingTitle
//        case 2:
//            cellIdentifier = kCellIdentifier_AddGroup_GroupMemberSettings
//            newCellType = .groupSettingMembers
        case 1: // Earlier 3
            cellIdentifier = kCellIdentifier_AddGroup_GroupInviteMembers
            newCellType = .groupInviteMembers
        default:
            break
        }
        
        if indexPath.row > 1 //Earlier 3
        {
            if groupModel.groupInviteMembers != nil, !(groupModel.groupInviteMembers?.isEmpty)!
            {
                if indexPath.row <= groupModel.groupInviteMembers!.count+1
                {
                    cellIdentifier = kCellIdentifier_AddGroup_GroupMember
                    newCellType = .groupMembers
                }
                else
                {
//                    if indexPath.row == tableView.numberOfRows(inSection: 0)-2
//                    {
                        cellIdentifier = kCellIdentifier_AddGroup_GroupLeaderMember
                        newCellType = .groupLeaderMember
//                    }
//                    else
//                    {
//                        cellIdentifier = kCellIdentifier_AddGroup_GroupLeaderMember
//                        newCellType = .groupCoLeaderMember
//                    }
                }
            }
        }
        
        
        cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! AddChatGroupCell
        cell.tag = indexPath.row
        cell.cellDelegate = self
        cell.cellType = newCellType
        cell.updateCell(usingModel: groupModel, forIndex: indexPath.row)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if isEditGroup
        {
            return
        }
        
        if indexPath.row == 1 { //Earlier 3
            let searchUserVC = self.storyboard?.instantiateViewController(withIdentifier: "searchUserNav") as! UINavigationController
            
            if let rootVC = searchUserVC.viewControllers[0] as? SearchUserViewController
            {
                rootVC.isMultipleSelectScreen = true
                rootVC.delegate = self
                if let members = groupModel.groupInviteMembers
                {
                    rootVC.selectedUsers = members
                }
            }
            
            self.navigationController?.present(searchUserVC, animated: true, completion: nil)
            return
        }
        
//        let cell = tableView.cellForRow(at: indexPath) as! AddChatGroupCell
//
//
//        if cell.cellType == .groupLeaderMember
//        {
//            let searchUserVC = self.storyboard?.instantiateViewController(withIdentifier: "addMemberNav") as! UINavigationController
//
//            if let rootVC = searchUserVC.viewControllers[0] as? AddMemberViewController
//            {
//                rootVC.isLeaderSelection = true
//                rootVC.delegate = self
//                if let members = groupModel.groupInviteMembers
//                {
//                    rootVC.arrayInvitedFriends = members
//                }
//            }
//            self.navigationController?.present(searchUserVC, animated: true, completion: nil)
//            return
//        }
//
//        if cell.cellType == .groupCoLeaderMember
//        {
//            let searchUserVC = self.storyboard?.instantiateViewController(withIdentifier: "addMemberNav") as! UINavigationController
//
//            if let rootVC = searchUserVC.viewControllers[0] as? AddMemberViewController
//            {
//                rootVC.isLeaderSelection = false
//                rootVC.delegate = self
//                if let members = groupModel.groupInviteMembers
//                {
//                    rootVC.arrayInvitedFriends = members
//                }
//            }
//            self.navigationController?.present(searchUserVC, animated: true, completion: nil)
//            return
//        }
    }
    
    
    //MARK:- Done button Action
    @IBAction func onDoneButtonAction(_ sender: UIButton) {
        
        if validateFields(isEditGroup) {
            isEditGroup ? updateGroupApi() : callCreateGroupApi()
        }
    }
    
    //MARK:- Create Group Api
    func callCreateGroupApi()
    {
        HUD.show(.systemActivity, onView: self.view.window)
        
        let params = groupModel.getDictionary()
        
        print(params)
        
        let urlToHit = EndPoints.createChatGroup(UtilityClass.getUserSidData()!).path
        
        let imageArray = [ImageDataDict (data: UIImageJPEGRepresentation(buttonImageGroup.image(for: .normal)!, 0.7)!, name: "picture")]
        
        AppWebHandler.sharedInstance().uploadImages(fromURL: urlToHit, imagesArray: imageArray, otherParameters: params)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                return
            }
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                if let dataDict = responseDictionary["data"] as? [String:Any]
                {
//                    UtilityClass.showAlertWithTitle(title: App_Name, message: "ban gya group", onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    self.moveToGroupChatScreen(dataDict)
                }
            }
            else
            {
                
            }
        }
    }
    
    //MARK:- Validate All Fields
    func validateFields(_ isGroupEditing : Bool) -> Bool
    {
        if groupModel.groupName == nil || (groupModel.groupName?.isEmptyString())!
        {
            UtilityClass.showAlertWithTitle(title: App_Name, message: "Please provide group name", onViewController: self, withButtonArray: nil, dismissHandler: nil)
            return false
        }
        
        if buttonImageGroup.imageView?.image == nil
        {
            UtilityClass.showAlertWithTitle(title: App_Name, message: "Please add group image", onViewController: self, withButtonArray: nil, dismissHandler: nil)
            return false
        }
        
        if isGroupEditing {
            return true
        }
        
        if groupModel.groupInviteMembers == nil || (groupModel.groupInviteMembers?.isEmpty)!
        {
            UtilityClass.showAlertWithTitle(title: App_Name, message: "Please select atleast three friends for the group", onViewController: self, withButtonArray: nil, dismissHandler: nil)
            return false
        }
        
        if (groupModel.groupInviteMembers?.count)! < 2
        {
            UtilityClass.showAlertWithTitle(title: App_Name, message: "Please select atleast two friends for the group", onViewController: self, withButtonArray: nil, dismissHandler: nil)
            return false
        }
        
        if groupModel.groupMemberLeader == nil
        {
            UtilityClass.showAlertWithTitle(title: App_Name, message: "Please select Leader", onViewController: self, withButtonArray: nil, dismissHandler: nil)
            return false
        }
        
//        if groupModel.groupMemberCoLeader == nil
//        {
//            UtilityClass.showAlertWithTitle(title: App_Name, message: "Please select Co-Leader", onViewController: self, withButtonArray: nil, dismissHandler: nil)
//            return false
//        }
        return true
    }
    
    //MARK:- Group Created; Move to chat screen
    func moveToGroupChatScreen(_ groupDictionary : [String:Any])
    {
        if let userPresentingController =  self.navigationController?.presentingViewController as? UserTabsController{
            if let navController = userPresentingController.selectedViewController as? UINavigationController
            {
                let chatViewController = UIStoryboard.getChatViewStoryboard().instantiateInitialViewController() as! GroupChatViewController
                
                chatViewController.hidesBottomBarWhenPushed = true
                
                chatViewController.isGroupChat = true
                
                chatViewController.chatUsername.text = groupDictionary["name"] as? String
                chatViewController.chatRoomID = groupDictionary["room_id"] as? String
                chatViewController.chatPicture = groupDictionary["picture"] as? String
                
                navController.pushViewController(chatViewController, animated: false)
                
                onBackButtonAction()
            }
        }
    }
    
    //MARK:- Edit Groups Details Api
    func userGroupDetails()
    {
        HUD.show(.systemActivity, onView: self.view.window)
        
        let urlToHit = EndPoints.viewGroupDetails(UtilityClass.getUserSidData()!, groupID).path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .get, parameters: nil)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: error?.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            let responseDictionary = dictionary!
            let message = responseDictionary["message"] as! String
            
            if statusCode == 200
            {
                if let dataDict = responseDictionary["data"] as? [String:Any]
                {
                    self.updateAddGroupModel(usingDictionary: dataDict)
                }
            }
            else
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
            }
        }
    }
    
    //MARK:- Update group model to display
    func updateAddGroupModel(usingDictionary dictionary : [String:Any])
    {
        groupModel.updateModel(usingDictionary: dictionary)
        previousName = groupModel.groupName
        buttonImageGroup.imageView?.setIndicatorStyle(.white)
        buttonImageGroup.imageView?.setShowActivityIndicator(true)
        buttonImageGroup.sd_setImage(with: URL (string: groupModel.groupPictureURL!), for: .normal, completed: nil)
        tableAddGroup.isHidden = false
        tableAddGroup.reloadData()
    }
    
    //MARK:-
    //MARK:- Create Group Api
    func updateGroupApi()
    {
        if previousName != groupModel.groupName {
            isNameChanged = true
        }
        HUD.show(.systemActivity, onView: self.view.window)
        
        let params = [
            "name" : groupModel.groupName!
        ]
        
        print(params)
        
        let urlToHit = EndPoints.updateUserGroup(UtilityClass.getUserSidData()!,groupID).path
        
        let imageArray = [ImageDataDict (data: UIImageJPEGRepresentation(buttonImageGroup.image(for: .normal)!, 0.7)!, name: "picture")]
        
        AppWebHandler.sharedInstance().uploadImages(fromURL: urlToHit, imagesArray: imageArray, otherParameters: params)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: error?.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            let responseDictionary = dictionary!
            let message = responseDictionary["message"] as! String
            
            if statusCode == 200
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: "The group settings have been updated.", onViewController: self, withButtonArray: nil, dismissHandler:
                    {[weak self] (buttonIndex) in
                        guard let `self` = self else {return}
                        self.goToRootViewController()
                })
            }
            else
            {
                 UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
            }
        }
    }
}

//MARK:-
//MARK:- Extension
extension AddGroupViewController : UITableViewDataSource, UITableViewDelegate, AddChatGroupCellDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, SearchUserViewControllerDelegate, AddMemberViewControllerDelegate
{
    //MARK:- AddChatGroupCellDelegate -> Group name
    func addGroupCell(_ cell: AddChatGroupCell, didUpdateGroupName groupName: String) {
        groupModel.groupName = groupName
    }
    
    //MARK:- AddChatGroupCellDelegate -> Invite Switch
    func addGroupCell(_ cell: AddChatGroupCell, didTapOnCanInviteButton inviteButton: UIButton) {
        
        if isEditGroup {
            return
        }
        
        groupModel.groupMemberCanInvite = !groupModel.groupMemberCanInvite
        
        var image = #imageLiteral(resourceName: "toggle_off")
        if groupModel.groupMemberCanInvite
        {
            image = #imageLiteral(resourceName: "toggle_on")
        }
        
        inviteButton.setImage(image, for: .normal)
    }
    
    //MARK:- AddChatGroupCellDelegate -> Make move switch
    func addGroupCell(_ cell: AddChatGroupCell, didTapOnMakeMovesButton makeMovesButton: UIButton) {
        
        if isEditGroup {
            return
        }
        
        groupModel.groupMemberCanMakeMoves = !groupModel.groupMemberCanMakeMoves
        
        var image = #imageLiteral(resourceName: "toggle_off")
        if groupModel.groupMemberCanMakeMoves
        {
            image = #imageLiteral(resourceName: "toggle_on")
        }
        
        makeMovesButton.setImage(image, for: .normal)
    }
    
    //MARK:- SearchUserViewControllerDelegate -> Invite members
    func onSearchUserMultipleSelectScreen(_ viewController: SearchUserViewController, didFinishSelectingUsers selectedUsers: [GroupMember]) {
        groupModel.groupInviteMembers = selectedUsers
        tableAddGroup.reloadData()
        
        viewController.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- AddMemberViewControllerDelegate -> Leader/Co-Leader
    func onAddMemberViewController(_ viewController: AddMemberViewController, didSelectMember member: GroupMember, isLeader: Bool) {
        if isLeader
        {
            groupModel.groupMemberLeader = member
        }
        else
        {
            groupModel.groupMemberCoLeader = member
        }
        self.tableAddGroup.reloadData()
        
        viewController.navigationController?.dismiss(animated: true, completion: nil)
    }
}
