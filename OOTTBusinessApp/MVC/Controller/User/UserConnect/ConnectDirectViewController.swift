//
//  ConnectDirectViewController.swift
//  OOTTBusinessApp
//
//  Created by Bharat Kumar Pathak on 10/07/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit
import PKHUD
import DZNEmptyDataSet

class ModelChatList
{
    /*
     
     "data": [
     {
     "lastMessage": {
     "receiver": "599c2f31052ce675159d0cb2",
     "message": "this is test msg",
     "sender": "598c1040f8332c2a47a3e21d",
     "updated": "2017-09-20T09:20:48.259Z",
     "created": "2017-09-20T09:20:48.259Z",
     "_id": "59c232f00b811c70a64c21b7",
     "seen": false
     },
     "user": {
     "user_id": "599c2f31052ce675159d0cb2",
     "name": "sssssssslol",
     "picture": "http://192.168.0.131/oott/build/public/uploads/users/tsZqConSodujOBeHJsF8jGsu0PyHKRRw.jpg"
     },
     "room_id": "59c232e786535a77efbe3268"
     }
     ],
     
 */
    
    var roomID = ""
    var userID = ""
    var userName = ""
    var userPicture = ""
    var lastMessage = ""
    var lastMessageTime = ""
    var unreadCount = 0
    
    var isBlocked = false
    
    var isAccountDeleted = false
    
    func updateModel(usingDictionary dictionary : [String:Any])
    {
        if let id = dictionary["room_id"] as? String
        {
            self.roomID = id
        }
        
        if let unseenCount = dictionary["unseenCount"] as? Int
        {
            self.unreadCount = unseenCount
        }
        
        if let userDict = dictionary["user"] as? [String:Any]
        {
            if let id = userDict["user_id"] as? String
            {
                self.userID = id
            }
            
            if let isArchive = userDict["isArchive"] as? Bool
            {
                self.isAccountDeleted = isArchive
            }
            
            if let isBlock = userDict["isBlocked"] as? Bool
            {
                self.isBlocked = isBlock
            }
            
            if let name = userDict["name"] as? String
            {
                self.userName = name
            }
            
            if let picture = userDict["picture"] as? String
            {
                self.userPicture = picture
            }
        }
        
        if let lastMsgDict = dictionary["lastMessage"] as? [String:Any]
        {
            if let msg = lastMsgDict["message"] as? String
            {
                self.lastMessage = msg.decodeStringFromBase64Encoding()
            }
            
            if lastMsgDict["isMedia"] as? Bool == true {
                let msg = lastMsgDict["message"] as! String
                if msg.contains(".jpg") {
                    self.lastMessage = "Photo"
                }
                else {
                    self.lastMessage = "Video"
                }
            }
            
            if let created = lastMsgDict["updated"] as? String
            {
                self.lastMessageTime = created
            }
        }
    }
}

// MARK:-
// MARK:- ConnectDirectViewController
class ConnectDirectViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    // MARK:-
    @IBOutlet weak var tableChatList: UITableView!
    
    
    var arrayChatList = [ModelChatList]()
    
    var isViewLoadedd = false
    
    var didDisappear = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !isViewLoadedd {
            getChatList()
            isViewLoadedd = true
        }
        else {
//            if didDisappear {
//                didDisappear = false
//                getChatList()
//            }
            getChatList()
        }
    }
    
    func viewAboutToBeSelected()
    {
        getChatList()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    deinit {
        print("ConnectDirectViewController deinit")
    }
    
    func setupTableView()
    {
        tableChatList.delegate = self
        tableChatList.dataSource = self
        tableChatList.emptyDataSetSource = self
        tableChatList.emptyDataSetDelegate = self
    }
    
    // MARK:- UITableView Delegate and Datasource Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayChatList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80 * scaleFactorX
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: ConnectDirectCell = tableView.dequeueReusableCell(withIdentifier: kCellIdentifier_connect_direct_cell) as! ConnectDirectCell
        cell.updateCell(usingModel: arrayChatList[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        didDisappear = true
        
        let chatViewController = UIStoryboard.getChatViewStoryboard().instantiateInitialViewController() as! GroupChatViewController
        
        chatViewController.hidesBottomBarWhenPushed = true
        
        chatViewController.isGroupChat = false
        
        chatViewController.isUserBlocked = arrayChatList[indexPath.row].isBlocked
        
        chatViewController.chatUsername.text = arrayChatList[indexPath.row].userName
        
        chatViewController.chatUserID = arrayChatList[indexPath.row].userID
        
        chatViewController.isAccountDeleted = arrayChatList[indexPath.row].isAccountDeleted
        
        self.navigationController?.pushViewController(chatViewController, animated: true)
        
    }

    // MARK:- Plus Icon Action
    @IBAction func onAddButtonAction(_ sender: UIButton) {
        
        let searchUserVC = self.storyboard?.instantiateViewController(withIdentifier: "searchUserNav") as! UINavigationController
        
        self.navigationController?.present(searchUserVC, animated: true, completion: nil)
    }
    
    
    //MARK:- Get Chat Listing Api
    func getChatList()
    {
//        HUD.show(.systemActivity, onView: self.view.window)
        SocketManager.sharedInstance().getUserChatList
            {[weak self] (chatListArray, statusCode) in
                
                HUD.hide()
                
                guard let `self` = self else {return}
                
                if statusCode == 200
                {
                    self.updateModel(usingArray: chatListArray)
                }
        }
    }
    
    //MARK:- Update Model
    func updateModel(usingArray array : [[String:Any]])
    {
        arrayChatList.removeAll()
        
        let dataArray = array
        
        for dict in dataArray // Iterating dictionaries
        {
            let model = ModelChatList () // Model creation
            model.updateModel(usingDictionary: dict) // Updating model
            arrayChatList.append(model) // Adding model to array
        }
        tableChatList.reloadData()
    }
}



//MARK:-
//MARK:- Extension
extension ConnectDirectViewController : DZNEmptyDataSetSource, DZNEmptyDataSetDelegate
{
    //MARK:- DZNEmptyDataSetSource -> Title
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        
        let text = "Tap the + icon to start a conversation"
        
        let attributes = [NSFontAttributeName:UIFont .boldSystemFont(ofSize: 16*scaleFactorX), NSForegroundColorAttributeName:UIColor.white]
        
        return NSAttributedString (string: text, attributes: attributes)
    }
    
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
}
