//
//  SearchUserViewController.swift
//  OOTTBusinessApp
//
//  Created by Santosh on 21/09/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit
import PKHUD
import DZNEmptyDataSet

protocol SearchUserViewControllerDelegate : class {
    func onSearchUserMultipleSelectScreen(_ viewController:SearchUserViewController, didFinishSelectingUsers selectedUsers:[GroupMember])
}

class SearchUserViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {

    
    @IBOutlet weak var tableSearchUsers: UITableView!
    
    @IBOutlet var containerSearchView: SearchBarView!
    
    @IBOutlet weak var searchTextField: UITextField!
    
    var arraySearchUsers = [ModelFriendsListing]()
    
    var arraySearchUsersTemp = [ModelFriendsListing]()
    
    var isSearchModeOn = false
    
    var searchedString = ""
    
    var isMultipleSelectScreen = false
    
    var selectedUsers = [GroupMember]()
    
    weak var delegate : SearchUserViewControllerDelegate?
    
    //MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupNavigationView()
        setupTableview()
        
        getFollowingFriends()
    }

    //MARK:-
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:-
    func setupNavigationView() {
        
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        searchTextField.setPlaceholderColor(UIColor.init(RED: 184.0, GREEN: 187.0, BLUE: 190.0, ALPHA: 1.0))
        searchTextField.customize(withLeftIcon: #imageLiteral(resourceName: "searchIcon"))
        searchTextField.delegate = self
        self.navigationItem.titleView = containerSearchView
        
        let rightBarButton = self.setRightBarButtonItem("cancel".uppercased())
        rightBarButton.action = #selector(onCancelAction)
        rightBarButton.target = self
        
        if isMultipleSelectScreen
        {
            let leftBarButton = self.setLeftBarButtonItem("done".uppercased())
            leftBarButton.action = #selector(onDoneAction)
            leftBarButton.target = self
            leftBarButton.isEnabled = !selectedUsers.isEmpty
        }
    }
    
    //MARK:-
    func setupTableview()
    {
        tableSearchUsers.delegate = self
        tableSearchUsers.dataSource = self
        
        tableSearchUsers.emptyDataSetDelegate = self
        tableSearchUsers.emptyDataSetSource = self
    }
    
    //MARK:-
    func onCancelAction()
    {
        self.view.endEditing(true)
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    //MARK:-
    func onDoneAction()
    {
        self.view.endEditing(true)
        manageSelectedUsersAndDismiss()
    }
    
    //MARK:- UITableView Delegate and DataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearchModeOn
        {
            return arraySearchUsersTemp.count
        }
        return arraySearchUsers.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 77 * scaleFactorX
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : SearchUserCell = tableView.dequeueReusableCell(withIdentifier: kCellIdentifier_SearchUserCell) as! SearchUserCell
        if isSearchModeOn
        {
            cell.updateCell(usingModel: arraySearchUsersTemp[indexPath.row])
        }
        else
        {
            cell.updateCell(usingModel: arraySearchUsers[indexPath.row])
        }
        
        let contains = selectedUsers.contains(where: { (model) -> Bool in
            if isSearchModeOn
            {
                return model.userID == arraySearchUsersTemp[indexPath.row].friendID!
            }
            return model.userID == arraySearchUsers[indexPath.row].friendID!
        })
        
        if contains
        {
            cell.accessoryType = .checkmark
        }
        else
        {
            cell.accessoryType = .none
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if isMultipleSelectScreen
        {
            let contains = selectedUsers.contains(where: { (model) -> Bool in
                if isSearchModeOn
                {
                    return model.userID == arraySearchUsersTemp[indexPath.row].friendID!
                }
                return model.userID == arraySearchUsers[indexPath.row].friendID!
            })
            
            if contains
            {
                if let index = selectedUsers.index(where: { (model) -> Bool in
                    if isSearchModeOn
                    {
                        return model.userID == arraySearchUsersTemp[indexPath.row].friendID!
                    }
                    return model.userID == arraySearchUsers[indexPath.row].friendID!
                }) {
                    selectedUsers.remove(at: index)
                }
            }
            else
            {
                var model = GroupMember ()
                model.userID = isSearchModeOn ? arraySearchUsersTemp[indexPath.row].friendID! : arraySearchUsers[indexPath.row].friendID!
                
                model.username = isSearchModeOn ? arraySearchUsersTemp[indexPath.row].friendName! : arraySearchUsers[indexPath.row].friendName!
                
                model.picture = isSearchModeOn ? arraySearchUsersTemp[indexPath.row].friendPicture! : arraySearchUsers[indexPath.row].friendPicture!
                
                selectedUsers.append(model)
            }
            
            self.navigationItem.leftBarButtonItem?.isEnabled = !selectedUsers.isEmpty
            
            self.tableSearchUsers.reloadData()
            return
        }
        
        
        if let userPresentingController =  self.navigationController?.presentingViewController as? UserTabsController{
            if let navController = userPresentingController.selectedViewController as? UINavigationController
            {
                let chatViewController = UIStoryboard.getChatViewStoryboard().instantiateInitialViewController() as! GroupChatViewController
                
                chatViewController.hidesBottomBarWhenPushed = true
                
                chatViewController.isGroupChat = false
                chatViewController.chatUsername.text = isSearchModeOn ? arraySearchUsersTemp[indexPath.row].friendName! : arraySearchUsers[indexPath.row].friendName!
                chatViewController.chatUserID = isSearchModeOn ? arraySearchUsersTemp[indexPath.row].friendID! : arraySearchUsers[indexPath.row].friendID!
                
                navController.pushViewController(chatViewController, animated: false)
                
                onCancelAction()
            }
        }
    }
    
    //MARK:- DZNEmptyDataSetSource -> Title
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        
        let text = "No Friends Found"
        
        let attributes = [NSFontAttributeName:UIFont .boldSystemFont(ofSize: 16*scaleFactorX), NSForegroundColorAttributeName:UIColor.white]
        
        return NSAttributedString (string: text, attributes: attributes)
    }
    
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
    
    //MARK:- Get Following List Api
    func getFollowingFriends()
    {
        HUD.show(.systemActivity, onView: self.view.window)

        let urlToHit = EndPoints.getFriendsList(UtilityClass.getUserSidData()!,String(1)).path
        print("url = ", urlToHit)
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .post, parameters: nil)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else { return }
            
            if error != nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: error?.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            let responseDictionary = dictionary!
            let message = responseDictionary["message"] as! String
            let type = responseDictionary["type"] as! Bool
            
            if statusCode == 203
            {
                // show alert
                // UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            if statusCode == 200
            {
                if type
                {
                    // success
                    let dataArray = responseDictionary["data"] as! [[String:Any]] // As array
                    self.updateModelArray(dataArray)
                    return
                }
                else
                {
                    // alert
                    UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
            }
        }
    }
    
    //MARK:-
    func updateModelArray(_ usingArray : [[String:Any]])
    {
        arraySearchUsers.removeAll()
        let dataArray = usingArray
        
        for dict in dataArray // Iterating dictionaries
        {
            let model = ModelFriendsListing () // Model creation
            model.updateModel(usingDictionary: dict) // Updating model
            if model.isAccountDeleted
            {
                continue
            }
            arraySearchUsers.append(model) // Adding model to array
        }
        
        tableSearchUsers.reloadData()
    }
    
    //MARK:-
    func manageSelectedUsersAndDismiss()
    {
        guard let delegate = self.delegate else {
            return
        }
        delegate.onSearchUserMultipleSelectScreen(self, didFinishSelectingUsers: selectedUsers)
    }
}

//MARK:-
//MARK:- Extension
extension SearchUserViewController : UITextFieldDelegate
{
    //MARK:- UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        isSearchModeOn = false
        arraySearchUsersTemp.removeAll()
        tableSearchUsers.reloadData()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var newString : String?
        var finalCount = 0
        
        if string.characters.count == 0
        {
            if textField.text?.characters.count==0
            {
                finalCount = 0
                newString = textField.text
            }
            else
            {
                finalCount = textField.text!.characters.count - 1
                var newStr = textField.text! as NSString
                newStr = newStr.replacingCharacters(in: range, with: string) as NSString
                newString = newStr as String
            }
        }
        else
        {
            finalCount = textField.text!.characters.count + 1
            var newStr = textField.text! as NSString
            
            newStr = newStr.replacingCharacters(in: range, with: string) as NSString
            
            newString = newStr as String
        }
        
        searchedString = newString!
        if finalCount == 0
        {
            isSearchModeOn = false
        }
        else
        {
            isSearchModeOn = true
            
            arraySearchUsersTemp = arraySearchUsers.filter { (model) -> Bool in
                
                let isAvailable = model.friendName!.localizedCaseInsensitiveContains(newString!)
                
                if isAvailable
                {
                    return true
                }
                return false
            }
        }
        
        self.tableSearchUsers.reloadData()
        
        return true
    }
}
