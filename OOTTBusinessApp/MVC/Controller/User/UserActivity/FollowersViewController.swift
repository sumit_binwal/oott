//
//  FollowersViewController.swift
//  OOTTBusinessApp
//
//  Created by Bharat Kumar Pathak on 10/07/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit
import PKHUD
import DZNEmptyDataSet

class FollowersViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, FollowersCellDelegate, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    
    @IBOutlet weak var tableMyActivity: UITableView!
    
    var arrayMyActivityList = [ModelMyActivity]()
    
    var refreshControl: UIRefreshControl!
    
    var currentPage = 0
    var limit = 0
    
    
    var isPageRefreshing = true
    var isEndOfResult = false
    
    var isViewLoadedd = false
    
    var didDisappear = false

    // MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        setupTableview()
        
//        getMyActivityListApi(onRefresh: false, shouldShowHUD: true)
        applyRefreshControl()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    deinit {
        print("FollowersViewController deinit")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !isViewLoadedd {
            getMyActivityListApi(onRefresh: true, shouldShowHUD: true)
            isViewLoadedd = true
        }
        else {
            if didDisappear {
                didDisappear = false
                getMyActivityListApi(onRefresh: true, shouldShowHUD: false)
            }
        }
    }
    
    func viewAboutToBeSelected()
    {
        getMyActivityListApi(onRefresh: true, shouldShowHUD: false)
    }
    
    func setupView()
    {
        
    }
    
    func setupTableview()
    {
        tableMyActivity.delegate = self
        tableMyActivity.dataSource = self
        tableMyActivity.emptyDataSetSource = self
        tableMyActivity.emptyDataSetDelegate = self
    }
    
    // MARK:- Refresh Control
    func applyRefreshControl() {
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.refreshPage), for: UIControlEvents.valueChanged)
        self.tableMyActivity.addSubview(refreshControl)
    }
    
    func refreshPage(sender:AnyObject) {
        currentPage = 0
        isPageRefreshing = true
        getMyActivityListApi(onRefresh: true, shouldShowHUD: true)
    }
    
    // MARK:- UITableView Delegate and Datasource Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayMyActivityList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if arrayMyActivityList[indexPath.row].isMakeMoveInvitation || arrayMyActivityList[indexPath.row].isFollowRequestActivity
        {
            return 121 * scaleFactorX
        }
        if arrayMyActivityList[indexPath.row].isCheckinCommentActivity {
            return UITableViewAutomaticDimension
        }
        return 78 * scaleFactorX
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell : FollowersCell!
        
        var index = 0
        var cellIdentifier = kCellIdentifier_followers_cell
        
        if arrayMyActivityList[indexPath.row].isMakeMoveInvitation || arrayMyActivityList[indexPath.row].isFollowRequestActivity
        {
            index = 1
            cellIdentifier = kCellIdentifier_followers_cell_MakeMove
        }
        
        if let cellOther : FollowersCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? FollowersCell
        {
            cell = cellOther
        }
        else
        {
            if let cellOther : FollowersCell = Bundle.main.loadNibNamed("FollowersCell", owner: self, options: nil)?[index] as? FollowersCell
            {
                cell = cellOther
            }
        }
        
        cell.cellDelegate = self
        cell.activityDelegate = self
        cell.tag = indexPath.row
        cell.updateCell(usingModel: arrayMyActivityList[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if arrayMyActivityList[indexPath.row].isCheckinCommentActivity {
            // go to comment screen
            
            didDisappear = true
            
            let commentListViewController = UIStoryboard.getUserFriendsStoryboard().instantiateViewController(withIdentifier: "commentsListingViewController") as! CommentsListingViewController
            commentListViewController.hidesBottomBarWhenPushed = true
            commentListViewController.isCheckingOwnProfile = true
            commentListViewController.checkInID = arrayMyActivityList[indexPath.row].checkinID
            self.navigationController?.pushViewController(commentListViewController, animated: true)
        }
        else if arrayMyActivityList[indexPath.row].isCheckinLikeActivity {
            
            didDisappear = true
            
            let likeVC = UIStoryboard.getUserHomeStoryboard().instantiateViewController(withIdentifier: "groupLikesViewController") as! GroupLikesViewController
            likeVC.viewType = .statusLikes
            
            likeVC.hidesBottomBarWhenPushed = true
            
            likeVC.userID = arrayMyActivityList[indexPath.row].checkinID
            
            self.navigationController?.pushViewController(likeVC, animated: true)
        }
    }
    
    //MARK:- DZNEmptyDataSetSource -> Title
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        
        let text = "No Activity"
        
        let attributes = [NSFontAttributeName:UIFont .boldSystemFont(ofSize: 16*scaleFactorX), NSForegroundColorAttributeName:UIColor.white]
        
        return NSAttributedString (string: text, attributes: attributes)
    }
    
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return true
    }

    // MARK:- FollowersCellDelegate -> Follow Button
    
    func followersCell(cell: FollowersCell, didTapOnFollowUnfollowButton followUnfollowButton: UIButton) {
        followUnfollowButton.isUserInteractionEnabled = false
        
        let index = cell.tag
        
        let userID = arrayMyActivityList[index].otherUser?.userID!
        
        if arrayMyActivityList[index].isFollowed {
            performUnfollowUserApi(forUserID: userID!, actionsCell: cell)
        } else {
            performFollowUserApi(forUserID: userID!, actionsCell: cell)
        }
    }
    
    // MARK:- FollowersCellDelegate -> Accept Invite Button
    func followersCell(cell: FollowersCell, didTapOnAcceptIniviteButton inivteButton: UIButton) {
        
        if arrayMyActivityList[cell.tag].isMakeMoveInvitation {
            acceptIniviteRequest(onCell: cell)
        }
        else
        {
            acceptFollowRequest(onCell: cell)
        }
    }
    
    // MARK:- FollowersCellDelegate -> Decline Invite Button
    func followersCell(cell: FollowersCell, didTapOnDeclineIniviteButton inivteButton: UIButton) {
        
        HUD.show(.systemActivity, onView: self.view.window)
        
        let urlToHit = arrayMyActivityList[cell.tag].isMakeMoveInvitation ? EndPoints.declineMakeMoveInvitation(UtilityClass.getUserSidData()!, arrayMyActivityList[cell.tag].activityID!).path : EndPoints.declineFollowRequest(UtilityClass.getUserSidData()!, (arrayMyActivityList[cell.tag].otherUser?.userID!)!).path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .get, parameters: nil)
        { [weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: error?.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            
            let responseDictionary = dictionary!
            let message = responseDictionary["message"] as! String
            let type = responseDictionary["type"] as! Bool
            
            if statusCode == 203
            {
                // show alert
                UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            if statusCode == 200
            {
                if type
                {
                    // success
                    self.arrayMyActivityList.remove(at: cell.tag)
                    self.tableMyActivity.reloadData()
                    //                    self.getMyActivityListApi(onRefresh: true, shouldShowHUD: true)
                    return
                }
                else
                {
                    // alert
                    UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
            }
        }
    }
    
    
    //MARK:-
    func performFollowUserApi(forUserID userID:String, actionsCell:FollowersCell) -> Void
    {
        UtilityClass.showNetworkActivityLoader(show: true)
        
        let urlToHit = EndPoints.followUser(UtilityClass.getUserSidData()!, userID).path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .get, parameters: nil)
        { [weak self] (data, dictionary, statusCode, error) in
            
            UtilityClass.showNetworkActivityLoader(show: false)
            
            guard let `self` = self else {return}
            
            actionsCell.followUnfollowButton.isUserInteractionEnabled = true
            
            if error != nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: error?.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            
            let responseDictionary = dictionary!
            let message = responseDictionary["message"] as! String
            let type = responseDictionary["type"] as! Bool
            
            if statusCode == 203
            {
                // show alert
                UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            if statusCode == 200
            {
                if type
                {
                    // success
                    self.getMyActivityListApi(onRefresh: true, shouldShowHUD: true)
//                    let index = actionsCell.tag
//                    let friendModel = self.arrayMyActivityList[index]
//                    friendModel.isFollowed = true
//                    self.arrayMyActivityList[index] = friendModel
//                    self.tableMyActivity.reloadRows(at: [IndexPath (row: index, section: 0)], with: .none)
                    return
                }
                else
                {
                    // alert
                    UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
            }
            
        }
    }
    
    //MARK:-
    func performUnfollowUserApi(forUserID userID:String, actionsCell:FollowersCell) -> Void
    {
        UtilityClass.showNetworkActivityLoader(show: true)
        
        let urlToHit = EndPoints.unfollowUser(UtilityClass.getUserSidData()!, userID).path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .get, parameters: nil)
        { [weak self] (data, dictionary, statusCode, error) in
            
            UtilityClass.showNetworkActivityLoader(show: false)
            
            guard let `self` = self else {return}
            
            actionsCell.followUnfollowButton.isUserInteractionEnabled = true
            
            if error != nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: error?.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            
            let responseDictionary = dictionary!
            let message = responseDictionary["message"] as! String
            let type = responseDictionary["type"] as! Bool
            
            if statusCode == 203
            {
                // show alert
                UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            if statusCode == 200
            {
                if type
                {
                    // success
                    let index = actionsCell.tag
                    let friendModel = self.arrayMyActivityList[index]
                    friendModel.isFollowed = false
                    self.arrayMyActivityList[index] = friendModel
                    self.tableMyActivity.reloadRows(at: [IndexPath (row: index, section: 0)], with: .none)
                    return
                }
                else
                {
                    // alert
                    UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
            }
            
        }
    }
    
    //MARK:- Get My Activity Listing Api
    func getMyActivityListApi(onRefresh: Bool,shouldShowHUD showHUD:Bool) -> Void
    {
        if showHUD
        {
            HUD.show(.systemActivity, onView: self.view.window)
        }
        
        let urlToHit = EndPoints.myActivityList(UtilityClass.getUserSidData()!).path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .get, parameters: nil)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else {return}
            
            self.refreshControl.endRefreshing()
            
            self.isPageRefreshing = false
            self.isEndOfResult = true
            
            if error != nil
            {
                self.currentPage = self.currentPage > 0 ? self.currentPage-1 : 0
                UtilityClass.showAlertWithTitle(title: App_Name, message: error?.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            let responseDictionary = dictionary!
            let message = responseDictionary["message"] as! String
            let type = responseDictionary["type"] as! Bool
            
            if statusCode == 203
            {
                self.currentPage = self.currentPage > 0 ? self.currentPage-1 : 0
                // show alert
//                UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            if statusCode == 200
            {
                if type
                {
                    // success
                    let dataArray = responseDictionary["data"] as! [[String:Any]] // As array
                    let dataLimit = responseDictionary["limit"] as! Int
                    if onRefresh {
                        self.currentPage = 0
                        self.arrayMyActivityList.removeAll()
                    }
                    if dataArray.count == dataLimit
                    {
                        self.isEndOfResult = false
                    }
                    self.updateModelArray(usingArray: dataArray)
                    return
                }
                else // No listing
                {
                    // alert
                    self.currentPage = self.currentPage > 0 ? self.currentPage-1 : 0
                    UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
            }
        }
    }
    
    //MARK:- Updating Model Array
    func updateModelArray(usingArray array : [[String:Any]]) -> Void
    {
        arrayMyActivityList.removeAll()
        
        let dataArray = array
        
        for dict in dataArray // Iterating dictionaries
        {
            let model = ModelMyActivity () // Model creation
            model.updateModel(usingDictionary: dict) // Updating model
            arrayMyActivityList.append(model) // Adding model to array
        }
        
        tableMyActivity.reloadData()
    }
    
    func acceptIniviteRequest(onCell cell: FollowersCell)
    {
        HUD.show(.systemActivity, onView: self.view.window)
        
        let params = [
            "business_id" : arrayMyActivityList[cell.tag].businessID,
            "group_id" : arrayMyActivityList[cell.tag].groupID
        ]
        
        let urlToHit = EndPoints.acceptMakeMoveInvitation(UtilityClass.getUserSidData()!).path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .post, parameters: params)
        { [weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: error?.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            
            let responseDictionary = dictionary!
            let message = responseDictionary["message"] as! String
            let type = responseDictionary["type"] as! Bool
            
            if statusCode == 203
            {
                // show alert
                UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            if statusCode == 200
            {
                if type
                {
                    // success
                    self.arrayMyActivityList.remove(at: cell.tag)
                    self.tableMyActivity.reloadData()
                    //                    self.getMyActivityListApi(onRefresh: true, shouldShowHUD: true)
                    return
                }
                else
                {
                    // alert
                    UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
            }
        }
    }
    
    func acceptFollowRequest(onCell cell: FollowersCell)
    {
        HUD.show(.systemActivity, onView: self.view.window)
        
        let urlToHit = EndPoints.acceptFollowRequest(UtilityClass.getUserSidData()!, (arrayMyActivityList[cell.tag].otherUser?.userID!)!).path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .get, parameters: nil)
        { [weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: error?.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            
            let responseDictionary = dictionary!
            let message = responseDictionary["message"] as! String
            let type = responseDictionary["type"] as! Bool
            
            if statusCode == 203
            {
                // show alert
                UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            if statusCode == 200
            {
                if type
                {
                    // success
                    self.arrayMyActivityList.remove(at: cell.tag)
                    self.tableMyActivity.reloadData()
                    self.getMyActivityListApi(onRefresh: true, shouldShowHUD: true)
                    return
                }
                else
                {
                    // alert
                    UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
            }
        }
    }
}

//MARK: - Extension -> FollowersActivityCellDelegate
extension FollowersViewController: FollowersActivityCellDelegate
{
    func followersCellDidTapOnImageView(_ cell: FollowersCell) {
        navigateToUserDetailPage(withIndex: cell.tag)
    }
    
    func navigateToUserDetailPage(withIndex index: Int)
    {
        didDisappear = true
        let profileViewController = UIStoryboard.getUserProfileStoryboard().instantiateViewController(withIdentifier: "userProfileViewController") as! UserProfileViewController
                profileViewController.targetUserID = arrayMyActivityList[index].otherUser?.userID
        profileViewController.isLoggedInUser = false
        profileViewController.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(profileViewController, animated: true)
    }
    
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        
//        if isEndOfResult
//        {
//            return
//        }
//        if tableMyActivity.contentOffset.y + tableMyActivity.frame.size.height >= tableMyActivity.contentSize.height
//        {
//            if !isPageRefreshing
//            {
//                isPageRefreshing = true
//                
//                currentPage+=1
//                
//                getMyActivityListApi(onRefresh: false)
//            }
//        }
//        
//    }
}
