//
//  MyGroupListViewController.swift
//  OOTTBusinessApp
//
//  Created by Santosh on 01/11/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit
import PKHUD
import DZNEmptyDataSet

//MARK:- makeMoveWithGroupDelegate Method Declaration
protocol MyGroupDelegate: class {
    func makeMoveWithGroup(cellIndex:Int)
}


class ModelMyGroup
{
    var groupID = ""
    var groupName = ""
    var groupPicture = ""
    var isInvitedToGroup = false
    var isAlreadyMoving = false
    
    init()
    {
        
    }
    
    func updateModel(usinfDictionary dictionary : [String:Any])
    {
        if let _id = dictionary["_id"] as? String
        {
            self.groupID = _id
        }
        
        if let name = dictionary["name"] as? String
        {
            self.groupName = name
        }
        
        if let picture = dictionary["picture"] as? String
        {
            self.groupPicture = picture
        }
        
        if let isInvited = dictionary["isInvited"] as? Bool
        {
            self.isInvitedToGroup = isInvited
        }
        
        if let isMoving = dictionary["isMoving"] as? Bool
        {
            self.isAlreadyMoving = isMoving
        }
    }
}

class MyGroupListViewController: UIViewController {
    
    @IBOutlet weak var tableMyGroups: UITableView!
    
    @IBOutlet var headerTableView: SearchBarView!
    @IBOutlet weak var searchTextField: UITextField!
    
    weak var groupDelegate : MyGroupDelegate?
    
    var isMakeMoveInvitation = false
    
    var isViewFavGroupMode = false
    
    var isSearchModeOn = false
    
    var searchedString = ""
    
    var arrayMyGroups = [ModelMyGroup]()
    
    var arrayMyGroupsTemp = [ModelMyGroup]()
    
    var userID : String?
    
    var businessID : String?
    var businessName : String?
    
    var selectedIndex : Int?
    
    //MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        setupNavigationView()
        setupTableview()
    }
    
    //MARK:-
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getMyGroupsListApi()
    }

    //MARK:-
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:-
    func setupNavigationView() {
        
        self.title = isViewFavGroupMode ? "Favorites" : isMakeMoveInvitation ? "Make Move On" : "Invite Friend To"
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        searchTextField.setPlaceholderColor(UIColor.init(RED: 184.0, GREEN: 187.0, BLUE: 190.0, ALPHA: 1.0))
        searchTextField.customize(withLeftIcon: #imageLiteral(resourceName: "searchIcon"))
        searchTextField.delegate = self
        searchTextField.placeholder = isViewFavGroupMode ? "Search" : "Select Group"
        self.navigationItem.titleView = headerTableView
        
        if isViewFavGroupMode {
            self.navigationItem.hidesBackButton = true
        }
        let rightBarButton = self.setRightBarButtonItem("cancel".uppercased())
        rightBarButton.action = #selector(onCancelAction)
        rightBarButton.target = self
    }
    
    //MARK:-
    func setupTableview()
    {
        tableMyGroups.delegate = self
        tableMyGroups.dataSource = self
        
        tableMyGroups.emptyDataSetDelegate = self
        tableMyGroups.emptyDataSetSource = self
    }
    
    //MARK:-
    func onCancelAction()
    {
        self.view.endEditing(true)
        if isViewFavGroupMode {
            self.navigationController?.popViewController(animated: true)
        }
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- UITableView Delegate and DataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearchModeOn
        {
            return arrayMyGroupsTemp.count
        }
        return arrayMyGroups.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 77 * scaleFactorX
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : SearchUserCell = tableView.dequeueReusableCell(withIdentifier: kCellIdentifier_SearchUserCell) as! SearchUserCell
        if isSearchModeOn
        {
            cell.updateCellForMyGroup(usingModel: arrayMyGroupsTemp[indexPath.row])
        }
        else
        {
            cell.updateCellForMyGroup(usingModel: arrayMyGroups[indexPath.row])
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if isViewFavGroupMode {
            if isSearchModeOn
            {
                let businessGroupModel = arrayMyGroupsTemp[indexPath.row]
                navigateToBusinessDetails(forBusinessId: businessGroupModel.groupID)
            }
            else
            {
                let businessGroupModel = arrayMyGroups[indexPath.row]
                navigateToBusinessDetails(forBusinessId: businessGroupModel.groupID)
            }
            return
        }
        
        let groupID = isSearchModeOn ? arrayMyGroupsTemp[indexPath.row].groupID : arrayMyGroups[indexPath.row].groupID

        if isMakeMoveInvitation
        {
            var isMoving = false
            if isSearchModeOn
            {
                isMoving = arrayMyGroupsTemp[indexPath.row].isAlreadyMoving
            }
            else
            {
                isMoving = arrayMyGroups[indexPath.row].isAlreadyMoving
            }
            
            if !isMoving
            {
                performGroupMakeMove(groupID)
            }
            else
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: "User is already making move on this group", onViewController: self, withButtonArray: nil, dismissHandler: nil)
            }
            
            return
        }
        var isInvited = false
        if isSearchModeOn
        {
            isInvited = arrayMyGroupsTemp[indexPath.row].isInvitedToGroup
        }
        else
        {
            isInvited = arrayMyGroups[indexPath.row].isInvitedToGroup
        }
        
        if !isInvited
        {
            performInviteToGroupAction(groupID)
        }
        else
        {
            UtilityClass.showAlertWithTitle(title: App_Name, message: "User is already a member of this group", onViewController: self, withButtonArray: nil, dismissHandler: nil)
        }
    }
    
    //MARK:- DZNEmptyDataSetSource -> Title
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        
        let text = isViewFavGroupMode ? "No Favorites" : "No groups found"
        
        let attributes = [NSFontAttributeName:UIFont .boldSystemFont(ofSize: 16*scaleFactorX), NSForegroundColorAttributeName:UIColor.white]
        
        return NSAttributedString (string: text, attributes: attributes)
    }
    
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
    
    //MARK:- Get My Groups List Api
    func getMyGroupsListApi()
    {
        HUD.show(.systemActivity, onView: self.view.window)
        
        let urlToHit = isViewFavGroupMode ? EndPoints.getFavouriteGroupList(UtilityClass.getUserSidData()!, userID!).path : EndPoints.myGroups(UtilityClass.getUserSidData()!).path
        
        
        let params =  isViewFavGroupMode ? nil : [
            "user_id" : userID as Any,
//            "canInvite" : isMakeMoveInvitation ? "0" : "1",
//            "canMove" : isMakeMoveInvitation ? "1" : "0"
            ] as [String : Any]
        
        let method = isViewFavGroupMode ? HttpMethodType.get : HttpMethodType.post
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: method, parameters: params)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else { return }
            
            if error != nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: error?.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            
            let responseDictionary = dictionary!
            let message = responseDictionary["message"] as! String
            let type = responseDictionary["type"] as! Bool
            
            if statusCode == 203
            {
                // show alert
                //                UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            if statusCode == 200
            {
                if type
                {
                    // success
                    let dataArray = responseDictionary["data"] as! [[String:Any]] // As array
                    self.updateModelArray(dataArray)
                    return
                }
                else
                {
                    // alert
                    UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
            }
        }
    }
    
    //MARK:- Update model using result array
    func updateModelArray(_ usingArray : [[String:Any]])
    {
        arrayMyGroups.removeAll()
        let dataArray = usingArray
        
        for dict in dataArray // Iterating dictionaries
        {
            let model =  ModelMyGroup() // Model creation
            model.updateModel(usinfDictionary: dict) // Updating model
            arrayMyGroups.append(model) // Adding model to array
        }
        tableMyGroups.reloadData()
    }
    
    //MARK:- Group make move
    func performGroupMakeMove(_ groupID : String)
    {
        HUD.show(.systemActivity, onView: self.view.window)
        
        let params = [
            "group_id" : groupID,
            "business_id" : businessID!
        ]
        
        let urlToHit = EndPoints.makeGroupMakeMove(UtilityClass.getUserSidData()!).path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .post, parameters: params)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else { return }
            
            if error != nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: error?.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            
            let responseDictionary = dictionary!
            let message = responseDictionary["message"] as! String
            let type = responseDictionary["type"] as! Bool
            
            if statusCode == 203
            {
                return
            }
            
            if statusCode == 200
            {
                if type
                {
                    //success
//                    UtilityClass.showAlertWithTitle(title: App_Name, message: <#T##String?#>, onViewController: <#T##UIViewController?#>, withButtonArray: <#T##[String]?#>, dismissHandler: <#T##((Int) -> ())?##((Int) -> ())?##(Int) -> ()#>)
                    self.makeMoveSocketNotify(toGroup: groupID)
                    self.onCancelAction()
                    self.groupDelegate?.makeMoveWithGroup(cellIndex: self.selectedIndex ?? 0)
//                    if self.selectedIndex != nil
//                    {
//                    self.groupDelegate?.makeMoveWithGroup(cellIndex: self.selectedIndex!)
//                    }
                    
                    
                    return
                }
                else
                {
                    // alert
                    UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
            }
            
        }
    }
    
    func makeMoveSocketNotify(toGroup: String) {
        if SocketManager.sharedInstance().isSocketConnected()
        {
            SocketManager.sharedInstance().sendGroupMessage(SocketModelSendGroupMessage (), messageDict: ["isInvitemove":true,"room_id": toGroup, "BusinessName": businessName ?? ""]) { result, statusCode in
                
            }
        }
    }
    
    //MARK:- Invite to group
    func performInviteToGroupAction(_ groupID : String)
    {
//        HUD.show(.systemActivity, onView: self.view.window)
        
        self.inviteUserSocketNotify(toGroup: groupID)
        
        return;
        
        let params = [
            "group_id" : groupID,
            "user_id" : userID!
        ]
        
        let urlToHit = EndPoints.inviteToGroup(UtilityClass.getUserSidData()!).path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .post, parameters: params)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else { return }
            
            if error != nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: error?.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            
            let responseDictionary = dictionary!
            let message = responseDictionary["message"] as! String
            let type = responseDictionary["type"] as! Bool
            
            if statusCode == 203
            {
                return
            }
            
            if statusCode == 200
            {
                if type
                {
                    //success
                    UtilityClass.showAlertWithTitle(title: App_Name, message: "Invitation Sent Successfully", onViewController: self, withButtonArray: nil, dismissHandler:
                        {[weak self] (buttonIndex) in
                            guard let `self` = self else {return}
                        self.inviteUserSocketNotify (toGroup: groupID)
                        self.onCancelAction()
                    })
                    
                    return
                }
                else
                {
                    // alert
                    UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
            }
            
        }
    }
    
    func inviteUserSocketNotify(toGroup: String) {
        if SocketManager.sharedInstance().isSocketConnected()
        {
            HUD.show(.systemActivity, onView: self.view.window)

            SocketManager.sharedInstance().sendGroupMessage(SocketModelSendGroupMessage (), messageDict: ["isAddedInGroup":true,"room_id": toGroup, "user_name": businessName ?? "", "user_id": userID!]) {[weak self] result, statusCode in
                HUD.hide()
                
                guard let `self` = self else { return }
                
                if statusCode == 200 {
                    //success
                    UtilityClass.showAlertWithTitle(title: App_Name, message: "Your invitation has been sent.", onViewController: self, withButtonArray: nil, dismissHandler:
                        {[weak self] (buttonIndex) in
                            guard let `self` = self else {return}
                            self.onCancelAction()
                    })
                }
            }
        }
    }
    
    //MARK:- Group Detail
    func navigateToBusinessDetails(forBusinessId businessID:String) {
        
        let detailsViewController = UIStoryboard.getBusinessDetailsStoryboard().instantiateViewController(withIdentifier: "businessDetailsViewController") as! BusinessDetailsViewController
        detailsViewController.businessId = businessID
        detailsViewController.isFromDetail = true
        detailsViewController.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(detailsViewController, animated: true)
    }
}

//MARK:-
//MARK:- extension
extension MyGroupListViewController : UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource, DZNEmptyDataSetDelegate, DZNEmptyDataSetSource
{
    //MARK:- UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        isSearchModeOn = false
        arrayMyGroupsTemp.removeAll()
        tableMyGroups.reloadData()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var newString : String?
        var finalCount = 0
        
        if string.characters.count == 0
        {
            if textField.text?.characters.count==0
            {
                finalCount = 0
                newString = textField.text
            }
            else
            {
                finalCount = textField.text!.characters.count - 1
                var newStr = textField.text! as NSString
                newStr = newStr.replacingCharacters(in: range, with: string) as NSString
                newString = newStr as String
            }
        }
        else
        {
            finalCount = textField.text!.characters.count + 1
            var newStr = textField.text! as NSString
            
            newStr = newStr.replacingCharacters(in: range, with: string) as NSString
            
            newString = newStr as String
        }
        
        searchedString = newString!
        if finalCount == 0
        {
            isSearchModeOn = false
        }
        else
        {
            isSearchModeOn = true
            
            arrayMyGroupsTemp = arrayMyGroups.filter { (model) -> Bool in
                
                let isAvailable = model.groupName.localizedCaseInsensitiveContains(newString!)
                
                if isAvailable
                {
                    return true
                }
                return false
            }
        }
        
        self.tableMyGroups.reloadData()
        
        return true
    }
}
