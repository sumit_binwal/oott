//
//  GroupLikesViewController.swift
//  OOTTBusinessApp
//
//  Created by Bharat Kumar Pathak on 12/07/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit
import PKHUD
import DZNEmptyDataSet

enum ViewType {
    case peopleLiked
    case friendsHere
    case followingList
    case followersList
    case membersOfGroup
    case statusLikes
    case statusCommentLike
}

class GroupLikesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, FollowingCellDelegate, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {

    @IBOutlet weak var tableGroupLikesList: UITableView!
    
    var arrayListing = [ModelFriendsListing]()
    var businessModel: ModelGroupListing!
    var refreshControl: UIRefreshControl!
    
    @IBOutlet var headerTableView: SearchBarView!

    
    // Business Case
    var businessID : String!
    var businessFriendCount : String!
    
    // User Case
    var userID: String!
    
    var commentID: String!

    
    var viewType = ViewType.peopleLiked
    
    var isBusinessGroup = false
    
    var pendingRequestWorkItem: DispatchWorkItem?
    
    var isFromCommentLikeCount = false
    
    var isUser = false
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var searchTextField: UITextField!
    
    var searchString = ""
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        if UtilityClass.getUserTypeData() == AppUserTypeEnum.user.rawValue {
            isUser = true
        }
        
        setupView()
        setupTableview()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        refreshPage(sender: self)
    }
    
    //MARK:- deinit
    deinit {
        print("GroupLikesViewController deinit")
    }
    
    //MARK:- Initial Setup View
    
    func setupView() -> Void
    {
        switch viewType {
        case .followersList,.followingList:
            break
        default:
            let leftBarButton = self.setLeftBarButtonItem(withImage: #imageLiteral(resourceName: "backButton"))
            leftBarButton.action = #selector(onBackButtonAction)
            leftBarButton.target = self
        }
        
        applyRefreshControl()
    }
    
    @IBAction func backButtonAction(_ sender: Any)
    {

                self.navigationController?.popViewController(animated: true)
       
    }
    
    @IBAction func searchButtonAction(_ sender: Any) {
        
        searchTextField.isHidden = false
        searchTextField.autocorrectionType = .no
        
        titleLabel.isHidden = true
        searchTextField.becomeFirstResponder()
    }
    
    func setupTableview()
    {
        tableGroupLikesList.delegate = self
        tableGroupLikesList.dataSource = self
        tableGroupLikesList.emptyDataSetSource = self
        tableGroupLikesList.emptyDataSetDelegate = self
    }
    
    func updateScreenTitle(usingLikesCount likesCount:Int)
    {
//        let titleSuffix = (likesCount == 1) ? "favorite" : "favorites"
        self.navigationItem.title = "favorites".uppercased()//(String(likesCount)+" "+titleSuffix).uppercased()
    }
    
    func onBackButtonAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK:- Refresh Control
    func applyRefreshControl() {
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.refreshPage), for: UIControlEvents.valueChanged)
        self.tableGroupLikesList.addSubview(refreshControl)
    }
    
    func setSearchBarOnNavigationBar()
    {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        searchTextField.setPlaceholderColor(UIColor.init(RED: 184.0, GREEN: 187.0, BLUE: 190.0, ALPHA: 1.0))
        searchTextField.customize(withLeftIcon: #imageLiteral(resourceName: "searchIcon"))
        searchTextField.delegate = self
        searchTextField.placeholder = "Search"
        self.navigationItem.titleView = headerTableView
        
        self.navigationItem.hidesBackButton = true
        
        let rightBarButton = self.setRightBarButtonItem("cancel".uppercased())
        rightBarButton.action = #selector(backButtonAction(_:))
        rightBarButton.target = self

    }
    
    
    func refreshPage(sender:AnyObject) {
        switch viewType {
        case .peopleLiked:
            self.navigationItem.title = "favorites".uppercased()
//            titleLabel.text = "favorites".uppercased()
        //    searchButton.isHidden = true
            getGroupLikesApi(forGroupId: businessID, onRefresh: true)
            
        case .friendsHere:
            self.navigationItem.title = "friends here".uppercased()
//            titleLabel.text = "friends here".uppercased()
          //  searchButton.isHidden = true
            getGroupFriendsApi(forGroupId: businessID, onRefresh: true)
            
        case .followingList:
//            self.navigationItem.title = "following".uppercased()
//            titleLabel.text = "following".uppercased()
            //searchButton.isHidden = false
            setSearchBarOnNavigationBar()
            
            getUserListApi()
            
        case .statusLikes:
            self.navigationItem.title = "likes".uppercased()
//            titleLabel.text = "likes".uppercased()
           // searchButton.isHidden = true
            getStatusLikesApi()
            
        case .statusCommentLike:
            self.navigationItem.title = "likes".uppercased()
            getStatusCommentLikesApi()
            
        case .membersOfGroup:
            self.navigationItem.title = "members".uppercased()
//            titleLabel.text = "members".uppercased()
         //   searchButton.isHidden = true
            getMembers()
        default:
//            self.navigationItem.title = "followers".uppercased()
            //titleLabel.text = "followers".uppercased()
            setSearchBarOnNavigationBar()
            getUserListApi()
            break
        }
        refreshControl.endRefreshing()
    }
    
    // MARK:- UITableView Delegate and Datasource Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayListing.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 162 * scaleFactorX
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: FollowingCell = tableView.dequeueReusableCell(withIdentifier: kCellIdentifier_following_cell) as! FollowingCell
        cell.tag = indexPath.row
        cell.cellDelegate = self
        cell.updateData(usingModel: arrayListing[indexPath.row])
        if !isUser {
            cell.followUnfollowButton.isHidden = true
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if viewType == .membersOfGroup {
            return
        }
        
        navigateToProfile(forUser: arrayListing[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        var editType:Bool = false
        if viewType == .membersOfGroup
        {
            if !isBusinessGroup
            {
                editType = true
                if UtilityClass.getUserIDData() == arrayListing[indexPath.row].friendID
                {
                    editType = false
                }
            }
        }
        else
        {
        editType = false
        }
        
        return editType
    }
    
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        
        if (editingStyle == .delete) {

            tableView.endEditing(true)
            self.performRemoveUserFromGroupApi(forUserID: arrayListing[indexPath.row].friendID!, cellIndexPath: indexPath)
            
        }
    }
    
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return "Remove"
    }

    //MARK:- DZNEmptyDataSetSource
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        
        var text = ""
        switch viewType {
        case .followersList:
            text = "You don't have any followers"
        case .followingList:
            text = "You are not following anyone"
        case .friendsHere:
            text = "None of your friends are here"
        case .membersOfGroup:
            text = "No members in group"
        case .statusLikes:
            text = "No likes on Status"
        default:
            text = "No favorites"
        }
        
        let attributes = [NSFontAttributeName:UIFont .boldSystemFont(ofSize: 16*scaleFactorX), NSForegroundColorAttributeName:UIColor.white]
        
        return NSAttributedString (string: text, attributes: attributes)
    }
    
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
    
    func navigateToProfile(forUser userModel:ModelFriendsListing) {
        if userModel.isSameUser
        {
            return
        }
        if userModel.isAccountDeleted {
            UtilityClass.showAlertWithTitle(title: "Profile Deactivated", message: "User has deactivated the profile.", onViewController: self, withButtonArray: nil, dismissHandler: nil)
            return
        }
        let profileViewController = UIStoryboard.getUserProfileStoryboard().instantiateViewController(withIdentifier: "userProfileViewController") as! UserProfileViewController
        profileViewController.targetUserID = userModel.friendID
        profileViewController.isLoggedInUser = false
        profileViewController.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(profileViewController, animated: true)
    }
    
    // MARK:- FollowingCell Delegate Methods
    
    func followingCell(cell: FollowingCell, didTapOnFollowUnfollowButton followUnfollowButton: UIButton) {
        
        let friendModel = arrayListing[cell.tag]
        
        if friendModel.isFollowed! {
            performUnfollowUserApi(forUserID: friendModel.friendID!, friendCell: cell)
        } else {
            performFollowUserApi(forUserID: friendModel.friendID!, friendCell: cell)
        }
    }
    
    //MARK:- People Liked Listing Api
    func getGroupLikesApi(forGroupId groupID:String, onRefresh: Bool) -> Void
    {
        HUD.show(.systemActivity, onView: self.view.window)
        
        let urlToHit = EndPoints.getGroupLikesList(UtilityClass.getUserSidData()!, groupID).path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .get, parameters: nil)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else { return }
            
            self.refreshControl.endRefreshing()
            
            if error != nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: error?.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            let responseDictionary = dictionary!
            let message = responseDictionary["message"] as! String
            let type = responseDictionary["type"] as! Bool
            
            if statusCode == 203
            {
                // show alert
//                UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                self.arrayListing.removeAll()
                self.tableGroupLikesList.reloadData()
                return
            }
            
            if statusCode == 200
            {
                if type
                {
                    // success
                    let dataArray = responseDictionary["data"] as! [String:Any] // As dict
                    let likeArray = dataArray["likes"] as! [[String:Any]]
                    if onRefresh {
                        self.arrayListing.removeAll()
                    }
                    self.updateModelArray(usingArray: likeArray)
                    return
                }
                else
                {
                    // alert
                    UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    self.arrayListing.removeAll()
                    self.tableGroupLikesList.reloadData()
                    return
                }
            }
        }
    }
    
    //MARK:- Friends Here Listing Api
    func getGroupFriendsApi(forGroupId groupID:String, onRefresh: Bool) -> Void
    {
        HUD.show(.systemActivity, onView: self.view.window)
        
        let urlToHit = EndPoints.getGroupFriendsList(UtilityClass.getUserSidData()!, groupID).path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .get, parameters: nil)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else { return }
            
            self.refreshControl.endRefreshing()
            
            if error != nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: error?.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            
            let responseDictionary = dictionary!
            let message = responseDictionary["message"] as! String
            let type = responseDictionary["type"] as! Bool
            
            if statusCode == 203
            {
                // show alert
                //                UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            if statusCode == 200
            {
                if type
                {
                    // success
                    let dataDict = responseDictionary["data"] as! [String:Any]  // As dict
                    let modeArray = dataDict["moves"] as! [[String:Any]]
                    if onRefresh {
                        self.arrayListing.removeAll()
                    }
                    self.updateModelArray(usingArray: modeArray)
                    return
                }
                else
                {
                    // alert
                    UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    self.arrayListing.removeAll()
                    self.tableGroupLikesList.reloadData()
                    return
                }
            }
        }
    }
    
    //MARK:- Followers/ Following List Api
    func getUserListApi()
    {
        HUD.show(.systemActivity, onView: self.view.window)
        
        var urlToHit = EndPoints.getFollowersList(UtilityClass.getUserSidData()!, userID).path
        
        if viewType == .followingList
        {
            urlToHit = EndPoints.getFollowingList(UtilityClass.getUserSidData()!, userID).path
        }
        
        let params = [
            "keyword" : searchString
        ]
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .post, parameters: params)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else { return }
            
            if error != nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: error?.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            
            let responseDictionary = dictionary!
            let message = responseDictionary["message"] as! String
            let type = responseDictionary["type"] as! Bool
            
            if statusCode == 203
            {
                return
            }
            
            if statusCode == 200
            {
                if type
                {
                    // success
                    let dataArray = responseDictionary["data"] as! [[String:Any]] // As array
                    print(dataArray)
                    self.updateModelArray(usingArray: dataArray)
                    return
                }
                else
                {
                    // alert
                    UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
            }
        }
    }
    
    //MARK:- Status likes List Api
    func getStatusLikesApi()
    {
        HUD.show(.systemActivity, onView: self.view.window)
        
        let urlToHit = EndPoints.getStatusLikes(UtilityClass.getUserSidData()!, userID).path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .get, parameters: nil)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else { return }
            
            if error != nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: error?.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            
            let responseDictionary = dictionary!
            let message = responseDictionary["message"] as! String
            let type = responseDictionary["type"] as! Bool
            
            if statusCode == 203
            {
                return
            }
            
            if statusCode == 200
            {
                if type
                {
                    // success
                    let dataArray = responseDictionary["data"] as! [[String:Any]] // As array
                    print(dataArray)
                    self.updateModelArray(usingArray: dataArray)
                    return
                }
                else
                {
                    // alert
                    UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
            }
        }
    }
    
    //MARK:- Comment Status likes List Api
    func getStatusCommentLikesApi()
    {
        HUD.show(.systemActivity, onView: self.view.window)
        
        var urlToHit = EndPoints.likeUserListCheckinComment(UtilityClass.getUserSidData()!, userID, commentID).path
        
//        urlToHit = urlToHit.appendingPathComponent(commentID)
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .get, parameters: nil)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else { return }
            
            if error != nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: error?.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            
            let responseDictionary = dictionary!
            let message = responseDictionary["message"] as! String
            let type = responseDictionary["type"] as! Bool
            
            if statusCode == 203
            {
                return
            }
            
            if statusCode == 200
            {
                if type
                {
                    // success
                    print(responseDictionary)
                    let dataArray = responseDictionary["results"] as! [[String:Any]] // As array
                    print(dataArray)
                    self.updateModelArray(usingArray: dataArray)
                    return
                }
                else
                {
                    // alert
                    UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
            }
        }
    }
    
    //MARK:- View Members in Group List Api
    func getMembers()
    {
        HUD.show(.systemActivity, onView: self.view.window)
        
        let urlToHit = EndPoints.viewMembersInGroup(UtilityClass.getUserSidData()!, userID).path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .get, parameters: nil)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else { return }
            
            if error != nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: error?.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            
            let responseDictionary = dictionary!
            let message = responseDictionary["message"] as! String
            let type = responseDictionary["type"] as! Bool
            
            if statusCode == 203
            {
                return
            }
            
            if statusCode == 200
            {
                if type
                {
                    // success
                    let dataDict = responseDictionary["data"] as! [String:Any] // As array
                    print(dataDict)
                    if let inviteeArray = dataDict["invitee"] as? [[String:Any]]
                    {
                        self.updateModelArray(usingArray: inviteeArray)
                    }
                    return
                }
                else
                {
                    // alert
                    UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
            }
        }
    }
    
    //MARK:- Updating Model Array
    func updateModelArray(usingArray array : [[String:Any]]) -> Void
    {
        arrayListing.removeAll()
        
        let dataArray = array
        
        for dict in dataArray // Iterating dictionaries
        {
            let model = ModelFriendsListing () // Model creation
            model.updateModel(usingDictionary: dict) // Updating model
            arrayListing.append(model) // Adding model to array
        }
//        updateScreenTitle(usingLikesCount: arrayListing.count)
        tableGroupLikesList.reloadData()
    }
    
    //MARK:-
    func performFollowUserApi(forUserID userID:String, friendCell:FollowingCell) -> Void
    {
        UtilityClass.showNetworkActivityLoader(show: true)
        
        let urlToHit = EndPoints.followUser(UtilityClass.getUserSidData()!, userID).path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .get, parameters: nil)
        { [weak self] (data, dictionary, statusCode, error) in
            
            UtilityClass.showNetworkActivityLoader(show: false)
           
            guard let `self` = self else { return }
            
            friendCell.followUnfollowButton.isUserInteractionEnabled = false
            
            if error != nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: error?.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            
            let responseDictionary = dictionary!
            let message = responseDictionary["message"] as! String
            let type = responseDictionary["type"] as! Bool
            
            if statusCode == 203
            {
                // show alert
                UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            if statusCode == 200
            {
                if type
                {
                    // success
                    friendCell.followUnfollowButton.isUserInteractionEnabled = true
                    self.refreshPage(sender: UIButton ())
//                    let index = friendCell.tag
//                    let friendModel = self.arrayListing[index]
//                    friendModel.isFollowed = true
//                    self.arrayListing[index] = friendModel
//                    self.tableGroupLikesList.reloadRows(at: [IndexPath (row: index, section: 0)], with: .none)
                    return
                }
                else
                {
                    // alert
                    UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
            }
            
        }
    }
    
    //MARK:-
    func performRemoveUserFromGroupApi(forUserID friendID:String, cellIndexPath:IndexPath) -> Void
    {
        UtilityClass.showNetworkActivityLoader(show: true)
        
        let urlToHit = EndPoints.leaveUserFromGroup(UtilityClass.getUserSidData()!,userID,friendID).path
        
        print(urlToHit)
        HUD.show(.systemActivity, onView: self.view.window)

        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .get, parameters: nil)
        { [weak self] (data, dictionary, statusCode, error) in
            
            UtilityClass.showNetworkActivityLoader(show: false)
            HUD.hide()

            guard let `self` = self else { return }
            
            //friendCell.followUnfollowButton.isUserInteractionEnabled = false
            
            if error != nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: error?.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            
            let responseDictionary = dictionary!
            let message = responseDictionary["message"] as! String
            let type = responseDictionary["type"] as! Bool
            
            if statusCode == 203
            {
                // show alert
                UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            if statusCode == 200
            {
                if type
                {
                    self.tableGroupLikesList.beginUpdates()
                    self.tableGroupLikesList.deleteRows(at: [cellIndexPath], with: .fade);
                    self.arrayListing.remove(at: cellIndexPath.row)
                    self.tableGroupLikesList.endUpdates()

                    
                    // success
                    //friendCell.followUnfollowButton.isUserInteractionEnabled = true
                    //self.refreshPage(sender: UIButton ())
                    //                    let index = friendCell.tag
                    //                    let friendModel = self.arrayListing[index]
                    //                    friendModel.isFollowed = true
                    //                    self.arrayListing[index] = friendModel
                    //                    self.tableGroupLikesList.reloadRows(at: [IndexPath (row: index, section: 0)], with: .none)
                    return
                }
                else
                {
                    // alert
                    UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
            }
            
        }
    }
    
    //MARK:-
    func performUnfollowUserApi(forUserID userID:String, friendCell:FollowingCell) -> Void
    {
        UtilityClass.showNetworkActivityLoader(show: true)
        
        let urlToHit = EndPoints.unfollowUser(UtilityClass.getUserSidData()!, userID).path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .get, parameters: nil)
        { [weak self] (data, dictionary, statusCode, error) in
            
            UtilityClass.showNetworkActivityLoader(show: false)
            
            guard let `self` = self else { return }
            
            friendCell.followUnfollowButton.isUserInteractionEnabled = false
            
            if error != nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: error?.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            
            let responseDictionary = dictionary!
            let message = responseDictionary["message"] as! String
            let type = responseDictionary["type"] as! Bool
            
            if statusCode == 203
            {
                // show alert
                UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            if statusCode == 200
            {
                if type
                {
                    // success
                    friendCell.followUnfollowButton.isUserInteractionEnabled = true
                    self.refreshPage(sender: UIButton ())
//                    let index = friendCell.tag
//                    let friendModel = self.arrayListing[index]
//                    friendModel.isFollowed = false
//                    self.arrayListing[index] = friendModel
//                    self.tableGroupLikesList.reloadRows(at: [IndexPath (row: index, section: 0)], with: .none)
                    return
                }
                else
                {
                    // alert
                    UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
            }
            
        }
    }

}

extension GroupLikesViewController : UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        let name = textField.text! as NSString
        
        // Cancel the currently pending item
        pendingRequestWorkItem?.cancel()
        
        // Wrap our request in a work item
        let requestWorkItem = DispatchWorkItem { [weak self] in
            
            self?.searchString = name as String
            // call api
            self?.getUserListApi()
        }
        
        // Save the new work item and execute it after 250 ms
        pendingRequestWorkItem = requestWorkItem
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(250), execute: requestWorkItem)
        
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        
        // Cancel the currently pending item
        pendingRequestWorkItem?.cancel()
        
        // Wrap our request in a work item
        let requestWorkItem = DispatchWorkItem { [weak self] in
            
            self?.searchString = ""
            // call api
            self?.getUserListApi()
        }
        
        // Save the new work item and execute it after 250 ms
        pendingRequestWorkItem = requestWorkItem
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(250), execute: requestWorkItem)
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        let  char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        
        if (isBackSpace == -92)
        {
            if textField.text?.count == 1
            {
                // Cancel the currently pending item
                pendingRequestWorkItem?.cancel()
                
                // Wrap our request in a work item
                let requestWorkItem = DispatchWorkItem { [weak self] in
                    
                    self?.searchString = ""
                    // call api
                    self?.getUserListApi()
                }
                
                // Save the new work item and execute it after 250 ms
                pendingRequestWorkItem = requestWorkItem
                DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(250), execute: requestWorkItem)
            }
        }
        
        var newString : String?
        
        if string.count == 0
        {
            if textField.text?.count == 0
            {
                newString = textField.text
            }
            else
            {
                var newStr = textField.text! as NSString
                newStr = newStr.replacingCharacters(in: range, with: string) as NSString
                newString = newStr as String
            }
        }
        else
        {
            var newStr = textField.text! as NSString
            
            newStr = newStr.replacingCharacters(in: range, with: string) as NSString
            
            newString = newStr as String
        }
        
        if (newString?.count)! >= 3
        {
            let name = newString! as NSString
            
            print("name = \(name)")
            
            // Cancel the currently pending item
            pendingRequestWorkItem?.cancel()
            
            // Wrap our request in a work item
            let requestWorkItem = DispatchWorkItem { [weak self] in
                
                self?.searchString = name as String
                // call api
                self?.getUserListApi()
            }
            
            // Save the new work item and execute it after 250 ms
            pendingRequestWorkItem = requestWorkItem
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(250), execute: requestWorkItem)
        }
        
        return true
    }
}

