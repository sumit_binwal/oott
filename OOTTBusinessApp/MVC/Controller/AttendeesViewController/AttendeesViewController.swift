//
//  AttendeesViewController.swift
//  SampleScreens
//
//  Created by Bharat Kumar Pathak on 04/07/17.
//  Copyright © 2017 Konstant. All rights reserved.
//

import UIKit
import PKHUD
class ModelAttendees
{
    var userID = ""
    var userName = ""
    var userFullName = ""
    var userPicture = ""
    
    init() {
        
    }
    
    func updateModel(usingDictionary dictionary : [String:Any])
    {
        if let id = dictionary["user_id"] as? String
        {
            self.userID = id
        }
        
        if let picture = dictionary["picture"] as? String
        {
            self.userPicture = picture
        }
        
        if let name = dictionary["name"] as? String
        {
            self.userFullName = name
        }
        
        if let username = dictionary["username"] as? String
        {
            self.userName = username
        }
    }
}

class AttendeesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var attendeeCountLabel: UILabel!
    @IBOutlet weak var attendeeDescLabel: UILabel!
   
    
    @IBOutlet weak var tableAttendeeList: UITableView!
    
    var arrayAttendees = [ModelAttendees]()
    
    var isViewMembers = true
    
    var chatRoomID = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpView()
        setupTableview()
        
        isViewMembers ? viewMembersApi() : viewMembersApi()
    }
    
    func setUpView() -> Void {
        let leftBarButton = self.setLeftBarButtonItem(withImage: #imageLiteral(resourceName: "backButton"))
        leftBarButton.action = #selector(onBackButtonAction)
        leftBarButton.target = self
        
        attendeeDescLabel.text = ""
        attendeeCountLabel.text = ""
        
        self.title = isViewMembers ? "Members" : "Move Meter"
        
    }
    
    func setupTableview(){
        tableAttendeeList.delegate = self
        tableAttendeeList.dataSource = self
    }

    func onBackButtonAction() -> Void
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    //Mark:- UITableView Delegate and Datasource Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayAttendees.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 78 * scaleFactorX
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: AttendeesTableViewCell = tableView.dequeueReusableCell(withIdentifier: "attendeesCell") as! AttendeesTableViewCell
        cell.updateCell(usingModel: arrayAttendees[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    //MARK:- View Members Api
    func viewMembersApi()
    {
        HUD.show(.systemActivity, onView: self.view.window)
        
        let urlToHit = EndPoints.viewMembersInGroup(UtilityClass.getUserSidData()!, chatRoomID).path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .get, parameters: nil)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 203
            {
                let responseDictionary = dictionary!
                guard (responseDictionary["message"] != nil) else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
                let messageStr = responseDictionary["message"] as! String
                
                UtilityClass.showAlertWithTitle(title: App_Name, message: messageStr, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let messageStr = responseDictionary["message"] as! String
                let type = responseDictionary["type"] as! Bool
                if type
                {
                    // Got the event, update view...
                    let dataDictionary = responseDictionary["data"] as! [String:Any]
                    self.updateModel(usingDictionary: dataDictionary)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: messageStr, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
            }
            
            
        }
    }
    
    func updateModel(usingDictionary dictionary : [String:Any])
    {
        if let inviteeArray = dictionary["invitee"] as? [[String:Any]]
        {
            for dict in inviteeArray // Iterating dictionaries
            {
                let model = ModelAttendees () // Model creation
                model.updateModel(usingDictionary: dict) // Updating model
                arrayAttendees.append(model) // Adding model to array
            }
        }
        
//        attendeeCountLabel.text = String (arrayAttendees.count)
        
        self.title = "\(arrayAttendees.count) Members"
//        attendeeDescLabel.text = isViewMembers ? "Members" : "Make Moves"
        
        tableAttendeeList.reloadData()
    }
}

