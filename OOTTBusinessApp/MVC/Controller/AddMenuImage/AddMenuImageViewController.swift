//
//  AddMenuImageViewController.swift
//  OOTTBusinessApp
//
//  Created by Santosh on 27/06/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit
import PKHUD

protocol AddMenuImageDelegate : class {
    func addMenuImageController(controller:AddMenuImageViewController, uploadedImageData imageData:[String:Any]) -> Void
}


class AddMenuImageViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    
    @IBOutlet weak var dividerLine: UIView!
    
    @IBOutlet weak var inputField: UITextField!
    
    @IBOutlet weak var containerMenuImage: UIView!
    
    @IBOutlet weak var imageviewMenu: UIImageView!
    
    weak var delegate : AddMenuImageDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.title = "ADD MENU";
        
        let backBarButton = self.setLeftBarButtonItem(withImage: #imageLiteral(resourceName: "backButton"))
        backBarButton.action = #selector(onBackButtonAction)
        backBarButton.target = self
        
        dividerLine.backgroundColor = color_dividerLine
        
        inputField.setPlaceholderColor(color_placeholderColor)
        
        inputField.placeholder = "Menu Title"
        
        inputField.text = ""
        
        addTapGesture()
    }
    
//    //MARK:- viewWillAppear
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        
//        self.showNavigationBar()
//    }
//    
//    //MARK:- viewWillDisappear
//    override func viewWillDisappear(_ animated: Bool) {
//        super.viewWillDisappear(animated)
//        
//        self.hideNavigationBar()
//    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Back button action
    func onBackButtonAction() -> Void {
        self.navigationController?.popViewController(animated: true)
    }

    func addTapGesture() -> Void
    {
        self.containerMenuImage.gestureRecognizers?.removeAll()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(onTapGesture(tapGesture:)))
        tapGesture.numberOfTapsRequired = 1
        self.containerMenuImage.addGestureRecognizer(tapGesture)
    }
    
    //MARK:- Tap Gesture Action
    func onTapGesture(tapGesture : UITapGestureRecognizer) -> Void
    {
        switch tapGesture.state {
        case .ended:
            
            self.view.endEditing(true)
            
            UtilityClass.showActionSheetWithTitle(title: "Add Menu Image", message: "Select from options", onViewController: self, withButtonArray: ["Camera", "Photos"])
            {[weak self] (buttonIndex) in
                guard let `self` = self else {return}
                if buttonIndex == 0
                {
                    self.openPhotoPickerForType(imageSelectType: .camera)
                }
                else
                {
                    self.openPhotoPickerForType(imageSelectType: .photos)
                }
            }
            
        default:
            break
        }
    }

    
    //MARK:- Show ImagePicker
    func openPhotoPickerForType(imageSelectType : ImageSelectType) -> Void
    {
        var isCamera = false
        
        if imageSelectType == ImageSelectType.camera
        {
            if !UIImagePickerController.isSourceTypeAvailable(.camera
                ) {
                UtilityClass.showAlertWithTitle(title: "This device does not have Camera. Try Photos!", message: nil, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            else
            {
                isCamera = true
            }
        }
        
        let imagePicker = UIImagePickerController.init()
        imagePicker.sourceType = isCamera ? .camera : .photoLibrary
        imagePicker.delegate = self
        
        imagePicker.navigationBar.isTranslucent = false
        imagePicker.navigationBar.barTintColor = UIColor.init(RED: 10, GREEN: 23, BLUE: 42, ALPHA: 0.6)
        imagePicker.navigationBar.tintColor = UIColor.white
        imagePicker.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
        
        
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    //MARK: - UIImagePickerControllerDelegate
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage
        {
            imageviewMenu.image = image
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func onDoneAction(_ sender: UIButton)
    {
        let isVerified = validateInputs()
        if isVerified
        {
            uploadMenuImage(withImage: imageviewMenu.image!)
        }
    }
    
    func validateInputs() -> Bool
    {
        if (inputField.text?.isEmptyString())!
        {
            UtilityClass.showAlertWithTitle(title: App_Name, message: "Please provide caption", onViewController: self, withButtonArray: nil, dismissHandler: nil)
            return false
        }
        if imageviewMenu.image == nil
        {
            UtilityClass.showAlertWithTitle(title: App_Name, message: "Please add menu image", onViewController: self, withButtonArray: nil, dismissHandler: nil)
            return false
        }
        return true
    }
    
    func uploadMenuImage(withImage image:UIImage) -> Void
    {
        self.view.endEditing(true)
        HUD.show(.systemActivity, onView: self.view.window)
        weak var weakSelf = self
        
        let urlToHit = EndPoints.addMenuImage(UtilityClass.getUserSidData()!).path
        
        let params = ["caption" : inputField.text!]
        
        
        let imageData = UIImageJPEGRepresentation(image, 0.7)!
        AppWebHandler.sharedInstance().uploadImages(fromURL: urlToHit, imagesArray: [ImageDataDict (data: imageData, name: "file")], otherParameters: params)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()

            guard let `self` = self else { return }
            
            if statusCode == 200
            {
                guard let delegateReference = self.delegate else
                {
                    self.onBackButtonAction()
                    return
                }
                
                let responseDictionary = dictionary!
                let dataDicitonary = responseDictionary["data"] as! [String:Any]
                
                delegateReference.addMenuImageController(controller: weakSelf!, uploadedImageData: dataDicitonary)
                
                self.onBackButtonAction()
            }
            else
            {
                if error != nil
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    
                    return
                }
                if statusCode == 203
                {
                    let responseDictionary = dictionary!
                    guard (responseDictionary["message"] != nil) else
                    {
                        UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                        return
                    }
                    let messageStr = responseDictionary["message"] as! String
                    
                    UtilityClass.showAlertWithTitle(title: App_Name, message: messageStr, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
            }
        }
    }
}
