//
//  ForgotPasswordViewController.swift
//  OOTTUserApp
//
//  Created by Santosh on 06/06/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit
import PKHUD

class ForgotPasswordViewController: UIViewController {

    //MARK:- Label header 1
    @IBOutlet weak var labelHeaderLine1: UILabel!
    
    //MARK:- Label header 2
    @IBOutlet weak var labelHeaderLine2: UILabel!
    
    //MARK:- Separator line
    @IBOutlet weak var bottomSeparatorLine: UIView!
    
    //MARK:- Input textfield
    @IBOutlet weak var inputTextField: UITextField!
    
    //MARK:- Default font size
    let fontSizeHeader : CGFloat = 16 * scaleFactorX
    let fontSizeTextField : CGFloat = 17 * scaleFactorX
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupView()
        // Do any additional setup after loading the view.
    }

    //MARK:- didReceiveMemoryWarning
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- viewDidLoad
    deinit {
        print("ForgotPasswordViewController Deinit")
    }
    
    //MARK:- viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.showNavigationBar()
    }
    
    //MARK:- viewWillDisappear
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.hideNavigationBar()
    }
    
    //MARK:- viewDidAppear
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.inputTextField.becomeFirstResponder()
    }
    
    //MARK:- Back button action
    func onBackButtonAction() -> Void {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Initial setup of view
    func setupView() -> Void
    {
        self.title = "FORGOT PASSWORD";
        
        let backBarButton = self.setLeftBarButtonItem(withImage: #imageLiteral(resourceName: "backButton")) 
        backBarButton.action = #selector(onBackButtonAction)
        backBarButton.target = self
        
        self.labelHeaderLine1.font = UIFont(name: FONT_PROXIMA_LIGHT, size: fontSizeHeader)
        
        self.labelHeaderLine2.font = UIFont(name: FONT_PROXIMA_LIGHT, size: fontSizeHeader)
        
        self.inputTextField.font = UIFont(name: FONT_PROXIMA_LIGHT, size: fontSizeTextField)
        
        self.inputTextField.setPlaceholderColor(color_placeholderColor)
        
        self.inputTextField.placeholder = "Email Address"
        self.inputTextField.text = ""
        
        self.bottomSeparatorLine.backgroundColor = color_dividerLine
    }
    
    //MARK:- Submit button action
    @IBAction func onSubmitAction(_ sender: UIButton)
    {
        if (inputTextField.text?.isValidEmailAddress())!
        {
            callForgotPasswordApi()
        }
        else
        {
            UtilityClass.showAlertWithTitle(title: "", message: "Provide valid email address", onViewController: self, withButtonArray: nil, dismissHandler: nil)
        }
    }
    
    //MARK:- Forgot Password Api
    func callForgotPasswordApi() -> Void
    {
        self.view.endEditing(true)
        
        HUD.show(.systemActivity, onView: self.view.window)
        
        let params = [LoginEnum.email.rawValue : inputTextField.text!]
        
        AppWebHandler.sharedInstance().fetchData(fromURL: EndPoints.forgotPassword.path, httpMethod: .post, parameters: params)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else {return}
            
            if statusCode == 203
            {
                let responseDictionary = dictionary!
                guard (responseDictionary["message"] != nil) else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
                let messageStr = responseDictionary["message"] as! String
                
                UtilityClass.showAlertWithTitle(title: App_Name, message: messageStr, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                guard (responseDictionary["message"] != nil) else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
                let messageStr = responseDictionary["message"] as! String
                
                let type = responseDictionary["type"] as! Bool
                
                if type
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: messageStr, onViewController: self, withButtonArray: nil, dismissHandler:
                        {[weak self] (buttonIndex) in
                            guard let `self` = self else {return}
                        self.onBackButtonAction()
                    })
                    return
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: messageStr, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
            }
            
            if error != nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
        }
    }
}
