//
//  CoverChargeViewController.swift
//  OOTTBusinessApp
//
//  Created by Santosh on 21/02/18.
//  Copyright © 2018 KonstantInfoSolutions. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

//MARK:-
//MARK:- Model Hours
private enum DayEnum : String {
    case monday = "Monday"
    case tuesday = "Tuesday"
    case wednesday = "Wednesday"
    case thursday = "Thursday"
    case friday = "Friday"
    case saturday = "Saturday"
    case sunday = "Sunday"
}

private class ModelCoverCharge
{
    var day : DayEnum
    var coverCharge : String?
    var isClosed : Bool
    
    init() {
        day = DayEnum.monday
        isClosed = false
        coverCharge = ""
    }
    
    deinit {
        print("ModelCoverCharge Deinit")
    }
    
    func isValidEntry() -> Bool {
        if !isClosed
        {
            if (coverCharge?.isEmpty)!
            {
                return false
            }
        }
        return true
    }
    
    func updateValues(usingString: String) {
        coverCharge = usingString
        
        if usingString.isEmptyString() {
            isClosed = true
        }
    }
}

private let kCellIdentifier_CoverChargeCell = "coverChargeCell"

//MARK:-
//MARK:- HoursViewDelegate
protocol CoverChargeCellDelegate : class {
    func coverChargeCell(cell:CoverChargeCell, didUpdateTextfieldWithString updatedString: String)
    func coverChargeCell(cell:CoverChargeCell, didTapOnClosed isClosed:Bool)
}

//MARK:- CoverChargeCell
class CoverChargeCell: UITableViewCell
{
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var containerCoverCharge: UIView!
    @IBOutlet weak var coverChargeField: UITextField!
    
    @IBOutlet weak var containerClosed: UIView!
    @IBOutlet weak var imageRadioClose: UIImageView!
    
    var isDayClosed = false
    
    
    weak var delegate : CoverChargeCellDelegate?
    
    var activeTextField : UITextField?
    
    //MARK:- deinit
    deinit {
        print("CoverChargeCell Deinit")
    }
    
    //MARK:- awakeFromNib
    override func awakeFromNib() {
        self.contentView.viewWithTag(100)?.backgroundColor = color_dividerLine
        self.containerCoverCharge.layer.cornerRadius = 5
        coverChargeField.delegate = self
        
        coverChargeField.setPlaceholderColor(UIColor (RED: 6, GREEN: 124, BLUE: 196, ALPHA: 1))
        
        addTapGestureOnClosedContainer()
    }
    
    //MARK:- Update Cell Using Model
    fileprivate func updateView(usingModel coverChargeModel:ModelCoverCharge)
    {
        dayLabel.text = coverChargeModel.day.rawValue
        coverChargeField.text = coverChargeModel.coverCharge
        isDayClosed = coverChargeModel.isClosed
        
        if isDayClosed
        {
            imageRadioClose.image = #imageLiteral(resourceName: "radioSelect")
            coverChargeField.isEnabled = false
        }
        else
        {
            imageRadioClose.image = #imageLiteral(resourceName: "radioDeselected")
            coverChargeField.isEnabled = true
        }
    }
    
    //MARK:- Tap Gesture Action
    func onTapGestureAction(gesture:UITapGestureRecognizer)
    {
        switch gesture.state {
        case .ended:
            onCloseContainerSelection()
        default:
            break
        }
    }
    
    //MARK:-
    func onCloseContainerSelection()  {
        isDayClosed = !isDayClosed
        
        coverChargeField.text = ""
        
        if isDayClosed
        {
            imageRadioClose.image = #imageLiteral(resourceName: "radioSelect")
            coverChargeField.isEnabled = false
        }
        else
        {
            imageRadioClose.image = #imageLiteral(resourceName: "radioDeselected")
            coverChargeField.isEnabled = true
        }
        
        guard let tempDelegate = delegate else {
            return
        }
        
        tempDelegate.coverChargeCell(cell: self, didTapOnClosed: isDayClosed)
    }
}

//MARK:-
//MARK:- CoverChargeCell extension
extension CoverChargeCell : UITextFieldDelegate
{
    func addTapGestureOnClosedContainer()
    {
        let tapGesture = UITapGestureRecognizer (target: self, action: #selector(onTapGestureAction(gesture:)))
        tapGesture.numberOfTapsRequired = 1
        containerClosed.addGestureRecognizer(tapGesture)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        // Username field, space not allowed
        if textField.tag == CellType.username.rawValue && string == " "
        {
            return false
        }
        
        var finalCount = 0
        var newString : String?
        
        if string.count == 0
        {
            if textField.text?.count==0
            {
                finalCount = 0
                newString = textField.text
            }
            else
            {
                finalCount = textField.text!.count - 1
                var newStr = textField.text! as NSString
                newStr = newStr.replacingCharacters(in: range, with: string) as NSString
                newString = newStr as String
            }
        }
        else
        {
            finalCount = textField.text!.count + 1
            var newStr = textField.text! as NSString
            
            newStr = newStr.replacingCharacters(in: range, with: string) as NSString
            
            newString = newStr as String
        }
        
        guard let tempDelegate = delegate else {
            return false
        }
        
        tempDelegate.coverChargeCell(cell: self, didUpdateTextfieldWithString: newString!)
        
        return true
    }
}


class CoverChargeViewController: UIViewController {
    
    
    @IBOutlet weak var tableCoverCharge: UITableView!
    
    fileprivate var arrayCoverChargeModel = [ModelCoverCharge]()
    
    weak var delegate : CoverChargeDelegate?
    
    var dictCoverCharge : [String:String]?
    
    var isFromDetailPage = false

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        setupView()
        showNavigationBar()
        setupTableview()
        dictCoverCharge == nil ? updateDummyModel() : updateModelWithLatestData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //MARK:- View setup
    func setupView()
    {
        self.title = "Cover charge".uppercased()
        self.setBackBarButton()
        if !isFromDetailPage {
            self.setDoneBarButton()
        }
    }
    
    //MARK:- Back Button Action
    func onBackButtonAction()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Done Button Action
    func onDoneButtonAction()
    {
        for model in arrayCoverChargeModel
        {
            if !model.isValidEntry() {
                UtilityClass.showAlertWithTitle(title: App_Name, message: "Provide valid cover charge", onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
        }
        
        var params : [String:String] = [String:String]()
        
        for model in arrayCoverChargeModel
        {
            params[model.day.rawValue] = model.coverCharge ?? ""
        }
        guard let tempDelegate = delegate else {
            onBackButtonAction()
            return
        }
        tempDelegate.coverChargeView(didTapOnDoneWithFinalObject: params)
        onBackButtonAction()
    }
    
    //MARK:- Tableview Setup
    func setupTableview()
    {
        self.tableCoverCharge.delegate = self
        self.tableCoverCharge.dataSource = self
    }
    
    //MARK:- Array Dummy Data
    func updateDummyModel()
    {
        for index in 0...6
        {
            let model = ModelCoverCharge ()
            
            switch index {
            case 0:
                model.day = .monday
            case 1:
                model.day = .tuesday
            case 2:
                model.day = .wednesday
            case 3:
                model.day = .thursday
            case 4:
                model.day = .friday
            case 5:
                model.day = .saturday
            default:
                model.day = .sunday
            }
            arrayCoverChargeModel.append(model)
        }
        tableCoverCharge.reloadData()
    }
    
    func updateModelWithLatestData()
    {
        for index in 0...6
        {
            let model = ModelCoverCharge ()
            
            switch index {
            case 0:
                model.day = .monday
//                model.updateValues(usingString: dictCoverCharge![model.day.rawValue]!)
            case 1:
                model.day = .tuesday
//                model.coverCharge = dictCoverCharge![model.day.rawValue]!
            case 2:
                model.day = .wednesday
//                model.coverCharge = dictCoverCharge![model.day.rawValue]!
            case 3:
                model.day = .thursday
//                model.coverCharge = dictCoverCharge![model.day.rawValue]!
            case 4:
                model.day = .friday
//                model.coverCharge = dictCoverCharge![model.day.rawValue]!
            case 5:
                model.day = .saturday
//                model.coverCharge = dictCoverCharge![model.day.rawValue]!
            default:
                model.day = .sunday
//                model.coverCharge = dictCoverCharge![model.day.rawValue]!
            }
            model.updateValues(usingString: dictCoverCharge![model.day.rawValue]!)
            arrayCoverChargeModel.append(model)
        }
        tableCoverCharge.reloadData()
    }
}


//MARK:-
//MARK:- extension
extension CoverChargeViewController : UITableViewDelegate, UITableViewDataSource
{
    //MARK:- UITableView Delegate and Datasource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayCoverChargeModel.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100 * scaleFactorX
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : CoverChargeCell = tableView.dequeueReusableCell(withIdentifier: kCellIdentifier_CoverChargeCell) as! CoverChargeCell
        
        cell.tag = indexPath.row
        
        cell.delegate = self
        
        cell.updateView(usingModel: arrayCoverChargeModel[indexPath.row])
        
        cell.isUserInteractionEnabled = !isFromDetailPage
        
        return cell
    }
}


//MARK:-
//MARK:- extension
extension CoverChargeViewController : CoverChargeCellDelegate
{
    //MARK:- Navigation bar buttons
    func setBackBarButton()
    {
        let buttonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "backButton"), style: .plain, target: self, action: #selector(onBackButtonAction))
        
        self.navigationItem.leftBarButtonItem = buttonItem
    }
    
    func setDoneBarButton()
    {
        let buttonItem = UIBarButtonItem (title: "Done", style: .plain, target: self, action: #selector(onDoneButtonAction))
        
        buttonItem.tintColor = UIColor (RED: 6, GREEN: 124, BLUE: 196, ALPHA: 1)
        
        self.navigationItem.rightBarButtonItem = buttonItem
    }
    
   
    func coverChargeCell(cell: CoverChargeCell, didTapOnClosed isClosed: Bool) {
        let index = cell.tag
        
        let model = arrayCoverChargeModel[index]
        model.coverCharge = ""
        model.isClosed = isClosed
        
        arrayCoverChargeModel[index] = model
    }
    
    func coverChargeCell(cell: CoverChargeCell, didUpdateTextfieldWithString updatedString: String) {
        let index = cell.tag
        let model = arrayCoverChargeModel[index]
        model.coverCharge = updatedString
        arrayCoverChargeModel[index] = model
    }
}

//MARK:-
//MARK:- Protocol -> CoverChargeDelegate
protocol CoverChargeDelegate : class
{
    func coverChargeView(didTapOnDoneWithFinalObject finalObject: [String:String])
}
