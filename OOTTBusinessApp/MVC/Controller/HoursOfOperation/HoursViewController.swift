//
//  HoursViewController.swift
//  OOTTBusinessApp
//
//  Created by Santosh on 14/07/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

private let kCellIdentifier_HoursCell = "hoursViewCell"

//MARK:-
//MARK:- HoursViewDelegate
protocol HoursViewCellDelegate : class {
    func hoursCell(cell:HoursViewCell, didChangeValue withString:String, textfieldTag:Int)
    func hoursCell(cell:HoursViewCell, didTapOnClosed isClosed:Bool)
}

//MARK:- HoursViewCell
class HoursViewCell: UITableViewCell
{
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var containerOpenTime: UIView!
    @IBOutlet weak var containerCloseTime: UIView!
    @IBOutlet weak var openTimeField: UITextField!
    @IBOutlet weak var closeTimeField: UITextField!
    
    @IBOutlet weak var dropArrowOpen: UIImageView!
    @IBOutlet weak var dropArrowClose: UIImageView!
    
    @IBOutlet weak var containerClosed: UIView!
    @IBOutlet weak var imageRadioClose: UIImageView!
    
    var isDayClosed = false
    
    
    weak var delegate : HoursViewCellDelegate?
    
    var activeTextField : UITextField?
    
    //MARK:- deinit
    deinit {
        print("HoursViewCell Deinit")
    }
    
    //MARK:- awakeFromNib
    override func awakeFromNib() {
        self.contentView.viewWithTag(100)?.backgroundColor = color_dividerLine
        self.containerOpenTime.layer.cornerRadius = 5
        self.containerCloseTime.layer.cornerRadius = 5
        
        dropArrowOpen.tintColor = UIColor (RED: 6, GREEN: 124, BLUE: 196, ALPHA: 1)
        dropArrowOpen.image = #imageLiteral(resourceName: "dropArrow").withRenderingMode(.alwaysTemplate)
        
        dropArrowClose.tintColor = dropArrowOpen.tintColor
        dropArrowClose.image = dropArrowOpen.image
        
        addTapGestureOnOpenContainer()
        addTapGestureOnCloseContainer()
        addTapGestureOnClosedContainer()
        
        let timePicker = getTimePicker()
        
        openTimeField.tag = 1001
        closeTimeField.tag = 1002
        
        openTimeField.delegate = self
        closeTimeField.delegate = self
        
        openTimeField.inputView = timePicker
        closeTimeField.inputView = timePicker
    }
    
    //MARK:- Update Cell Using Model
    fileprivate func updateView(usingModel hourModel:ModelHoursView)
    {
        dayLabel.text = hourModel.day.rawValue
        openTimeField.text = hourModel.openTime
        closeTimeField.text = hourModel.closeTime
        isDayClosed = hourModel.isClosed
        
        if isDayClosed
        {
            imageRadioClose.image = #imageLiteral(resourceName: "radioSelect")
            openTimeField.isEnabled = false
            closeTimeField.isEnabled = false
        }
        else
        {
            imageRadioClose.image = #imageLiteral(resourceName: "radioDeselected")
            openTimeField.isEnabled = true
            closeTimeField.isEnabled = true
        }
    }
    
    //MARK:- Tap Gesture Action
    func onTapGestureAction(gesture:UITapGestureRecognizer)
    {
        switch gesture.state {
        case .ended:
            
            if gesture.view == containerOpenTime
            {
                openTimeField.becomeFirstResponder()
                return
            }
            else if gesture.view == containerCloseTime
            {
                closeTimeField.becomeFirstResponder()
                return
            }
            onCloseContainerSelection()
            
        default:
            break
        }
    }
    
    //MARK:-
    func onCloseContainerSelection()  {
        isDayClosed = !isDayClosed
        
        openTimeField.text = ""
        closeTimeField.text = ""
        
        if isDayClosed
        {
            imageRadioClose.image = #imageLiteral(resourceName: "radioSelect")
            openTimeField.isEnabled = false
            closeTimeField.isEnabled = false
        }
        else
        {
            imageRadioClose.image = #imageLiteral(resourceName: "radioDeselected")
            openTimeField.isEnabled = true
            closeTimeField.isEnabled = true
        }
        
        guard let tempDelegate = delegate else {
            return
        }
        tempDelegate.hoursCell(cell: self, didTapOnClosed: isDayClosed)
    }
    
    //MARK:- Date Picker Action
    func onTimePickerValueChanged(timePicker:UIDatePicker)
    {
        activeTextField?.text = timePicker.date.getStringForFormat(format: "h:mm a")
        guard let tempDelegate = delegate else {
            return
        }
        tempDelegate.hoursCell(cell: self, didChangeValue: (activeTextField?.text)!, textfieldTag: (activeTextField?.tag)!)
    }
}

//MARK:-
//MARK:- HoursViewCell extension
extension HoursViewCell : UITextFieldDelegate
{
    //MARK:- Add Gesture on Views
    func addTapGestureOnOpenContainer()
    {
        let tapGesture = UITapGestureRecognizer (target: self, action: #selector(onTapGestureAction(gesture:)))
        tapGesture.numberOfTapsRequired = 1
        containerOpenTime.addGestureRecognizer(tapGesture)
    }
    
    func addTapGestureOnCloseContainer()
    {
        let tapGesture = UITapGestureRecognizer (target: self, action: #selector(onTapGestureAction(gesture:)))
        tapGesture.numberOfTapsRequired = 1
        containerCloseTime.addGestureRecognizer(tapGesture)
    }
    
    func addTapGestureOnClosedContainer()
    {
        let tapGesture = UITapGestureRecognizer (target: self, action: #selector(onTapGestureAction(gesture:)))
        tapGesture.numberOfTapsRequired = 1
        containerClosed.addGestureRecognizer(tapGesture)
    }
    
    //MARK:- TimePicker create
    func getTimePicker() -> UIDatePicker
    {
        let timePicker = UIDatePicker ()
        timePicker.datePickerMode = .time
        timePicker.addTarget(self, action: #selector(onTimePickerValueChanged(timePicker:)), for: .valueChanged)
        return timePicker
    }
    
    //MARK:- UITextField Delegate
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
//        if textField.isAskingCanBecomeFirstResponder {
//            return true
//        }
        activeTextField = textField
        
        if !(activeTextField?.text?.isEmpty)!
        {
            if let timePicker = activeTextField?.inputView as? UIDatePicker {
                timePicker.date = Date.getDate(fromString: (activeTextField?.text)!, usingFormat: "h:mm a")
            }
        }
        else
        {
            if let timePicker = activeTextField?.inputView as? UIDatePicker {
                activeTextField?.text = timePicker.date.getStringForFormat(format: "h:mm a")
                
                guard let tempDelegate = delegate else {
                    return true
                }
                tempDelegate.hoursCell(cell: self, didChangeValue: (activeTextField?.text)!, textfieldTag: (activeTextField?.tag)!)
            }
        }
        
        return true
    }
}

//MARK:-
//MARK:- Model Hours
private enum DayEnum : String {
    case monday = "Monday"
    case tuesday = "Tuesday"
    case wednesday = "Wednesday"
    case thursday = "Thursday"
    case friday = "Friday"
    case saturday = "Saturday"
    case sunday = "Sunday"
}

private class ModelHoursView
{
    var day : DayEnum
    var openTime : String?
    var closeTime : String?
    var isClosed : Bool
    
    init() {
        day = DayEnum.monday
        isClosed = false
        openTime = ""
        closeTime = ""
    }
    
    deinit {
        print("ModelHoursView Deinit")
    }
    
    func isValidEntry() -> Bool
    {
        if !isClosed
        {
            if (openTime?.isEmpty)! || (closeTime?.isEmpty)!
            {
                return false
            }
        }
        return true
    }
    
    func getProperObject() -> String {
        if isClosed
        {
            return ""
        }
        return openTime! + "-" + closeTime!
    }
    
    func getOpenTime(fromString combinedString:String) -> String
    {
        if combinedString.isEmptyString()
        {
            isClosed = true
        }
        return combinedString.components(separatedBy: "-").first!
    }
    
    func getCloseTime(fromString combinedString:String) -> String
    {
        if combinedString.isEmptyString()
        {
            isClosed = true
        }
        return combinedString.components(separatedBy: "-").last!
    }
}

//MARK:-
//MARK:- HoursViewController
class HoursViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    
    @IBOutlet weak var tableHoursView: UITableView!
    fileprivate var arrayHoursModel = [ModelHoursView]()
    
    weak var hoursDelegate : HoursViewDelegate?
    
    var dictHours : [String:String]?
    
    var isFromDetailPage = false
    
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupView()
        showNavigationBar()
        setupTableview()
        dictHours == nil ? updateDummyModel() : updateModelWithLatestData()
    }
    
    //MARK:- deinit
    deinit {
        print("HoursViewController Deinit")
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        self.showNavigationBar()
//    }
//    
//    override func viewWillDisappear(_ animated: Bool) {
//        super.viewWillDisappear(animated)
//        self.hideNavigationBar()
//    }

    //MARK:- didReceiveMemoryWarning
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- View setup
    func setupView()
    {
        self.title = "Hours".uppercased()
        self.setBackBarButton()
        if !isFromDetailPage {
            self.setDoneBarButton()
        }
    }
    
    //MARK:- Back Button Action
    func onBackButtonAction()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Done Button Action
    func onDoneButtonAction()
    {
        for model in arrayHoursModel
        {
            if !model.isValidEntry() {
                UtilityClass.showAlertWithTitle(title: App_Name, message: "Provide valid entries", onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
        }
        
        var params : [String:String] = [String:String]()
        
        for model in arrayHoursModel
        {
            params[model.day.rawValue] = model.getProperObject()
        }
        guard let delegate = hoursDelegate else {
            onBackButtonAction()
            return
        }
        delegate.hoursView(didTapOnDoneWithFinalObject: params)
        onBackButtonAction()
    }
    
    //MARK:- Tableview Setup
    func setupTableview()
    {
        self.tableHoursView.delegate = self
        self.tableHoursView.dataSource = self
    }
    
    //MARK:- Array Dummy Data
    func updateDummyModel()
    {
        for index in 0...6
        {
            let model = ModelHoursView ()
            
            switch index {
            case 0:
                model.day = .monday
            case 1:
                model.day = .tuesday
            case 2:
                model.day = .wednesday
            case 3:
                model.day = .thursday
            case 4:
                model.day = .friday
            case 5:
                model.day = .saturday
            default:
                model.day = .sunday
            }
            arrayHoursModel.append(model)
        }
    }
    
    func updateModelWithLatestData()
    {
        for index in 0...6
        {
            let model = ModelHoursView ()
            
            switch index {
            case 0:
                model.day = .monday
                model.openTime = model.getOpenTime(fromString: dictHours![model.day.rawValue]!)
                model.closeTime = model.getCloseTime(fromString: dictHours![model.day.rawValue]!)
            case 1:
                model.day = .tuesday
                model.openTime = model.getOpenTime(fromString: dictHours![model.day.rawValue]!)
                model.closeTime = model.getCloseTime(fromString: dictHours![model.day.rawValue]!)
            case 2:
                model.day = .wednesday
                model.openTime = model.getOpenTime(fromString: dictHours![model.day.rawValue]!)
                model.closeTime = model.getCloseTime(fromString: dictHours![model.day.rawValue]!)
            case 3:
                model.day = .thursday
                model.openTime = model.getOpenTime(fromString: dictHours![model.day.rawValue]!)
                model.closeTime = model.getCloseTime(fromString: dictHours![model.day.rawValue]!)
            case 4:
                model.day = .friday
                model.openTime = model.getOpenTime(fromString: dictHours![model.day.rawValue]!)
                model.closeTime = model.getCloseTime(fromString: dictHours![model.day.rawValue]!)
            case 5:
                model.day = .saturday
                model.openTime = model.getOpenTime(fromString: dictHours![model.day.rawValue]!)
                model.closeTime = model.getCloseTime(fromString: dictHours![model.day.rawValue]!)
            default:
                model.day = .sunday
                model.openTime = model.getOpenTime(fromString: dictHours![model.day.rawValue]!)
                model.closeTime = model.getCloseTime(fromString: dictHours![model.day.rawValue]!)
            }
            arrayHoursModel.append(model)
        }
    }
    
    //MARK:- UITableView Delegate and Datasource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayHoursModel.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100 * scaleFactorX
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : HoursViewCell = tableView.dequeueReusableCell(withIdentifier: kCellIdentifier_HoursCell) as! HoursViewCell
        
        cell.tag = indexPath.row
        
        cell.delegate = self
        
        cell.updateView(usingModel: arrayHoursModel[indexPath.row])
        
        cell.isUserInteractionEnabled = !isFromDetailPage
        
        return cell
    }
}

//MARK:-
//MARK:- HoursViewController extension
extension HoursViewController : HoursViewCellDelegate
{
    //MARK:- Navigation bar buttons
    func setBackBarButton()
    {
        let buttonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "backButton"), style: .plain, target: self, action: #selector(onBackButtonAction))
        
        self.navigationItem.leftBarButtonItem = buttonItem
    }
    
    func setDoneBarButton()
    {
        let buttonItem = UIBarButtonItem (title: "Done", style: .plain, target: self, action: #selector(onDoneButtonAction))
        
        buttonItem.tintColor = UIColor (RED: 6, GREEN: 124, BLUE: 196, ALPHA: 1)
        
        self.navigationItem.rightBarButtonItem = buttonItem
    }
    
    //MARK:- HoursViewDelegate -> Time changed
    func hoursCell(cell: HoursViewCell, didChangeValue withString: String, textfieldTag: Int) {
        let index = cell.tag
        
        let model = arrayHoursModel[index]
        
        if textfieldTag == 1001
        {
            model.openTime = withString
        }
        else
        {
            model.closeTime = withString
        }
        
        arrayHoursModel[index] = model
    }
    
    func hoursCell(cell: HoursViewCell, didTapOnClosed isClosed: Bool) {
        let index = cell.tag
        
        let model = arrayHoursModel[index]
        
        model.openTime = ""
        model.closeTime = ""
        model.isClosed = isClosed
        
        arrayHoursModel[index] = model
    }
}

//MARK:-
//MARK:- HoursViewController extension
protocol HoursViewDelegate : class
{
    func hoursView(didTapOnDoneWithFinalObject finalObject: [String:String])
}
