//
//  InsightsViewController.swift
//  OOTTBusinessApp
//
//  Created by Santosh on 15/01/18.
//  Copyright © 2018 KonstantInfoSolutions. All rights reserved.
//

import UIKit
import PKHUD

class AgeRangeModel {
    var ageRange: String? = "0"
    var agePercentage: CGFloat = 0
    
    func updateModel(usingDictionary dictionary: [String:Any]) {
        ageRange = dictionary["range"] as? String
        agePercentage = dictionary["value"] as! CGFloat
    }
}
class ModelInisightsView {
    var malePercentage: CGFloat = 0
    var femalePercentage: CGFloat = 0
    var ageRangeArray: [AgeRangeModel]?
    
    func updateModel(usingDictionary dictionary: [String:Any]) {
        if let genderDict = dictionary["gender"] as? [String:CGFloat] {
            malePercentage = genderDict["male"]!
            femalePercentage = genderDict["female"]!
        }
        if let ageRangeArr = dictionary["age"] as? [[String: Any]] {
            ageRangeArray = [AgeRangeModel] ()
            for dict in ageRangeArr {
                let model = AgeRangeModel ()
                model.updateModel(usingDictionary: dict)
                ageRangeArray?.append(model)
            }
        }
    }
}

class InsightsViewController: UIViewController {
    
    @IBOutlet weak var tableInsights: UITableView!
    
    var refreshControl: UIRefreshControl!
    
    var modelinsights = ModelInisightsView ()
    
    //MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupView()
        setupTableview()
        applyRefreshControl()
    }

    //MARK: -
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: -
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        getInsightsData()
    }
    
    // MARK:- Refresh Control
    func applyRefreshControl() {
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.refreshPage), for: UIControlEvents.valueChanged)
        tableInsights.addSubview(refreshControl)
    }
    
    //MARK: -
    func refreshPage(sender:AnyObject) {
        getInsightsData()
    }
    
    //MARK: -
    func setupView() {
        self.showNavigationBar()
        self.navigationItem.title = "insights".capitalized
    }
    
    //MARK: -
    func setupTableview() {
        tableInsights.delegate = self
        tableInsights.dataSource = self
    }
}


//MARK: -
//MARK: - extension UITableViewDelegate
extension InsightsViewController: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return modelinsights.ageRangeArray != nil ? (modelinsights.ageRangeArray?.count)! + 3 : 3;
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 || indexPath.row == 2 {
            return 74
        }
        if indexPath.row == 1 {
            return 168 * scaleFactorX
        }
        return 33 * scaleFactorX
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 || indexPath.row == 2 {
            let header: InsightsHeaderCell = tableView.dequeueReusableCell(withIdentifier: kCellIdentifier_InsightsHeaderCell) as! InsightsHeaderCell
            
            header.updateCellContents(forRow: indexPath.row)
            
            return header
        }
        if indexPath.row == 1 {
            let cell: InsightsGenderCell = tableView.dequeueReusableCell(withIdentifier: kCellIdentifier_InsightsGenderCell) as! InsightsGenderCell
            cell.updateCell(usingModel: modelinsights)
            return cell
        }
        
        let cell: InsightsAgeRangeCell = tableView.dequeueReusableCell(withIdentifier: kCellIdentifier_InsightsAgeRangeCell) as! InsightsAgeRangeCell
        cell.updateCell(usingModel: modelinsights.ageRangeArray![indexPath.row - 3])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

//MARK: -
//MARK: - extension -> Insights Api
extension InsightsViewController
{
    func getInsightsData() {
        
        self.refreshControl.endRefreshing()
        
        HUD.show(.systemActivity, onView: self.view.window)
        
        let urlToHit = EndPoints.getInsights(UtilityClass.getUserSidData()!, UtilityClass.getUserIDData()!).path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .get, parameters: nil)
        {[weak self] (data, dictionary, statusCode, error) in
            HUD.hide()
            
            guard let `self` = self else { return }
            
            if error != nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: error?.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            let responseDictionary = dictionary!
            let message = responseDictionary["message"] as! String
            let type = responseDictionary["type"] as! Bool
            
            if statusCode == 203
            {
                return
            }
            
            if statusCode == 200
            {
                if type
                {
                    // success
                    let dataDict = responseDictionary["data"] as! [String:Any]// As dict
                    self.updateModel(usingDictionary: dataDict)
                    return
                }
                else
                {
                    // alert
                    UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
            }
        }
    }
    
    func updateModel(usingDictionary dictionary: [String:Any]) {
        modelinsights.updateModel(usingDictionary: dictionary)
        tableInsights.reloadData()
    }
}
