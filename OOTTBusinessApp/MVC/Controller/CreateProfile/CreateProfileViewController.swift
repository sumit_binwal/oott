//
//  CreateProfileViewController.swift
//  OOTTBusinessApp
//
//  Created by Santosh on 09/06/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit
import PKHUD
import SDWebImage

//MARK:-
//MARK:- CreateProfileViewController
class CreateProfileViewController: UIViewController,UITableViewDelegate,UITableViewDataSource, UIImagePickerControllerDelegate,UITextViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UINavigationControllerDelegate, CreateProfileCellDelegate, AddMenuImageDelegate, UICollectionViewDataSourcePrefetching {
    //MARK: Table View Header
    @IBOutlet var headerView: UIView!
    
    //MARK: Table View Footer
    @IBOutlet var footerView: UIView!
    
    //MARK: Cover Image
    @IBOutlet var btnCoverImg: UIButton!
    
    //MARK: Profile Image
    @IBOutlet var btnProfileImg: UIButton!
    
    //Menu Listing Image Array
    var menuListingArray = [PhotoData]()
    
    //Uploaded Images Array
    var uploadedImagesArray = [PhotoData]()
    
    //MARK: Array Holding Values
    var arrayModelCreateProfile : [ModelCreateProfile] = []
    
    //MARK: Table View
    @IBOutlet var tableViewCreateProfile: UITableView!
    
    var isFromSettings = false
    
    var arrayRadiusValue = ["Small", "Medium", "Large"]
    
    
    //MARK: Default font size
    let fontSizeHeader : CGFloat = 16 * scaleFactorX
    let fontSizeTextField : CGFloat = 17 * scaleFactorX
    
    var isCoverImageAdded = false
    var isProfilePictureAdded = false
    
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.setupView()
        self.loadDummyData()
        self.setupTableView()
        self.setupTableHeaderView()
        self.setupTableFooterView()
        
        if isFromSettings
        {
            getProfileDataApi()
        }
        
        // Do any additional setup after loading the view.
    }
    
    //MARK:- didReceiveMemoryWarning
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- deinit
    deinit {
        print("CreateProfileViewController Deinit")
    }
    
    //MARK:- Initial SetupView
    func setupView()
    {
        self.showNavigationBar()
        
        var array = ["Not Specified", "Female", "Male"]
        
        
        self.navigationItem.title="CREATE PROFILE"
        if isFromSettings
        {
            self.navigationItem.title="EDIT PROFILE"
            let backBarButton = self.setLeftBarButtonItem(withImage: #imageLiteral(resourceName: "backButton"))
            backBarButton.action=#selector(backButtonClicked)
            backBarButton.target=self
        }
    }
    
    //MARK:- Load Dummy Array Data
    func loadDummyData() -> Void
    {
        menuListingArray.append(PhotoData(image: #imageLiteral(resourceName: "addMenuIcon"), imageName: nil, isUploading: false))
        uploadedImagesArray.append(PhotoData(image: #imageLiteral(resourceName: "addMenuIcon"), imageName: nil, isUploading: false))
        
        arrayModelCreateProfile.append(ModelCreateProfile(withKey: CreateProfileEnum.businessName.rawValue, value: UtilityClass.getUserNameData()!))
        
        arrayModelCreateProfile.append(ModelCreateProfile(withKey: CreateProfileEnum.scene.rawValue, value: ""))
        
        //        arrayModelCreateProfile.append(ModelCreateProfile(withKey: CreateProfileEnum.about.rawValue, value: ""))
        
        arrayModelCreateProfile.append(ModelCreateProfile(withKey: CreateProfileEnum.contact.rawValue, value: ""))
        
        arrayModelCreateProfile.append(ModelCreateProfile(withKey: CreateProfileEnum.location.rawValue, value: ""))
        
        arrayModelCreateProfile.append(ModelCreateProfile(withKey: CreateProfileEnum.info.rawValue, value: ""))
        
        arrayModelCreateProfile.append(ModelCreateProfile(withKey: CreateProfileEnum.website.rawValue, value: ""))
        
        arrayModelCreateProfile.append(ModelCreateProfile(withKey: CreateProfileEnum.none.rawValue, value: ""))
        
        arrayModelCreateProfile.append(ModelCreateProfile(withKey: CreateProfileEnum.charges.rawValue, value: [String:String]()))
        
        arrayModelCreateProfile.append(ModelCreateProfile(withKey: CreateProfileEnum.dressCode.rawValue, value: ""))
        
        //        arrayModelCreateProfile.append(ModelCreateProfile(withKey: CreateProfileEnum.totalOccupancy.rawValue, value: ""))
        
        arrayModelCreateProfile.append(ModelCreateProfile(withKey: CreateProfileEnum.openTill.rawValue, value: [String:String]()))
        
        arrayModelCreateProfile.append(ModelCreateProfile(withKey: CreateProfileEnum.none.rawValue, value: ""))
        
        arrayModelCreateProfile.append(ModelCreateProfile(withKey: CreateProfileEnum.radius.rawValue, value: arrayRadiusValue[1]))
        
        
        let urlImage = URL (string: UtilityClass.getUserImageString()!)
        btnProfileImg.sd_setImage(with: urlImage!, for: .normal) {[weak self] (image, error, cache, url) in
            
            guard let `self` = self else {return}
            
            if image != nil
            {
                self.isProfilePictureAdded = true
            }
        }
        
    }
    
    //MARK:- Table setup
    func setupTableView() -> Void
    {
        self.tableViewCreateProfile.delegate=self;
        self.tableViewCreateProfile.dataSource = self;
    }
    
    //MARK:- Table Header setup
    func setupTableHeaderView() -> Void
    {
        headerView.frame = CGRect(x: 0, y: 0, width: tableViewCreateProfile.frame.size.width, height: headerView.frame.size.height * scaleFactorX)
        
        btnProfileImg.layer.cornerRadius=btnProfileImg.frame.size.width/2*scaleFactorX
        btnProfileImg.clipsToBounds=true;
        btnProfileImg.layer.borderColor=UIColor.init(red: 29/255, green: 158/255, blue: 240/255, alpha: 1).cgColor
        btnProfileImg.layer.borderWidth=1
        
        btnCoverImg.imageView?.contentMode = .scaleAspectFill
        btnProfileImg.imageView?.contentMode = .scaleAspectFill
        
        tableViewCreateProfile.tableHeaderView=headerView
        
        btnProfileImg.isAccessibilityElement = true
        btnProfileImg.accessibilityLabel = "Profile Image"
        
        btnCoverImg.isAccessibilityElement = true
        btnCoverImg.accessibilityLabel = "Cover Image"
    }
    
    //MARK:- Table Footer Setup
    func setupTableFooterView() -> Void
    {
        footerView.frame = CGRect(x: 0, y: 0, width: tableViewCreateProfile.frame.size.width * scaleFactorX, height: footerView.frame.size.height * scaleFactorX)
        
        tableViewCreateProfile.tableFooterView=footerView
    }
    
    //MARK:- Back Button
    func backButtonClicked()  {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Get Profile Api
    func getProfileDataApi() -> Void
    {
        HUD.show(.systemActivity, onView: self.view.window)
        
        let urlToHit = EndPoints.viewBusiness(UtilityClass.getUserSidData()!, UtilityClass.getUserIDData()!, String(LocationManager.sharedInstance().newLatitude), String(LocationManager.sharedInstance().newLongitude)).path
        
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .get, parameters: nil, shouldDeserialize: true)
        { [weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else {return}
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let type = responseDictionary["type"] as! Bool
                
                let dataDictionary = responseDictionary["data"] as! [String:Any]
                
                if type
                {
                    self.loadArray(withDictionary: dataDictionary)
                }
                return
            }
            else
            {
                if error != nil
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    
                    return
                }
                if statusCode == 203
                {
                    let responseDictionary = dictionary!
                    guard (responseDictionary["message"] != nil) else
                    {
                        UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                        return
                    }
                    let messageStr = responseDictionary["message"] as! String
                    
                    UtilityClass.showAlertWithTitle(title: App_Name, message: messageStr, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
            }
        }
    }
    
    //MARK:- Load View With Data
    func loadArray(withDictionary dictionary:[String:Any]) -> Void
    {
        let businessDict = dictionary
        
        for index in 0...arrayModelCreateProfile.count-1
        {
            let model = arrayModelCreateProfile[index]
            switch model.keyName
            {
                //            case CreateProfileEnum.about.rawValue:
            //                model.keyValue = businessDict[model.keyName] as! String
            case CreateProfileEnum.businessName.rawValue:
                model.keyValue = businessDict[model.keyName] as! String
            case CreateProfileEnum.radius.rawValue:
                var radiusValue = String()
                if businessDict[model.keyName] as! String == "50"
                {
                 radiusValue = "Small"
                }
                else if businessDict[model.keyName] as! String == "100"
                {
                    radiusValue = "Medium"
                }
                else if businessDict[model.keyName] as! String == "150"
                {
                    radiusValue = "Large"
                }
                
                model.keyValue = radiusValue
            case CreateProfileEnum.charges.rawValue:
                model.keyValue = businessDict[model.keyName] as! [String:String]
            case CreateProfileEnum.contact.rawValue:
                model.keyValue = businessDict[model.keyName] as! String
            case CreateProfileEnum.dressCode.rawValue:
                model.keyValue = businessDict[model.keyName] as! String
            case CreateProfileEnum.info.rawValue:
                model.keyValue = businessDict[model.keyName] as! String
            case CreateProfileEnum.website.rawValue:
                model.keyValue = businessDict[model.keyName] as? String ?? ""
            case CreateProfileEnum.location.rawValue:
                model.keyValue = businessDict[model.keyName] as! String
            case CreateProfileEnum.openTill.rawValue:
                model.keyValue = businessDict[model.keyName] as! [String:String]
                //            case CreateProfileEnum.totalOccupancy.rawValue:
                //                let newTotal = businessDict[model.keyName] as! Int
            //                model.keyValue = String(newTotal)
            case CreateProfileEnum.scene.rawValue:
                if businessDict["scene"] != nil
                {
                    let sceneArray = businessDict["scene"] as! [String]
                    if !sceneArray.isEmpty
                    {
                        var sceneString = ""
                        sceneArray.forEach({ (string) in
                            sceneString = sceneString.isEmpty ? string : sceneString.appending(","+string)
                        })
                        model.keyValue = sceneString
                    }
                }
            default:
                break
            }
        }
        
        if let latitudeDouble = businessDict["latitude"] as? Double
        {
            arrayModelCreateProfile.append(ModelCreateProfile (withKey: CreateProfileEnum.latitude.rawValue, value: String(latitudeDouble)))
        }
        
        if let longitudeDouble = businessDict["longitude"] as? Double
        {
            arrayModelCreateProfile.append(ModelCreateProfile (withKey: CreateProfileEnum.longitude.rawValue, value: String(longitudeDouble)))
        }
        
        if let menuArray = businessDict["menuImages"] as? [[String:String]]
        {
            if !menuArray.isEmpty
            {
                for imageDict in menuArray {
                    let photoData = PhotoData (image: nil, imageName: imageDict["caption"], isUploading: false)
                    photoData.imageID = imageDict["_id"]
                    photoData.imageURLString = imageDict["file_url"]
                    
                    menuListingArray.append(photoData)
                }
                //                menuArray.forEach({ (imageDict) in
                //                    let photoData = PhotoData (image: nil, imageName: imageDict["caption"], isUploading: false)
                //                    photoData.imageID = imageDict["_id"]
                //                    photoData.imageURLString = imageDict["file_url"]
                //
                //                    menuListingArray.append(photoData)
                //                })
            }
        }
        
        if let businessArray = businessDict["businessImages"] as? [[String:String]]
        {
            if !businessArray.isEmpty
            {
                for imageDict in businessArray
                {
                    let photoData = PhotoData (image: nil, imageName: imageDict["caption"], isUploading: false)
                    photoData.imageID = imageDict["_id"]
                    photoData.imageURLString = imageDict["file_url"]
                    
                    uploadedImagesArray.append(photoData)
                }
                //                businessArray.forEach({ (imageDict) in
                //                    let photoData = PhotoData (image: nil, imageName: imageDict["caption"], isUploading: false)
                //                    photoData.imageID = imageDict["_id"]
                //                    photoData.imageURLString = imageDict["file_url"]
                //
                //                    uploadedImagesArray.append(photoData)
                //                })
            }
        }
        
        let urlCoverImage = URL (string: businessDict["coverPicture"] as! String)
        if urlCoverImage != nil
        {
            btnCoverImg.sd_setBackgroundImage(with: urlCoverImage!, for: .normal, completed:
                {[weak self] (image, error, cache, url) in
                    guard let `self` = self else {return}
                    if image != nil
                    {
                        self.isCoverImageAdded = true
                    }
            })
        }
        
        let urlProfileImage = URL (string: businessDict["picture"] as! String)
        
        if urlProfileImage != nil
        {
            btnProfileImg.sd_setImage(with: urlProfileImage!, for: .normal, completed:
                {[weak self] (image, error, cache, url) in
                    guard let `self` = self else {return}
                    if image != nil
                    {
                        self.isProfilePictureAdded = true
                    }
            })
        }
        
        tableViewCreateProfile.reloadData()
    }
    
    //MARK:- UICollectionView Delegate
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag==10
        {
            return uploadedImagesArray.count
        }
        else
            
        {
            return menuListingArray.count
            
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        var cellIdentifier=""
        
        switch indexPath.row {
        case 0:
            cellIdentifier=kCellIdentifier_add_image
        default:
            cellIdentifier=kCellIdentifier_uploaded_image
        }
        
        let cell = collectionView .dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! CreateProfileCollectionVwCell
        if collectionView.tag==10
        {
            cell.labelCellTitle.text = "Add photos"
            cell.labelCellTitle.isHidden = false
            if indexPath.row != 0
            {
                cell.labelCellTitle.isHidden=true
                
                if uploadedImagesArray[indexPath.item].isInProgress
                {
                    cell.imageUploadingProperties()
                }
                else
                {
                    if uploadedImagesArray[indexPath.item].isImageUploadFailed
                    {
                        // retry
                        cell.imageUploadFailProperties()
                    }
                    else
                    {
                        // done
                        
                        cell.buttonMenuImages.sd_setImage(with: URL (string: uploadedImagesArray[indexPath.item].imageURLString!), for: .normal, completed: nil)
                        cell.imageUploadSuccessProperties()
                    }
                }
            }
        }
        else // Menu image
        {
            cell.labelCellTitle.text = "Add Menu"
            if indexPath.item != 0 {
                
                let photoData = menuListingArray[indexPath.item]
                
                cell.buttonMenuImages.sd_setImage(with: URL (string: photoData.imageURLString!), for: .normal, completed: nil)
                
                cell.labelCellTitle.text = photoData.imageName
                
                cell.imageUploadSuccessProperties()
            }
        }
        
        if indexPath.item != 0
        {
            cell.buttonDeleteImage.tag = indexPath.item
            cell.buttonDeleteImage.addTarget(self, action: #selector(onDeleteImageAction(sender:)), for: .touchUpInside)
            print("delete button tag is: %d",cell.buttonDeleteImage.tag)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        //        weak var weakSelf = self
        
        if collectionView.tag==10
        {
            if indexPath.row==0
            {
                UtilityClass.showActionSheetWithTitle(title: "Add Business Photos", message: "Select from options", onViewController: self, withButtonArray: ["Camera", "Photos"])
                {[weak self] (buttonIndex) in
                    
                    guard let `self` = self else {return}
                    
                    if buttonIndex == 0
                    {
                        print(indexPath.row)
                        self.openPhotoPickerForType(imageSelectType: .camera, imageTypeIndex: collectionView.tag)
                    }
                    else
                    {
                        self.openPhotoPickerForType(imageSelectType: .photos, imageTypeIndex: collectionView.tag)
                        
                    }
                }
            }
        }
        else
        {
            if indexPath.row==0
            {
                let addMenuVC = self.storyboard?.instantiateViewController(withIdentifier: "addMenuVC") as! AddMenuImageViewController
                addMenuVC.delegate = self
                self.navigationController?.pushViewController(addMenuVC, animated: true)
            }
        }
    }
    
    //MARK:- Show ImagePicker
    func openPhotoPickerForType(imageSelectType : ImageSelectType, imageTypeIndex:NSInteger) -> Void
    {
        var isCamera = false
        
        if imageSelectType == ImageSelectType.camera
        {
            if !UIImagePickerController.isSourceTypeAvailable(.camera
                ) {
                UtilityClass.showAlertWithTitle(title: "This device does not have Camera. Try Photos!", message: nil, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            else
            {
                isCamera = true
            }
        }
        
        let imagePicker = UIImagePickerController.init()
        if imageTypeIndex == 201
        {
            imagePicker.allowsEditing = true
        }
        imagePicker.sourceType = isCamera ? .camera : .photoLibrary
        imagePicker.delegate = self
        imagePicker.view.tag=imageTypeIndex
        
        imagePicker.navigationBar.isTranslucent = false
        imagePicker.navigationBar.barTintColor = UIColor.init(RED: 10, GREEN: 23, BLUE: 42, ALPHA: 0.6)
        imagePicker.navigationBar.tintColor = UIColor.white
        imagePicker.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
        
        
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    //MARK: - UIImagePickerControllerDelegate
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        picker.dismiss(animated: true, completion: nil)
        
        if let image = picker.view.tag == 201 ? info[UIImagePickerControllerEditedImage] as? UIImage : info[UIImagePickerControllerOriginalImage] as? UIImage
        {
            if picker.view.tag==10 {
                
                let indexPath = IndexPath(row: picker.view.tag, section: 0)
                let cell: CreateProfileCell = tableViewCreateProfile.cellForRow(at: indexPath) as! CreateProfileCell
                
                uploadedImagesArray.append(PhotoData (image: nil, imageName: "", isUploading: true))
                
                cell.collectionViewImages.insertItems(at: [IndexPath (item: 1, section: 0)])
                
                uploadBusinessImage(forIndex: uploadedImagesArray.count-1, imageToUpload: image, collection: cell.collectionViewImages)
                
                return
            }
            else
            {
                if picker.view.tag==200
                {
                    isCoverImageAdded = true
                    btnCoverImg.setBackgroundImage(image, for: .normal)
                }
                else
                {
                    isProfilePictureAdded = true
                    btnProfileImg.setImage(image, for: .normal)
                }
            }
        }
    }
    
    
    //MARK:- UITableView Delegate Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 12
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 160*scaleFactorX;
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            
            return 51 * scaleFactorX
            
        case 4:
            
            return UITableViewAutomaticDimension
            
        case 6,10:
            
            return 160*scaleFactorX;
            
        default:
            return 64 * scaleFactorX;
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell : CreateProfileCell
        
        var cellIdentifier = kCellIdentifier_create_profile
        
        var newCellType = ProfileCellType.none
        
        switch indexPath.row
        {
        case 0:
            newCellType = .businessName
            
        case 1:
            newCellType = .scenes
            
            //        case 2:
            //            cellIdentifier = kCellIdentifier_create_profile_textView
            //
            //            newCellType = .bio
            
        case 2:
            newCellType = .contact
            
        case 3:
            newCellType = .location
            
        case 4:
            newCellType = .genralInfo
            
            cellIdentifier = kCellIdentifier_create_profile_textView
        case 5:
            newCellType = .website
            
        case 6:
            newCellType = .menuListing
            cellIdentifier = kCellIdentifier_upload_image
            
        case 7:
            newCellType = .coverCharge
            
        case 8:
            newCellType = .dressCode
            
            //        case 9:
            //            newCellType = .totalOccupancy
            
        case 9:
            newCellType = .openTill
            
        case 10:
            newCellType = .uploadImage
            cellIdentifier = kCellIdentifier_upload_image
        case 11:
            newCellType = .radius
            
        default:
            break
        }
        
        cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! CreateProfileCell
        
        if indexPath.row==6||indexPath.row==10
        {
            cell.collectionViewImages.delegate=self
            cell.collectionViewImages.dataSource=self
            cell.collectionViewImages.prefetchDataSource=self
            cell.collectionViewImages.tag=indexPath.row
            cell.collectionViewImages.reloadData()
            cell.labelCellTitle.text = indexPath.row == 6 ? "Upload Menus" : "Upload Photos"
        }
        
        cell.cellType = newCellType;
        cell.profileDelegate = nil;
        cell.profileDelegate = self;
        
        if cellIdentifier !=  kCellIdentifier_upload_image
        {
            cell.updateValue(usingModel: arrayModelCreateProfile[indexPath.row])
        }
        
        return cell
    }
    
    //MARK:- Profile Image Button Click Action
    @IBAction func profileImageButtonClicked(_ sender: UIButton)
    {
        UtilityClass.showActionSheetWithTitle(title: "Add Profile Image", message: "Select from options", onViewController: self, withButtonArray: ["Camera", "Photos"])
        {[weak self] (buttonIndex : Int) in
            
            guard let `self` = self else {return}
            
            if buttonIndex == 0
            {
                self.openPhotoPickerForType(imageSelectType: .camera, imageTypeIndex: sender.tag)
            }
            else
            {
                self.openPhotoPickerForType(imageSelectType: .photos, imageTypeIndex: sender.tag)
                
            }
        }
    }
    
    //MARK:- Cover Image Button Click Action
    @IBAction func coverImageButtonClicked(_ sender: UIButton) {
        
        UtilityClass.showActionSheetWithTitle(title: "Add Cover Image", message: "Select from options", onViewController: self, withButtonArray: ["Camera", "Photos"])
        {[weak self] (buttonIndex : Int) in
            
            guard let `self` = self else {return}
            
            if buttonIndex == 0
            {
                self.openPhotoPickerForType(imageSelectType: .camera, imageTypeIndex: sender.tag)
            }
            else
            {
                self.openPhotoPickerForType(imageSelectType: .photos, imageTypeIndex: sender.tag)
            }
        }
    }
    
    //MARK:- CreateProfileCell Delegate -> Textfield updated Action
    func createProfileCell(cell: CreateProfileCell, updatedInputfieldText: Any) {
        
        if cell.model.keyName == CreateProfileEnum.radius.rawValue {
            let pickerview = UtilityClass.getNormalPickerView()
            cell.inputTextField.inputView = pickerview
            pickerview.delegate = nil
            pickerview.delegate = self;
            pickerview.reloadAllComponents()
            
            pickerview.selectRow(arrayRadiusValue.index(of: updatedInputfieldText as! String)!, inComponent: 0, animated: true)
            
//            cell.inputTextField.becomeFirstResponder()
            
            return;
        }
        
        if cell.model.keyName != CreateProfileEnum.none.rawValue
        {
            let isAlreadyContained = arrayModelCreateProfile.contains { (model) -> Bool in
                return model.keyName == cell.model.keyName
            }
            if !isAlreadyContained
            {
                arrayModelCreateProfile.append(cell.model)
            }
            else
            {
                let newModel = cell.model
                newModel.keyValue = updatedInputfieldText
                let index = arrayModelCreateProfile.index(where: { (model) -> Bool in
                    model.keyName == newModel.keyName
                })
                arrayModelCreateProfile[index!].keyValue = newModel.keyValue
                
                if cell.model.keyName == CreateProfileEnum.location.rawValue
                {
                    let isAlreadyContained = arrayModelCreateProfile.contains { (model) -> Bool in
                        return model.keyName == CreateProfileEnum.latitude.rawValue
                    }
                    
                    if !isAlreadyContained
                    {
                        arrayModelCreateProfile.append(ModelCreateProfile (withKey: CreateProfileEnum.latitude.rawValue, value: cell.model.latitude!))
                        arrayModelCreateProfile.append(ModelCreateProfile (withKey: CreateProfileEnum.longitude.rawValue, value: cell.model.longitude!))
                    }
                    else
                    {
                        let index = arrayModelCreateProfile.index(where: { (model) -> Bool in
                            model.keyName == CreateProfileEnum.latitude.rawValue
                        })
                        arrayModelCreateProfile[index!].keyValue = cell.model.latitude!
                        
                        let index1 = arrayModelCreateProfile.index(where: { (model) -> Bool in
                            model.keyName == CreateProfileEnum.longitude.rawValue
                        })
                        arrayModelCreateProfile[index1!].keyValue = cell.model.longitude!
                    }
                }
            }
        }
    }
    
    
    //MARK:- Business Image Upload
    func uploadBusinessImage(forIndex index:Int, imageToUpload image:UIImage, collection:UICollectionView) -> Void
    {
        let urlToHit = EndPoints.addBusinessImage(UtilityClass.getUserSidData()!).path
        
        let imageData = UIImageJPEGRepresentation(image, 0.7)!
        AppWebHandler.sharedInstance().uploadImages(fromURL: urlToHit, imagesArray: [ImageDataDict (data: imageData, name: "file")], otherParameters: nil)
        {[weak self] (data, dictionary, statusCode, error) in
            
            guard let `self` = self else {return}
            
            self.uploadedImagesArray[index].isInProgress = false
            
            if error != nil
            {
                self.uploadedImagesArray[index].isImageUploadFailed = true
            }
            
            if statusCode == 200
            {
                //weakSelf?.uploadedImagesArray[index].image = image
                self.uploadedImagesArray[index].isImageUploadFailed = false
                
                let responseDictionary = dictionary!
                let dataDict = responseDictionary["data"] as! [String:Any]
                
                self.uploadedImagesArray[index].imageID = dataDict["_id"] as? String
                self.uploadedImagesArray[index].imageURLString = dataDict["file_url"] as? String
            }
            else
            {
                self.uploadedImagesArray[index].isImageUploadFailed = true
            }
            collection.reloadData()
        }
    }
    
    //MARK:- Business Image Delete
    func onDeleteImageAction(sender:UIButton) -> Void
    {
        print("delete button")
        
        let index = sender.tag
        let collection = sender.superviewOfClassType(UICollectionView.self) as? UICollectionView
        
        if collection != nil
        {
            if collection!.tag == 10
            {
                if index <= uploadedImagesArray.count-1 {
                    sender.isUserInteractionEnabled = false
                    sender.superview?.isUserInteractionEnabled = false
                    let model = uploadedImagesArray[index]
                    deleteBusinessImage(forModel:model, button: sender)
                }
            }
            else
            {
                if index <= menuListingArray.count-1 {
                    sender.isUserInteractionEnabled = false
                    sender.superview?.isUserInteractionEnabled = false
                    let model = menuListingArray[index]
                    deleteMenuImage(forModel:model, button: sender)
                }
            }
        }
    }
    
    //MARK:- Delete Business Image
    func deleteBusinessImage(forModel model:PhotoData, button:UIButton) -> Void
    {
        let urlToHit = EndPoints.deleteBusinessImage(UtilityClass.getUserSidData()!,model.imageID!).path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .get, parameters: nil, shouldDeserialize: true)
        {[weak self] (data, dictionary, statusCode, error) in
            
            guard let `self` = self else {return}
            
            button.superview?.isUserInteractionEnabled = true
            button.isUserInteractionEnabled = true
            
            if statusCode == 200
            {
                let newIndex = self.uploadedImagesArray.index(where:
                { (dataModel) -> Bool in
                    return dataModel.imageID == model.imageID
                })
                if newIndex != nil
                {
                    self.uploadedImagesArray.remove(at: newIndex!)
                }
            }
            self.tableViewCreateProfile.reloadData()
        }
    }
    
    //MARK:- Delete Business Image
    func deleteMenuImage(forModel model:PhotoData, button:UIButton) -> Void
    {
        let urlToHit = EndPoints.deleteMenuImage(UtilityClass.getUserSidData()!,model.imageID!).path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .get, parameters: nil, shouldDeserialize: true)
        {[weak self] (data, dictionary, statusCode, error) in
            
            guard let `self` = self else {return}
            
            button.superview?.isUserInteractionEnabled = true
            button.isUserInteractionEnabled = true
            
            if statusCode == 200
            {
                let newIndex = self.menuListingArray.index(where:
                { (dataModel) -> Bool in
                    return dataModel.imageID == model.imageID
                })
                if newIndex != nil
                {
                    self.menuListingArray.remove(at: newIndex!)
                }
            }
            self.tableViewCreateProfile.reloadData()
        }
    }
    
    //MARK:- AddMenuImageViewController Delegate
    func addMenuImageController(controller: AddMenuImageViewController, uploadedImageData imageData: [String : Any]) {
        
        let newImagedata = imageData
        let photoData : PhotoData = PhotoData(image: nil, imageName: newImagedata["caption"] as? String, isUploading: false)
        photoData.imageID = newImagedata["_id"] as? String
        photoData.imageURLString = newImagedata["file_url"] as? String
        
        menuListingArray.append(photoData)
        
        tableViewCreateProfile.reloadData()
    }
    //MARK:- Save Data Api
    @IBAction func onDoneAction(_ sender: UIButton)
    {
        let isVerified = validateTheInputFields()
        
        if isVerified
        {
            performCreateProfileTask()
        }
    }
    
    //MARK:- Validate input fields
    func validateTheInputFields() -> Bool
    {
        if !isCoverImageAdded {
            UtilityClass.showAlertWithTitle(title: "", message: "Please add cover image", onViewController: self, withButtonArray: nil, dismissHandler: nil)
            return false
        }
        
        if !isProfilePictureAdded {
            UtilityClass.showAlertWithTitle(title: "", message: "Please add profile image", onViewController: self, withButtonArray: nil, dismissHandler: nil)
            return false
        }
        
        if arrayModelCreateProfile.count == 0
        {
            UtilityClass.showAlertWithTitle(title: "", message: "Please fill up entries", onViewController: self, withButtonArray: nil, dismissHandler: nil)
            
            return false
        }
        
        for model in arrayModelCreateProfile
        {
            switch model.keyName {
            case CreateProfileEnum.businessName.rawValue:
                if (model.keyValue as! String).isEmptyString()
                {
                    UtilityClass.showAlertWithTitle(title: "", message: "Please provide business name", onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return false
                }
                
            case CreateProfileEnum.scene.rawValue:
                if (model.keyValue as! String).isEmptyString()
                {
                    UtilityClass.showAlertWithTitle(title: "", message: "Please provide scene tags", onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return false
                }
                
                //            case CreateProfileEnum.about.rawValue:
                //                if (model.keyValue as! String).isEmptyString()
                //                {
                //                    UtilityClass.showAlertWithTitle(title: "", message: "Please provide about information", onViewController: self, withButtonArray: ["OK"], dismissHandler: nil)
                //                    return false
                //                }
                
            case CreateProfileEnum.contact.rawValue:
                if (model.keyValue as! String).isEmptyString()
                {
                    UtilityClass.showAlertWithTitle(title: "", message: "Please provide contact", onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return false
                }
                
            case CreateProfileEnum.location.rawValue:
                if (model.keyValue as! String).isEmptyString()
                {
                    UtilityClass.showAlertWithTitle(title: "", message: "Please provide location", onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return false
                }
                
            case CreateProfileEnum.info.rawValue:
                if (model.keyValue as! String).isEmptyString()
                {
                    UtilityClass.showAlertWithTitle(title: "", message: "Please provide general information", onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return false
                }
                
            case CreateProfileEnum.website.rawValue:
                if !(model.keyValue as! String).isValidWebsite()
                {
                    UtilityClass.showAlertWithTitle(title: "", message: "Please provide valid website url", onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return false
                }
                
            case CreateProfileEnum.charges.rawValue:
                if (model.keyValue as! [String:String]).isEmpty
                {
                    UtilityClass.showAlertWithTitle(title: "", message: "Please provide cover charges", onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return false
                }
                
            case CreateProfileEnum.dressCode.rawValue:
                if (model.keyValue as! String).isEmptyString()
                {
                    UtilityClass.showAlertWithTitle(title: "", message: "Please provide dress code", onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return false
                }
                
                //            case CreateProfileEnum.totalOccupancy.rawValue:
                //                if (model.keyValue as! String).isEmptyString()
                //                {
                //                    UtilityClass.showAlertWithTitle(title: "", message: "Please provide total occupancy", onViewController: self, withButtonArray: ["OK"], dismissHandler: nil)
                //                    return false
                //                }
                
            case CreateProfileEnum.openTill.rawValue:
                if (model.keyValue as! [String:String]).isEmpty
                {
                    UtilityClass.showAlertWithTitle(title: "", message: "Please provide business hours", onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return false
                }
                
            default:
                break
            }
        }
        return true
    }
    
    //MARK:- Create Profile Api
    func performCreateProfileTask() -> Void
    {
        HUD.show(.systemActivity, onView: self.view.window)
        
        self.view.endEditing(true)
        
        var params = [String:Any]()
        var imageArray = [ImageDataDict]()
        
        for model in arrayModelCreateProfile
        {
            if model.keyName ==  CreateProfileEnum.none.rawValue
            {
                continue
            }
                
            else if model.keyName == CreateProfileEnum.radius.rawValue
            {
                if model.keyValue != nil
                {
                    var radiusValue = String()
                    if model.keyValue as! String  == "Small"
                    {
                        radiusValue = "50"
                    }
                    else if model.keyValue as! String  == "Medium"
                    {
                        radiusValue = "100"
                    }
                    else if model.keyValue as! String  == "Large"
                    {
                        radiusValue = "150"
                    }
                    
                    params[model.keyName] = radiusValue
                }
            }
            else{
                params[model.keyName] = model.keyValue
            }
        }
        
        //        if let latitudeString = params[CreateProfileEnum.latitude.rawValue] as? String
        //        {
        //            params.updateValue(Double(latitudeString)!, forKey: CreateProfileEnum.latitude.rawValue)
        //        }
        //
        //        if let longitudeString = params[CreateProfileEnum.longitude.rawValue] as? String
        //        {
        //            params.updateValue(Double(longitudeString)!, forKey: CreateProfileEnum.longitude.rawValue)
        //        }
        
        imageArray.append(ImageDataDict(data: UIImageJPEGRepresentation(btnCoverImg.backgroundImage(for: .normal)!, 0.7)!, name: "coverPicture"))
        
        imageArray.append(ImageDataDict(data: UIImageJPEGRepresentation(btnProfileImg.image(for: .normal)!, 0.7)!, name: "picture"))
        
        let urlToHit = EndPoints.createProfile(UtilityClass.getUserSidData()!).path
        
        AppWebHandler.sharedInstance().uploadImages(fromURL: urlToHit, imagesArray: imageArray, otherParameters: params)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else {return}
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let type = responseDictionary["type"] as! Bool
                
                if type
                {
                    let businessData = responseDictionary["data"] as! [String:Any]
                    
                    let businessName = businessData["name"] as! String
                    
                    UtilityClass.saveUserUpdatedName(updatedName: businessName)
                    
                    let imageString = businessData["picture"] as! String
                    UtilityClass.saveUserImageString(updatedString: imageString)
                    
                    if self.isFromSettings
                    {
                        UtilityClass.showAlertWithTitle(title: App_Name, message: "Your profile has been successfully updated.", onViewController: self, withButtonArray: nil, dismissHandler: {[weak self] (buttonIndex) in
                            guard let `self` = self else {return}
                            self.navigationController?.popViewController(animated: true)
                        })
                    }
                    else
                    {
                        UtilityClass.saveBusinessInfoData(businessDict: businessData)
                        
                        UtilityClass.updateProfileDone(isDone: true)
                        
                        let businessHomeVC = UIStoryboard.getBusinessTabBarFlowStoryboard().instantiateInitialViewController()
                        
                        UtilityClass.changeRootViewController(with: businessHomeVC!)
                    }
                }
            }
            else
            {
                if error != nil
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    
                    return
                }
                if statusCode == 203
                {
                    let responseDictionary = dictionary!
                    guard (responseDictionary["message"] != nil) else
                    {
                        UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                        return
                    }
                    let messageStr = responseDictionary["message"] as! String
                    
                    UtilityClass.showAlertWithTitle(title: App_Name, message: messageStr, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
            }
        }
    }
}

extension CreateProfileViewController:UIPickerViewDelegate,UIPickerViewDataSource
{
    //MARK:
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrayRadiusValue.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrayRadiusValue[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let cell = tableViewCreateProfile.cellForRow(at: IndexPath (row: 11, section: 0)) as! CreateProfileCell
        let newModel = cell.model
        newModel.keyValue = arrayRadiusValue[row]
        cell.inputTextField.text = arrayRadiusValue[row]
        let index = arrayModelCreateProfile.index(where: { (model) -> Bool in
            
  
            
            model.keyName == newModel.keyName
        })
        
        
        arrayModelCreateProfile[11].keyValue = arrayRadiusValue[row]
    }
}


