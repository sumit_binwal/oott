//
//  SignUpViewController.swift
//  OOTTUserApp
//
//  Created by Santosh on 07/06/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit
import PKHUD
import CoreTelephony
import SDWebImage

var isFromFacebook = false

class SignUpViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate, LoginCellDelegate {

    //MARK:- Tableview signup
    @IBOutlet weak var tableSignUp: UITableView!
    
    //MARK:- Table header view
    @IBOutlet var headerViewTable: UIView!
    
    @IBOutlet weak var profilePicButton: UIButton!
    
    //MARK:- Table bottom view
    @IBOutlet var bottomViewTable: UIView!
    @IBOutlet weak var loginHereButton: UIButton!
    
    var isImageSelected = false
    
    var dictFacebookData : [String:Any] = [:]
    
    
    //MARK:- Array Holding Values
    var arrayModelLogin : [ModelLogin] = []
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.loadDummyData()
        self.setupTableView()
        self.updateTableHeaderView()
        self.updateTableFooterView()
    }

    //MARK:- didReceiveMemoryWarning
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Deinit
    deinit {
        print("SignUpViewController deinit")
    }
    
    //MARK:- Load Dummy Data
    func loadDummyData() -> Void
    {
        arrayModelLogin.append(ModelLogin(withKey: LoginEnum.fullname.rawValue, value: ""))
        
        arrayModelLogin.append(ModelLogin(withKey: LoginEnum.username.rawValue, value: ""))
        
        arrayModelLogin.append(ModelLogin(withKey: LoginEnum.dob.rawValue, value: ""))
        
        let code = "+1"
        
        arrayModelLogin.append(ModelLogin(withKey: LoginEnum.mobileNo.rawValue, value: code+"-"))
        
        arrayModelLogin.append(ModelLogin(withKey: LoginEnum.email.rawValue, value: ""))
        
        arrayModelLogin.append(ModelLogin(withKey: LoginEnum.password.rawValue, value: ""))
        
        arrayModelLogin.append(ModelLogin(withKey: LoginEnum.confirmPassword.rawValue, value: ""))
        
        arrayModelLogin.append(ModelLogin(withKey: LoginEnum.userType.rawValue, value: AppUserTypeEnum.user.rawValue))
    }
    
    //MARK:- Initial table setup
    func setupTableView() -> Void
    {
        self.tableSignUp.delegate = self
        self.tableSignUp.dataSource = self
        
        self.tableSignUp.contentInset = UIEdgeInsetsMake(74 * scaleFactorX, 0, 0, 0)
    }
    
    //MARK:- Table header setup
    func updateTableHeaderView() -> Void
    {
        self.headerViewTable.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.headerViewTable.frame.size.height * scaleFactorX)
        
        self.headerViewTable.backgroundColor = UIColor.clear
        
        self.profilePicButton.backgroundColor = UIColor.yellow
        self.profilePicButton.layer.cornerRadius = (self.profilePicButton.frame.size.width/2) * scaleFactorX
        self.profilePicButton.clipsToBounds = true
        self.tableSignUp.tableHeaderView = self.headerViewTable
        self.profilePicButton.imageView?.contentMode = .scaleAspectFill
    }
    
    //MARK:- Table footer setup
    func updateTableFooterView() -> Void
    {
        self.bottomViewTable.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 80 * scaleFactorX)
        
        self.bottomViewTable.backgroundColor = UIColor.clear
        
        self.tableSignUp.tableFooterView = self.bottomViewTable
    }
    
    //MARK:- UITableView Delegate and Datasource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 9
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (indexPath.row == tableView.numberOfRows(inSection: indexPath.section) - 1)
        {
            return 230 * scaleFactorX;
        }
        if (indexPath.row == 0)
        {
            return 54 * scaleFactorX;
        }
        if indexPath.row == 5 || indexPath.row == 6
        {
            return isFromFacebook ? 0 : 64 * scaleFactorX
        }
        if indexPath.row == 7
        {
            return 100 * scaleFactorX
        }
        return 64 * scaleFactorX;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell : LoginInputCell
        
        var cellIdentifier = kCellIdentifier_login_input
        
        var newCellType = CellType.none
        
        switch indexPath.row
        {
        case 0:
            cellIdentifier = kCellIdentifier_login_input
            newCellType = .fullName;
        case 1:
            cellIdentifier = kCellIdentifier_login_input
            newCellType = .username
        case 2:
            cellIdentifier = kCellIdentifier_login_input
            newCellType = .dob
        case 3:
            cellIdentifier = kCellIdentifier_login_phone_cell
            newCellType = .phone
        case 4:
            cellIdentifier = kCellIdentifier_login_input
            newCellType = .email
        case 5:
            cellIdentifier = kCellIdentifier_login_input
            newCellType = .pass
        case 6:
            cellIdentifier = kCellIdentifier_login_input
            newCellType = .confirmPassword
        case 7:
            cellIdentifier = kCellIdentifier_login_userType_cell
            newCellType = .userType
        case 8:
            cellIdentifier = kCellIdentifier_login_button
            newCellType = .none
            
        default:
            break
        }
        
        cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! LoginInputCell
        
        cell.cellType = newCellType
        cell.loginDelegate = self
        
        if cellIdentifier !=  kCellIdentifier_login_button{
            cell.updateValue(usingModel: arrayModelLogin[indexPath.row])
        }
       // cell.updateValue(usingArray: arrayModelLogin)
        
        return cell
    }
    
    //MARK:- LoginCellDelegate -> Facebook Button Tap
   
    
    func loginCell(cell: LoginInputCell, didTapOnFacebookButton: UIButton) {
        
        HUD.show(.systemActivity, onView: self.view)
        
        FacebookHelper.loginFacebook(withReadPermissions: ["public_profile","email","user_birthday"]) { (resultDict) in
            
            resultDict.forEach({ (key,value) in
                print(key,"=",value)
            })
            if resultDict["error"] != nil
            {
                HUD.hide()
                UtilityClass.showAlertWithTitle(title: App_Name, message: resultDict["error"] as? String, onViewController: self, withButtonArray: ["OK"], dismissHandler: nil)
            }
            else
            {
                self.processFacebookData(withLoginData: resultDict)
            }
        }
    }
    
    //MARK:- LoginCellDelegate -> Terms and Condition Button Tap

    func loginCell(cell: LoginInputCell, didTapOnTermsAndConditionButton: UILabel, urlString: String) {
        
        UtilityClass.openSafariControllerWithString(usingLink: urlString, onViewController: self.navigationController)

    }

    
    //MARK:- LoginCellDelegate -> Create Account Button Tap
    func loginCell(cell: LoginInputCell, didTapOnCreateAccountButton: UIButton) {
        
        let isVerified = validateTheRegisterInputFields()
        
        if isVerified
        {
            moveToOTPScreen()
        }
    }
    
    //MARK:- LoginCellDelegate -> Textfield updated Action
    func loginCell(cell: LoginInputCell, updatedInputfieldText: String) {
        if cell.model.keyName != LoginEnum.none.rawValue
        {
            let isAlreadyContained = arrayModelLogin.contains { (model) -> Bool in
                return model.keyName == cell.model.keyName
            }
            if !isAlreadyContained
            {
                arrayModelLogin.append(cell.model)
            }
            else
            {
                let newModel = cell.model
                newModel.keyValue = updatedInputfieldText
                let index = arrayModelLogin.index(where: { (model) -> Bool in
                    model.keyName == newModel.keyName
                })
                arrayModelLogin[index!].keyValue = newModel.keyValue
                arrayModelLogin[index!].validUsername = newModel.validUsername
//                arrayModelLogin.remove(at: index!)
//                arrayModelLogin.append(newModel)
            }
        }
    }
    
    //MARK:- Profile Image Action
    @IBAction func onProfilePicAction(_ sender: UIButton) {
        
        UtilityClass.showActionSheetWithTitle(title: "Add Profile Picture", message: "Select from options", onViewController: self, withButtonArray: ["Camera", "Photos"]) { (buttonIndex : Int) in
            if buttonIndex == 0
            {
                self.openPhotoPickerForType(imageSelectType: .camera)
            }
            else
            {
                self.openPhotoPickerForType(imageSelectType: .photos)
            }
        }
    }
    
    //MARK:- Login here action
    @IBAction func onLoginHereAction(_ sender: UIButton) {
        isFromFacebook = false
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Show ImagePicker
    func openPhotoPickerForType(imageSelectType : ImageSelectType) -> Void
    {
        var isCamera = false
        
        if imageSelectType == ImageSelectType.camera
        {
            if !UIImagePickerController.isSourceTypeAvailable(.camera
                ) {
                UtilityClass.showAlertWithTitle(title: "This device does not have Camera. Try Photos!", message: nil, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            else
            {
                isCamera = true
            }
        }
        
        let imagePicker = UIImagePickerController.init()
        imagePicker.sourceType = isCamera ? .camera : .photoLibrary
        imagePicker.allowsEditing = true
        imagePicker.delegate = self
        
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    //MARK: - UIImagePickerControllerDelegate
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let image = info[UIImagePickerControllerEditedImage] as? UIImage
        {
            self.profilePicButton.setImage(image, for: .normal)
            isImageSelected = true
        }
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- Validate input fields
    func validateTheRegisterInputFields() -> Bool
    {
        
        if !isImageSelected {
            UtilityClass.showAlertWithTitle(title: "", message: "Please add profile picture", onViewController: self, withButtonArray: nil, dismissHandler: nil)
            return false
        }
        
        if arrayModelLogin.count == 0
        {
            UtilityClass.showAlertWithTitle(title: "", message: "Please fill up entries", onViewController: self, withButtonArray: nil, dismissHandler: nil)
            
            return false
        }
        
        for model in arrayModelLogin
        {
            switch model.keyName {
            case LoginEnum.email.rawValue:
                if model.keyValue.isEmptyString()
                {
                    UtilityClass.showAlertWithTitle(title: "", message: "Please provide email address", onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return false
                }
                
                if !model.keyValue.isValidEmailAddress()
                {
                    UtilityClass.showAlertWithTitle(title: "", message: "Please provide valid email address", onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return false
                }
                
            case LoginEnum.mobileNo.rawValue:
                
                let mobileNo : String = model.keyValue
                let mobileArr = mobileNo.components(separatedBy: "-")
                if !mobileArr.isEmpty
                {
                    let stdCode : String = mobileArr.first!
                    let newMobileNo : String = mobileArr.last!
                    
                    if stdCode.isEmptyString()
                    {
                        UtilityClass.showAlertWithTitle(title: "", message: "Please provide phone STD code", onViewController: self, withButtonArray: nil, dismissHandler: nil)
                        return false
                    }
                    if newMobileNo.isEmptyString()
                    {
                        UtilityClass.showAlertWithTitle(title: "", message: "Please provide phone number", onViewController: self, withButtonArray: nil, dismissHandler: nil)
                        return false
                    }
                    if newMobileNo.count<10
                    {
                        UtilityClass.showAlertWithTitle(title: "", message: "Please provide valid phone number", onViewController: self, withButtonArray: nil, dismissHandler: nil)
                        return false
                    }
                }
                
            case LoginEnum.dob.rawValue:
                if model.keyValue.isEmptyString()
                {
                    UtilityClass.showAlertWithTitle(title: "", message: "Please provide date of birth", onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return false
                }
                
            case LoginEnum.fullname.rawValue:
                if model.keyValue.isEmptyString()
                {
                    UtilityClass.showAlertWithTitle(title: "", message: "Please provide full name", onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return false
                }
                
            case LoginEnum.username.rawValue:
                if model.keyValue.isEmptyString()
                {
                    UtilityClass.showAlertWithTitle(title: "", message: "Please provide valid username", onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return false
                }
                if model.validUsername == .inValid
                {
                    UtilityClass.showAlertWithTitle(title: "", message: "Username already taken, provide unique", onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return false
                }
                
            case LoginEnum.password.rawValue:
                if isFromFacebook
                {
                    break
                }
                if model.keyValue.isEmptyString()
                {
                    UtilityClass.showAlertWithTitle(title: "", message: "Please provide password", onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return false
                }
                if model.keyValue.count < 6
                {
                    UtilityClass.showAlertWithTitle(title: "", message: "Password should be atleast 6 character", onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return false
                }
            case LoginEnum.confirmPassword.rawValue:
                if isFromFacebook
                {
                    break
                }
                if model.keyValue.isEmptyString()
                {
                    UtilityClass.showAlertWithTitle(title: "", message: "Please provide confirm password", onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return false
                }
                
                var passwordValue = String()
                for model in arrayModelLogin
                {
                    if model.keyName == LoginEnum.password.rawValue
                    {
                        passwordValue = model.keyValue
                    }
                }
                
                if model.keyValue != passwordValue
                {
                    UtilityClass.showAlertWithTitle(title: "", message: "Password and confirm password should be same", onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return false
                }
                
                
            default: break
                
                
            }
        }
        return true
    }
    
    //MARK:- Go to OTP screen
    func moveToOTPScreen() -> Void
    {
        self.view.endEditing(true)
        
        var params = [String:String]()
       
        
        for model in arrayModelLogin
        {
            params[model.keyName] = model.keyValue
        }
        params["deviceToken"] = appDeviceToken
        
        let mobileNo : String = params[LoginEnum.mobileNo.rawValue]!
        let stdCode : String = mobileNo.components(separatedBy: "-").first!
        let newMobileNo : String = mobileNo.components(separatedBy: "-").last!
        params[LoginEnum.countryCode.rawValue] = stdCode
        params.updateValue(newMobileNo, forKey: LoginEnum.mobileNo.rawValue)
        
        getTheOTPForUser(withParameters: params)
        
        
    }
    
    //MARK:- Handle Facebook Data
    func processFacebookData(withLoginData loginData:[String:Any]) -> Void
    {
        isFromFacebook = true
        
        dictFacebookData = loginData
        
        callLoginApi()
    }
    
    
    //MARK:- Login Api Call
    func callLoginApi() -> Void
    {
        self.view.endEditing(true)
        
        var params = [String:String]()
        
        params[LoginEnum.socialId.rawValue] = dictFacebookData["id"] as? String
        params[LoginEnum.sourceType.rawValue] = "facebook"
        params["deviceToken"] = appDeviceToken
        
        let urlToHit = EndPoints.socialLogin.path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .post, parameters: params, shouldDeserialize: true)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else {return}
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyType = responseDictionary["type"] as! Bool
                
                if !replyType
                {
                    //sign up
                    self.updateTheModelArray()
                }
                else
                {
                    // login...
                    
                    let userDataDict = (responseDictionary["data"] as! [String : Any] )
                    
                    UtilityClass.saveUserInfoData(userDict: userDataDict)
                    
//                    if logoutCase == .addAccountLogout // Adding current user to stack list...
//                    {
//                        logoutCase = .none
//                        UtilityClass.addCurrentUserToStack()
//                    }
                    
                    if userDataDict["userType"] != nil
                    {
                        let userType = userDataDict["userType"] as! String
                        
                        if userType == AppUserTypeEnum.user.rawValue
                        {
                            // User home page
                        }
                        else if userType == AppUserTypeEnum.provider.rawValue
                        {
                            // business home page
                            let isProfileUpdateDone = userDataDict["isProfileDone"] as! Bool
                            
                            if !isProfileUpdateDone
                            {
                                self.goToCreateProfileScreen()
                            }
                            else
                            {
                                self.goToBusinessHomeScreen()
                            }
                        }
                    }
                }
            }
            else
            {
                self.updateTheModelArray()
            }
        }
    }
    
    //MARK:- Update Model Array From Facebook
    func updateTheModelArray() -> Void
    {
        for item in arrayModelLogin
        {
            switch item.keyName
            {
            case LoginEnum.email.rawValue:
                if dictFacebookData["email"] != nil
                {
                    item.keyValue = dictFacebookData["email"] as! String
                }
            case LoginEnum.fullname.rawValue:
                if dictFacebookData["name"] != nil
                {
                    item.keyValue = dictFacebookData["name"] as! String
                }
            case LoginEnum.dob.rawValue:
                if dictFacebookData["birthday"] != nil
                {
                    item.keyValue = dictFacebookData["birthday"] as! String
                }
            default:
                break
            }
        }
        
        if dictFacebookData["id"] != nil
        {
            let isAlreadyContained = arrayModelLogin.contains { (model) -> Bool in
                return model.keyName == LoginEnum.socialId.rawValue
            }
            if isAlreadyContained
            {
                let index = arrayModelLogin.index(where: { (model) -> Bool in
                    model.keyName == LoginEnum.socialId.rawValue
                })
                arrayModelLogin.remove(at: index!)
                
                let index1 = arrayModelLogin.index(where: { (model) -> Bool in
                    model.keyName == LoginEnum.sourceType.rawValue
                })
                arrayModelLogin.remove(at: index1!)
            }
            
            
            let keyValue = dictFacebookData["id"] as! String
            arrayModelLogin.append(ModelLogin(withKey: LoginEnum.socialId.rawValue, value: keyValue))
            
            arrayModelLogin.append(ModelLogin(withKey: LoginEnum.sourceType.rawValue, value: "facebook"))
        }
        if dictFacebookData["picture"] != nil
        {
            unowned let weakSelf = self
            
            let datadict = dictFacebookData["picture"] as! [String:Any]
            let urldict = datadict["data"] as! [String:Any]
            let imageString : String = urldict["url"]! as! String
            profilePicButton.sd_setImage(with: URL(string: imageString), for: .normal, completed: { (image, error, cacheType, url) in
                if image != nil
                {
                    weakSelf.isImageSelected = true
                }
                else
                {
                    weakSelf.isImageSelected = false
                }
            })
        }
        
        arrayModelLogin.forEach { (model) in
            print("********** ",model.keyName," = ",model.keyValue)
        }
        
        dictFacebookData.removeAll()
        
        tableSignUp.reloadData()
        tableSignUp.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
    }
    
    //MARK:- Get OTP Api
    func getTheOTPForUser(withParameters:[String:String]) -> Void
    {
        let params=withParameters
        HUD.show(.systemActivity, onView: self.view.window)
        
        let otpParams : [String:String] = [LoginEnum.mobileNo.rawValue : params[LoginEnum.mobileNo.rawValue]! , LoginEnum.countryCode.rawValue :params[LoginEnum.countryCode.rawValue]!,LoginEnum.email.rawValue:params[LoginEnum.email.rawValue]!]
        
        AppWebHandler.sharedInstance().fetchData(fromURL: EndPoints.sendOTP.path, httpMethod: .post, parameters: otpParams, shouldDeserialize: true)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 203
            {
                let responseDictionary = dictionary!
                guard (responseDictionary["message"] != nil) else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
                let messageStr = responseDictionary["message"] as! String
                
                UtilityClass.showAlertWithTitle(title: App_Name, message: messageStr, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                print(responseDictionary)

                guard (responseDictionary["message"] != nil) else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
                let messageStr = responseDictionary["message"] as! String
                let type = responseDictionary["type"] as! Bool
                if type
                {
                     var imageArray = [ImageDataDict]()
                    imageArray.append(ImageDataDict(data: UIImageJPEGRepresentation(self.profilePicButton.image(for: .normal)!, 0.7)!, name: "picture"))
                    
                    let otpVC = self.storyboard?.instantiateViewController(withIdentifier: "otpVC") as! OTPViewController
                    
                    otpVC.paramsDict = params
                    otpVC.imageArray = imageArray
                    
                    self.navigationController?.pushViewController(otpVC, animated: true)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: messageStr, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }

            }
        }
    }
    
    //MARK:- Go To Create Profile
    func goToCreateProfileScreen() -> Void
    {
        appDelegate?.window??.makeToast("Please complete profile updation", duration: 2, position: .bottom)
        
        let createProfileVC = UIStoryboard.getCreateProfileStoryboard().instantiateInitialViewController()
        
        UtilityClass.changeRootViewController(with: createProfileVC!)
    }
    
    //MARK:- Go To Business Home
    func goToBusinessHomeScreen() -> Void
    {
        let businessVC = UIStoryboard.getBusinessTabBarFlowStoryboard().instantiateInitialViewController()
        
        UtilityClass.changeRootViewController(with: businessVC!)
    }
}
