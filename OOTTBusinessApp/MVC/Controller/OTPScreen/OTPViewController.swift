//
//  OTPViewController.swift
//  OOTTUserApp
//
//  Created by Santosh on 07/06/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit
import PKHUD

//MARK:-
//MARK:- OTPViewController
class OTPViewController: UIViewController {

    @IBOutlet weak var labelHeader1: UILabel!
    @IBOutlet weak var labelHeader2: UILabel!
    @IBOutlet weak var otpViewBase: OTPView!
    
    var paramsDict = [String:String]()
    var imageArray = [ImageDataDict]()
    
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        UtilityClass.showAlertWithTitle(title: App_Name, message: "Verification PIN has been sent to your phone.", onViewController: self, withButtonArray: nil, dismissHandler: nil)
        self.setupView()
//        self.getTheOTPForUser()
    }
    
    //MARK:- Deinit
    deinit {
        print("OTPViewController Deinit")
    }
    
    //MARK:- viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.showNavigationBar()
    }
    
    //MARK:- viewWillDisappear
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.hideNavigationBar()
    }
    
    //MARK:- viewDidAppear
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

    //MARK:- didReceiveMemoryWarning
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Initial Setup View
    func setupView() -> Void
    {
        self.title = "Verification".capitalized
        
        let backBarButton = self.setLeftBarButtonItem(withImage: #imageLiteral(resourceName: "backButton"))
        backBarButton.action = #selector(onBackButtonAction)
        backBarButton.target = self
        
        otpViewBase.backgroundColor = UIColor.clear
    }
    
    //MARK:- Back button action
    func onBackButtonAction() -> Void {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- ResentOTP button action
    @IBAction func onResendOTPAction(_ sender: UIButton) {
        getTheOTPForUser()
    }
    
    //MARK:- Next button action
    @IBAction func onNextAction(_ sender: UIButton) {
        
        let isVerified = otpViewBase.isOTPFieldsValid()
        
        if isVerified
        {
            paramsDict[LoginEnum.otp.rawValue] = otpViewBase.getOTPValue()
            callRegisterApi()
        }
        else
        {
            UtilityClass.showAlertWithTitle(title: App_Name, message: "Please provide valid PIN", onViewController: self, withButtonArray: nil, dismissHandler: nil)
        }
    }
    
    //MARK:- Get OTP Api
    func getTheOTPForUser() -> Void
    {
        HUD.show(.systemActivity, onView: self.view.window)
        
        let otpParams : [String:String] = [LoginEnum.mobileNo.rawValue : paramsDict[LoginEnum.mobileNo.rawValue]! , LoginEnum.countryCode.rawValue : paramsDict[LoginEnum.countryCode.rawValue]!, LoginEnum.email.rawValue:paramsDict[LoginEnum.email.rawValue]!]
        
        AppWebHandler.sharedInstance().fetchData(fromURL: EndPoints.sendOTP.path, httpMethod: .post, parameters: otpParams, shouldDeserialize: true)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()

            guard let `self` = self else { return }
            
            if error != nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 203 || statusCode == 200
            {
                let responseDictionary = dictionary!
                guard (responseDictionary["message"] != nil) else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
                let messageStr = responseDictionary["message"] as! String
                
                UtilityClass.showAlertWithTitle(title: App_Name, message: messageStr, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
        }
    }
    
    //MARK:- Register Api call
    func callRegisterApi() -> Void
    {
        HUD.show(.systemActivity, onView: self.view.window)
        
        let urlToHit = isFromFacebook ? EndPoints.socialRegister.path : EndPoints.register.path
        
        AppWebHandler.sharedInstance().uploadImages(fromURL: urlToHit, imagesArray: imageArray, otherParameters: paramsDict)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()

            guard let `self` = self else { return }
            
            if error != nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 203
            {
                let responseDictionary = dictionary!
                guard (responseDictionary["message"] != nil) else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
                let messageStr = responseDictionary["message"] as! String
                
                UtilityClass.showAlertWithTitle(title: App_Name, message: messageStr, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            if statusCode == 200
            {
                guard dictionary!["data"] != nil else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
                
                let userDataDict = (dictionary!["data"] as! [String : Any] )
                
                UtilityClass.saveUserInfoData(userDict: userDataDict)
                print("registration completed")
                
                if logoutCase == .addAccountLogout // Adding current user to stack list...
                {
                    logoutCase = .none
                    UtilityClass.addCurrentUserToStack()
                }
                
                if let userType = UtilityClass.getUserTypeData()
                {
                    if userType == AppUserTypeEnum.user.rawValue // UserType is of type user
                    {
                        self.goToUserHomeScreen() // Go to user home page
                        return
                    }
                }
                self.goToCreateProfileScreen() // Go to business create profile page
            }
        }
    }
    
    //MARK:- Go To Create Profile
    func goToCreateProfileScreen() -> Void
    {
        let createProfileVC = UIStoryboard.getCreateProfileStoryboard().instantiateInitialViewController()
        
        UtilityClass.changeRootViewController(with: createProfileVC!)
    }
    
    //MARK:- Go To User Home
    func goToUserHomeScreen() -> Void
    {
        let userHomeVC = UIStoryboard.getUserTabsStoryboard().instantiateInitialViewController()
        
        UtilityClass.changeRootViewController(with: userHomeVC!)
    }
}
