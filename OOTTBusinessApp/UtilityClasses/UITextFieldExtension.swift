//
//  UITextFieldExtension.swift
//  OOTTUserApp
//
//  Created by Santosh on 06/06/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit
import Foundation

extension UITextField
{
    func setPlaceholderColor(_ placeholderColor:UIColor?) -> Void
    {
        guard let color = placeholderColor else {
            return
        }
        self.setValue(color, forKeyPath: "_placeholderLabel.textColor")
    }
    
    func customize(withLeftIcon imageIcon:UIImage) {
    
        self.leftViewMode = .always;
        let leftImageView = UIImageView.init(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        leftImageView.contentMode = .right
        leftImageView.image = imageIcon
        self.leftView = leftImageView
    }
}
