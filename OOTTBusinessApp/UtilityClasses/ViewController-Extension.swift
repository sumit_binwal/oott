//
//  ViewController-Extension.swift
//  OOTTUserApp
//
//  Created by Santosh on 06/06/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import Foundation
import UIKit


extension UIViewController
{
    func showNavigationBar() -> Void {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    func hideNavigationBar() -> Void {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    func setLeftBarButtonItem(withImage barImage:UIImage) -> UIBarButtonItem
    {
        
        let buttonItem = UIBarButtonItem(image: barImage, style: .plain, target: nil, action: nil)
        
        self.navigationItem.leftBarButtonItem = buttonItem
        
        return buttonItem
    }
    
    func setCreateImageViewBarButtonItem() -> UIBarButtonItem
    {
        let barImageView   = UIImageView()
        barImageView.frame=CGRect(x: 0, y: 0, width: 26, height: 26)
        barImageView.layer.cornerRadius=barImageView.frame.size.width/2;
        barImageView.clipsToBounds=true
        let buttonItem = UIBarButtonItem(customView: barImageView)
        
        return buttonItem
    }
    
    func setMoreMenuBarButtonItem(withImage barImage:UIImage) -> UIBarButtonItem {
        
        let buttonItem = UIBarButtonItem(image: barImage, style: .plain, target: nil, action: nil)
        return buttonItem
    }
    
    func setRightBarButtonItem(withImage barImage:UIImage) -> UIBarButtonItem
    {
        let buttonItem = UIBarButtonItem(image: barImage, style: .plain, target: nil, action: nil)
        
        self.navigationItem.rightBarButtonItem = buttonItem
        
        return buttonItem
    }
    
    func setRightBarButtonItem(_ buttonTitle:String) -> UIBarButtonItem
    {
        let buttonItem = UIBarButtonItem (title: buttonTitle, style: .plain, target: nil, action: nil)
        
        self.navigationItem.rightBarButtonItem = buttonItem
        
        return buttonItem
    }
    
    func setLeftBarButtonItem(_ buttonTitle:String) -> UIBarButtonItem
    {
        let buttonItem = UIBarButtonItem (title: buttonTitle, style: .plain, target: nil, action: nil)
        
        self.navigationItem.leftBarButtonItem = buttonItem
        
        return buttonItem
    }
}
