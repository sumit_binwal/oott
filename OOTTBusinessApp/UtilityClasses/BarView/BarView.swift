//
//  BarView.swift
//  OOTTBusinessApp
//
//  Created by Santosh on 16/01/18.
//  Copyright © 2018 KonstantInfoSolutions. All rights reserved.
//

import UIKit

class BarView: UIView {
    
    var barValue: CGFloat = 0
    var barColor: UIColor!
    private var oldBarValue: CGFloat = -1

    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
        let updatedWidth = (barValue/100) * bounds.size.width
        let path = UIBezierPath (rect: CGRect (x: rect.origin.x, y: rect.origin.y, width: updatedWidth, height: rect.size.height))
        self.barColor.setFill()
        path.fill()
    }

}
