//
//  Storyboard-Extension.swift
//  OOTTUserApp
//
//  Created by Santosh on 07/06/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import Foundation
import UIKit

extension UIStoryboard
{
    
    class func getLoginFlowStoryboard() -> UIStoryboard
    {
        return UIStoryboard(name: "LoginFlow", bundle: Bundle.main)
    }
    
    class func getSignUpFlowStoryboard() -> UIStoryboard
    {
        return UIStoryboard(name: "SignUpFlow", bundle: Bundle.main)
    }
    
    class func getBusinessTabBarFlowStoryboard() -> UIStoryboard
    {
        return UIStoryboard(name: "BusinessTabs", bundle: Bundle.main)
    }
    
    class func getBusinessHomeFlowStoryboard() -> UIStoryboard
    {
        return UIStoryboard(name: "BusinessHome", bundle: Bundle.main)
    }
    
    class func getBusinessEmployeeListingFlowStoryboard() -> UIStoryboard
    {
        return UIStoryboard(name: "EmployeeListingFlow", bundle: Bundle.main)
    }
    
    class func getCreateProfileStoryboard() -> UIStoryboard
    {
        return UIStoryboard(name: "CreateProfile", bundle: Bundle.main)
    }
    
    class func getCountryCodeStoryboard() -> UIStoryboard
    {
        return UIStoryboard(name: "CountryCodeList", bundle: Bundle.main)
    }
    
    class func getEventListStoryboard() -> UIStoryboard
    {
        return UIStoryboard(name: "EventListing", bundle: Bundle.main)
    }
    
    class func getEmployeeDashboardStoryboard() -> UIStoryboard
    {
        return UIStoryboard(name: "Dashboard", bundle: Bundle.main)
    }
    
    class func getUserTabsStoryboard() -> UIStoryboard
    {
        return UIStoryboard(name: "UserTabs", bundle: Bundle.main)
    }
    
    class func getUserHomeStoryboard() -> UIStoryboard
    {
        return UIStoryboard(name: "UserHome", bundle: Bundle.main)
    }
    
    class func getUserActivityStoryboard() -> UIStoryboard
    {
        return UIStoryboard(name: "UserActivity", bundle: Bundle.main)
    }
    
    class func getUserFriendsStoryboard() -> UIStoryboard
    {
        return UIStoryboard(name: "UserFriends", bundle: Bundle.main)
    }
    
    class func getUserSettingsStoryboard() -> UIStoryboard
    {
        return UIStoryboard(name: "UserSettings", bundle: Bundle.main)
    }
    
    class func getChatViewStoryboard() -> UIStoryboard
    {
        return UIStoryboard(name: "ChatView", bundle: Bundle.main)
    }
    
    class func getGalleryViewStoryboard() -> UIStoryboard
    {
        return UIStoryboard(name: "GalleryView", bundle: Bundle.main)
    }
    
    class func getHourSelectionViewStoryboard() -> UIStoryboard
    {
        return UIStoryboard(name: "HoursView", bundle: Bundle.main)
    }
    
    class func getUserProfileStoryboard() -> UIStoryboard
    {
        return UIStoryboard(name: "UserProfile", bundle: Bundle.main)
    }
    
    class func getUserConnectStoryboard() -> UIStoryboard
    {
        return UIStoryboard(name: "UserConnect", bundle: Bundle.main)
    }
    
    class func getBusinessDetailsStoryboard() -> UIStoryboard
    {
        return UIStoryboard(name: "BusinessDetails", bundle: Bundle.main)
    }
    
    class func getCheckinPopupStoryboard() -> UIStoryboard
    {
        return UIStoryboard(name: "CheckinPopup", bundle: Bundle.main)
    }
    
    class func getMediaViewerStoryboard() -> UIStoryboard
    {
        return UIStoryboard(name: "MediaViewer", bundle: Bundle.main)
    }
}
