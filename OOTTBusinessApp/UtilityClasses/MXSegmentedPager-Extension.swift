//
//  MXSegmentedPager-Extension.swift
//  OOTTBusinessApp
//
//  Created by Bharat Kumar Pathak on 10/07/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import MXSegmentedPager

extension MXSegmentedPager {
    
    func customizeBackground(image: UIImage) {
        
        let backImageView = UIImageView.init(frame: UIScreen.main.bounds)
        backImageView.image = image
        self.addSubview(backImageView)
        self.sendSubview(toBack: backImageView)
    }
    
    func initializeSegments() {
        
        // Parallax Header
        self.parallaxHeader.view = UIView()
        self.parallaxHeader.mode = .top
        
        
        self.parallaxHeader.height = UIDevice.isIphoneX ? 84 :64
        
        
        self.parallaxHeader.minimumHeight = UIDevice.isIphoneX ? 84 :64

        self.bounces = false
        
        // Segmented Control customization
        self.segmentedControl.selectionIndicatorLocation = .down
        self.segmentedControl.backgroundColor = .clear
        
        let segmentNormalColor = UIColor.init(RED: 78.0, GREEN: 114.0, BLUE: 168.0, ALPHA: 1.0)
        let segmentSelectColor = UIColor.init(RED: 237.0, GREEN: 43.0, BLUE: 93.0, ALPHA: 1.0)
        //        let segmentTitleFont = UIFont(name: FONT_PROXIMA_LIGHT, size: 17) ?? UIFont.systemFont(ofSize: 17)
        
        //NSFontAttributeName: segmentTitleFont,
        self.segmentedControl.titleTextAttributes = [NSForegroundColorAttributeName : segmentNormalColor]
        self.segmentedControl.selectedTitleTextAttributes = [NSForegroundColorAttributeName : segmentSelectColor]
        self.segmentedControl.selectionStyle = .fullWidthStripe
        self.segmentedControl.selectionIndicatorColor = segmentSelectColor
        self.segmentedControl.selectionIndicatorHeight = CGFloat(1)
        self.customizeBackground(image: #imageLiteral(resourceName: "appBG"))
    }
}
