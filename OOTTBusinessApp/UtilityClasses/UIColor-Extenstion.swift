//
//  UIColor-Extenstion.swift
//  OOTTBusinessApp
//
//  Created by Sumit Sharma on 14/06/2017.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import Foundation
import UIKit
extension UIColor {
    convenience init(RED red: CGFloat, GREEN green: CGFloat, BLUE blue: CGFloat, ALPHA alpha: CGFloat) {
        self.init(red: red/255, green: green/255, blue: blue/255, alpha: alpha)
    }
    
    struct MyApp {
        static let genderChartMalecolor = UIColor (RED: 0, GREEN: 158, BLUE: 255, ALPHA: 1)
         static let genderChartFemalecolor = UIColor (RED: 6, GREEN: 124, BLUE: 196, ALPHA: 1)
    }
}
