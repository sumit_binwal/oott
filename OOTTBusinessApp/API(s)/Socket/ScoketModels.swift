//
//  ScoketModels.swift
//  OOTTBusinessApp
//
//  Created by Santosh on 08/09/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import Foundation

//MARK:- SocketParametersKeyword
// Socket functions parameters
enum SocketParametersKeyword : String {
    case ReceieverID = "receiver"
    case Message = "message"
    case MessageTo = "messageTo"
    
    case isMedia = "isMedia"
    
    // Chat History
    case RoomID = "room_id"
    case UserID = "user_id"
    
    // Like-Unlike message
    case MessageID = "message_id"
    case action = "action"
    
    case isLeftGroup = "isLeftUser"
}


//MARK:- SocketModelSendMessage
// Send message model class
class SocketModelSendMessage
{
    var message = ""
    var normalMessage = ""
    var receiverID = ""
    var isMedia = false
    
    init(_ receiverID : String, message : String, normalMessage: String, isMedia: Bool)
    {
        self.receiverID = receiverID
        self.message = message
        self.normalMessage = normalMessage
        self.isMedia = isMedia
    }
    
    func getDictionary() -> [String:String]
    {
        return [
            SocketParametersKeyword.ReceieverID.rawValue : receiverID,
            SocketParametersKeyword.Message.rawValue : message,
            SocketParametersKeyword.MessageTo.rawValue : normalMessage,
            SocketParametersKeyword.isMedia.rawValue : isMedia ? "1" : "0"
        ]
    }
}

//MARK:- SocketModelSendGroupMessage
// Send group message model class
class SocketModelSendGroupMessage
{
    enum MessageType {
        case normal
        case isLeftGroup
    }
    var messageType = MessageType.normal
    var message = ""
    var normalMessage = ""
    var roomID = ""
    var isMedia = false
    var isLeftGroup = false
    
    init() {
        
    }
    
    init(_ roomID : String, message : String, isMedia: Bool, normalMessage: String, messageType: MessageType)
    {
        self.roomID = roomID
        self.message = message
        self.normalMessage = normalMessage
        self.isMedia = isMedia
        self.messageType = messageType
    }
    
    func getDictionary() -> [String:String]
    {
        return [
            SocketParametersKeyword.RoomID.rawValue : roomID,
            SocketParametersKeyword.Message.rawValue : message,
            SocketParametersKeyword.MessageTo.rawValue : normalMessage,
            SocketParametersKeyword.isMedia.rawValue : isMedia ? "1" : "0"
        ]
    }
    
    func getDictionaryLeftUser() -> [String:Any]
    {
        return [
            SocketParametersKeyword.RoomID.rawValue : roomID,
            SocketParametersKeyword.isLeftGroup.rawValue : isLeftGroup
        ]
    }
}

extension SocketModelSendGroupMessage {
    convenience init(_ isLeftGroup: Bool, roomID: String, messageType: MessageType) {
        self.init()
        self.isLeftGroup = isLeftGroup
        self.roomID = roomID
        self.messageType = messageType
    }
}


//MARK:- SocketModelGetHistory
// Get chat history with the user or group
class SocketModelGetHistory
{
    var userID : String?
    var roomID : String?
    
    init(_ userID : String?, roomID : String?)
    {
        self.userID = userID
        self.roomID = roomID
    }
    
    func getDictionary() -> [String:String]
    {
        if roomID == nil
        {
            return [
                SocketParametersKeyword.UserID.rawValue : self.userID!
            ]
        }
        
        return [
            SocketParametersKeyword.RoomID.rawValue : self.roomID!,
            SocketParametersKeyword.UserID.rawValue : self.userID ?? ""
        ]
    }
}

//MARK:- SocketModelGetLikesHistory
// Get group chat likes history with the room and message
class SocketModelGetLikesHistory
{
    var messageID : String?
    var roomID : String?
    
    init(_ messageID : String?, roomID : String?)
    {
        self.messageID = messageID
        self.roomID = roomID
    }
    
    func getDictionary() -> [String:String]
    {
        return [
            SocketParametersKeyword.RoomID.rawValue : self.roomID!,
            SocketParametersKeyword.MessageID.rawValue : self.messageID ?? ""
        ]
    }
}

//MARK:- SocketModelLikeMessage
// Like message
class SocketModelLikeMessage
{
    var messageID : String?
    var roomID : String?
    var like = false
    
    
    init(_ messageID : String?, roomID : String?, like : Bool)
    {
        self.messageID = messageID
        self.roomID = roomID
        self.like = like
    }
    
    func getDictionary() -> [String:String]
    {
        return [
            SocketParametersKeyword.RoomID.rawValue : self.roomID!,
            SocketParametersKeyword.MessageID.rawValue : self.messageID!,
            SocketParametersKeyword.action.rawValue : self.like ? "1" : "0"
        ]
    }
}
