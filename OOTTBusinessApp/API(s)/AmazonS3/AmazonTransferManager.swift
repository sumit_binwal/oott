//  AmazonTransferManager.swift
//  OOTTBusinessApp
//
//  Created by Santosh on 27/12/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import Foundation
import AWSS3

class AmazonTransferManager: NSObject {
    
    typealias AmazonProgressBlock = (AWSS3TransferUtilityTask, Progress) -> ()
    typealias AmazonUploadCompletionBlock = (AWSS3TransferUtilityUploadTask?, Error?) -> ()
    typealias AmazonDownloadCompletionBlock = (AWSS3TransferUtilityDownloadTask?, Error?) -> ()
    
    var transferUtility: AWSS3TransferUtility!
    
    
    //MARK:- Instance Creation (Private)
    private static var instance : AmazonTransferManager =
    {
        let newInstance = AmazonTransferManager.init()
        
        return newInstance
    }()
    
    //MARK:- init (Private)
    private override init() {
        super.init()
        transferUtility = AWSS3TransferUtility .default()
    }
    
    //MARK:- Shared Instance(Global)
    class func defaultTransferManager() -> AmazonTransferManager {
        return instance
    }
    
    //MARK:- Bucket name (Get and Set)
//    var bucketName: String?
    
    //MARK:- Upload Data
    func uploadData(_ data: Data, bucketName: String, keyName: String, contentType: String, progressBlock: AmazonProgressBlock?, completionHandler: AmazonUploadCompletionBlock?) {
        
        // Assert if the bucket name is nil.
        
        var expression: AWSS3TransferUtilityUploadExpression?
        
        // If progress block is not nil, then create AWSS3TransferUtilityUploadExpression and add progress block to the request.
        if progressBlock != nil {
            
            // Creating the expression
            expression = AWSS3TransferUtilityUploadExpression()
            
            // Adding the progress block
            expression?.progressBlock = {(task, progress) in DispatchQueue.main.async(execute: {
                
                
                // Calling the progress block
                progressBlock!(task, progress)
            })
            }
        }
        
        // Upload data called with the required details
        
        transferUtility.uploadData(data, bucket: bucketName, key: keyName, contentType: contentType, expression: expression) { (task, error) in
            DispatchQueue.main.async(execute: {
                // if completion handler is nil, then returning from here and not moving ahead
                if completionHandler == nil
                {
                    return
                }
                // Calling the handler and updating the user
                completionHandler!(task,error)
            })
        }
    }
    
    //MARK:- Download Data
    func downloadData(_ dataURL: URL, bucketName: String, keyName: String, progressBlock: AmazonProgressBlock?, completionHandler: AmazonDownloadCompletionBlock?) {
        
        var expression: AWSS3TransferUtilityDownloadExpression?
        
        // If progress block is not nil, then create AWSS3TransferUtilityDownloadExpression and add progress block to the request.
        if progressBlock != nil {
            
            // Creating the expression
            expression = AWSS3TransferUtilityDownloadExpression()
            
            // Adding the progress block
            expression?.progressBlock = {(task, progress) in DispatchQueue.main.async(execute: {
                
                // Calling the progress block
                progressBlock!(task, progress)
            })
            }
        }
        
        // Download data called with the required details
        
        transferUtility.download(to: dataURL, bucket: bucketName, key: keyName, expression: expression) { (task, url, data, error) in
            
            DispatchQueue.main.async(execute: {
                // if completion handler is nil, then returning from here and not moving ahead
                if completionHandler == nil
                {
                    return
                }
                // Calling the handler and updating the user
                completionHandler!(task,error)
            })
        }
    }
}
