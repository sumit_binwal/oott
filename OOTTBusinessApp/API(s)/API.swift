//
//  API.swift
//  OOTTBusinessApp
//
//  Created by Santosh on 15/06/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import Foundation

//MARK:- DEVELOPMENT BASE URL
//let baseURLString = "http://192.168.0.131:7471/"

//MARK:- STAGING BASE URL
//let baseURLString = "http://192.168.0.131:7472/"

//MARK:- CLIENT BASE URLo
//let baseURLString = "http://202.157.76.19:7472/"

//MARK:- CLIENT DOMAIN URL
//let baseURLString = "http://api.oottnightlife.com:7484/"

//MARK:- CLIENT DOMAIN URL (Dev)
let baseURLString = "http://api.oottnightlife.com:7494/"

//MARK:- Protocol Endpoint path
protocol ApiEndpoint {
    var path :URL {get}
}

//MARK:- Enum Endpoint Final Path
enum EndPoints : ApiEndpoint {
    case login
    case register
    case socialLogin
    case socialRegister
    case logout(String)
    case sendOTP
    case checkUser
    case changePassword(String)
    case forgotPassword
    case addEmployee(String)
    case listEmployee(String)
    case getUserRole
    case leaveUserFromGroup(String,String,String)
    case deleteEmployee(String,String)
    case addBusinessImage(String)
    case deleteBusinessImage(String,String)
    case createProfile(String)
    case addMenuImage(String)
    case deleteMenuImage(String,String)
    case editEmployee(String,String)
    case getScene(String)
    case getCurrentStatus(String)
    case updateStatus(String)
    case viewBusiness(String,String,String,String)
    case eventListing(String,String)
    case deleteEvent(String,String)
    case createEvent(String)
    case editEvent(String,String)
    case myBusinessGroupDetail(String,String,String)
    case getUpcomingEvents(String,String)
    case getGroupsHome(String)
    case makeMoveToGroup(String,String)
    case cancelMakeMove(String,String)
    case favouriteGroup(String,String)
    case unFavouriteGroup(String,String)
    case getGroupFriendsList(String,String)
    case getGroupLikesList(String,String)
    case getFavouriteGroupList(String,String)
    
    case getFriendsList(String,String)
    case getSuggestedFriendsList(String,String)
    case likeUserListCheckinComment(String,String,String)
    case getUserProfileApi(String,String)
    case followUser(String,String)
    case unfollowUser(String,String)
    
    case myActivityList(String)
    case followingsActivityList(String)
    case deleteAccount(String)
    case getUserCheckinComments(String,String)
    case addCommentOnCheckIn(String,String)
    case likeCheckin(String,String)
    case UnlikeCheckin(String,String)
    case editProfile(String)
    
    case createChatGroup(String)
    case viewMembersInGroup(String,String)
    case myGroups(String)
    case makeGroupMakeMove(String)
    case inviteToGroup(String)
    
    case getBlockedUserList(String)
    
    case acceptMakeMoveInvitation(String)
    case declineMakeMoveInvitation(String,String)
    
    case getFollowingList(String,String)
    case getFollowersList(String,String)
    case updateUserGroup(String,String)
    
    case viewGroupDetails(String,String)
    
    case likeCheckinComment(String,String,String)
    case UnlikeCheckinComment(String,String,String)
    
    case acceptFollowRequest(String,String)
    case declineFollowRequest(String,String)
    
    case getInsights(String,String)
    case getStatusLikes(String,String)
    
    case getFriendsSearchList(String,String)
    case getSuggestedFriendsSearchList(String)
    
    var path: URL
    {
        switch self {
        case .login:
            return URL(string: String(baseURLString+"login"))!
        case .register:
            return URL(string: String(baseURLString+"register"))!
        case .socialLogin:
            return URL(string: String(baseURLString+"socialLogin"))!
        case .socialRegister:
            return URL(string: String(baseURLString+"socialRegister"))!
        case .logout(let sid):
            return URL(string: String(baseURLString+"logout/"+sid))!
        case .sendOTP:
            return URL(string: String(baseURLString+"sendOTP"))!
        case .checkUser:
            return URL(string: String(baseURLString+"checkUser"))!
        case .changePassword(let sid):
            return URL(string: String(baseURLString+"changePassword/"+sid))!
        case .forgotPassword:
            return URL(string: String(baseURLString+"forgotPassword"))!
        case .addEmployee(let sid):
            return URL(string: String(baseURLString+"addEmployee/"+sid))!
        case .getUserRole:
            return URL(string: String(baseURLString+"userRoles"))!
        case .listEmployee(let sid):
            return URL(string: String(baseURLString+"listEmployee/"+sid))!
        case .deleteEmployee(let sid, let userID):
            return URL(string: String(baseURLString+"deleteEmployee/"+sid+"/"+userID))!
        case .addBusinessImage(let sid):
            return URL(string: String(baseURLString+"addBusinessImage/"+sid))!
        case .deleteBusinessImage(let sid, let picID):
            return URL(string: String(baseURLString+"removeBusinessImage/"+sid+"/"+picID))!
        case .createProfile(let sid):
            return URL(string: String(baseURLString+"createProfile/"+sid))!
        case .addMenuImage(let sid):
            return URL(string: String(baseURLString+"addMenuImage/"+sid))!
        case .deleteMenuImage(let sid, let picID):
            return URL(string: String(baseURLString+"removeMenuImage/"+sid+"/"+picID))!
        case .editEmployee(let sid, let employeeID):
            return URL(string: String(baseURLString+"editEmployee/"+sid+"/"+employeeID))!
        case .getScene(let sceneTag):
            return URL(string: String(baseURLString+"getScene/"+sceneTag))!

        case .getCurrentStatus(let sid):
            return URL(string: String(baseURLString+"currentStatus/"+sid))!
        case .updateStatus(let sid):
            return URL(string: String(baseURLString+"updateStatus/"+sid))!
        case .viewBusiness(let sid, let businessID, let latitude, let longitude):
            return URL(string: String(baseURLString+"viewBusiness/"+sid+"/"+businessID+"?latitude="+latitude+"&longitude="+longitude))!
        case .eventListing(let sid, let businessID):
            return URL(string: String(baseURLString+"listEvent/"+sid+"/"+businessID))!
        case .myBusinessGroupDetail(let sid, let latitude, let longitude):
            return URL(string: String(baseURLString+"myBusinessGroup/"+sid+"?latitude="+latitude+"&longitude="+longitude))!
        case .deleteEvent(let sid, let userID):
            return URL(string: String(baseURLString+"deleteEvent/"+sid+"/"+userID))!
        case .createEvent(let sid):
            return URL(string: String(baseURLString+"addEvent/"+sid))!
        case .editEvent(let sid, let userID):
            return URL(string: String(baseURLString+"editEvent/"+sid+"/"+userID))!
        case .getUpcomingEvents(let sid, let businessID):
            return URL(string: String(baseURLString+"upcommingEvent/"+sid+"/"+businessID))!
            
            // MARK: User Apis Path
        case .getGroupsHome(let sid):
            return URL(string: String(baseURLString+"getBusiness/"+sid))!
            
        case .cancelMakeMove(let sid, let businessID):
            return URL(string: String(baseURLString+"cancelmove/"+sid+"/"+businessID))!

        case .makeMoveToGroup(let sid, let businessID):
            return URL(string: String(baseURLString+"move/"+sid+"/"+businessID))!
        case .favouriteGroup(let sid, let businessID):
            return URL(string: String(baseURLString+"like/"+sid+"/"+businessID))!
        case .unFavouriteGroup(let sid, let businessID):
            return URL(string: String(baseURLString+"unlike/"+sid+"/"+businessID))!
        case .getGroupFriendsList(let sid, let businessID):
            return URL(string: String(baseURLString+"friendsOnBusiness/"+sid+"/"+businessID))!
        case .getGroupLikesList(let sid, let businessID):
            return URL(string: String(baseURLString+"businessLikes/"+sid+"/"+businessID))!
        case .getFavouriteGroupList(let sid, let userID):
            return URL(string: String(baseURLString+"favouriteBusinessList/"+sid+"/"+userID))!
            
            
        case .getFriendsList(let sid, let page):
            return URL(string: String(baseURLString+"followingsList/"+sid+"?page="+page))!
        case .getSuggestedFriendsList(let sid, let page):
            return URL(string: String(baseURLString+"suggestiveFriends/"+sid+"?page="+page))!
            
            
        case .getUserProfileApi(let sid, let userID):
            return URL(string: String(baseURLString+"viewUser/"+sid+"/"+userID))!
        case .followUser(let sid, let userID):
            return URL(string: String(baseURLString+"follow/"+sid+"/"+userID))!
        case .unfollowUser(let sid, let userID):
            return URL(string: String(baseURLString+"unfollow/"+sid+"/"+userID))!
            
        case .myActivityList(let sid):
            return URL(string: String(baseURLString+"myActivityList/"+sid))!
        case .followingsActivityList(let sid):
            return URL(string: String(baseURLString+"followingsActivityList/"+sid))!
            
        case .deleteAccount(let sid):
            return URL(string: String(baseURLString+"deleteAccout/"+sid))!
            
        case .getUserCheckinComments(let sid, let checkInID):
            return URL(string: String(baseURLString+"checkinCommentList/"+sid+"/"+checkInID))!
        case .addCommentOnCheckIn(let sid, let checkInID):
            return URL(string: String(baseURLString+"commentOnCheckin/"+sid+"/"+checkInID))!
            
        case .likeCheckin(let sid, let checkInID):
            return URL(string: String(baseURLString+"checkinLike/"+sid+"/"+checkInID))!
        case .UnlikeCheckin(let sid, let checkInID):
            return URL(string: String(baseURLString+"checkinUnlike/"+sid+"/"+checkInID))!
            
        case .editProfile(let sid):
            return URL(string: String(baseURLString+"editProfile/"+sid))!
            
        case .createChatGroup(let sid):
            return URL(string: String(baseURLString+"createGroup/"+sid))!
            
            
        case .viewMembersInGroup(let sid, let groupID):
            return URL(string: String(baseURLString+"peopleOnGroup/"+sid+"/"+groupID))!
            
        case .myGroups(let sid):
            return URL(string: String(baseURLString+"myGroups/"+sid))!
            
        case .makeGroupMakeMove(let sid):
            return URL(string: String(baseURLString+"groupmove/"+sid))!
            
        case .inviteToGroup(let sid):
            return URL(string: String(baseURLString+"inviteToGroup/"+sid))!
            
        case .getBlockedUserList(let sid):
            return URL(string: String(baseURLString+"blockedUserList/"+sid))!
            
        case .acceptMakeMoveInvitation(let sid):
            return URL(string: String(baseURLString+"acceptInvitation/"+sid))!
            
        case .declineMakeMoveInvitation(let sid, let userID):
            return URL(string: String(baseURLString+"closeMoveRequest/"+sid+"/"+userID))!
            
        case .getFollowersList(let sid, let userID):
            return URL(string: String(baseURLString+"followersList/"+sid+"/"+userID))!
            
        case .getFollowingList(let sid, let userID):
            return URL(string: String(baseURLString+"followingsList/"+sid+"/"+userID))!
            
        case .getFriendsSearchList(let sid, let userID):
            return URL(string: String(baseURLString+"followingsList/"+sid+"/"+userID+"?page=1"))!
            
        case .getSuggestedFriendsSearchList(let sid):
            return URL(string: String(baseURLString+"suggestiveFriends/"+sid+"?page=1"))!
            
        case .updateUserGroup(let sid, let groupID):
            return URL(string: String(baseURLString+"updateGroup/"+sid+"/"+groupID))!
            
        case .viewGroupDetails(let sid, let groupID):
            return URL(string: String(baseURLString+"viewGroup/"+sid+"/"+groupID))!
        
        case .leaveUserFromGroup(let sid, let groupID, let userID):
            return URL(string: String(baseURLString+"leaveUserfromGroup/"+sid+"/"+groupID+"/"+userID))!
            
        case .likeCheckinComment(let sid, let checkInID, let commentID):
            return URL(string: String(baseURLString+"checkinCommentLike/"+sid+"/"+checkInID+"/"+commentID))!
        
        case .likeUserListCheckinComment(let sid, let checkInID, let commentID):
            return URL(string: String(baseURLString+"checkinCommentLikes/"+sid+"/"+checkInID+"/"+commentID))!
            
        case .UnlikeCheckinComment(let sid, let checkInID, let commentID):
            return URL(string: String(baseURLString+"checkinCommentUnlike/"+sid+"/"+checkInID+"/"+commentID))!
            
            
        case .acceptFollowRequest(let sid, let activityID):
            return URL(string: String(baseURLString+"acceptFollowReq/"+sid+"/"+activityID))!
            
        case .declineFollowRequest(let sid, let activityID):
            return URL(string: String(baseURLString+"declineFollowReq/"+sid+"/"+activityID))!
            
        case .getInsights(let sid, let businessID):
            return URL(string: String(baseURLString+"insights/"+sid+"/"+businessID))!
            
        case .getStatusLikes(let sid, let statusID):
            return URL(string: String(baseURLString+"statusLikes/"+sid+"/"+statusID))!
        }
    }
}
//MARK:- Enum Endpoint Final Path
enum LinksEnum
{
    case privacyPolicy
    case termsAndCond
    case other(String)
    
    var path: String {
        switch self {
        case .privacyPolicy:
            return "http://api.oottnightlife.com:7472/privacyPolicies"
        case .termsAndCond:
            return "http://api.oottnightlife.com:7472/termsOfService"
        case .other(let link):
            return link
        }
    }
}
