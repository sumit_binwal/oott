//
//  AppWebHandler.swift
//  network
//
//  Created by Vaibhav Khatri on 23/05/17.
//  Copyright © 2017 vaibhav. All rights reserved.
//

import UIKit
import Foundation

class ImageDataDict
{
    var imageData : Data
    var paramName : String
    
    init(data : Data, name : String)
    {
        imageData = data
        paramName = name
    }
}

enum HttpMethodType {
    case get // GET request
    case post // POST request
}

class AppWebHandler : NSObject, URLSessionTaskDelegate {
    
    // Private URLSession object to handle all the network related calls.
    private var httpSession : URLSession?
    
    typealias CompletionHandler = (_ data:Data?, _ dictionary:Dictionary<String, Any>?, _ statusCode:Int, _ error:Error?) -> ()
    
    //    typealias DataTaskSuccessfullHandler = (_ data:Data?, _ dictionary:Dictionary<String, Any>?, _ statusCode:Int) -> ()
    //
    //    typealias DataTaskFailureHandler = (_ error:Error?) -> ()
    
    //MARK: AppWebHandler Singleton Instance
    
    // Class object, which is lazily initiated when called first time. Only single instance resides through application life cycle.
    private static var instance : AppWebHandler =
    {
        let newInstance = AppWebHandler.init()
        
        return newInstance
    }()
    
    private override init() {
        
        super.init()
        
        let sessionConfiguration = URLSessionConfiguration.default
        
        httpSession = URLSession.init(configuration: sessionConfiguration)
    }
    
    class func sharedInstance() -> AppWebHandler {
        return instance
    }
    
    
    //MARK: API - Fetch data from specified url
    func fetchData(fromURL url:URL?, httpMethod:HttpMethodType, parameters:Dictionary<String,Any>?, shouldDeserialize deSerialize:Bool? = true, completionHandler:CompletionHandler?) -> Void {
        
        guard let url1 = url else {
            assertionFailure("Url provided is invalid")
            return
        }
        
        let dataTask : URLSessionDataTask?
        var urlRequest : URLRequest?
        
        switch httpMethod {
        case .get:
            urlRequest = self.createGetRequest(withUrl: url1)
            break

        default:
            
            urlRequest = self.createPostRequest(withUrl: url1, andParameters: parameters)
            break
        }
        
        urlRequest!.addValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest!.addValue("1.0", forHTTPHeaderField: "version")
        urlRequest!.addValue("ios", forHTTPHeaderField: "os")
        
        dataTask = httpSession!.dataTask(with: urlRequest!, completionHandler: { (data, urlResponse, dataError) in
            
            DispatchQueue.main.async {
                
                
                // Checking for error, if there is any
                if dataError != nil
                {
                    // If there is error, then checking the condition if its error handling block is nil.
                    if completionHandler == nil
                    {
                        return
                    }
                    else
                    {
                        // User is posted with the error.
                        completionHandler!(nil,nil,0,dataError)
                        return
                    }
                }
                
                let statusCode = (urlResponse! as! HTTPURLResponse).statusCode
                
                
                // If no completion handler, then return.
                if completionHandler == nil
                {
                    return
                }
                
                
                if deSerialize!
                {
//                    let string = String(data: data!, encoding: .utf8)
//                    print(string!)
                    do {
                        let deSerializedDictionary = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! [String:Any]
//                        print(deSerializedDictionary)
                        
                        completionHandler!(nil,deSerializedDictionary,statusCode,nil)
                        return
                        
                    } catch {
                        if completionHandler == nil
                        {
                            return
                        }
                        else
                        {
                            // User is posted with the error.
                            completionHandler!(nil,nil,statusCode,error)
                            return
                        }
                    }
                }
                else
                {
                    // Sending data and dictionary as nil object because 'deSerialize is FALSE'.
                    completionHandler!(data!,nil,statusCode,nil)
                    return
                }
            }
        })
        dataTask?.resume()
    }
    
    
    //MARK: API - Image + Parameters data
    func uploadImages(fromURL url:URL?, imagesArray:[ImageDataDict]?, otherParameters:Dictionary<String,Any>?, completionHandler:CompletionHandler?) -> Void
    {
        
        guard let url1 = url else {
            assertionFailure("Url provided is invalid")
            return
        }
        
        let dataTask : URLSessionDataTask?
        var urlRequest : URLRequest?
        
        urlRequest = createMultipleImageMultipartRequest(withUrl: url1, imageDataArray: imagesArray, andParameters: otherParameters)
        
//        urlRequest!.addValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest!.addValue("1.0", forHTTPHeaderField: "version")
        urlRequest!.addValue("ios", forHTTPHeaderField: "os")
        
        dataTask = httpSession!.dataTask(with: urlRequest!, completionHandler: { (data, urlResponse, dataError) in
            
            DispatchQueue.main.async {
                
                // Checking for error, if there is any
                if dataError != nil
                {
                    // If there is error, then checking the condition if its error handling block is nil.
                    if completionHandler == nil
                    {
                        return
                    }
                    else
                    {
                        // User is posted with the error.
                        completionHandler!(nil,nil,0,dataError)
                        return
                    }
                }
                
                let statusCode = (urlResponse! as! HTTPURLResponse).statusCode
                
                
                // If no completion handler, then return.
                if completionHandler == nil
                {
                    return
                }
                
                do {
                    let deSerializedDictionary = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! [String:Any]
//                    print(deSerializedDictionary)
                    
                    completionHandler!(nil,deSerializedDictionary,statusCode,nil)
                    
                } catch {
                    if completionHandler == nil
                    {
                        return
                    }
                    else
                    {
                        // User is posted with the error.
                        completionHandler!(nil,nil,statusCode,error)
                    }
                }
            }
        })
        dataTask?.resume()
    }
    
    //MARK: API - Image + Parameters data
//    func uploadImageProgressively(fromURL url:URL?, image:ImageDataDict, completionHandler:CompletionHandler?) -> Void
//    {
//        
//        guard let url1 = url else {
//            assertionFailure("Url provided is invalid")
//            return
//        }
//        
//        let dataTask : URLSessionDataTask?
//        var urlRequest : URLRequest
//        
//        urlRequest = URLRequest.init(url: url1, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 120)
//        
//        // Setting http method as POST...
//        urlRequest.httpMethod="POST"
//        
//        //        urlRequest!.addValue("application/json", forHTTPHeaderField: "Content-Type")
//        urlRequest.addValue("1.0", forHTTPHeaderField: "version")
//        urlRequest.addValue("ios", forHTTPHeaderField: "os")
//        
//        dataTask = httpUploadSession?.dataTask(with: urlRequest)
//        dataTask?.resume()
//        
//        dataTask = httpSession!.dataTask(with: urlRequest!, completionHandler: { (data, urlResponse, dataError) in
//            
//            DispatchQueue.main.async {
//                
//                // Checking for error, if there is any
//                if dataError != nil
//                {
//                    // If there is error, then checking the condition if its error handling block is nil.
//                    if completionHandler == nil
//                    {
//                        return
//                    }
//                    else
//                    {
//                        // User is posted with the error.
//                        completionHandler!(nil,nil,0,dataError)
//                    }
//                }
//                
//                let statusCode = (urlResponse! as! HTTPURLResponse).statusCode
//                
//                
//                // If no completion handler, then return.
//                if completionHandler == nil
//                {
//                    return
//                }
//                
//                do {
//                    let deSerializedDictionary = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! [String:Any]
//                    print(deSerializedDictionary)
//                    
//                    completionHandler!(nil,deSerializedDictionary,statusCode,nil)
//                    
//                } catch {
//                    if completionHandler == nil
//                    {
//                        return
//                    }
//                    else
//                    {
//                        // User is posted with the error.
//                        completionHandler!(nil,nil,statusCode,error)
//                    }
//                }
//            }
//        })
//        dataTask?.resume()
//    }
    
    
    //MARK: - Private func - Create Get Request
    private func createGetRequest(withUrl url:URL) -> URLRequest
    {
        // Creating the base request with given url...
        var baseUrlRequest = URLRequest.init(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 40)
        
        // Setting http method as GET...
        baseUrlRequest.httpMethod="GET"
        
        return baseUrlRequest
    }
    
    //MARK: - Private func - Create Post Request
    private func createPostRequest(withUrl url:URL, andParameters parameters:Dictionary<String,Any>?) -> URLRequest
    {
        // Creating the base request with given url...
        var baseUrlRequest = URLRequest.init(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 40)
        
        // Setting http method as POST...
        baseUrlRequest.httpMethod="POST"
        
        // Gaurding the parameters Dictionary(also unwraps) and handling the invalid(else condition). If parameters Dictionary is empty or nil, then returning the base request...
        guard let params = parameters else {
            print("Parameter dictionary is invalid")
            return baseUrlRequest
        }
        
        // Checking whether the Dictionary is returning the valid Json string...
        let isValidJson = JSONSerialization.isValidJSONObject(params)
        
        // Guarding the Bool and checking for True else AssertionFailure with message. Can not proceed further
        guard isValidJson else {
            assertionFailure("Parameter dictionary is invalid - \n\(params)")
            return baseUrlRequest
        }
        
        // Optional Data variable to hold the request post data...
        var postData : Data?
        
        // Do-Catch syntax to handle the Dictionary to Json string conversion. Here we are force unwrapping try because we are sure the object will return proper Json string...
        do {
            postData = try! JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
        }
        
        // Setting base request http body
        baseUrlRequest.httpBody = postData!
        
        return baseUrlRequest
    }
    
    //MARK: - Private func - Create Multipart Post Request
    private func createMultipleImageMultipartRequest(withUrl url:URL, imageDataArray:[ImageDataDict]?, andParameters parameters:Dictionary<String,Any>?) -> URLRequest
    {
        let boundary = String(format: "Boundary+%08X%08X", arc4random(), arc4random())
        let contentType = String(format: "multipart/form-data; boundary=%@", boundary)
        
        // Creating the base request with given url...
        var baseUrlRequest = URLRequest.init(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 40)
        
        // Setting http method as POST...
        baseUrlRequest.httpMethod="POST"
        
        baseUrlRequest.addValue(contentType, forHTTPHeaderField: "Content-Type")
        
        var body = Data()
        
//        body.append(String(format: "\r\n--%@\r\n", boundary).data(using: .utf8)!)
        
        if imageDataArray != nil
        {
            for item in imageDataArray!
            {
                body.append(String(format:"\r\n--%@\r\n",boundary).data(using: .utf8)!)
                
                let string1 = String(format: "Content-Disposition: form-data; name=\"%@\"; filename=\"%@.jpg\"\r\n", item.paramName, item.paramName)
                
                body.append(string1.data(using: .utf8)!)
                
                body.append("Content-Type: image/jpeg\r\n\r\n".data(using: .utf8)!)
                
                body.append(item.imageData)
                
//                body.append(String(format:"\r\n--%@\r\n",boundary).data(using: .utf8)!)
            }
        }
        
        if parameters != nil
        {
            for (key,value) in parameters!
            {
                body.append(String(format:"\r\n--%@\r\n",boundary).data(using: .utf8)!)
                
                if let newValue = value as? [String:Any]
                {
                    let string1 = String(format: "Content-Disposition: form-data; name=\"%@\"\r\n\r\n%@", key, self.createJSONString(usingDictionary: newValue) as CVarArg)
                    
                    body.append(string1.data(using: .utf8)!)
                }
//                else if let newValue = value as? [Any]
//                {
//                    let string1 = String(format: "Content-Disposition: form-data; name=\"%@\"\r\n\r\n%@", key, self.createJSONString(usingArray: newValue) as CVarArg)
//                    
//                    body.append(string1.data(using: .utf8)!)
//                }
                else
                {
                    let string1 = String(format: "Content-Disposition: form-data; name=\"%@\"\r\n\r\n%@", key, value as! CVarArg)
                    
                    body.append(string1.data(using: .utf8)!)
                }
                
//                body.append(String(format:"\r\n--%@\r\n",boundary).data(using: .utf8)!)
            }
        }
        
        body.append(String(format:"\r\n--%@--\r\n",boundary).data(using: .utf8)!)
        
        // Setting base request http body
        baseUrlRequest.httpBody = body
        
        return baseUrlRequest
    }
    
    //MARK:-
    func createJSONString(usingDictionary dictionary : Dictionary<String,Any>) -> String {
        // Checking whether the Dictionary is returning the valid Json string...
        let isValidJson = JSONSerialization.isValidJSONObject(dictionary)
        
        // Guarding the Bool and checking for True else AssertionFailure with message. Can not proceed further
        guard isValidJson else {
            assertionFailure("Parameter dictionary is invalid - \n\(dictionary)")
            return ""
        }
        
        // Optional Data variable to hold the request post data...
        var postData : Data?
        var postString : String?
        
        
        // Do-Catch syntax to handle the Dictionary to Json string conversion. Here we are force unwrapping try because we are sure the object will return proper Json string...
        do {
            postData = try! JSONSerialization.data(withJSONObject: dictionary, options: .prettyPrinted)
            
            postString = String.init(data: postData!, encoding: .utf8)
        }
        
        return postString!
    }
    
    //MARK:-
    func createJSONString(usingArray array : [Any]) -> String {
        // Checking whether the Dictionary is returning the valid Json string...
        let isValidJson = JSONSerialization.isValidJSONObject(array)
        
        // Guarding the Bool and checking for True else AssertionFailure with message. Can not proceed further
        guard isValidJson else {
            assertionFailure("Parameter array is invalid - \n\(array)")
            return ""
        }
        
        // Optional Data variable to hold the request post data...
        var postData : Data?
        var postString : String?
        
        
        // Do-Catch syntax to handle the Dictionary to Json string conversion. Here we are force unwrapping try because we are sure the object will return proper Json string...
        do {
            postData = try! JSONSerialization.data(withJSONObject: array, options: .prettyPrinted)
            
            postString = String.init(data: postData!, encoding: .utf8)
        }
        
        return postString!
    }
    
    //MARK:-
    func cancelTask(forEndpoint endpoint:String) -> Void
    {
        httpSession?.getAllTasks(completionHandler: { (taskArray) in
            for task in taskArray
            {
                if let originalRequest = task.originalRequest
                {
                    if (originalRequest.url?.absoluteString.contains(endpoint))!
                    {
                        task.cancel()
                        return
                    }
                }
            }
        })
    }
}
